#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current busload.
 *
 * @deprecated
 *   Replaced by BusLoad.
 *
 * Returns the current busload of the specified channel.
 *
 * @param channel
 *   CAN channel.
 *   - Vector API driver: Values 1 ... 32
 *   - Softing API driver: Values 1, 2
 *
 * @return
 *   Current busload of the specified channel in percent.
 */
long VECTOR_CAPL_EXPORT canGetBusLoad(long channel);

}
