#include "canSetConfiguration.h"

#include <iostream>

namespace capl
{

long canSetConfiguration(int channel, CANSettings settings)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
