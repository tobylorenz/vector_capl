#pragma once

#include "CAN.h"
#include <map>
#include <ISO/11898.h>

namespace caplIntern {

/** list of CAN controllers */
extern std::map<unsigned short, ISO11898::Controller *> canControllers;

}
