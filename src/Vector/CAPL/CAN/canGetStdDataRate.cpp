#include "canGetStdDataRate.h"

#include <iostream>

namespace capl
{

long canGetStdDataRate(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
