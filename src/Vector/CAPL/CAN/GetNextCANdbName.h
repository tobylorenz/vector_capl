#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Finds out the names of the other assigned databases.
 *
 * Finds out the names of the other assigned databases with pos.
 *
 * @param pos
 *   Position number of the database to be found.
 *
 * @param buffer
 *   Buffer in which the database name is written.
 *
 * @param size
 *   Size of the buffer in Byte.
 *
 * @return
 *   If successful unequal 0, otherwise 0.
 */
dword VECTOR_CAPL_EXPORT GetNextCANdbName(dword pos, char * buffer, dword size);

}
