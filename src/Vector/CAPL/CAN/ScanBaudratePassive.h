#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Starts the scan and detects the baud rate on the given channel.
 *
 * Baud rate scanner checks different baud rates and tries to receive a message on the
 * channel. Function starts the scan and detects the baud rate on the given channel. Result
 * of the function is written into the Write window.
 *
 * If a wrong baudrate is present, CANoe cannot receive messages and sends an ErrorFrame,
 * which can be put on the bus using the parameter bAcknowledge.
 *
 * @param channel
 *   Channel number. (1,..,32)
 *
 * @param messageID
 *   ID of the message that the scanner will receive to detect the baudrate.
 *   If this value is 0xffff the scanner will receive all the messages on the channel.
 *
 * @param firstBaudrate, lastBaudrate
 *   Baudrate range to scan.
 *   - If both values are set to zero the scanner checks the most commonly used baudrates:
 *     33.333, 50.0, 83.333, 100.0, 125.0, 250.0, 500.0, 1000.0 [kBaud]
 *   - If both values are the same but not zero the scanner multiplies the baudrate with a
 *     given factor (Value range 0.25-5.0). The factor is changed with steps of 0.25.
 *   - If both values are different, all possible baudrate values in the ranged are scanned.
 *     The incremental step in the range is 1.5%.
 *
 * @param timeout
 *   Period of time [ms] the scanner waits when the message is sent.
 *
 * @param bAcknowledge
 *   Acknowledge mode on (1)/off (0).
 *   If a wrong baud rate is present, CANoe cannot receive messages and sends an ErrorFrame,
 *   which can be put on the bus using the parameter bAcknowledge.
 *   The parameter serves for the fact that CANoe - as a passive receiver - can participate
 *   indirectly in the network communication by sending an ErrorFrame.
 *   The parameter does not change the Acknowledge settings of the Hardware Configuration
 *   dialog. The parameter has an effect only during runtime of the function.
 *
 * @return
 *   Returns 0 if the scan function was successfully started. Otherwise the return value is nonzero.
 */
long VECTOR_CAPL_EXPORT ScanBaudratePassive(dword channel, dword messageID, double firstBaudrate, double lastBaudrate, dword timeout, dword bAcknowledge);

}
