#include "canGetConfiguration.h"

#include <iostream>

namespace capl
{

long canGetConfiguration(int channel, CANSettings settings)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
