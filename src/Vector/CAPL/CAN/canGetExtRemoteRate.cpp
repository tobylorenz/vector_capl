#include "canGetExtRemoteRate.h"

#include <iostream>

namespace capl
{

long canGetExtRemoteRate(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
