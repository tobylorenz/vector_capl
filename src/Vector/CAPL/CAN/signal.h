#pragma once

#include "../DataTypes.h"
#include "dbSig.h"
#include "message.h"

#include "../vector_capl_export.h"

namespace capl
{

template<typename RawType>
class VECTOR_CAPL_EXPORT signal
{
public:
    signal(message * parent = nullptr);
    signal(const signal & rhs);
    virtual ~signal();
    signal & operator =(const signal & rhs);

    /** Start bit of the signals in the message */
    dword bitstart;

    /** Number of bits in the signal */
    dword bitcount;

    /* bit order */
    bool bigEndian;

    /** Offset for conversion raw value -> physical value */
    double offset;

    /** Factor for conversion raw value -> physical value */
    double factor;

    /** Unit of the signal */
    char * unit;

    /** Minimum of the signal (0, if not defined) */
    double minimum;

    /** Maximum of the signal (0, if not defined) */
    double maximum;

    /** Signal definition from the database */
    dbSig * dbtype;

    /* whenever the message is updated call this to extract signal data */
    bool messageUpdated();

    /* raw signal */
    operator RawType() const; // get
    signal & operator=(const RawType b); // set

    /* physical signal */
    double getPhys();
    void setPhys(double newValue);

    /* the following part must be auto-generated based on configuration */
    #if 0
        /** Name of self-defined database attribute */
        //char * AttributeName; // for string attribute
        //double AttributeName; // otherwise double
    #endif

private:
    message * parent;
    RawType rawValue;
};

}
