#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current error count.
 *
 * @deprecated
 *   Replaced by RxChipErrorCount.
 *
 * Returns the current RX error count in the receiver of the specified CAN channel.
 *
 * @param channel
 *   CAN channel.
 *   - Vector API driver: Values 1 ... 32
 *   - Softing API driver: Values 1, 2
 *
 * @return
 *   Current error count in the receiver of the specified CAN channel.
 */
long VECTOR_CAPL_EXPORT canGetRxErrorCount(long channel);

}
