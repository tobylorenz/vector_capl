#include "resetCan.h"

#include <iostream>

namespace capl
{

void resetCan(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
