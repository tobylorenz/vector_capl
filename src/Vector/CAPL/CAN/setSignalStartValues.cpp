#include "setSignalStartValues.h"

#include <iostream>

namespace capl
{

long setSignalStartValues(message msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long setSignalStartValues(multiplexed_message msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long setSignalStartValues(FRFrame frame)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long setSignalStartValues(FRFrame frame, byte uninitializedData)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long setSignalStartValues(FrPDU pdu)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long setSignalStartValues(FrPDU pdu, byte uninitializedData)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long setSignalStartValues(pg paramGroup)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long setSignalStartValues(J1587Param parameter)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long setSignalStartValues(linMessage msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
