#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current number of Tx errors in the CAN receiver of a channel.
 *
 * Returns the current number of Tx errors in the CAN receiver of a channel.
 *
 * @return
 *   Current number of errors in the CAN receiver of a channel.
 */
long VECTOR_CAPL_EXPORT TxChipErrorCount(void);

}
