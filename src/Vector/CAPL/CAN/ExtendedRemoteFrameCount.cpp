#include "ExtendedRemoteFrameCount.h"

#include <iostream>

namespace capl
{

long ExtendedRemoteFrameCount(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
