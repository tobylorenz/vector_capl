#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets various CANcab modes.
 *
 * Various CANcabs modes may be set. Replaces the setPortBits functions.
 *
 * @param ntype
 *   Unused, must be set to 0
 *
 * @param nchannel
 *   CAN channel
 *
 * @param nmode
 *   Is used for the control of the board lines via a bit pattern.
 *   Mode, valid values:
 *   - 0: NORMAL
 *   - 1: SLEEP
 *   - 2: HIVOLTAGE
 *   - 3: HISPEED
 *   - 4: DUAL_WIRE
 *   - 5: SINGLE_WIRE_LOW
 *   - 6: SINGLE_WIRE_HIGH
 *   - 7: is reserved
 *   For CANcab Eva two output lines can be set.
 *   - 8: Line2=0, Line1=0
 *   - 9: Line2=0, Line1=1
 *   - 10: Line2=1, Line1=0
 *   - 11: Line2=1, Line1=1
 *
 * @param nflags
 *   Is used for the control of the board lines via a bit pattern.
 *   Flag, valid values:
 *   - 0x01: AUTOWAKEUP, only together with SLEEP
 *   - 0x02: HIGHPRIO, only CANcab 5790 c, 1=clear tx-buffers
 *
 * @return
 *   - 0: ok
 *   - !=0: Error
 */
long VECTOR_CAPL_EXPORT setCanCabsMode(long ntype, long nchannel, long nmode, long nflags);

}
