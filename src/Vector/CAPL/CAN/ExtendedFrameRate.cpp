#include "ExtendedFrameRate.h"

#include <iostream>

namespace capl
{

long ExtendedFrameRate(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
