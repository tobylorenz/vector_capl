#include "canGetRxErrorCount.h"

#include <iostream>

namespace capl
{

long canGetRxErrorCount(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
