#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Determines the type of CAN controller used.
 *
 * Determines the type of CAN controller used.
 *
 * @param channel
 *   CAN channel
 *   - 0: both controller
 *   - 1: Channel 1
 *   - 2: Channel 2
 *
 * @return
 *   Type of controller with the following values:
 *   - 5: NEC 72005
 *   - 200: Philipps PCA82C200
 *   - 526: Intel 82526
 *   - 527: Intel 82527
 *   - 1000, 1001: Philipps SJA1000
 *   Other types may occur. DEMO versions return the result 0 or simulate one of the existing
 *   types. If an attempt is made to access a nonexistent channel (e.g. Channel 2 for CPC/PP)
 *   or if the driver used does not support this function, the functional result is 0.
 */
long VECTOR_CAPL_EXPORT getChipType(long channel);

}
