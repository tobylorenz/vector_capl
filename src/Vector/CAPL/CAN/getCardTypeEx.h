#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Determines the card type of CAN channel.
 *
 * Determines the card type of CAN channel. Is needed e.g. to program the BTR / OCR
 * values.
 *
 * @param can
 *   Channel number
 *
 * @return
 *   Type of board as one of the following values:
 *   - 0: DBB196 - Daimler-Benz-Board with FullCAN
 *   - 1: DBB196B - with BasicCAN
 *   - 2: CANIB - Bosch CANIB
 *   - 3: DEMO - Demo driver
 *   - 6: CANAC2 - Softing AC2/200/ANA
 *   - 7: CANAC2X - Softing AC 2/527/ANA
 *   - 8: CPC/PP = EMS wish module
 *   - 9: INDIGO - Silicon Graphics Indigo2
 *   - 10: CANCARD - PCMCIA 11 Bit
 *   - 11: CANCARDX - PCMCIA 29 Bit
 *   - 12: CANAC2B - Softing AC2/527 11 Bit
 *   - 15: Peak CAN-Dongle
 *   - 16: Vector CAN-Dongle
 *   - 17: Vector PCMCIA CANcardX
 *   - 18: Virtual CAN driver
 *   - 20: Softing PCMCIA CANcard SJA1000
 *   - 25: Vector PCMCIA CANcardXL
 *   - 27: Vector USB CANcaseXL
 *   - 28: Vector CANcaseXLLog (USB + memory)
 *   - 29: Vector CANboardXL PCI
 *   - 30: Vector CPCI CANboardXL Compact
 *   - 31: Vector CANboardXL PCI express
 *   - 33: Vector VN7600
 *   - 34: Vector ExpressCard CANcardXLe
 *   Additional types may be added!
 */
int VECTOR_CAPL_EXPORT getCardTypeEx(int can);

}
