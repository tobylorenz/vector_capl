#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets another baud rate.
 *
 * Sets another baud rate. The values do not become active until the next call of the
 * function resetCan.
 *
 * It should be noted that these values depend on the CAN controller used.
 *
 * @param channel
 *   CAN channel
 *   - 0: Both controllers
 *   - 1: channel 1
 *   - 2: channel 2
 *
 * @param btr0
 *   Value of Bit Timing Register 0.
 *
 * @param btr1
 *   Value of Bit Timing Register 1.
 *
 * @return
 *   Always 1
 */
long VECTOR_CAPL_EXPORT setBtr(long channel, byte btr0, byte btr1);

}
