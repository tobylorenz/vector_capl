#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current rate of CAN error messages of a channel.
 *
 * Returns the current rate of CAN error messages of a channel.
 *
 * @return
 *   Current rate of CAN error messages on a channel in messages per second.
 */
long VECTOR_CAPL_EXPORT ErrorFrameRate(void);

}
