#include "message.h"

namespace capl
{

message::message() :
    name(nullptr),
    data()
{
}

message::~message()
{
    if (name != nullptr)
        delete name;
}

byte message::Byte(int x)
{
    return data[x];
}

word message::Word(int x)
{
    return
        (data[x+0] << 8) |
        (data[x+1] << 0);
}

dword message::DWord(int x)
{
    return
        (data[x+0] << 24) |
        (data[x+1] << 16) |
        (data[x+2] <<  8) |
        (data[x+3] <<  0);
}

qword message::QWord(int x)
{
    return
        (data[x+0] << 56) |
        (data[x+1] << 48) |
        (data[x+2] << 40) |
        (data[x+3] << 32) |
        (data[x+4] << 24) |
        (data[x+5] << 16) |
        (data[x+6] <<  8) |
        (data[x+7] <<  0);
}

char message::Char(int x)
{
    return data[x];
}

short int message::Int(int x)
{
    return
        (data[x+0] << 8) |
        (data[x+1] << 0);
}

long message::Long(int x)
{
    return
        (data[x+0] << 24) |
        (data[x+1] << 16) |
        (data[x+2] <<  8) |
        (data[x+3] <<  0);
}

int64 message::Int64(int x)
{
    return
        (data[x+0] << 56) |
        (data[x+1] << 48) |
        (data[x+2] << 40) |
        (data[x+3] << 32) |
        (data[x+4] << 24) |
        (data[x+5] << 16) |
        (data[x+6] <<  8) |
        (data[x+7] <<  0);
}

}
