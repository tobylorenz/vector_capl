#include "ChipState.h"

#include <iostream>

namespace capl
{

long ChipState(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
