#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the number of error frames on a channel since start of measurement.
 *
 * Returns the number of error frames on a channel since start of measurement.
 *
 * @return
 *   Number of error frames on a channel since start of measurement.
 */
long VECTOR_CAPL_EXPORT ErrorFrameCount(void);

}
