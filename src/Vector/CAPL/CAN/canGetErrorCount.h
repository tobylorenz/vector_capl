#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the number of Error Frames.
 *
 * @deprecated
 *   Replaced by ErrorFrameCount.
 *
 * Returns the number of Error Frames on the specified channel since start of measurement.
 *
 * @param channel
 *   CAN channel.
 *   - Vector API driver: Values 1 ... 32
 *   - Softing API driver: Values 1, 2
 *
 * @return
 *   Number of Error Frames on the specified channel since start of measurement.
 */
long VECTOR_CAPL_EXPORT canGetErrorCount(long channel);

}
