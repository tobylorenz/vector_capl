#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns an extended ID.
 *
 * Returns an extended id.
 *
 * @param id
 *   Id part of a message.
 *
 * @return
 *   Extended identifier
 */
dword VECTOR_CAPL_EXPORT mkExtId(dword id);

}
