#include "canGetErrorRate.h"

#include <iostream>

namespace capl
{

long canGetErrorRate(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
