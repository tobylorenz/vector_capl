#include "output.h"
#include "CAN_intern.h"
#include <iostream>
#include <ISO/11898.h>

namespace capl
{

void output(message msg)
{
    ISO11898::Controller * canController;
    canController = caplIntern::canControllers[msg.MsgChannel];
    if (canController == nullptr)
        return;

    /* define the message */
    ISO11898::Message isoMessage;
    isoMessage.identifier = msg.ID & 0x3fffffff;
    isoMessage.remoteTransmissionRequest = (msg.RTR == 1);
    isoMessage.identifierExtension = ((msg.ID & (1<<31)) != 0);
    isoMessage.extendedDataLength = (msg.EDL == 1);
    isoMessage.bitRateSwitch = (msg.BRS == 1);
    isoMessage.errorStateIndicator = (msg.ESI == 1);
    isoMessage.dataLengthCode = msg.DLC;
    isoMessage.data = msg.data;

    /* send it out */
    canController->sendMessage(isoMessage);
}

void output(errorFrame msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
