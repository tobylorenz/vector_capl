#include "canGetExtData.h"

#include <iostream>

namespace capl
{

long canGetExtData(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
