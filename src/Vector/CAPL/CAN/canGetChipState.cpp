#include "canGetChipState.h"

#include <iostream>

namespace capl
{

long canGetChipState(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
