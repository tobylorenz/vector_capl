#pragma once

#include "../DataTypes.h"
#include <vector>

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 */
class VECTOR_CAPL_EXPORT errorActive
{
public:
    errorActive();
    virtual ~errorActive();

    /**
     * Receive error counter
     */
    long errorCountRX;

    /**
     * Transmit error counter
     */
    long errorCountTX;

    /**
     * Assign the channel
     */
    word can;
};

extern std::vector<void (*)(class errorActive)> on_errorActive;

}
