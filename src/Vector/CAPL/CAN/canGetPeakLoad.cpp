#include "canGetPeakLoad.h"

#include <iostream>

namespace capl
{

long canGetPeakLoad(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
