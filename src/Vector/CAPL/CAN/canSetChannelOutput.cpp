#include "canSetChannelOutput.h"

#include <iostream>

namespace capl
{

long canSetChannelOutput(long channel, long silent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
