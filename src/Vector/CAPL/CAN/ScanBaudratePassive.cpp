#include "ScanBaudratePassive.h"

#include <iostream>

namespace capl
{

long ScanBaudratePassive(dword channel, dword messageID, double firstBaudrate, double lastBaudrate, dword timeout, dword bAcknowledge)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
