#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current number of errors.
 *
 * @deprecated
 *   Replaced by TxChipErrorCount.
 *
 * Returns the current number of TX errors in the CAN receiver of the specified channel.
 *
 * @param channel
 *   CAN channel.
 *   - Vector API driver: Values 1 ... 32
 *   - Softing API driver: Values 1, 2
 *
 * @return
 *   Current number of errors in the CAN receiver of the specified channel.
 */
long VECTOR_CAPL_EXPORT canGetTxErrorCount(long channel);

}
