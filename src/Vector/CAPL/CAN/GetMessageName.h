#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Finds out the message name.
 *
 * Finds out the message name.
 *
 * @param id
 *   Id of the message for which the message name should be found.
 *
 * @param context
 *   Context of assigned databases.
 *   The low word of context contains the channel number.
 *   The high word of context contains the bus type.
 *   Available bus types:
 *   - 1: CAN
 *   - 5: LIN
 *   - 6: MOST
 *   - 7: FlexRay
 *   - 8: BEAN
 *   - 9: J1708
 *
 * @param buffer
 *   Buffer in which the message name is written.
 *
 * @param size
 *   Size of the buffer in Byte.
 *
 * @return
 *   If successful unequal 0, otherwise 0.
 */
dword VECTOR_CAPL_EXPORT GetMessageName(dword id, dword context, char * buffer, dword size);

}
