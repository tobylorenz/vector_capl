#include "canFdSetConfiguration.h"

#include <iostream>

namespace capl
{

long canFdSetConfiguration(int channel, CANSettings abrSettings, CANSettings dbrSettings)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
