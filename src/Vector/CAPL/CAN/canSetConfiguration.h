#pragma once

#include "../DataTypes.h"
#include "CANSettings.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   The CAN controller parameters can be set.
 *
 * The CAN controller parameters can be set.
 *
 * canSetConfiguration performs an automatic reset of the CAN controller.
 *
 * @param channel
 *   The CAN channel.
 *
 * @param settings
 *   baudrate settings
 *
 * @return
 *   - 1 = success
 *   - 0 = error
 */
long VECTOR_CAPL_EXPORT canSetConfiguration(int channel, CANSettings settings);

}
