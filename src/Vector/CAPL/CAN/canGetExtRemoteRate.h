#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current rate of extended remote CAN messages.
 *
 * @deprecated
 *   Replaced by ExtendedRemoteFrameRate.
 *
 * Returns the current rate of extended remote CAN messages on the specified channel.
 *
 * @param channel
 *   CAN channel.
 *   - Vector API driver: Values 1 ... 32
 *   - Softing API driver: Values 1, 2
 *
 * @return
 *   Current rate of extended remote CAN messages on the specified channel in frames per second.
 */
long VECTOR_CAPL_EXPORT canGetExtRemoteRate(long channel);

}
