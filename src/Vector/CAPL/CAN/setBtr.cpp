#include "setBtr.h"

#include <iostream>

namespace capl
{

long setBtr(long channel, byte btr0, byte btr1)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 1;
}

}
