#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 */
struct VECTOR_CAPL_EXPORT CANSettings
{
    double baudrate; //in bit/s
    unsigned char tseg1, tseg2;
    unsigned char sjw;
    unsigned char sam;
    unsigned int flags;
};

}
