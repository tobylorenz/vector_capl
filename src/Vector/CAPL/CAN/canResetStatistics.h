#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Resets CAN statistics.
 *
 * Resets CAN channel statistics.
 */
void VECTOR_CAPL_EXPORT canResetStatistics(void);

/**
 * @ingroup CAN
 *
 * @brief
 *   Resets CAN statistics.
 *
 * Resets CAN channel statistics.
 *
 * @param channel
 *   CAN channel (1-based)
 */
void VECTOR_CAPL_EXPORT canResetStatistics(long channel);

}
