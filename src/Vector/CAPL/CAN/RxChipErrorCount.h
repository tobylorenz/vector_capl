#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current Rx error count in the receiver of a channel.
 *
 * Returns the current Rx error count in the receiver of a channel.
 *
 * @return
 *   Current error count in the receiver of a channel.
 */
long VECTOR_CAPL_EXPORT RxChipErrorCount(void);

}
