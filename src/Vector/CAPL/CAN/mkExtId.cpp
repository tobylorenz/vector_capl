#include "mkExtId.h"

#include <iostream>

namespace capl
{

dword mkExtId(dword id)
{
    return (id | (1 << 31));
}

}
