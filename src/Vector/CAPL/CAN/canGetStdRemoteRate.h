#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current rate of standard remote CAN frames.
 *
 * @deprecated
 *   Replaced by StandardRemoteFrameRate.
 *
 * Returns the current rate of standard remote CAN frames of the specified channel.
 *
 * @param channel
 *   CAN channel.
 *   - Vector API driver: Values 1 ... 32
 *   - Softing API driver: Values 1, 2
 *
 * @return
 *   Current rate of standard remote CAN frames of the specified channel in messages per second.
 */
long VECTOR_CAPL_EXPORT canGetStdRemoteRate(long channel);

}
