#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Defines the response of the CAN controller to the bus traffic and sets the ACK bit.
 *
 * Defines the response of the CAN controller to the bus traffic and sets the ACK bit.
 * The CAN transmitter of the channel is switched off. So CANoe doesn't generate an Ack bit
 * here, and messages can no longer be sent. It is still possible to receive messages.
 *
 * @param channel
 *   CAN channel
 *
 * @param silent
 *   silent
 *   - 0: silent
 *   - 1: normal
 *
 * @return
 *   - 0: ok
 *   - !=0: error
 */
long VECTOR_CAPL_EXPORT canSetChannelOutput(long channel, long silent);

}
