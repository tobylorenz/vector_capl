#include "canGetStdRemoteRate.h"

#include <iostream>

namespace capl
{

long canGetStdRemoteRate(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
