#include "canSetChannelAcc.h"

#include <iostream>

namespace capl
{

long canSetChannelAcc(long channel, dword code, dword mask)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
