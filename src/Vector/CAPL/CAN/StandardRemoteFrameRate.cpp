#include "StandardRemoteFrameRate.h"

#include <iostream>

namespace capl
{

long StandardRemoteFrameRate(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
