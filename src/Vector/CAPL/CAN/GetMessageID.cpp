#include "GetMessageID.h"

#include <iostream>

namespace capl
{

dword GetMessageID(char * messageName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword GetMessageID(char * dbName, char * messageName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
