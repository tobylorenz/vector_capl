#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current chip state.
 *
 * @deprecated
 *   Replaced by ChipState.
 *
 * Returns the current chip state of the specified CAN controller.
 *
 * @param channel
 *   CAN channel.
 *
 * @return
 *   Chip state of the specified CAN controller. See the following table for a description of the return values.
 *   - 0: Value not available
 *   - 1: Simulated
 *   - 3: Error Active
 *   - 4: Warning Level
 *   - 5: Error Passive
 *   - 6: Bus Off
 */
long VECTOR_CAPL_EXPORT canGetChipState(long channel);

}
