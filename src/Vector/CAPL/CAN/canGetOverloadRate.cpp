#include "canGetOverloadRate.h"

#include <iostream>

namespace capl
{

long canGetOverloadRate (long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
