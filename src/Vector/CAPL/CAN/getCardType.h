#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Determines the type of CAN platform being used.
 *
 * Determines the type of CAN platform being used. Is needed e.g. to program the BTR /
 * OCR values.
 *
 * @return
 *   17: for Vector drivers
 *   For other manufacturer other values will be returned.
 */
long VECTOR_CAPL_EXPORT getCardType(void);

}
