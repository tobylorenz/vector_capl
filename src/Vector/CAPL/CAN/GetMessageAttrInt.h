#pragma once

#include "../DataTypes.h"
#include "CAN.h"
#include "../J1939/J1939.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Gets the value of a message attribute from the database.
 *
 * Gets the value of a message attribute from the database.
 *
 * A user-defined attribute with the name specified in the parameter, and of the Integer
 * type, must be defined in the database. If no such attribute is defined, the function
 * returns 0. If no attribute value is assigned to the message in the database, the default
 * value of the attribute definition is returned.
 *
 * @param canMessage
 *   Message variable
 *
 * @param attributeName
 *   Attribute name
 *
 * @return
 *   Value of the attribute (or default value) from the database.
 */
long VECTOR_CAPL_EXPORT GetMessageAttrInt(message canMessage, char * attributeName);

/**
 * @ingroup CAN
 *
 * @brief
 *   Gets the value of a message attribute from the database.
 *
 * Gets the value of a message attribute from the database.
 *
 * A user-defined attribute with the name specified in the parameter, and of the Integer
 * type, must be defined in the database. If no such attribute is defined, the function
 * returns 0. If no attribute value is assigned to the message in the database, the default
 * value of the attribute definition is returned.
 *
 * @param parameterGroup
 *   Parameter group variable
 *
 * @param attributeName
 *   Attribute name
 *
 * @return
 *   Value of the attribute (or default value) from the database.
 */
long VECTOR_CAPL_EXPORT GetMessageAttrInt(pg parameterGroup, char * attributeName);

}
