#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Sets the Output Control Register.
 *
 * Sets the Output Control Register. The values do not become active until the next call of
 * the function resetCan().
 *
 * It should be noted that these values depend on the CAN platform used.
 *
 * @param channel
 *   - 0: all channels
 *   - >0: only the given channel
 *
 * @param ocr
 *   Value of the Output Control Register
 *
 * @return
 *   1: success
 *   0: error
 */
long VECTOR_CAPL_EXPORT setOcr(long channel, byte ocr);

}
