#pragma once

#include "../DataTypes.h"

#include <cstdint>
#include <vector>

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * CAN message
 */
class VECTOR_CAPL_EXPORT message
{
public:
    message();
    virtual ~message();

    /* Identification */

    union {
        /**
         * Transmission channel or channel through which the frame has been received.
         *
         * Value range: 1..32
         */
        word CAN;

        /**
         * Transmission channel or channel through which the frame has been received.
         *
         * Value range: 1..32
         */
        word MsgChannel;
    };

    /**
     * Message identifier
     */
    dword ID;

    /**
     * (unqualified) symbolic name of the message from the database (since version 7.2)
     */
    char * name;

    /**
     * Direction of transmission, event classification;
     *
     * possible values: RX, TX, TXREQUEST
     */
    enum dir /* : byte */ {
        RX, /**< Message was received (DIR == RX) */
        TX, /**< Message was transmitted (DIR == TX) */
        TXREQUEST /**< Transmission request was set for message (DIR == TXREQUEST) */
    } dir;

    /**
     * Remote Transmission Request;
     *
     * possible values: 0 (no RTR), 1 (RTR)
     */
    byte RTR;

    /**
     * Combination of DIR and RTR for an efficient evaluation. (TYPE = (RTR << 8) | DIR )
     */
    enum TYPE /* : word */ {
        RXREMOTE, /**< Remote message was received ((DIR == RX) && RTR) */
        TXREMOTE, /**< Remote message was transmitted ((DIR == TX) && RTR) */
        TXREQUESTREMOTE, /**< Transmission request was set for remote message ((DIR == TXREQUEST) && RTR) */
        RXDATA, /**< Data message was received ((DIR == RX) && !RTR) */
        TXDATA, /**< Data message was transmitted ((DIR == TX) && !RTR) */
        TXREQUESTDATA /**< Transmission request was set for data message ((DIR == TXREQUEST) && !RTR) */
    };

    /**
     * The data field length of a message is coded with the DLC (Data Length Code).
     *
     * Value range: 0...15
     *
     * The data field length for
     * CAN messages is in the range 0...8, and for
     * CAN FD messages in the range 0...64.
     */
    byte DLC;

    /**
     * Data length in bytes.
     */
    byte DataLength;

    /* Message Times and Lengths */

    /**
     * Point in time, units: nanoseconds
     */
    int64 TIME_NS;

    /**
     * Point in time, units: 10 microseconds
     *
     * This selector is not available when executing CAPL code directly on hardware interfaces. Therefore you must use Time_ns.
     */
    dword TIME;

    /**
     * Start-of-Frame time stamp in ns.
     *
     * For some CAN hardware this value is not available (value 0). In such a case a software calculation is required (see CANoe Options dialog).
     */
    int64 SOF;

    /**
     * Frame duration in ns.
     *
     * Note, that the frame duration always includes three bits of the Interframe Space.
     * For some CAN hardware this value is not available (value 0). In such a case a software calculation is required (see CANoe Options dialog).
     */
    dword FrameLen;

    /**
     * Number of bits of a CAN/CAN FD message (incl. IFS bits).
     *
     * For some CAN hardware this value is not available (value 0). In such a case a software calculation is required (seeCANoe Options dialog).
     */
    word Bitcount;

    /* Data Access */

    /**
     * Message data byte (unsigned 8 bit); possible values for x: 0...7
     */
    byte Byte(int x);

    /**
     * Message data word (unsigned 16 bit); possible values for x: 0...6
     *
     * The index is byte-oriented; for example, word(1) references to the data beginning at byte 1 and consists of byte 1...2 (16 bit).
     */
    word Word(int x);

    /**
     * Message data word (unsigned 32 bit); possible values for x: 0...4
     *
     * The index is byte-oriented; for example, dword(1) references to the data beginning at byte 1 and consists of byte 1...4 (32 bit).
     */
    dword DWord(int x);

    /**
     * Message data word (unsigned 64 bit); possible values for x: only 0
     */
    qword QWord(int x);

    /**
     * Message data byte (signed 8 bit); possible values for x: 0...7
     */
    char Char(int x);

    /**
     * Message data word (signed 16 bit); possible values for x: 0...6
     * The index is byte-oriented; for example, int(1) references to the data beginning at byte 1 and consists of byte 1...2 (16 bit).
     */
    short int Int(int x);

    /**
     * Message data word (signed 32 bit); possible values for x: 0...4
     * The index is byte-oriented; for example, long(1) references to the data beginning at byte 1 and consists of byte 1...4 (32 bit).
     */
    long Long(int x);

    /**
     * Message data word (signed 64 bit); possible values for x: only 0
     */
    int64 Int64(int x);

    /* Flags */

    /**
     * Transmit and receive flags
     *
     * - 0x02: The transmit buffer will be emptied before transmitting the message - the message is not blocked by other messages.
     * - 0x04: The NERR flag of the CAN transceiver was active at message reception (system is in single wire mode)
     * - 0x08: The message will be sent or was received in "high voltage" mode
     * - 0x10: Remote Frame
     * - 0x40: Transmit Acknowledge (equal to DIR==TX)
     * - 0x80: Transmit Request (equal to DIR==TXREQUEST)
     */
    dword MsgFlags;

    /**
     * Message has been sent by a simulated CAPL-node; possible values: 0 (no), 1 (yes)
     *
     * Has been modified for the simulation setup for CANoe version 1.2 resp. 2.0 to distinguish between real and simulated components.
     */
    byte SIMULATED;

    /**
     * Extended Data Length
     *
     * - 0 = CAN message
     * - 1 = CAN FD message
     */
    char EDL;

    /**
     * Bit Rate Switch
     *
     * Only for CAN FD messages.
     *
     * - 0: Use arbitration bit rate for data segment
     * - 1: use data bit rate for data segment
     */
    char BRS;

    /**
     * Error State Indicator
     *
     * - 0 = ESI not set
     * - 1 = ESI set
     * - 2 = CAN controller sets ESI automatically depending on the state of the controller
     */
    char ESI;

    /**
     * Inverted SRR. (This value is only available for some CAN hardware and a specific driver.)
     *
     * Value range: 0, 1
     *
     * Default: 0
     */
    char SRR_INV;

    /**
     * Level of the R0 bit on the bus. (This value is only available for some CAN hardware and a specific driver.)
     *
     * Value range: 0, 1
     */
    char R0;

    /**
     * Level of the R1 bit on the bus. (This value is only available for some CAN hardware and a specific driver.)
     *
     * Value range: 0, 1
     */
    char R1;

    /**
     * When sending a message you can use TxReqCount to set the number of transmission attempts (single shot mode).
     *
     * If it wasn't possible to send the message and transmission error notification is active you will be notified in the Trace Window and in CAPL with a TXReq message and the TxFailed message selector.
     *
     * Value range: 0, 1
     */
    char TxFailed;

    /* Other Properties */

    /**
     * Checksum of the message.
     */
    dword CRC;

    /**
     * Number of transmission attempts for a message. (This value is only available for some CAN hardware and a specific driver.)
     * With 0 the message will be repeated until it has been sent successfully once.
     *
     * See also transmission error notification.
     *
     * Value range: 0...15
     *
     * Default: 0
     */
    char TxReqCount;

    /**
     * Number of required transmission attempts for a message (only valid for TxReqCount > 0).
     *
     * Value range: 0...15
     */
    char TxCountRequired;

/* the following part must be auto-generated based on configuration */
#if 0
    /**
     * Send nodes of the message;
     * empty string if the number of send nodes is zero or more than one
     */
    char * Transmitter;

    /** Name of self-defined database attribute */
    //char * AttributeName; // for string attribute
    //double AttributeName; // otherwise double
#endif

    /* intern */
    std::vector<std::uint8_t> data;
};

}
