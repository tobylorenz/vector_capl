#include "ScanBaudrateActive.h"

#include <iostream>

namespace capl
{

long ScanBaudrateActive(dword channel, dword messageID, double firstBaudrate, double lastBaudrate, dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
