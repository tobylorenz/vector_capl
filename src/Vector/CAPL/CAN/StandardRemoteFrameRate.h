#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup CAN
 *
 * @brief
 *   Returns the current rate of standard CAN frames on a channel.
 *
 * Returns the current rate of standard CAN frames on a channel.
 *
 * @return
 *   Current rate of standard remote CAN frames of a channel in messages per second.
 */
long VECTOR_CAPL_EXPORT StandardRemoteFrameRate(void);

}
