#include "canGetErrorCount.h"

#include <iostream>

namespace capl
{

long canGetErrorCount(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
