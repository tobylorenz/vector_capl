#include "GetFirstCANdbName.h"

#include <iostream>

namespace capl
{

dword GetFirstCANdbName(char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
