#include "GetMessageAttrInt.h"

#include <iostream>

namespace capl
{

long GetMessageAttrInt(message canMessage, char * attributeName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long GetMessageAttrInt(pg parameterGroup, char * attributeName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
