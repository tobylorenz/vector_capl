#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TSL
 *
 * @brief
 *   Initializes TSL to be used subsequently.
 *
 * Initializes TSL to be used subsequently.
 *
 * The tests are executed in the context of a specific tester-role. Dependent to the setting of the role, the error handling is more or less strict. This setting influences the transformation from an error class to an impact.
 *
 * Available tester-roles:
 * - developer: Less strict error handling, additional information during test execution.
 * - user: More strict error handling
 *
 * Intended usage:
 * During development of a test module, the TSL can be set more verbose and some problems in the test program are tolerated to get a longer list of issues before the test module is stopped. Purpose is to reduce the count of iterations between test module implementation and test module execution.
 * Once the test module is completed, the role should be set to user. Then the TSL is less verbose and possible errors do have bigger impact.
 * Purpose: The tester really notices problems in the test configuration.
 *
 * Details about the differences in the error handling are described here Test Service Library: Roles and Error Impacts.
 *
 * If the function is used, then it is recommended to perform the initialization once during preStart section of the CAPL program.
 *
 * @param aRole
 *   Allowed value range: developer, user
 *
 * @return
 *   - -1: invalid arguments
 *   - 0: arguments accepted
 */
long ChkConfig_Init(char * aRole);

}
