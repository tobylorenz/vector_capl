#include "ChkConfig_Init.h"

#include <iostream>

namespace capl
{

long ChkConfig_Init(char * aRole)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
