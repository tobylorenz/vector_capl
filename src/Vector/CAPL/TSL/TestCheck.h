/**
 * Checks are functions which run parallel to the defined test sequence or simulation node and continuously
 * check compliance with specific conditions.
 */

#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TSL
 */
class TestCheck
{
public:
    void CreateAllNodesBabbling(void);
    void StartAllNodesBabbling(void);
    void CreateAllNodesDead(void);
    void StartAllNodesDead(void);
    void CreateErrorFramesOccured(void);
    void StartErrorFramesOccured(void);
    void CreateInconsistentDLC(void);
    void StartInconsistentDLC(void);
    void CreateInconsistentRxDLC(void);
    void StartInconsistentRxDLC(void);
    void CreateInconsistentTxDLC(void);
    void StartInconsistentTxDLC(void);
    void CreateMostCriticalUnlock(void);
    void StartMostCriticalUnlock(void);
    void CreateMostErrorMessage(void);
    void StartMostErrorMessage(void);
    void CreateMostLightOff(void);
    void StartMostLightOff(void);
    void CreateMostMethodProtocolError(void);
    void StartMostMethodProtocolError(void);
    void CreateMostNetState(void);
    void StartMostNetState(void);
    void CreateMostPropertyProtocolError(void);
    void StartMostPropertyProtocolError(void);
    void CreateMostShortUnlock(void);
    void StartMostShortUnlock(void);
    void CreateMostStableLock(void);
    void StartMostStableLock(void);
    void CreateMsgAbsCycleTimeViolation(void);
    void StartMsgAbsCycleTimeViolation(void);
    void CreateMsgDistViolation(void);
    void StartMsgDistViolation(void);
    void CreateMsgOccurrenceCount(void);
    void StartMsgOccurrenceCount(void);
    void CreateMsgRelCycleTimeViolation(void);
    void StartMsgRelCycleTimeViolation(void);
    void CreateMsgRelOccurrenceViolation(void);
    void StartMsgRelOccurrenceViolation(void);
    void CreateMsgSignalValueInvalid(void);
    void StartMsgSignalValueInvalid(void);
    void CreateMsgSignalValueRangeViolation(void);
    void StartMsgSignalValueRangeViolation(void);
    void CreateNodeBabbling(void);
    void StartNodeBabbling(void);
    void CreateNodeDead(void);
    void StartNodeDead(void);
    void CreateNodeMsgsRelCycleTimeViolation(void);
    void StartNodeMsgsRelCycleTimeViolation(void);
    void CreateNodeMsgsRelOccurrenceViolation(void);
    void StartNodeMsgsRelOccurrenceViolation(void);
    void CreateSignalValueChange(void);
    void StartSignalValueChange(void);
    void CreateTimeout(void);
    void StartTimeout(void);
    void CreateUndefinedMessageReceived(void);
    void StartUndefinedMessageReceived(void);
    void StartLINDiagDelayTimesViolation(void);
    void StartLINETFViolation(void);
    void StartLINHeaderToleranceViolation(void);
    void StartLINMasterBaudrateViolation(void);
    void StartLINMasterInitTimeViolation(void);
    void StartLINReconfRequestFormatViolation(void);
    void StartLINRespErrorSignal(void);
    void StartLINRespToleranceViolation(void);
    void StartLINSchedTableViolation(void);
    void StartLINSyncBreakTimingViolation(void);
    void StartLINSyncDelTimingViolation(void);
    void StartLINWakeupReqLengthViolation(void);
    void StartLINWakeupRetryViolation(void);

    void destroy(void);

    void reset(void);
    void start(void);
    void stop(void);
    void QueryEventInterval(void);
    void QueryEventMessageContents(void);
    void QueryEventMessageId(void);
    void QueryEventMessageName(void);
    void QueryEventNodeName(void);
    void QueryEventReason(void);
    void QueryEventSchedSlotIndex(void);
    void QueryEventSignalValue(void);
    void QueryEventStatus(void);
    void QueryEventStatusToLog(void);
    void QueryEventStatusToWrite(void);
    void QueryEventTimestamp(void);
    void QueryEventTiming(void);
    void QueryNumEvents(void);
    void QueryNumRequests(void);
    void QueryNumTimedoutRequests(void);
    void QueryPrecision(void);
    void QueryRequestDstAdr(void);
    void QueryRequestFBlockId(void);
    void QueryRequestFunctionId(void);
    void QueryRequestInstId(void);
    void QueryRequestOpType(void);
    void QueryRequestSrcAdr(void);
    void QueryRequestTimestamp(void);
    void QueryStatAvResponseTime(void);
    void QueryStatEventFreePeriodAvg(void);
    void QueryStatEventFreePeriodMax(void);
    void QueryStatEventFreePeriodMed(void);
    void QueryStatEventFreePeriodMin(void);
    void QueryStatMaxValidResponseTime(void);
    void QueryStatMinResponseTime(void);
    void QueryStatNumProbes(void);
    void QueryStatProbeIntervalAvg(void);
    void QueryStatProbeIntervalMax(void);
    void QueryStatProbeIntervalMin(void);
    void QueryValid(void);
};

}
