/**
 * This class can be used to generate a specific value pattern for a signal or an environment variable for
 * stimulating a device.
 */

#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TSL
 */
class TestStimulus
{
public:
    void CreateCSV(void);
    // void CreateCSV(void);
    void CreateEnvVar(void);
    void CreateRamp(void);
    // void CreateRamp(void);
    void Constructor(void);
    void CreateToggle(void);
    // void CreateToggle(void);

    void destroy(void);

    void reset(void);
    void start(void);
    void stop(void);
    void QueryValid(void);
};

}
