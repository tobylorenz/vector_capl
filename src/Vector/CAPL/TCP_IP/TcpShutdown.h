#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Disables send operations on a specified socket.
 *
 * The function disables send operations on the specified socket. This function may be used
 * to shutdown a TCP connection.
 *
 * @param socket
 *   The socket handle.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastSocketError to get a more specific
 *     error code.
 */
long TcpShutdown(dword socket);

}
