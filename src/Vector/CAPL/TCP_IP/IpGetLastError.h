#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Returns the Winsock 2 error code of the last operation that failed.
 *
 * The function returns the Winsock 2 error code of the last operation that failed.
 *
 * @return
 *   The error code as provided by the Winsock 2 WSAGetLastError function.
 */
long IpGetLastError(void);

}
