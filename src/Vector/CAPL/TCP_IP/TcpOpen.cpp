#include "TcpOpen.h"

#include <iostream>

namespace capl
{

dword TcpOpen(dword address, dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
