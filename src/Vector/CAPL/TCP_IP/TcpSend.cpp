#include "TcpSend.h"

#include <iostream>

namespace capl
{

long TcpSend(dword socket, char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
