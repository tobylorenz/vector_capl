#include "TcpListen.h"

#include <iostream>

namespace capl
{

long TcpListen(dword socket)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
