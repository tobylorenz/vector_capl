#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Retrieves the addresses associated with a network interface.
 *
 * The function retrieves the addresses associated with a network interface. The interface is
 * specified by it's 1-based index in the list of network interfaces, i.e. the first interface has
 * index 1.
 *
 * @param index
 *   The 1-based network interface index.
 *
 * @param address
 *   The array used to store the addresses in network-byte order.
 *
 * @param count
 *   The size of the address array.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - ERROR_NOT_ENOUGH_MEMORY (8): The address array was insufficient.
 *   - WSA_INVALID_PARAMETER (87): The specified network interface index was invalid.
 */
long IpGetAdapterAddress(dword index, dword * address, dword count);

}
