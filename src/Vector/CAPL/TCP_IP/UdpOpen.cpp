#include "UdpOpen.h"

#include <iostream>

namespace capl
{

dword UdpOpen(dword address, dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
