#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Returns the number of network interfaces for the local computer.
 *
 * The function returns the number of network interfaces for the local computer, not
 * including the loopback interface.
 *
 * @return
 *   The number of network interfaces.
 */
dword IpGetAdapterCount(void);

}
