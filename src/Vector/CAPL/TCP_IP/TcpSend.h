#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Sends data on a specified socket.
 *
 * The function sends data on the specified socket. If the send operation doesn't complete
 * immediately the operation is performed asynchronously and the function will return
 * SOCKET_ERROR (-1). Use IpGetLastSocketError to get a more specific error code. If the
 * specific error code is WSA_IO_PENDING (997) the CAPL callback OnTcpSend will be called
 * on completion (successful or not), provided it is implemented in the same CAPL program.
 *
 * @param socket
 *   The socket handle.
 *
 * @param buffer
 *   The buffer containing the data to be sent.
 *
 * @param size
 *   The size of the data to be sent.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
 *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastSocketError to get a more specific
 *     error code.
 */
long TcpSend(dword socket, char * buffer, dword size);

}
