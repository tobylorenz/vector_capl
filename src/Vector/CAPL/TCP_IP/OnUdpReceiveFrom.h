#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   It is dispatched when an asynchronous receive operation on an UDP socket completes.
 *
 * Provided the CAPL program implements this callback it is dispatched when an
 * asynchronous receive operation on an UDP socket completes.
 *
 * @param socket
 *   The socket handle.
 *
 * @param result
 *   The result code of the asynchronous operation. If the operation completed successfully
 *   the value is zero. Otherwise the value is non-zero.
 *
 * @param address
 *   The remote address of the location which sent the data in network-byte order.
 *
 * @param port
 *   The remote port of the location which sent the data in host-byte order.
 *
 * @param buffer
 *   The buffer into which the data was stored.
 *
 * @param size
 *   The number of bytes received.
 */
void OnUdpReceiveFrom(dword socket, long result, dword address, dword port, char * buffer, dword size);

}
