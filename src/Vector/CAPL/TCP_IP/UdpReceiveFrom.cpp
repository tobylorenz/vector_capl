#include "UdpReceiveFrom.h"

#include <iostream>

namespace capl
{

long UdpReceiveFrom(dword socket, char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
