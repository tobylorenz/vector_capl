#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   It is dispatched when an asynchronous send operation on an UDP socket completes.
 *
 * Provided the CAPL program implements this callback it is dispatched when an
 * asynchronous send operation on an UDP socket completes.
 *
 * @param socket
 *   The socket handle.
 *
 * @param result
 *   The result code of the asynchronous operation. If the operation completed successfully
 *   the value is zero. Otherwise the value is non-zero.
 *
 * @param buffer
 *   The buffer provided with the send operation.
 *
 * @param size
 *   The number of bytes sent.
 */
void OnUdpSendTo(dword socket, long result, char * buffer, dword size);

}
