#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   It is dispatched when a connection request for a specified socket is received.
 *
 * Provided the CAPL program implements this callback it is dispatched when a connection
 * request for the specified socket is received.
 *
 * @param socket
 *   The socket handle.
 *
 * @param result
 *   The specific result code of the operation. If the operation completed successfully the
 *   value is zero. Otherwise the value is non-zero.
 */
void OnTcpListen(dword socket, long result);

}
