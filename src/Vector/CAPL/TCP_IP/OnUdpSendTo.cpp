#include "OnUdpSendTo.h"

#include <iostream>

namespace capl
{

void OnUdpSendTo(dword socket, long result, char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
