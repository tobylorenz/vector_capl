#pragma once

/**
 * @defgroup TCP_IP TCP/IP API
 */

#include "TcpSocket.h"
#include "UdpSocket.h"

/* IP API */
#include "IpBind.h"
#include "IpGetAdapterAddress.h"
#include "IpGetAdapterAddressAsString.h"
#include "IpGetAdapterCount.h"
#include "IpGetAdapterDescription.h"
#include "IpGetAdapterGateway.h"
#include "IpGetAdapterGatewayAsString.h"
#include "IpGetAdapterMask.h"
#include "IpGetAdapterMaskAsString.h"
#include "IpGetAddressAsNumber.h"
#include "IpGetAddressAsString.h"
#include "IpGetAddressAsArray.h"
#include "IpGetLastError.h"
#include "IpGetLastSocketError.h"
#include "IpGetLastSocketErrorAsString.h"
#include "IpSetSocketOption.h"

/* UDP API */
#include "UdpClose.h"
#include "UdpOpen.h"
#include "UdpReceiveFrom.h"
#include "UdpSendTo.h"
#include "OnUdpReceiveFrom.h"
#include "OnUdpSendTo.h"

/* TCP API */
#include "TcpAccept.h"
#include "TcpClose.h"
#include "TcpConnect.h"
#include "TcpGetRemoteAddress.h"
#include "TcpGetRemoteAddressAsString.h"
#include "TcpListen.h"
#include "TcpOpen.h"
#include "TcpReceive.h"
#include "TcpSend.h"
#include "TcpShutdown.h"
#include "OnTcpClose.h"
#include "OnTcpConnect.h"
#include "OnTcpListen.h"
#include "OnTcpReceive.h"
#include "OnTcpSend.h"

/* Winsock 2 Error Codes */
#define WSAEINTR                10004
#define WSAEACCES               10013
#define WSAEFAULT               10014
#define WSAEINVAL               10022
#define WSAEMFILE               10024
#define WSAEWOULDBLOCK          10035
#define WSAEINPROGRESS          10036
#define WSAEALREADY             10037
#define WSAENOTSOCK             10038
#define WSAEDESTADDRREQ         10039
#define WSAEMSGSIZE             10040
#define WSAEPROTOTYPE           10041
#define WSAENOPROTOOPT          10042
#define WSAEPROTONOSUPPORT      10043
#define WSAESOCKTNOSUPPORT      10044
#define WSAEOPNOTSUPP           10045
#define WSAEPFNOSUPPORT         10046
#define WSAEAFNOSUPPORT         10047
#define WSAEADDRINUSE           10048
#define WSAEADDRNOTAVAIL        10049
#define WSAENETDOWN             10050
#define WSAENETUNREACH          10051
#define WSAENETRESET            10052
#define WSAECONNABORTED         10053
#define WSAECONNRESET           10054
#define WSAENOBUFS              10055
#define WSAEISCONN              10056
#define WSAENOTCONN             10057
#define WSAESHUTDOWN            10058
#define WSAETIMEDOUT            10060
#define WSAECONNREFUSED         10061
#define WSAEHOSTDOWN            10064
#define WSAEHOSTUNREACH         10065
#define WSAEPROCLIM             10067
#define WSASYSNOTREADY          10091
#define WSAVERNOTSUPPORTED      10092
#define WSANOTINITIALISED       10093
#define WSAEDISCON              10101
#define WSATYPE_NOT_FOUND       10109
#define WSAHOST_NOT_FOUND       11001
#define WSATRY_AGAIN            11002
#define WSANO_RECOVERY          11003
#define WSANO_DATA              11004
#define WSA_INVALID_HANDLE      6
#define WSA_INVALID_PARAMETER   87
#define WSA_IO_INCOMPLETE       996
#define WSA_IO_PENDING          997
#define WSA_NOT_ENOUGH_MEMORY   8
#define WSA_OPERATION_ABORTED   995
#define WSAINVALIDPROCTABLE     10104
#define WSAINVALIDPROVIDER      10105
#define WSAPROVIDERFAILEDINIT   10106
#define WSASYSCALLFAILURE       10107
