#include "TcpSocket.h"

#include <iostream>

namespace capl
{

void TcpSocket::accept(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

long TcpSocket::bind(dword address, dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TcpSocket::close(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TcpSocket::connect(dword address, dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TcpSocket::getLastSocketError(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TcpSocket::getLastSocketErrorAsString(char * text, dword count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TcpSocket::listen(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

void TcpSocket::open(dword address, dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

long TcpSocket::receive(char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TcpSocket::send(char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TcpSocket::setSocketOption(long level, long name, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TcpSocket::shutdown(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
