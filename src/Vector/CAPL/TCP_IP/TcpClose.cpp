#include "TcpClose.h"

#include <iostream>

namespace capl
{

long TcpClose(dword socket)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
