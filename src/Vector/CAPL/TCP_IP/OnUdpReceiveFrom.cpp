#include "OnUdpReceiveFrom.h"

#include <iostream>

namespace capl
{

void OnUdpReceiveFrom(dword socket, long result, dword address, dword port, char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
