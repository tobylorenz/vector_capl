#include "TcpGetRemoteAddress.h"

#include <iostream>

namespace capl
{

dword TcpGetRemoteAddress(dword socket)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
