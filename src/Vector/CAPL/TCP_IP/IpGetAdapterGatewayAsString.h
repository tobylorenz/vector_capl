#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Retrieves the string representation of the default gateway address associated with a specified network interface.
 *
 * The function retrieves the string representation of the default gateway address
 * associated with the specified network interface.
 *
 * @param index
 *   The 1-based network interface index.
 *
 * @param address
 *   The buffer used to store the gateway address string in dot notation.
 *
 * @param count
 *   The size of the address buffer.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - ERROR_NOT_ENOUGH_MEMORY (8): The address buffer was insufficient.
 *   - WSA_INVALID_PARAMETER (87): The specified network interface index was invalid.
 */
long IpGetAdapterGatewayAsString(dword index, char * address, dword count);

}
