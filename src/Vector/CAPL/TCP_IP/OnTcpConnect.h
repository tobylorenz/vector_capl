#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   It is dispatched when an asynchronous connection operation completes.
 *
 * Provided the CAPL program implements this callback it is dispatched when an
 * asynchronous connection operation completes.
 *
 * @param socket
 *   The socket handle.
 *
 * @param result
 *   The specific result code of the operation. If the operation completed successfully the
 *   value is zero. Otherwise the value is non-zero.
 */
void OnTcpConnect(dword socket, long result);

}
