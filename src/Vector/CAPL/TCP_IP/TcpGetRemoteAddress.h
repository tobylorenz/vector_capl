#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Retrieves the remote address of the specified socket.
 *
 * This function retrieves the remote address of the specified socket.
 *
 * @param socket
 *   The socket handle.
 *
 * @return
 *   The address of the remote host in network-byte order.
 */
dword TcpGetRemoteAddress(dword socket);

}
