#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Creates a TCP socket for use in connection-based, message-oriented communications.
 *
 * The function creates a TCP socket for use in connection-based, message-oriented
 * communications. All parameters may be zero. If the port parameter is non-zero the
 * socket is implicitly bound to the given port.
 *
 * @param address
 *   The local address in network-byte order to be used with the socket.
 *
 * @param port
 *   The port in host-byte order to be used with the socket.
 *
 * @return
 *   - INVALID_SOCKET (~0): The function failed. Call IpGetLastError to get a more specific error
 *     code.
 *   - Any other value: A valid socket handle identifying the created socket.
 */
dword TcpOpen(dword address, dword port);

}
