#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Retrieves the default gateway address associated with a specified network interface.
 *
 * The function retrieves the default gateway address associated with the specified network
 * interface.
 *
 * @param index
 *   The 1-based network interface index.
 *
 * @param address
 *   The array used to store the gateway addresses in network-byte order.
 *
 * @param count
 *   The size of the address array.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - ERROR_NOT_ENOUGH_MEMORY (8): The address array was insufficient.
 *   - WSA_INVALID_PARAMETER (87): The specified network interface index was invalid.
 */
long IpGetAdapterGateway(dword index, dword * address, dword count);

}
