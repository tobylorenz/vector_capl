#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Retrieves the string representation of the first address mask associated with a specified network interface.
 *
 * The function retrieves the string representation of the first address mask associated with
 * the specified network interface.
 *
 * @param index
 *   The 1-based network interface index.
 *
 * @param mask
 *   The buffer used to store the address mask string in dot notation.
 *
 * @param count
 *   The size of the mask buffer.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - ERROR_NOT_ENOUGH_MEMORY (8): The mask buffer was insufficient.
 *   - WSA_INVALID_PARAMETER (87): The specified network interface index was invalid.
 */
long IpGetAdapterMaskAsString(dword index, char * mask, dword count);

}
