#include "IpGetLastSocketErrorAsString.h"

#include <iostream>

namespace capl
{

long IpGetLastSocketErrorAsString(dword socket, char * text, dword count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
