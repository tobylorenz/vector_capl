#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Retrieves the remote address of the specified socket in Internet standard dotted-decimal format.
 *
 * This function retrieves the remote address of the specified socket in Internet standard
 * dotted-decimal format.
 *
 * @param socket
 *   The socket handle.
 *
 * @param buffer
 *   The buffer used to store the converted address.
 *
 * @param size
 *   The size of the address buffer.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - 1: The provided buffer is too small.
 *   - 2: Anything else failed (e.g. wrong socket parameter).
 */
long TcpGetRemoteAddressAsString(dword socket, char * buffer, dword size);

}
