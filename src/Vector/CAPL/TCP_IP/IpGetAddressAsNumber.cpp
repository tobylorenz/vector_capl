#include "IpGetAddressAsNumber.h"

#include <iostream>

namespace capl
{

dword IpGetAddressAsNumber(char * address)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
