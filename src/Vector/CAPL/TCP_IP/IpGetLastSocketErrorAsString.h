#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Retrieves the error message of the last operation that failed on a specified socket.
 *
 * The function retrieves the error message of the last operation that failed on the specified
 * socket (see Winsock 2 error code).
 *
 * @param socket
 *   The socket handle.
 *
 * @param text
 *   The buffer used to store the error message.
 *
 * @param count
 *   The size of the text buffer.
 *
 * @return
 *   - 0: The error message was written into the text buffer. In case of an invalid error code,
 *        the error message has the format "Unknown error: x" assuming the last error code x for
 *        the specified socket.
 *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
 */
long IpGetLastSocketErrorAsString(dword socket, char * text, dword count);

}
