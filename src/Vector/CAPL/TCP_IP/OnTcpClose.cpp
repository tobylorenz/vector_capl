#include "OnTcpClose.h"

#include <iostream>

namespace capl
{

void OnTcpClose(dword socket, long result)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
