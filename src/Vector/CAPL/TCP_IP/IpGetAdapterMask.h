#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Retrieves the address masks associated with a specified network interface.
 *
 * The function retrieves the address masks (subnet masks) associated with the specified
 * network interface.
 *
 * @param index
 *   The 1-based network interface index.
 *
 * @param mask
 *   The array used to store the address masks in network-byte order.
 *
 * @param count
 *   The size of the mask array.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - ERROR_NOT_ENOUGH_MEMORY (8): The mask array was insufficient.
 *   - WSA_INVALID_PARAMETER (87): The specified network interface index was invalid.
 */
long IpGetAdapterMask(dword index, dword * mask, dword count);

}
