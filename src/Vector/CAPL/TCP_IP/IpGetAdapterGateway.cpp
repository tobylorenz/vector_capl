#include "IpGetAdapterGateway.h"

#include <iostream>

namespace capl
{

long IpGetAdapterGateway(dword index, dword * address, dword count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
