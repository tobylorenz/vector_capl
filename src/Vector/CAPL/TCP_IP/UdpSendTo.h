#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Sends data to a specified location.
 *
 * The function sends data to the specified location. If the send operation doesn't complete
 * immediately the operation is performed asynchronously and the function will return
 * SOCKET_ERROR (-1). Use IpGetLastSocketError to get a more specific error code. If the
 * specific error code is WSA_IO_PENDING (997) the CAPL callback OnUdpSendTo will be
 * called on completion (successful or not), provided it is implemented in the same CAPL
 * program.
 *
 * @param socket
 *   The socket handle.
 *
 * @param address
 *   The address of the destination in network-byte order.
 *
 * @param port
 *   The port of the destination in host-byte order.
 *
 * @param buffer
 *   The buffer containing the data to be sent.
 *
 * @param size
 *   The size of the data to be sent.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
 *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastSocketError to get a more specific
 *     error code.
 */
long UdpSendTo(dword socket, dword address, dword port, char * buffer, dword size);

}
