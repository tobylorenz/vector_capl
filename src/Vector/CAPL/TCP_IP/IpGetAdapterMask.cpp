#include "IpGetAdapterMask.h"

#include <iostream>

namespace capl
{

long IpGetAdapterMask(dword index, dword * mask, dword count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
