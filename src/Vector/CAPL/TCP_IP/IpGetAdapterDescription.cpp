#include "IpGetAdapterDescription.h"

#include <iostream>

namespace capl
{

long IpGetAdapterDescription(dword index, char * name, dword count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
