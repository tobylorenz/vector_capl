#include "TcpAccept.h"

#include <iostream>

namespace capl
{

dword TcpAccept(dword socket)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
