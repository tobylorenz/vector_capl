#include "TcpGetRemoteAddressAsString.h"

#include <iostream>

namespace capl
{

long TcpGetRemoteAddressAsString(dword socket, char * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
