#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TCP_IP
 *
 * @brief
 *   Associates an address and a port with a specified socket.
 *
 * The function associates an address and a port with the specified socket.
 *
 * @param socket
 *   The socket handle.
 *
 * @param address
 *   The local address in network-byte order.
 *
 * @param port
 *   The local port in host-byte order.
 *
 * @return
 *   - 0: The function completed successfully.
 *   - WSA_INVALID_PARAMETER (87): The specified socket was invalid.
 *   - SOCKET_ERROR (-1): The function failed. Call IpGetLastError to get a more specific error code.
 */
long IpBind(dword socket, dword address, dword port);

}
