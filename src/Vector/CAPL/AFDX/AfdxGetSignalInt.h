#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX integer (32 bit) signal.
 *
 * This function gets the content of an AFDX integer signal (32 bit) within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param offset
 *   offset in byte within the payload according to SignalAddress of Interface Control DocumentICD, no DBC required
 *
 * @return
 *   read value as integer (32 bit) or 0 if error (diagnostics with AfdxGetLastError)
 */
long AfdxGetSignalInt(long packet, ulong offset);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX integer (32 bit) signal.
 *
 * This function gets the content of an AFDX integer signal (32 bit) within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @return
 *   read value as integer (32 bit) or 0 if error (diagnostics with AfdxGetLastError)
 */
long AfdxGetSignalInt(long packet, const char * sigName);

}
