#include "AfdxSetSignalReal.h"

#include <iostream>

namespace capl
{

long AfdxSetSignalReal(long packet, ulong offset, long isDouble, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalReal(long packet, const char * sigName, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalReal(long packet, const char * sigName, double value, long fdsStatus)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
