#include "AfdxSetTokenInt64.h"

#include <iostream>

namespace capl
{

long AfdxSetTokenInt64(long packet, char * protocolDesignator, char * tokenDesignator, int64 value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetTokenInt64(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, int64 length, long networkByteOrder, int64 value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
