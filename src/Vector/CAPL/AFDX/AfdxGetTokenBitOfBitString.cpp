#include "AfdxGetTokenBitOfBitString.h"

#include <iostream>

namespace capl
{

long AfdxGetTokenBitOfBitString(long packet, char * protocolDesignator, char * tokenDesignator, char * namedBit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxGetTokenBitOfBitString(long packet, char * protocolDesignator, char * tokenDesignator, long bitPosition)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
