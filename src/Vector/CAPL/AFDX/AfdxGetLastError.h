#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the last error code.
 *
 * Returns the error code of the last called Afdx... function.
 *
 * @return
 *   error code
 */
long AfdxGetLastError(void);

}
