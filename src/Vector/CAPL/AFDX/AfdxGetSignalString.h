#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX string signal.
 *
 * This function gets the content of an AFDX string signal within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param offset
 *   offset in byte within the payload according to SignalAddress of Interface Control DocumentICD, no DBC required
 *
 * @param bufSize
 *   size of the user defined buffer
 *
 * @param buffer
 *   buffer containing the read string value
 *
 * @return
 *   length of the read string, values > bufSize are error codes
 */
long AfdxGetSignalString(long packet, ulong offset, ulong bufSize, char * buffer);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX string signal.
 *
 * This function gets the content of an AFDX string signal within a packet, with or without DBC information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @param bufSize
 *   size of the user defined buffer
 *
 * @param buffer
 *   buffer containing the read string value
 *
 * @return
 *   length of the read string, values > bufSize are error codes
 */
long AfdxGetSignalString(long packet, const char * sigName, ulong bufSize, char * buffer);

}
