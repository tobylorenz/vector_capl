#include "AfdxGetMessageName.h"

#include <iostream>

namespace capl
{

long AfdxGetMessageName(long packet, long bufferSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
