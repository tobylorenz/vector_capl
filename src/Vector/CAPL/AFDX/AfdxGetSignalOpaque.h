#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX opaque signal.
 *
 * This function gets the content of an AFDX opaque signal within a packet, with or without DBC information.
 * The length of the data set is derived from the first two byte of the signal, the data set itself does not contain the length information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param offset
 *   offset in byte within the payload according to SignalAddress of Interface Control DocumentICD, no DBC required
 *
 * @param bufSize
 *   size of the user defined buffer
 *
 * @param buffer
 *   buffer containing the read data
 *
 * @return
 *   length of the read data set, values > bufSize are error codes
 */
long AfdxGetSignalOpaque(long packet, ulong offset, ulong bufSize, char * buffer);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX opaque signal.
 *
 * This function gets the content of an AFDX opaque signal within a packet, with or without DBC information.
 * The length of the data set is derived from the first two byte of the signal, the data set itself does not contain the length information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param offset
 *   offset in byte within the payload according to SignalAddress of Interface Control DocumentICD, no DBC required
 *
 * @param numBytes
 *   length of fixed length opaque signal in bytes respective number of bytes to read
 *
 * @param bufSize
 *   size of the user defined buffer
 *
 * @param buffer
 *   buffer containing the read data
 *
 * @return
 *   length of the read data set, values > bufSize are error codes
 */
long AfdxGetSignalOpaque(long packet, ulong offset, ulong numBytes, ulong bufSize, char * buffer);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the content of an AFDX opaque signal.
 *
 * This function gets the content of an AFDX opaque signal within a packet, with or without DBC information.
 * The length of the data set is derived from the first two byte of the signal, the data set itself does not contain the length information.
 *
 * @param packet
 *   handle of a received or generated packet
 *
 * @param sigName
 *   signal name according to DBC
 *
 * @param bufSize
 *   size of the user defined buffer
 *
 * @param buffer
 *   buffer containing the read data
 *
 * @return
 *   length of the read data set, values > bufSize are error codes
 */
long AfdxGetSignalOpaque(long packet, const char * sigName, ulong bufSize, char * buffer);

}
