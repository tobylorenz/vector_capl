cmake_minimum_required(VERSION 2.8)

set(function_group AFDX)

set(source_files
  AfdxAddToken.cpp
  AfdxCompletePacket.cpp
  AfdxDeregisterReceiveCallback.cpp
  AfdxGetLastError.cpp
  AfdxGetLastErrorText.cpp
  AfdxGetMessageId.cpp
  AfdxGetMessageName.cpp
  AfdxGetSignalBool.cpp
  AfdxGetSignalInt64.cpp
  AfdxGetSignalInt.cpp
  AfdxGetSignalOpaque.cpp
  AfdxGetSignalReal.cpp
  AfdxGetSignalStatus.cpp
  AfdxGetSignalString.cpp
  AfdxGetTokenBitOfBitString.cpp
  AfdxGetTokenData.cpp
  AfdxGetTokenInt64.cpp
  AfdxGetTokenInt.cpp
  AfdxGetTokenLengthBit.cpp
  AfdxGetTokenReal.cpp
  AfdxGetTokenString.cpp
  AfdxGetTokenSubString.cpp
  AfdxInitPacket.cpp
  AfdxInitProtocol.cpp
  AfdxIsPacketValid.cpp
  AfdxIsReceiveCallbackRegistered.cpp
  AfdxIsTokenAvailable.cpp
  AfdxOutputPacket.cpp
  AfdxOutputPacketRaw.cpp
  AfdxRegisterReceiveCallback.cpp
  AfdxReleasePacket.cpp
  AfdxRemoveToken.cpp
  AfdxResizeToken.cpp
  AfdxSetSignalBool.cpp
  AfdxSetSignalInt64.cpp
  AfdxSetSignalInt.cpp
  AfdxSetSignalOpaque.cpp
  AfdxSetSignalReal.cpp
  AfdxSetSignalStatus.cpp
  AfdxSetSignalString.cpp
  AfdxSetTokenBitOfBitString.cpp
  AfdxSetTokenData.cpp
  AfdxSetTokenInt64.cpp
  AfdxSetTokenInt.cpp
  AfdxSetTokenReal.cpp
  AfdxSetTokenString.cpp
  AfdxSetVerbosity.cpp
  OnAfdxPacket.cpp)

set(header_files
  AfdxAddToken.h
  AfdxCompletePacket.h
  AfdxDeregisterReceiveCallback.h
  AfdxGetLastError.h
  AfdxGetLastErrorText.h
  AfdxGetMessageId.h
  AfdxGetMessageName.h
  AfdxGetSignalBool.h
  AfdxGetSignalInt64.h
  AfdxGetSignalInt.h
  AfdxGetSignalOpaque.h
  AfdxGetSignalReal.h
  AfdxGetSignalStatus.h
  AfdxGetSignalString.h
  AfdxGetTokenBitOfBitString.h
  AfdxGetTokenData.h
  AfdxGetTokenInt64.h
  AfdxGetTokenInt.h
  AfdxGetTokenLengthBit.h
  AfdxGetTokenReal.h
  AfdxGetTokenString.h
  AfdxGetTokenSubString.h
  AFDX.h
  AfdxInitPacket.h
  AfdxInitProtocol.h
  AfdxIsPacketValid.h
  AfdxIsReceiveCallbackRegistered.h
  AfdxIsTokenAvailable.h
  AfdxOutputPacket.h
  AfdxOutputPacketRaw.h
  AfdxRegisterReceiveCallback.h
  AfdxReleasePacket.h
  AfdxRemoveToken.h
  AfdxResizeToken.h
  AfdxSetSignalBool.h
  AfdxSetSignalInt64.h
  AfdxSetSignalInt.h
  AfdxSetSignalOpaque.h
  AfdxSetSignalReal.h
  AfdxSetSignalStatus.h
  AfdxSetSignalString.h
  AfdxSetTokenBitOfBitString.h
  AfdxSetTokenData.h
  AfdxSetTokenInt64.h
  AfdxSetTokenInt.h
  AfdxSetTokenReal.h
  AfdxSetTokenString.h
  AfdxSetVerbosity.h
  OnAfdxPacket.h)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

add_library(${PROJECT_NAME}_${function_group} OBJECT ${source_files} ${header_files})

if(OPTION_USE_GCOV)
  target_link_libraries(${PROJECT_NAME}_${function_group} gcov)
  add_definitions(-g -O0 -fprofile-arcs -ftest-coverage)
endif()

if(CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif()

install(
  FILES ${header_files}
  DESTINATION include/Vector/CAPL/${function_group})
