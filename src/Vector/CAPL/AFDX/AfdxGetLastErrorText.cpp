#include "AfdxGetLastErrorText.h"

#include <iostream>

namespace capl
{

long AfdxGetLastErrorText(dword bufferSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
