#include "AfdxGetSignalBool.h"

#include <iostream>

namespace capl
{

long AfdxGetSignalBool(long packet, ulong offset, long bitPos)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxGetSignalBool(long packet, const char * sigName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
