#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Determines if a CAPL callback function with this name is installed.
 *
 * Use this function to determine if a CAPL callback function with this name is installed.
 *
 * @param onPacketCallback
 *   name of CAPL callback function to check
 *
 * @return
 *   - 0: not installed
 *   - 1: installed
 *   - or error code
 */
long AfdxIsReceiveCallbackRegistered(char * onPacketCallback);

}
