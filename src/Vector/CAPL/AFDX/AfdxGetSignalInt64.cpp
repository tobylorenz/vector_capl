#include "AfdxGetSignalInt64.h"

#include <iostream>

namespace capl
{

long AfdxGetSignalInt64(long packet, ulong offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxGetSignalInt64(long packet, const char * sigName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
