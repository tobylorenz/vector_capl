#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Registers a CAPL callback function to receive AFDX packets.
 *
 * Use this function to register a CAPL callback function to receive AFDX packets.
 *
 * A AfdxRegisterReceiveCallback( char onPacketCallback[] ) call is equivalent to a AfdxRegisterReceiveCallback( char onPacketCallback[], BothDirections, DroppedAndNotDropped, ValidPackets ) call.
 *
 * @param onPacketCallback
 *   name of CAPL callback function
 *
 * @return
 *   0 or error code
 */
long AfdxRegisterReceiveCallback(char * onPacketCallback);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Registers a CAPL callback function to receive AFDX packets.
 *
 * Use this function to register a CAPL callback function to receive AFDX packets.
 *
 * A AfdxRegisterReceiveCallback( char onPacketCallback[] ) call is equivalent to a AfdxRegisterReceiveCallback( char onPacketCallback[], BothDirections, DroppedAndNotDropped, ValidPackets ) call.
 *
 * @param onPacketCallback
 *   name of CAPL callback function
 *
 * @param direction
 *   specifies the direction for the packet or fragment, the callback will be called
 *   - 0: RxDirection
 *   - 1: TxDirection
 *   - 2: BothDirections
 *
 * @param dropped
 *   specifies if the callback shall be called for dropped packets or fragments
 *   - 0: Dropped
 *   - 1: NotDropped
 *   - 2: DroppedAndNotDropped
 *
 * @param type
 *   specifies if the callback shall be called for every kind of frame or just for complete and error free IP fragments or packets.
 *   - 0: ValidPackets
 *   - 1: ValidFragments
 *   - 2: EveryFrameAndPacket
 *
 * @return
 *   0 or error code
 */
long AfdxRegisterReceiveCallback(char * onPacketCallback, long direction, long dropped, long type);

}
