#include "AfdxSetSignalString.h"

#include <iostream>

namespace capl
{

long AfdxSetSignalString(long packet, ulong offset, const char * value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalString(long packet, const char * sigName, const char * value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalString(long packet, const char * sigName, const char * value, long fdsStatus)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
