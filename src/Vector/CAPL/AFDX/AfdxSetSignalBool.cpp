#include "AfdxSetSignalBool.h"

#include <iostream>

namespace capl
{

long AfdxSetSignalBool(long packet, ulong offset, long bitPos, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalBool(long packet, const char * sigName, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalBool(long packet, const char * sigName, long value, long fdsStatus)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
