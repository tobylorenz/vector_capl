#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Deletes an AFDX packet.
 *
 * The function deletes a packet created with AfdxInitPacket. The handle can not be used any longer.
 *
 * @param packet
 *   handle of the packet to delete
 *
 * @return
 *   0 or error code
 */
long AfdxReleasePacket(long packet);

}
