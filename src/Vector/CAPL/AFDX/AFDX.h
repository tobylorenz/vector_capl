#pragma once

/**
 * @defgroup AFDX AFDX CAPL Functions
 */

/* Callback Functions */
#include "OnAfdxPacket.h"

/* General Functions */
#include "AfdxGetLastError.h"
#include "AfdxGetLastErrorText.h"
#include "AfdxSetVerbosity.h"

/* Packet API */
#include "AfdxInitPacket.h"
#include "AfdxCompletePacket.h"
#include "AfdxOutputPacket.h"
#include "AfdxReleasePacket.h"
#include "AfdxRegisterReceiveCallback.h"
#include "AfdxDeregisterReceiveCallback.h"
#include "AfdxIsReceiveCallbackRegistered.h"
#include "AfdxIsPacketValid.h"
#include "AfdxIsTokenAvailable.h"
#include "AfdxInitProtocol.h"
#include "AfdxAddToken.h"
#include "AfdxRemoveToken.h"
#include "AfdxResizeToken.h"
#include "AfdxGetTokenBitOfBitString.h"
#include "AfdxGetTokenData.h"
#include "AfdxGetTokenInt.h"
#include "AfdxGetTokenInt64.h"
#include "AfdxGetTokenLengthBit.h"
#include "AfdxGetTokenReal.h"
#include "AfdxGetTokenString.h"
#include "AfdxGetTokenSubString.h"
#include "AfdxOutputPacketRaw.h"
#include "AfdxSetTokenBitOfBitString.h"
#include "AfdxSetTokenData.h"
#include "AfdxSetTokenInt.h"
#include "AfdxSetTokenInt64.h"
#include "AfdxSetTokenReal.h"
#include "AfdxSetTokenString.h"

/* Signal API */
#include "AfdxGetMessageId.h"
#include "AfdxGetMessageName.h"
#include "AfdxGetSignalBool.h"
#include "AfdxGetSignalInt.h"
#include "AfdxGetSignalInt64.h"
#include "AfdxGetSignalOpaque.h"
#include "AfdxGetSignalReal.h"
#include "AfdxGetSignalStatus.h"
#include "AfdxGetSignalString.h"
#include "AfdxSetSignalBool.h"
#include "AfdxSetSignalInt.h"
#include "AfdxSetSignalInt64.h"
#include "AfdxSetSignalOpaque.h"
#include "AfdxSetSignalReal.h"
#include "AfdxSetSignalStatus.h"
#include "AfdxSetSignalString.h"
