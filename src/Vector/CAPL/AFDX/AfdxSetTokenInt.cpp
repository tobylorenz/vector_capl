#include "AfdxSetTokenInt.h"

#include <iostream>

namespace capl
{

long AfdxSetTokenInt(long packet, char * protocolDesignator, char * tokenDesignator, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetTokenInt(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, long networkByteOrder, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
