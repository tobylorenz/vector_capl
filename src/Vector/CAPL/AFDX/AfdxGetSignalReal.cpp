#include "AfdxGetSignalReal.h"

#include <iostream>

namespace capl
{

long AfdxGetSignalReal(long packet, ulong offset, long isDouble)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxGetSignalReal(long packet, const char * sigName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
