#include "AfdxSetSignalInt.h"

#include <iostream>

namespace capl
{

long AfdxSetSignalInt(long packet, ulong offset, int32 value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalInt(long packet, const char * sigName, int32 value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxSetSignalInt(long packet, const char * sigName, int32 value, long fdsStatus)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}


}
