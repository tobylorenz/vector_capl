#include "AfdxOutputPacketRaw.h"

#include <iostream>

namespace capl
{

long AfdxOutputPacketRaw(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
