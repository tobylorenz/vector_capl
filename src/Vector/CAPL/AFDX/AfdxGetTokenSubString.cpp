#include "AfdxGetTokenSubString.h"

#include <iostream>

namespace capl
{

long AfdxGetTokenSubString(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
