#include "AfdxOutputPacket.h"

#include <iostream>

namespace capl
{

long AfdxOutputPacket(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxOutputPacket(long packet, long cyclicTransmission, long ipFragmentation, long vlScheduling, long seqNoManagement, long redundancyManagement)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
