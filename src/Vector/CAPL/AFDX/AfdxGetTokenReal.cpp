#include "AfdxGetTokenReal.h"

#include <iostream>

namespace capl
{

long AfdxGetTokenReal(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
