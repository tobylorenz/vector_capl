#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   CAPL callback to receive data of AFDX packets.
 *
 * The function is called when a registered AFDX packet is received.
 *
 * @param dir
 *   direction of the packet or fragment
 *   - 0: Rx
 *   - 1: Tx
 *
 * @param line
 *   line of the packet or fragment
 *   - 0: LineUndefined (e.g. for reassembled packets)
 *   - 1: LineA
 *   - 2: LineB
 *   - 3: LineAB
 *
 * @param timestamp
 *   timestamp of the packet or fragment in ns
 *
 * @param bag
 *   time difference since last packet of this Virtual Link in ms
 *
 * @param afdxFlags
 *   status info
 *     bit 0: this packet was received from Line-B
 *     bit 1: this packet is redundant
 *     bit 2: this packet is fragmented
 *     bit 3: this packet is from a SAP port
 *   error info
 *     bit 4: packet was received in wrong interface (Line-A versus Line-B)
 *     bit 5: packet has an invalid Ethernet frame content
 *     bit 6: packet has an invalid sequence number
 *     bit 7: packet has a time-out on redundancy reception
 *     bit 8: packet had encountered a fragmentation error
 *     bit 9: packet has protocol errors on higher protocol level
 *     bit 10: packet has been reassembled
 *
 * @param packet
 *   handle of the received packet or fragment
 */
void OnAfdxPacket(long dir, long line, int64 timestamp, long bag, long afdxFlags, long packet);

}
