#include "AfdxSetTokenReal.h"

#include <iostream>

namespace capl
{

long AfdxSetTokenReal(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
