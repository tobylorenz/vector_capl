#pragma once

#include "../DataTypes.h"

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Adds a token at the end of a protocol header.
 *
 * The function adds an additional token at the end of a protocol header
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol
 *
 * @param tokenDesignator
 *   name of the token
 *
 * @return
 *   0 or error code
 */
long VECTOR_CAPL_EXPORT AfdxAddToken(long packet, char * protocolDesignator, char * tokenDesignator);

}
