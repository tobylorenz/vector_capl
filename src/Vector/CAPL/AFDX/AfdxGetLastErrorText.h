#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the description of the last error code.
 *
 * Gets the error code description of the last called Afdx... function.
 *
 * @param bufferSize
 *   size of buffer in which the description text is copied
 *
 * @param buffer
 *   buffer in which the description text is copied
 *
 * @return
 *   number of copied bytes
 */
long AfdxGetLastErrorText(dword bufferSize, char * buffer);

}
