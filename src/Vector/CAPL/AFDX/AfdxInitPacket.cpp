#include "AfdxInitPacket.h"

#include <iostream>

namespace capl
{

long AfdxInitPacket(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxInitPacket(char * protocolDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxInitPacket(char * protocolDesignator, char * packetTypeDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxInitPacket(long packetToCopy)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxInitPacket(long rawDataLength, byte * rawData)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxInitPacket(long rawDataLength, char * rawData)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxInitPacket(long rawDataLength, void * * rawDataStruct)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxInitPacket(long srcIP, long dstIP, long srcUdpPort, long dstUdpPort, long virtualLinkId, long payloadSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long AfdxInitPacket(long msgID, char * msgName, long initSignals)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
