#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Sets the verbosity level of the AFDX IL.
 *
 * This function set the verbosity level of the AFDX IL.
 *
 * The default setting is that only error messages are written to the write window of CANoe.
 *
 * @param verbosity
 *   verbosity level
 *   - 0 - do not write messages to the write window
 *   - 1 - write only error messages (default)
 *   - 2 - write warning and error messages
 *   - 3 - write information, warning and error messages
 *
 * @return
 *   0 or error code
 */
long AfdxSetVerbosity(long verbosity);

}
