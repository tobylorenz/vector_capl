#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the value of a bit in a bit string.
 *
 * This function gets the value of a bit in a bit string that is specified either by its name (1) or by its position (2).
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param namedBit
 *   name of the bit
 *
 * @return
 *   value of the bit
 */
long AfdxGetTokenBitOfBitString(long packet, char * protocolDesignator, char * tokenDesignator, char * namedBit);

/**
 * @ingroup AFDX
 *
 * @brief
 *   Gets the value of a bit in a bit string.
 *
 * This function gets the value of a bit in a bit string that is specified either by its name (1) or by its position (2).
 *
 * @param packet
 *   handle of a packet that has been created with AfdxInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "afdx"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param bitPosition
 *   zero based index of the bit
 *
 * @return
 *   value of the bit
 */
long AfdxGetTokenBitOfBitString(long packet, char * protocolDesignator, char * tokenDesignator, long bitPosition);

}
