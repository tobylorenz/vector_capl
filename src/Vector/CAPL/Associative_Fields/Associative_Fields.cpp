#include "Associative_Fields.h"

namespace capl
{

void Associative_Fields::clear(void)
{
    keys.clear();
}

int Associative_Fields::containsKey(unsigned int key)
{
    return keys.count(key);
}

int Associative_Fields::remove(unsigned int key)
{
    return keys.erase(key);
}

int Associative_Fields::size(void)
{
    return keys.size();
}

}
