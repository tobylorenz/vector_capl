#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Stops the macro from playing.
 *
 * Stops the macro from playing with the handle handle.
 *
 * @param handle
 *   Handle of the started macro.
 *   The handle is returned from the StartMacroFile function when starting the macro.
 */
void StopMacroFile(dword handle);

}
