#include "setTimer.h"

#include <iostream>

namespace capl
{

void setTimer(msTimer t, long duration)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void setTimer(timer t, long duration)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void setTimer(timer t, long durationSec, long durationNanoSec)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
