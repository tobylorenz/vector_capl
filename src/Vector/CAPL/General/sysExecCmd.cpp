#include "sysExecCmd.h"

#include <iostream>

namespace capl
{

long sysExecCmd(char * cmd, char * params)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysExecCmd(char * cmd, char * params, char * directory)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
