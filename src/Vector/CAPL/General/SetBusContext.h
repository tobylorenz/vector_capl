#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the bus context of the CAPL block.
 *
 * Sets the bus context of the CAPL block.
 *
 * @param context
 *   The new bus context to be set.
 *
 * @return
 *   The bus context that was valid before the call was made is returned.
 */
dword SetBusContext(dword context);

}
