#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates a random number.
 *
 * Calculates a random number between 0 and x-1.
 *
 * @param x
 *   Maximum value to be returned by the function.
 *
 * @return
 *   Random number between 0 and x-1.
 */
dword random(dword x);

}
