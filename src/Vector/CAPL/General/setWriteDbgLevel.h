#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the priority level for the writeDbgLevel CAPL function.
 *
 * This function sets the priority level for the writeDbgLevel CAPL function. The output
 * priority must be set for every network node.
 *
 * @param priority
 *   Priority of current CAPL node for output to the Write window.
 *   Rays for priority: 0 to 15
 *   - 0: Only write output with a priority of 0 are shown in the write window.
 *   - 5: Write output with a priority ranging from 0 to 5 are shown.
 *   - 15: All outputs are shown.
 */
void setWriteDbgLevel(unsigned int priority);

}
