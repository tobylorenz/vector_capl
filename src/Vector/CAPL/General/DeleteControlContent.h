#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 */
void DeleteControlContent(char * panel, char * control);

}
