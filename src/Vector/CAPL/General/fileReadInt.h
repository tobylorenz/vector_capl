#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Searches for a variable entry in a file section.
 *
 * @deprecated
 *   Replaced by getProfileInt
 *
 * Searches for the variable entry in the section section of the file filename.
 * If its value is a number, this number is returned as the functional result.
 * If the file or entry is not found, or if entry does not contain a valid number,
 * the default value def is returned as the functional result.
 *
 * @param section
 *   Section of file
 *
 * @param entry
 *   Name of variable
 *
 * @param def
 *   Value
 *
 * @param file
 *   Name of file
 *
 * @return
 *   Integer read
 */
long fileReadInt(char * section, char * entry, long def, char * file);

}
