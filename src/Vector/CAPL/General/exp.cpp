#include "exp.h"

#include <complex>

namespace capl
{

double exp(double x)
{
    return std::exp(x);
}

}
