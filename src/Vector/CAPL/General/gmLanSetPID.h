#pragma once

#include "../DataTypes.h"
#include "../GMLAN/GMLAN.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the parameter ID of the message.
 *
 * gmLanSetPID sets the parameter ID of the message.
 *
 * @param msg
 *   Message
 *
 * @param v
 *   Parameter ID, source address or priority
 */
void gmLanSetPID(gmLanMessage msg, long v);

}
