#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Supplies the current simulation time [nanoseconds].
 *
 * Supplies the current simulation time.
 *
 * The simulation time can be correlated with the hardware results of the interface cards
 * (e.g. CANcardXL).
 * The resolution of this time is dependent upon the hardware used (usually a millisecond or
 * better).
 *
 * Depending on the hardware configuration, the simulation time
 * - will be the same as the message time calculated by the interface cards
 *   (e.g., system with two CAN channels connected to one CANcardXL)
 * or
 * - the message times will have a higher accuracy
 *   (e.g., system with one LIN channel connected to one LINda)
 *
 * @return
 *   Simulation time in 10 microseconds.
 */
double TimeNowNS(void);

}
