#include "envvarData.h"

#include <iostream>

namespace capl
{

envvarData::envvarData() :
    envvar(),
    value(),
    size()
{
}

envvarData::~envvarData()
{
}

size_t envvarData::valueSize()
{
    return size;
}

}
