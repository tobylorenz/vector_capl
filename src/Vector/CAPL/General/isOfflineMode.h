#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Gets the information if CANoe/CANalyzer is in offline mode.
 *
 * This function is used to get the information if CANoe/CANalyzer is in offline mode.
 *
 * @return
 *   - 1: True, CANoe / CANalyzer is in offline mode.
 *   - 0: False, CANoe / CANalyzer is in online mode.
 */
long isOfflineMode(void);

}
