#include "callAllOnEnvVar.h"

#include <iostream>

namespace capl
{

void callAllOnEnvVar(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
