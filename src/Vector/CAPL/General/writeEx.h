#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Writes the text into the last line of the specified window or into a tab of the Write window without previously creating a new line.
 *
 * Writes the text into the last line of the specified window, into a tab of the Write window
 * or into a logging file without previously creating a new line.
 *
 * @param sink
 *   Sink identifier of the page to which the output will take place.
 *   Values:
 *   - -3: Trace window
 *   - -2: Output to the logging file (only in ASC format and if the CAPL node is inserted in
 *         the measurement setup in front of the logging block)
 *   - -1: Reserved
 *   - 0: Output to the System tab of the Write window
 *   - 1: Output to the CAPL tab of the Write window
 *   - 4: Output to the Test tab of the Write window
 *
 * @param severity
 *   Constant for the type of message.
 *   Values:
 *   - 0: Success
 *   - 1: Information
 *   - 2: Warning
 *   - 3: Error
 *   The parameter severity has no meaning for output to the Trace window.
 *
 * @param format
 *   Formatting character sequence.
 */
void writeEx(dword sink, dword severity, const char * format, ...);

}
