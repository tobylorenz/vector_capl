#include "startStatisticAcquisition.h"

#include <iostream>

namespace capl
{

void startStatisticAcquisition(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
