#include "StartReplayFile.h"

#include <iostream>

namespace capl
{

dword StartReplayFile(char * fileName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
