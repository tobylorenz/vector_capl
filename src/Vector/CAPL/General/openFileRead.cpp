#include "openFileRead.h"

#include <iostream>

namespace capl
{

dword openFileRead(char * filename, dword mode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
