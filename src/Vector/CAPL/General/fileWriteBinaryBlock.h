#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Writes bytes in the specified file.
 *
 * This function writes buffsize bytes in the specified file.
 *
 * @param buff
 *   Buffer, the characters are read-out
 *
 * @param buffsize
 *   Number of bytes
 *
 * @param fileHandle
 *   Handle to the file
 *
 * @return
 *   The function returns the number of bytes written.
 */
long fileWriteBinaryBlock(byte * buff, long buffsize, dword fileHandle);

}
