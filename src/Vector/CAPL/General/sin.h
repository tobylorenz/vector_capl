#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates the sine.
 *
 * Calculates sine of x.
 *
 * @param x
 *   Value in radians whose sine is to be calculated.
 *
 * @return
 *   Sine of x.
 */
double sin(double x);

}
