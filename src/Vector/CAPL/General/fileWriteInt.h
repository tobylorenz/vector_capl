#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Opens a file, finds a special section and writes the variable entry with a special value.
 *
 * @deprecated
 *   Replaced by writeProfileInt
 *
 * Analogous to fileWriteString, but writes a long variable to the file instead of a text.
 *
 * @param section
 *   Section of file
 *
 * @param entry
 *   Name of variable
 *
 * @param def
 *   Value
 *
 * @param file
 *   Name of file
 *
 * @return
 *   0 if an error has occurred else 1
 */
long fileWriteInt(char * section, char * entry, long def, char * file);

}
