#pragma once

#include "../DataTypes.h"
#include "../CAN/CAN.h"
#include "../J1939/J1939.h"
#include "../MOST/MOST.h"
#include "../GMLAN/GMLAN.h"
#include "../LIN/LIN.h"
#include "../J1587/J1587.h"
#include "../BEAN/BEAN.h"
#include "../FlexRay/FlexRay.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param msg
 *   Message event variable of type "message".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(message msg);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param msg
 *   Message event variable of type "pg".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(pg msg);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param msg
 *   Message event variable of type "gmLanMessage".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(gmLanMessage msg);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param msg
 *   Message event variable of type "linmessage".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(linMessage msg);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param msg
 *   Message event variable of type "beanMessage".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(beanMessage msg);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param msg
 *   Message event variable of type "mostMessage".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(mostMessage msg);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param msg
 *   Message event variable of type "mostAmsMessage".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(mostAMSMessage msg);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param msg
 *   Message event variable of type "mostRawMessage".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(mostRawMessage msg);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param msg
 *   Message event variable of type "j1587Param".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(J1587Param msg);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "linBaudrateEvent".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(linBaudrateEvent event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "linCsError".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(linCsError event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "linDlcinfo".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(linDlcInfo event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "linReceiveError".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(linReceiveError event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "linSchedulerModeChange".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(linSchedulerModeChange event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "linSlaveTimeout".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(linSlaveTimeout event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "linSleepModeEvent".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(linSleepModeEvent event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "linSyncError".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(linSyncError event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "linTransmError".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(linTransmError event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "linWakeupFrame".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(linWakeupFrame event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "beanError".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(beanError event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "mostLightLockError".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(mostLightLockError event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "FRSlot".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(FRSlot event);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param msg
 *   Message event variable of type "FRFrame".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(FRFrame msg);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the Event Sorting state.
 *
 * Determines the Event Sorting state.
 *
 * With active Event Sorting the return value indicates if the given event in the simulation
 * setup of CANoe or transmit branch of CANalyzer was processed in correct time sequence.
 *
 * @param event
 *   Message event variable of type "FrStartCycle".
 *
 * @return
 *   - 0: Invalid state.
 *        The Event Sorting is inactive or it concerns a program internal event that will never be
 *        sorted (e.g. environment variable).
 *   - 1: sorted
 *        The Event Sorting is active and the event was processed and sorted in the correct time
 *        sequence.
 *   - 2: unsorted
 *        The Event Sorting is active but the event arrived too late or too soon so that the event
 *        was processed unsorted.
 */
int GetEventSortingStatus(FRStartCycle event);

}
