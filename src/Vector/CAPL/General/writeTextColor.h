#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the color for text of the specified page in the Write window.
 *
 * Sets the color for text of the specified page in the Write window.
 *
 * You can use the following constants for the target identifier:
 *
 * // write sinks
 * DWORD WRITE_SYSTEM = 0;
 * DWORD WRITE_CAPL = 1;
 *
 * In addition you can use one of the target identifiers returned by the function
 * writeCreate.
 *
 * The color settings have also an effect on the All tab of the Write window.
 *
 * @param sink
 *   The target identifier of the page on which the color settings should have an effect.
 *
 * @param red
 *   Specifies the intensity of the red color.
 *
 * @param green
 *   Specifies the intensity of the green color.
 *
 * @param blue
 *   Specifies the intensity of the blue color.
 */
void writeTextColor(dword sink, dword red, dword green, dword blue);

}
