#pragma once

#include "../DataTypes.h"
#include "General.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Return value indicates whether a specific timer is active.
 *
 * Return value indicates whether a specific timer is active.
 *
 * This is the case between the call to the setTimer function and the call to the on timer
 * event procedure.
 *
 * @param t
 *   timer variable
 *
 * @return
 *   - 1, if the timer is active; otherwise 0.
 *   - 0 is also returned within the on timer event procedure.
 */
int isTimerActive(timer t);

/**
 * @ingroup General
 *
 * @brief
 *   Return value indicates whether a specific timer is active.
 *
 * Return value indicates whether a specific timer is active.
 *
 * This is the case between the call to the setTimer function and the call to the on timer
 * event procedure.
 *
 * @param t
 *   mstimer variable
 *
 * @return
 *   - 1, if the timer is active; otherwise 0.
 *   - 0 is also returned within the on timer event procedure.
 */
int isTimerActive(msTimer t);

}
