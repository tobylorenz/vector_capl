#include "isOfflineMode.h"

#include <iostream>

namespace capl
{

long isOfflineMode(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
