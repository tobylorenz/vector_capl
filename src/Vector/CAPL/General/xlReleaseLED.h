#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Releases specified LEDs.
 *
 * Releases the specified LEDs after they were successfully acquired with a call of
 * xlAcquireLED.
 *
 * This function is supported by the following device: VN8900 (driver version > 7.5)
 *
 * @param ledBitMask
 *   The LEDs you want to release.
 *   The following LEDs are available. Note that you can bitwise combine the values to
 *   specify multiple LEDs.
 *   - 0x001: Module (M)
 *   - 0x008: CAN Channel 1
 *   - 0x010: CAN Channel 2
 *   - 0x020: CAN Channel 3
 *   - 0x040: CAN Channel 4
 *   - 0x080: FlexRay Channel 1 A
 *   - 0x100: FlexRay Channel 1 B
 *   - 0x200: Keypad S1
 *   - 0x400: Keypad S2
 *
 * @return
 *   - 0: no error, function succeeded.
 *   - != 0: error, function failed. Check whether your device supports this function and you
 *     have an appropriate driver installed on the device.
 */
dword xlReleaseLED(dword ledBitMask);

}
