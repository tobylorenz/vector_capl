#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Writes a byte to the specified parallel port.
 *
 * @deprecated
 *   The Parallel Port is only supported with Windows XP 32 bit.
 *   For simple control applications you can use the IOcab, the IOpiggy or a simple measurement hardware, e.g. the Meilhaus-RedLab series or an appropriate NI card.
 *
 * Output of a byte to a port.
 *
 * @param addr
 *   Port address or a predefined LPTx constant.
 *
 * @param value
 *   Output value
 *   Symbolic assignment:
 *   - LPT1 -> 0x378
 *   - LPT2 -> 0x278
 *   - LPT3 -> 0x3BC
 *   The LPT constant could not be used in arithmetic expression!
 */
void outport(word addr, byte value);

}
