#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates the floor of a value.
 *
 * Calculates the floor of a value, i.e. the largest integer smaller or equal to the value.
 *
 * @param x
 *   Value of which the floor shall be calculated.
 *
 * @return
 *   Floor of x.
 */
double _floor(double x);

}
