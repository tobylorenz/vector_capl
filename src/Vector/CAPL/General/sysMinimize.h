#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Minimizes or restores the application window of CANalyzer/CANoe.
 *
 * The application window of CANalyzer/CANoe will be minimized or restored. The first call
 * of the function minimizes the window, afterwards the window will be restored to normal
 * size and minimized alternaltly.
 */
void sysMinimize(void);

}
