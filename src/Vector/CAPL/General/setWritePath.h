#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the write path for the function openFileWrite.
 *
 * This function sets the write path for the functions openFileWrite, writeProfileString,
 * writeProfileInt and writeProfileFloat. The path can be given as absolute or relative to
 * the currently active configuration.
 *
 * @param relativeOrAbsolutePath
 *   The file path as a string.
 */
void setWritePath(char * relativeOrAbsolutePath);

}
