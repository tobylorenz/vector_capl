#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Clears the contents of the specified page in the Write window.
 *
 * Clears the contents of the specified page in the Write window.
 * You can use the following constants for the target identifier or type of message:
 *
 * // write sinks
 * DWORD WRITE_SYSTEM = 0;
 * DWORD WRITE_CAPL = 1;
 *
 * In addition you can use one of the target identifiers returned by the function writeCreate.
 * The All page can't be cleared.
 *
 * @param sink
 *   Target identifier for the page to be deleted.
 */
void writeClear(dword sink);

}
