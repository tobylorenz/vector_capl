#pragma once

#include "../DataTypes.h"
#include "../CAN/CAN.h"
#include "../J1939/J1939.h"
#include "../LIN/LIN.h"
#include "../MOST/MOST.h"
#include "../FlexRay/FlexRay.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the font, and background color for the message display in the Trace window.
 *
 * Sets the text and background color for displaying the message in the trace window. The
 * makeRGB function can be used for the colors.
 *
 * @param msg
 *   Variable type: message
 *
 * @param font
 *   Font color (RGB value)
 *
 * @param bkgnd
 *   Background color (RGB value)
 *
 * @return
 *   - 0: OK
 *   - -1: Invalid RGB value of a color.
 */
void traceSetEventColors(message msg, long font, long bkgnd);

/**
 * @ingroup General
 *
 * @brief
 *   Sets the font, and background color for the message display in the Trace window.
 *
 * Sets the text and background color for displaying the message in the trace window. The
 * makeRGB function can be used for the colors.
 *
 * @param msg
 *   Variable type: errorFrame
 *
 * @param font
 *   Font color (RGB value)
 *
 * @param bkgnd
 *   Background color (RGB value)
 *
 * @return
 *   - 0: OK
 *   - -1: Invalid RGB value of a color.
 */
void traceSetEventColors(errorFrame msg, long font, long bkgnd);

/**
 * @ingroup General
 *
 * @brief
 *   Sets the font, and background color for the message display in the Trace window.
 *
 * Sets the text and background color for displaying the message in the trace window. The
 * makeRGB function can be used for the colors.
 *
 * @param msg
 *   Variable type: pg
 *
 * @param font
 *   Font color (RGB value)
 *
 * @param bkgnd
 *   Background color (RGB value)
 *
 * @return
 *   - 0: OK
 *   - -1: Invalid RGB value of a color.
 */
void traceSetEventColors(pg msg, long font, long bkgnd);

/**
 * @ingroup General
 *
 * @brief
 *   Sets the font, and background color for the message display in the Trace window.
 *
 * Sets the text and background color for displaying the message in the trace window. The
 * makeRGB function can be used for the colors.
 *
 * @param msg
 *   Variable type: linmessage
 *
 * @param font
 *   Font color (RGB value)
 *
 * @param bkgnd
 *   Background color (RGB value)
 *
 * @return
 *   - 0: OK
 *   - -1: Invalid RGB value of a color.
 */
void traceSetEventColors(linMessage msg, long font, long bkgnd);

/**
 * @ingroup General
 *
 * @brief
 *   Sets the font, and background color for the message display in the Trace window.
 *
 * Sets the text and background color for displaying the message in the trace window. The
 * makeRGB function can be used for the colors.
 *
 * @param msg
 *   Variable type: mostRawMessage
 *
 * @param font
 *   Font color (RGB value)
 *
 * @param bkgnd
 *   Background color (RGB value)
 *
 * @return
 *   - 0: OK
 *   - -1: Invalid RGB value of a color.
 */
void traceSetEventColors(mostRawMessage msg, long font, long bkgnd);

/**
 * @ingroup General
 *
 * @brief
 *   Sets the font, and background color for the message display in the Trace window.
 *
 * Sets the text and background color for displaying the message in the trace window. The
 * makeRGB function can be used for the colors.
 *
 * @param msg
 *   Variable type: mostMessage
 *
 * @param font
 *   Font color (RGB value)
 *
 * @param bkgnd
 *   Background color (RGB value)
 *
 * @return
 *   - 0: OK
 *   - -1: Invalid RGB value of a color.
 */
void traceSetEventColors(mostMessage msg, long font, long bkgnd);

/**
 * @ingroup General
 *
 * @brief
 *   Sets the font, and background color for the message display in the Trace window.
 *
 * Sets the text and background color for displaying the message in the trace window. The
 * makeRGB function can be used for the colors.
 *
 * @param msg
 *   Variable type: mostAmsMessage
 *
 * @param font
 *   Font color (RGB value)
 *
 * @param bkgnd
 *   Background color (RGB value)
 *
 * @return
 *   - 0: OK
 *   - -1: Invalid RGB value of a color.
 */
void traceSetEventColors(mostAMSMessage msg, long font, long bkgnd);

/**
 * @ingroup General
 *
 * @brief
 *   Sets the font, and background color for the message display in the Trace window.
 *
 * Sets the text and background color for displaying the message in the trace window. The
 * makeRGB function can be used for the colors.
 *
 * @param msg
 *   Variable type: frFrame
 *
 * @param font
 *   Font color (RGB value)
 *
 * @param bkgnd
 *   Background color (RGB value)
 *
 * @return
 *   - 0: OK
 *   - -1: Invalid RGB value of a color.
 */
void traceSetEventColors(FRFrame msg, long font, long bkgnd);

/**
 * @ingroup General
 *
 * @brief
 *   Sets the font, and background color for the message display in the Trace window.
 *
 * Sets the text and background color for displaying the message in the trace window. The
 * makeRGB function can be used for the colors.
 *
 * @param msg
 *   Variable type: frError
 *
 * @param font
 *   Font color (RGB value)
 *
 * @param bkgnd
 *   Background color (RGB value)
 *
 * @return
 *   - 0: OK
 *   - -1: Invalid RGB value of a color.
 */
void traceSetEventColors(FRError msg, long font, long bkgnd);

}
