#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the transmitted signal to the accompanying value.
 *
 * @deprecated
 *   Replaced by SetSignal
 *
 * Sets the transmitted signal aSignal to the accompanying value.
 *
 * If no suitable signal driver exists and thus no signal can be stimulated,
 * then the verdict of the test module is set to "fail".
 *
 * GM special (several send nodes)
 *
 * @param aSignal
 *   Signal to be set.
 *
 * @param aNode
 *   Send node.
 *
 * @param aValue
 *   Physical value to be accepted.
 */
void SetSignalByTxNode(char * aSignal, char * aNode, double aValue);

}
