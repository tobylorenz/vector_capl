#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Reads a string from the specified file.
 *
 * The function reads a string from the specified file. The returned string contains a new
 * line character. See also fileGetStringSZ.
 *
 * Characters continue to be read out until the end of line is reached or the number of readout
 * characters is equal to buffsize -1.
 *
 * @param buff
 *   Buffer for the read-out string
 *
 * @param buffsize
 *   Length of the string
 *
 * @param fileHandle
 *   Handle to the file
 *
 * @return
 *   If an error occurs the return value is 0, else 1.
 */
long fileGetString(char * buff, long buffsize, dword fileHandle);

}
