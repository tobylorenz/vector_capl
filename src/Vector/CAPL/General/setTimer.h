#pragma once

#include "../DataTypes.h"
#include "General.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets a timer.
 *
 * Sets a timer.
 *
 * @param t
 *   MsTimer variable.
 *
 * @param duration
 *   An expression which specifies the duration of the timer.
 */
void setTimer(msTimer t, long duration);

/**
 * @ingroup General
 *
 * @brief
 *   Sets a timer. (Type 1)
 *
 * Sets a timer.
 *
 * @param t
 *   Timer variable.
 *
 * @param duration
 *   An expression which specifies the duration of the timer.
 */
void setTimer(timer t, long duration);

/**
 * @ingroup General
 *
 * @brief
 *   Sets a timer. (Type 2)
 *
 * Sets a timer.
 *
 * @param t
 *   Timer variable.
 *
 * @param durationSec
 *   An expression which specifies the duration of the timer.
 *
 * @param durationNanoSec
 *   An expression which specifies the duration of the timer.
 */
void setTimer(timer t, long durationSec, long durationNanoSec);

}
