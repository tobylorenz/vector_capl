#include "random.h"

#include <chrono>
#include <random>

namespace capl
{

dword random(dword x)
{
    // get seed from system clock
    unsigned int seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<unsigned long> distribution(0, x-1);
    return distribution(generator);
}

}
