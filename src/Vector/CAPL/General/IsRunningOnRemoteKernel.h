#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 */
long IsRunningOnRemoteKernel(void);

}
