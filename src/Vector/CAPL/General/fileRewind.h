#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Resets the position pointer to the beginning of the file.
 *
 * This function resets the position pointer to the beginning of the file.
 *
 * @param fileHandle
 *   The integer indicates the file handle.
 *
 * @return
 *   If an error occurs the return value is 0, else 1.
 */
long fileRewind(dword fileHandle);

}
