#include "envvarFloat.h"

#include <iostream>

namespace capl
{

envvarFloat::envvarFloat() :
    envvar(),
    value()
{
}

envvarFloat::~envvarFloat()
{
}

size_t envvarFloat::valueSize()
{
    return sizeof(value);
}

}
