#include "fileReadArray.h"

#include <iostream>

namespace capl
{

long fileReadArray(char * section, char * entry, char * buffer, long bufferlen, char * file)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
