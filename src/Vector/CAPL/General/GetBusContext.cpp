#include "GetBusContext.h"

#include <iostream>

namespace capl
{

dword GetBusContext(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
