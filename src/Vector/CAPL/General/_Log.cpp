#include "_Log.h"

#include <iostream>

namespace capl
{

double _Log(double x)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
