#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets a property of an ActiveX control.
 *
 * Sets a property of an ActiveX control.
 *
 * The panel is accessed by its individual panel name that is entered in the Panel Editor.
 *
 * It is easier to access color properties by MakeRGB.
 *
 * @param panel
 *   Panel name ("" - references all opened panels)
 *
 * @param control
 *   Name of the panel element ("" - references all elements on the panel)
 *
 * @param property
 *   Name of the property
 *
 * @param value
 *   Value to be set (long value)
 */
void SetControlProperty(char * panel, char * control, char * property, long value);

/**
 * @ingroup General
 *
 * @brief
 *   Sets a property of an ActiveX control.
 *
 * Sets a property of an ActiveX control.
 *
 * The panel is accessed by its individual panel name that is entered in the Panel Editor.
 *
 * It is easier to access color properties by MakeRGB.
 *
 * @param panel
 *   Panel name ("" - references all opened panels)
 *
 * @param control
 *   Name of the panel element ("" - references all elements on the panel)
 *
 * @param property
 *   Name of the property
 *
 * @param value
 *   Value to be set (float value)
 */
void SetControlProperty(char * panel, char * control, char * property, double value);

/**
 * @ingroup General
 *
 * @brief
 *   Sets a property of an ActiveX control.
 *
 * Sets a property of an ActiveX control.
 *
 * The panel is accessed by its individual panel name that is entered in the Panel Editor.
 *
 * It is easier to access color properties by MakeRGB.
 *
 * @param panel
 *   Panel name ("" - references all opened panels)
 *
 * @param control
 *   Name of the panel element ("" - references all elements on the panel)
 *
 * @param property
 *   Name of the property
 *
 * @param value
 *   Value to be set (string value)
 */
void SetControlProperty(char * panel, char * control, char * property, char * value);

}
