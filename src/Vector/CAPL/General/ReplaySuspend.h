#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Suspends the Replay block.
 *
 * Suspends the Replay block with the name pName.
 *
 * @param pName
 *   Name of the Replay block
 *
 * @return
 *   - 1: If successful
 *   - 0: If the Replay block does not exist or cannot be restarted
 */
dword ReplaySuspend(char * pName);

}
