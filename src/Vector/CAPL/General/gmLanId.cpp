#include "gmLanId.h"

#include <iostream>

namespace capl
{

dword gmLanId(char * aMessage, dword aSourceId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword gmLanId(char * aMessage, char * aTxNode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
