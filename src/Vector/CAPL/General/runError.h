#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Triggers a run error.
 *
 * Triggers a run error. Outputs the error number to the Write window indicating the error
 * number and the passed number, and then terminates the measurement.
 *
 * @param err
 *   Numbers that are represented in CANalyzer as a references for the user.
 *   The values under 1000 are reserved for internal purposes.
 *
 * @param res
 *   The second parameter is reserved for future expansions.
 */
void runError(long err, long res);

}
