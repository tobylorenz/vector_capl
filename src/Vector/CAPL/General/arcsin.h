#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates arcsine of a value.
 *
 * Calculates arcsine of x.
 *
 * @param x
 *   Value between -1 and 1 whose arcsine is to be calculated. Values outside this range
 *   cause a CAPL runtime error.
 *
 * @return
 *   Arcus Sine of x.
 */
double arcsin(double x);

}
