#include "outport.h"

#include <iostream>

namespace capl
{

void outport(word addr, byte value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
