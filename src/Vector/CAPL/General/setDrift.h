#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets a constant deviation for the timers of a network node.
 *
 * A constant deviation can be set for the timers of a network node with this function. Inputs
 * for the two values may lie between -10000 and 10000 (corresponds to -100.00% to
 * 100.00%). If the value does not lie within this range, a message is output in the Write
 * window.
 *
 * @param drift
 *   Integer for the constant deviation.
 */
void setDrift(int drift);

}
