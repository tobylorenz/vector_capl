#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Retrieves the fully qualified name of the computer.
 *
 * Retrieves the fully qualified name of the computer.
 *
 * @param buffer
 *   Space for the returned name.
 *
 * @param bufferSize
 *   Length of the buffer.
 *
 * @return
 *   0 if the function completed successfully, else unequal 0.
 */
long GetComputerName(char * buffer, dword bufferSize);

}
