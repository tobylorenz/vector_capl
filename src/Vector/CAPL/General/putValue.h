#pragma once

#include "../DataTypes.h"
#include "General.h"
#include "envvarInt.h"
#include "envvarFloat.h"
#include "envvarString.h"
#include "envvarData.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the environment variable. (form 1)
 *
 * Assigns the value val to the environment variable with identifier EnvVarName/name.
 * Integers are assigned to discrete environment variables (form 1 and 6), floating point
 * numbers are assigned to continuous environment variables (form 2 and 7). The contents of
 * a character string is assigned to character string environment variables (form 3 and 8).
 * For data byte environment variables (form 4, 5, 9 and 10) the bytes of the data buffer are
 * copied into the environment variable.
 *
 * @param EnvVarName
 *   Environment variable name
 *
 * @param val
 *   New value of environment variable (form 1 and 2) or buffer with new data (form 3, 4 and
 *   5) and for form 5 the number of bytes to be copied.
 */
void putValue(envvarInt & EnvVarName, int val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the environment variable. (form 2)
 *
 * Assigns the value val to the environment variable with identifier EnvVarName/name.
 * Integers are assigned to discrete environment variables (form 1 and 6), floating point
 * numbers are assigned to continuous environment variables (form 2 and 7). The contents of
 * a character string is assigned to character string environment variables (form 3 and 8).
 * For data byte environment variables (form 4, 5, 9 and 10) the bytes of the data buffer are
 * copied into the environment variable.
 *
 * @param EnvVarName
 *   Environment variable name
 *
 * @param val
 *   New value of environment variable (form 1 and 2) or buffer with new data (form 3, 4 and
 *   5) and for form 5 the number of bytes to be copied.
 */
void putValue(envvarFloat & EnvVarName, double val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the environment variable. (form 3)
 *
 * Assigns the value val to the environment variable with identifier EnvVarName/name.
 * Integers are assigned to discrete environment variables (form 1 and 6), floating point
 * numbers are assigned to continuous environment variables (form 2 and 7). The contents of
 * a character string is assigned to character string environment variables (form 3 and 8).
 * For data byte environment variables (form 4, 5, 9 and 10) the bytes of the data buffer are
 * copied into the environment variable.
 *
 * @param EnvVarName
 *   Environment variable name
 *
 * @param val
 *   New value of environment variable (form 1 and 2) or buffer with new data (form 3, 4 and
 *   5) and for form 5 the number of bytes to be copied.
 */
void putValue(envvarString & EnvVarName, char * val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the environment variable. (form 4)
 *
 * Assigns the value val to the environment variable with identifier EnvVarName/name.
 * Integers are assigned to discrete environment variables (form 1 and 6), floating point
 * numbers are assigned to continuous environment variables (form 2 and 7). The contents of
 * a character string is assigned to character string environment variables (form 3 and 8).
 * For data byte environment variables (form 4, 5, 9 and 10) the bytes of the data buffer are
 * copied into the environment variable.
 *
 * @param EnvVarName
 *   Environment variable name
 *
 * @param val
 *   New value of environment variable (form 1 and 2) or buffer with new data (form 3, 4 and
 *   5) and for form 5 the number of bytes to be copied.
 */
void putValue(envvarData & EnvVarName, byte * val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the environment variable. (form 5)
 *
 * Assigns the value val to the environment variable with identifier EnvVarName/name.
 * Integers are assigned to discrete environment variables (form 1 and 6), floating point
 * numbers are assigned to continuous environment variables (form 2 and 7). The contents of
 * a character string is assigned to character string environment variables (form 3 and 8).
 * For data byte environment variables (form 4, 5, 9 and 10) the bytes of the data buffer are
 * copied into the environment variable.
 *
 * @param EnvVarName
 *   Environment variable name
 *
 * @param val
 *   New value of environment variable (form 1 and 2) or buffer with new data (form 3, 4 and
 *   5) and for form 5 the number of bytes to be copied.
 *
 * @param vSize
 *   Size
 */
void putValue(envvarData & EnvVarName, byte * val, long vSize);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the environment variable. (form 6)
 *
 * Assigns the value val to the environment variable with identifier EnvVarName/name.
 * Integers are assigned to discrete environment variables (form 1 and 6), floating point
 * numbers are assigned to continuous environment variables (form 2 and 7). The contents of
 * a character string is assigned to character string environment variables (form 3 and 8).
 * For data byte environment variables (form 4, 5, 9 and 10) the bytes of the data buffer are
 * copied into the environment variable.
 *
 * @param name
 *   Environment variable name
 *
 * @param val
 *   New value of environment variable (form 1 and 2) or buffer with new data (form 3, 4 and
 *   5) and for form 5 the number of bytes to be copied.
 */
void putValue(char * name, int val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the environment variable. (form 7)
 *
 * Assigns the value val to the environment variable with identifier EnvVarName/name.
 * Integers are assigned to discrete environment variables (form 1 and 6), floating point
 * numbers are assigned to continuous environment variables (form 2 and 7). The contents of
 * a character string is assigned to character string environment variables (form 3 and 8).
 * For data byte environment variables (form 4, 5, 9 and 10) the bytes of the data buffer are
 * copied into the environment variable.
 *
 * @param name
 *   Environment variable name
 *
 * @param val
 *   New value of environment variable (form 1 and 2) or buffer with new data (form 3, 4 and
 *   5) and for form 5 the number of bytes to be copied.
 */
void putValue(char * name, double val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the environment variable. (form 8)
 *
 * Assigns the value val to the environment variable with identifier EnvVarName/name.
 * Integers are assigned to discrete environment variables (form 1 and 6), floating point
 * numbers are assigned to continuous environment variables (form 2 and 7). The contents of
 * a character string is assigned to character string environment variables (form 3 and 8).
 * For data byte environment variables (form 4, 5, 9 and 10) the bytes of the data buffer are
 * copied into the environment variable.
 *
 * @param name
 *   Environment variable name
 *
 * @param val
 *   New value of environment variable (form 1 and 2) or buffer with new data (form 3, 4 and
 *   5) and for form 5 the number of bytes to be copied.
 */
void putValue(char * name, char * val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the environment variable. (form 9)
 *
 * Assigns the value val to the environment variable with identifier EnvVarName/name.
 * Integers are assigned to discrete environment variables (form 1 and 6), floating point
 * numbers are assigned to continuous environment variables (form 2 and 7). The contents of
 * a character string is assigned to character string environment variables (form 3 and 8).
 * For data byte environment variables (form 4, 5, 9 and 10) the bytes of the data buffer are
 * copied into the environment variable.
 *
 * @param name
 *   Environment variable name
 *
 * @param val
 *   New value of environment variable (form 1 and 2) or buffer with new data (form 3, 4 and
 *   5) and for form 5 the number of bytes to be copied.
 */
void putValue(char * name, byte * val);

/**
 * @ingroup General
 *
 * @brief
 *   Assigns the value to the environment variable. (form 10)
 *
 * Assigns the value val to the environment variable with identifier EnvVarName/name.
 * Integers are assigned to discrete environment variables (form 1 and 6), floating point
 * numbers are assigned to continuous environment variables (form 2 and 7). The contents of
 * a character string is assigned to character string environment variables (form 3 and 8).
 * For data byte environment variables (form 4, 5, 9 and 10) the bytes of the data buffer are
 * copied into the environment variable.
 *
 * @param name
 *   Environment variable name
 *
 * @param val
 *   New value of environment variable (form 1 and 2) or buffer with new data (form 3, 4 and
 *   5) and for form 5 the number of bytes to be copied.
 *
 * @param vSize
 *   Size
 */
void putValue(char * name, byte * val, long vSize);

}
