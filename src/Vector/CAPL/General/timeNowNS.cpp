#include "timeNowNS.h"

#include <iostream>

namespace capl
{

double TimeNowNS(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
