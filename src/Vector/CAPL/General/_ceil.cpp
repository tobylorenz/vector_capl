#include "_ceil.h"

#include <iostream>
#include <math.h>

namespace capl
{

double _ceil(double x)
{
    return ceil(x);
}

}
