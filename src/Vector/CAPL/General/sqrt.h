#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates the square root.
 *
 * Calculates the square root.
 *
 * @param x
 *   Value whose square root is to be calculated.
 *
 * @return
 *   Square root of x.
 */
double sqrt(double x);

}
