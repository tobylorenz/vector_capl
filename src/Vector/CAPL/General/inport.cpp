#include "inport.h"

#include <iostream>

namespace capl
{

byte inport(word addr)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
