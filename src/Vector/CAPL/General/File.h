/**
 * This class is used to read from a file or write to a file.
 */

#pragma once

#include "../DataTypes.h"

#include <fstream>

#include "../vector_capl_export.h"

namespace capl
{

/**
 * @ingroup General
 */
class VECTOR_CAPL_EXPORT File
{
public:
    void open(char * filename, dword access, dword mode);
    long close(void);
    long getBinaryBlock(byte * buff, long buffsize);
    long getString(char * buff, long buffsize);
    long getStringSZ(char * buff, long buffsize);
    long putString(char * buff, long buffsize);
    long rewind(void);
    long writeBinaryBlock(byte * buff, long buffsize);
    long writeString(char * buff, long buffsize);

private:
    std::fstream fs;
};

}
