#include "cancelTimer.h"

#include <iostream>

namespace capl
{

void cancelTimer(msTimer t)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void cancelTimer(timer t)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
