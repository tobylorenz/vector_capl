#include "writeDbgLevel.h"

#include <iostream>

namespace capl
{

long writeDbgLevel(unsigned int priority, const char * format, ...)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
