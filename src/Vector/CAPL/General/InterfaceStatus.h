#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   The callback occurs when the status of the connection to the interface hardware is changed.
 *
 * The callback can be inserted in the sections callback function or function.
 *
 * The callback occurs when the status of the connection to the interface hardware is
 * changed (e.g. when Windows reports a lost connection to a
 * CAN/WLAN gateway or to a WLAN interface hardware for Car2x communication).
 *
 * @param time
 *   Time, resolution 10us
 *
 * @param channel
 *   The channel number
 *
 * @param status
 *   Status of the channel
 *   Values:
 *   - 3015: The connection is lost
 */
void InterfaceStatus(long time, long channel, long status);

}
