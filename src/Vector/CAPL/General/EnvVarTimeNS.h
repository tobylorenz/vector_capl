#pragma once

#include "../DataTypes.h"
#include "General.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns the time stamp of the environment variable envVariable in nanoseconds.
 *
 * Returns the time stamp of the environment variable envVariable in nanoseconds.
 *
 * @param envVariable
 *   Name of the environment variable.
 *
 * @return
 *   Time stamp of the environment variable in ns.
 */
double EnvVarTimeNS(envvar envVariable);

}
