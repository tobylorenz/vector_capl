#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Determines the number of elements of an array.
 *
 * Determines the number of elements of an array.
 *
 * @param ...
 *   Array of any arbitrary type.
 *
 * @return
 *   Number of elements.
 */
//long elCount(...);
#define elCount(x) (sizeof(x) / sizeof(x[0]))

}
