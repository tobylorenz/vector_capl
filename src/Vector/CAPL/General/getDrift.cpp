#include "getDrift.h"

#include <iostream>

namespace capl
{

int getDrift(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
