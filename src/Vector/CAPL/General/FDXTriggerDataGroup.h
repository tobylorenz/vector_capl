#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Triggers the transmission of a data group via CANoe FDX protocol.
 *
 * This function triggers the transmission of a data group via CANoe FDX protocol.
 *
 * @param groupID
 *   ID of the FDX data group that should be transmitted.
 *
 * @return
 *   - 0: Success
 *   - -1: The given data group is not configured.
 *   - -2: No client is registered to receive this data group.
 */
long FDXTriggerDataGroup(word groupID);

}
