#include "timeNowInt64.h"

#include <iostream>

namespace capl
{

int64 timeNowInt64(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
