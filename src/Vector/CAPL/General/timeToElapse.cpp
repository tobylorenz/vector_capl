#include "timeToElapse.h"

#include <iostream>

namespace capl
{

long timeToElapse(timer t)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long timeToElapse(msTimer t)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
