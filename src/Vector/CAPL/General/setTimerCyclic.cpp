#include "setTimerCyclic.h"

#include <iostream>

namespace capl
{

void setTimerCyclic(msTimer t, long firstDuration, long period)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void setTimerCyclic(msTimer t, long period)
{
    setTimerCyclic(t, period, period);
}

}
