#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Reads the value of the given variable from the specified section in the specified file.
 *
 * Searches the file under section section for the variable entry. Entry is interpreted as a
 * list of characters, separated by comma, tab, space, semicolon or slash. A 0x prefix
 * indicates hex values. Maximum characters of entry will be read into buff.
 *
 * @param section
 *   Section of the file as a string.
 *
 * @param entry
 *   Variable name as a string.
 *
 * @param buff
 *   Buffer for the read-in characters.
 *
 * @param buffsize
 *   Size of buff (max. 1279 characters).
 *
 * @param filename
 *   File path as a string.
 *
 * @return
 *   Number of characters read in.
 */
long getProfileArray(char * section, char * entry, char * buff, long buffsize, char * filename);

}
