#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Checks whether a string completely matches a regular expression pattern.
 *
 * Checks whether a string completely matches a regular expression pattern.
 *
 * @param s
 *   String to be checked.
 *
 * @param pattern
 *   Regular expression against which the string is matched. For the regular expression, the
 *   same syntax is used as in the Perl programming language.
 *
 * @return
 *   1 if the string matches the pattern, 0 if it doesn't match the pattern.
 */
long str_match_regex(char * s, char * pattern);

}
