#pragma once

#include "../DataTypes.h"
#include <cstddef>

namespace capl
{

/**
 * Environment variable
 */
class envvar
{
public:
    envvar();
    virtual ~envvar();

    /**
     * Point in time, units: nanoseconds
     */
    int64 TIME_NS;

    virtual size_t valueSize();
};

typedef envvar dbEnvVar;

}
