#include "CompleteStop.h"

#include <iostream>

namespace capl
{

void CompleteStop(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
