#include "strncmp_off.h"

#include <iostream>

namespace capl
{

long strncmp_off(char * s1, long s1offset, char * s2, long s2offset, long len)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
