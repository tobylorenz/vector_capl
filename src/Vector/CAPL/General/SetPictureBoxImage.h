#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Replaces the image of the Panel Designer Picture Box element.
 *
 * Replaces the image of the Panel Designer Picture Box control during runtime.
 *
 * The panel is accessed by its individual panel name that is entered in the Panel
 * Designer.
 *
 * @param panel
 *   Panel name ("" - references all opened panels)
 *
 * @param control
 *   Name of the panel element ("" - references all controls on the panel)
 *
 * @param imagefile
 *   Path and name of the image file.
 */
void setPictureBoxImage(char * panel, char * control, char * imagefile);

}
