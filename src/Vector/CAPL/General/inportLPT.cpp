#include "inportLPT.h"

#include <iostream>

namespace capl
{

byte inportLPT(word addr)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
