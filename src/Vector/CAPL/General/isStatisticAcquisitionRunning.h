#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Tests whether an acquisition range has already been started.
 *
 * This function is used to test whether an acquisition range has already been started.
 *
 * @return
 *   The function returns 1 if an evaluation is already running. Otherwise it returns 0.
 */
int isStatisticAcquisitionRunning(void);

}
