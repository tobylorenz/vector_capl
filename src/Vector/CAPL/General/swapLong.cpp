#include "swapLong.h"

#include <iostream>

namespace capl
{

long swapLong(long x)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
