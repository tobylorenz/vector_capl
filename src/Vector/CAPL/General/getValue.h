#pragma once

#include "../DataTypes.h"
#include "General.h"
#include "envvarInt.h"
#include "envvarFloat.h"
#include "envvarString.h"
#include "envvarData.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Determines the value of the environment variable. (form 1)
 *
 * Determines the value of the environment variable with identifier EnvVarName/name. The
 * type of the return value is based on the type of environment variable (int for discrete
 * (form 1), float for continuous environment variables (form 2 and 6)). For character string
 * environment variables (form 3 and 7) and environment variables with data bytes (form 4,
 * 5, 8 and 9) the active value is saved to a buffer which you identify in the function call.
 *
 * @param EnvVarName
 *   Environment variable name
 *
 * @return
 *   Active value of the environment variable.
 */
int getValue(envvarInt & EnvVarName);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the value of the environment variable. (form 2)
 *
 * Determines the value of the environment variable with identifier EnvVarName/name. The
 * type of the return value is based on the type of environment variable (int for discrete
 * (form 1), float for continuous environment variables (form 2 and 6)). For character string
 * environment variables (form 3 and 7) and environment variables with data bytes (form 4,
 * 5, 8 and 9) the active value is saved to a buffer which you identify in the function call.
 *
 * @param EnvVarName
 *   Environment variable name
 *
 * @return
 *   Active value of the environment variable.
 */
double getValue(envvarFloat & EnvVarName);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the value of the environment variable. (form 3)
 *
 * Determines the value of the environment variable with identifier EnvVarName/name. The
 * type of the return value is based on the type of environment variable (int for discrete
 * (form 1), float for continuous environment variables (form 2 and 6)). For character string
 * environment variables (form 3 and 7) and environment variables with data bytes (form 4,
 * 5, 8 and 9) the active value is saved to a buffer which you identify in the function call.
 *
 * @param EnvVarName
 *   Environment variable name
 *
 * @param buffer
 *   Return buffer (form 3, 4 and 5)
 *
 * @return
 *   Number of bytes copied.
 */
long getValue(envvarString & EnvVarName, char * buffer);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the value of the environment variable. (form 4)
 *
 * Determines the value of the environment variable with identifier EnvVarName/name. The
 * type of the return value is based on the type of environment variable (int for discrete
 * (form 1), float for continuous environment variables (form 2 and 6)). For character string
 * environment variables (form 3 and 7) and environment variables with data bytes (form 4,
 * 5, 8 and 9) the active value is saved to a buffer which you identify in the function call.
 *
 * @param EnvVarName
 *   Environment variable name
 *
 * @param buffer
 *   Return buffer (form 3, 4 and 5)
 *
 * @return
 *   Number of bytes copied.
 */
long getValue(envvarData & EnvVarName, byte * buffer);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the value of the environment variable. (form 5)
 *
 * Determines the value of the environment variable with identifier EnvVarName/name. The
 * type of the return value is based on the type of environment variable (int for discrete
 * (form 1), float for continuous environment variables (form 2 and 6)). For character string
 * environment variables (form 3 and 7) and environment variables with data bytes (form 4,
 * 5, 8 and 9) the active value is saved to a buffer which you identify in the function call.
 *
 * @param EnvVarName
 *   Environment variable name
 *
 * @param buffer
 *   Return buffer (form 3, 4 and 5)
 *
 * @param offset
 *   Offset of the first data byte copied (form 5)
 *
 * @return
 *   Number of bytes copied.
 */
long getValue(envvarData & EnvVarName, byte * buffer, long offset);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the value of the environment variable. (form 6)
 *
 * Determines the value of the environment variable with identifier EnvVarName/name. The
 * type of the return value is based on the type of environment variable (int for discrete
 * (form 1), float for continuous environment variables (form 2 and 6)). For character string
 * environment variables (form 3 and 7) and environment variables with data bytes (form 4,
 * 5, 8 and 9) the active value is saved to a buffer which you identify in the function call.
 *
 * @param name
 *   Environment variable name
 *
 * @return
 *   Active value of the environment variable.
 */
double getValue(char * name);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the value of the environment variable. (form 7)
 *
 * Determines the value of the environment variable with identifier EnvVarName/name. The
 * type of the return value is based on the type of environment variable (int for discrete
 * (form 1), float for continuous environment variables (form 2 and 6)). For character string
 * environment variables (form 3 and 7) and environment variables with data bytes (form 4,
 * 5, 8 and 9) the active value is saved to a buffer which you identify in the function call.
 *
 * @param name
 *   Environment variable name
 *
 * @param buffer
 *   Return buffer (form 3, 4 and 5)
 *
 * @return
 *   Number of bytes copied.
 */
long getValue(char * name, char * buffer);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the value of the environment variable. (form 8)
 *
 * Determines the value of the environment variable with identifier EnvVarName/name. The
 * type of the return value is based on the type of environment variable (int for discrete
 * (form 1), float for continuous environment variables (form 2 and 6)). For character string
 * environment variables (form 3 and 7) and environment variables with data bytes (form 4,
 * 5, 8 and 9) the active value is saved to a buffer which you identify in the function call.
 *
 * @param name
 *   Environment variable name
 *
 * @param buffer
 *   Return buffer (form 3, 4 and 5)
 *
 * @return
 *   Number of bytes copied.
 */
long getValue(char * name, byte * buffer);

/**
 * @ingroup General
 *
 * @brief
 *   Determines the value of the environment variable. (form 9)
 *
 * Determines the value of the environment variable with identifier EnvVarName/name. The
 * type of the return value is based on the type of environment variable (int for discrete
 * (form 1), float for continuous environment variables (form 2 and 6)). For character string
 * environment variables (form 3 and 7) and environment variables with data bytes (form 4,
 * 5, 8 and 9) the active value is saved to a buffer which you identify in the function call.
 *
 * @param name
 *   Environment variable name
 *
 * @param buffer
 *   Return buffer (form 3, 4 and 5)
 *
 * @param offset
 *   Offset of the first data byte copied (form 5)
 *
 * @return
 *   Number of bytes copied.
 */
long getValue(char * name, byte * buffer, long offset);

}
