#include "MakeRGB.h"

#include <iostream>

namespace capl
{

long MakeRGB(long Red, long Green, long Blue)
{
    return
            ((Red   & 0xff) <<  0) |
            ((Green & 0xff) <<  8) |
            ((Blue  & 0xff) << 16);
}

}
