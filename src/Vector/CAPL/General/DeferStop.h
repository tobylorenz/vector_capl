#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Defers a measurement stop so that activities can be completed before the stop takes effect.
 *
 * Defers measurement stop.
 *
 * The function can be called in the on preStop handler or even at an earlier time instance.
 * Measurement is deferred until CompleteStop is called in the same node or the simulation
 * time has advanced by the amount given in parameter maxDeferTime since the arrival of a
 * stop request (and call of the on preStop handler).
 *
 * DeferStop enables waiting for completion of activities that have to be carried out before
 * measurement stop takes effect, e.g. a reset of an attached ECU.
 *
 * @param maxDeferTime
 *   Indicates the time interval in milliseconds after which completion of pre-stop activities is
 *   indicated automatically if it has not yet been done explicitly via CompleteStop.
 */
void DeferStop(dword maxDeferTime);

}
