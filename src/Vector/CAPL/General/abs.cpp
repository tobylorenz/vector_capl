#include "abs.h"

#include <complex>

namespace capl
{

long abs(long x)
{
    return std::abs(x);
}

double abs(double x)
{
    return std::abs(x);
}

}
