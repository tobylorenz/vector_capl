#include "_gcvt.h"

#include <iostream>

namespace capl
{

void _gcvt(double val, int digits, char * s)
{
    (void) gcvt(val, digits, s);
}

}
