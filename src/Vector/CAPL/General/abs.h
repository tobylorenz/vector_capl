#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns the absolute value
 *
 * Returns the absolute value.
 *
 * @param x
 *   Value whose absolute value is to be returned.
 *
 * @return
 *   Absolute value of x.
 */
long abs(long x);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the absolute value
 *
 * Returns the absolute value.
 *
 * @param x
 *   Value whose absolute value is to be returned.
 *
 * @return
 *   Absolute value of x.
 */
double abs(double x);

}
