#include "getValueSize.h"

#include <iostream>

namespace capl
{

int getValueSize(envvar & EnvVarName)
{
    return (int) EnvVarName.valueSize();
}

int getValueSize(char * name)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
