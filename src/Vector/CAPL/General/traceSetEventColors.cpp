#include "traceSetEventColors.h"

#include <iostream>

namespace capl
{

void traceSetEventColors(message msg, long font, long bkgnd)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void traceSetEventColors(errorFrame msg, long font, long bkgnd)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void traceSetEventColors(pg msg, long font, long bkgnd)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void traceSetEventColors(linMessage msg, long font, long bkgnd)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void traceSetEventColors(mostRawMessage msg, long font, long bkgnd)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void traceSetEventColors(mostMessage msg, long font, long bkgnd)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void traceSetEventColors(mostAMSMessage msg, long font, long bkgnd)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void traceSetEventColors(FRFrame msg, long font, long bkgnd)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void traceSetEventColors(FRError msg, long font, long bkgnd)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
