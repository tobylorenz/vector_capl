#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns the current bus context of the CAPL block.
 *
 * Returns the current bus context of the CAPL block.
 *
 * @return
 *   The current bus context.
 */
dword GetBusContext(void);

}
