#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Starts all logging blocks immediately bypassing all logging trigger settings.
 *
 * Starts all logging blocks immediately bypassing all logging trigger settings.
 */
void startLogging(void);

/**
 * @ingroup General
 *
 * @brief
 *   Starts all logging blocks immediately bypassing all logging trigger settings.
 *
 * Starts a logging block with name strLoggingBlockName immediately bypassing all logging
 * trigger settings.
 *
 * @param strLoggingBlockName
 *   Name of the logging block.
 */
void startLogging(char * strLoggingBlockName);

/**
 * @ingroup General
 *
 * @brief
 *   Starts all logging blocks immediately bypassing all logging trigger settings.
 *
 * Starts a logging block with name strLoggingBlockName bypassing all logging trigger
 * settings.
 *
 * Function also sets a pre-trigger time to a value of the preTriggerTime.
 *
 * @param strLoggingBlockName
 *   Name of the logging block.
 *
 * @param preTriggerTime
 *   Pre-trigger time interval in ms.
 */
void startLogging(char * strLoggingBlockName, long preTriggerTime);

}
