#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Rounds a number.
 *
 * Rounds x to the nearest integral number. The rounding method used is symmetric
 * arithmetic rounding.
 *
 * @param x
 *   Number to be rounded
 *
 * @return
 *   Nearest integral number.
 *   For very large numbers, you should use _round64, which returns a 64 bit integer.
 */
long _round(double x);

}
