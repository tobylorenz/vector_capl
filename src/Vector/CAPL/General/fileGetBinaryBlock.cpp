#include "fileGetBinaryBlock.h"

#include <iostream>

namespace capl
{

long fileGetBinaryBlock(byte * buff, long buffsize, dword fileHandle)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
