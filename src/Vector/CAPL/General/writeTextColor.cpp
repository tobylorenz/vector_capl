#include "writeTextColor.h"
#include "General_intern.h"

namespace capl
{

void writeTextColor(dword sink, dword red, dword green, dword blue)
{
    caplIntern::WriteCommand wc;
    wc.command = caplIntern::WriteCommand::Command::WriteTextColor;
    wc.sink = sink;
    wc.red = red;
    wc.green = green;
    wc.blue = blue;
    for (auto fct: caplIntern::on_write)
        fct(wc);
}

}
