#include "fileName.h"

#include <iostream>

namespace capl
{

void fileName(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
