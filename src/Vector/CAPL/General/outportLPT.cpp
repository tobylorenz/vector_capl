#include "outportLPT.h"

#include <iostream>

namespace capl
{

void outportLPT(word addr, byte value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
