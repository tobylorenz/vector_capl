#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Opens the file for the read access.
 *
 * This function opens the file named filename for the read access.
 *
 * If mode=0 the file is opened in ASCII mode;
 * if mode=1 the file is opened in binary mode.
 *
 * @param filename
 *   file name
 *
 * @param mode
 *   mode
 *
 * @return
 *   The return value is the file handle that must be used for read operations.
 *   If an error occurs the return value is 0.
 */
dword openFileRead(char * filename, dword mode);

}
