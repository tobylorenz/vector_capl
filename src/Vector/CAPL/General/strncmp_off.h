#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Compares the number of characters of two strings.
 *
 * This function compares s1 with s2 for a maximum of len characters.
 *
 * If they are identical the functional result is 0.
 * If s1 is less than s2 the result is -1, else +1.
 * Comparison starts in s1 at s1offset and in s2 at s2offset.
 *
 * @param s1
 *   First string
 *
 * @param s1offset
 *   Offset in s1
 *
 * @param s2
 *   Second string
 *
 * @param s2offset
 *   Offset in s2
 *
 * @param len
 *   Maximum number of characters to compare
 *
 * @return
 *   - -1 if s1 is less than s2.
 *   - 1 if s2 is less than s1.
 *   - 0 if the strings are equal.
 */
long strncmp_off(char * s1, long s1offset, char * s2, long s2offset, long len);

}
