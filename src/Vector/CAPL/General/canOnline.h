#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Restores the connection of the node to the bus. (Form 1 obsolete)
 *
 * Restores the connection of the node to the bus. After a call to the function canOffline()
 * the node can be connected to the bus with the function canOnline(). Messages send
 * from the node are passed through to the bus.
 *
 * If the node is set to offline, output instructions for sending messages in CAPL or
 * NodeLayer DLL are ignored (refers to a node locally only).
 * Regardless of the status, all messages are received in the CAPL program/NodeLayer.
 *
 * This form only has an effect on the CAPL-program.
 */
void canOnline(void);

/**
 * @ingroup General
 *
 * @brief
 *   Restores the connection of the node to the bus. (Form 2)
 *
 * Restores the connection of the node to the bus. After a call to the function canOffline()
 * the node can be connected to the bus with the function canOnline(). Messages send
 * from the node are passed through to the bus.
 *
 * If the node is set to offline, output instructions for sending messages in CAPL or
 * NodeLayer DLL are ignored (refers to a node locally only).
 * Regardless of the status, all messages are received in the CAPL program/NodeLayer.
 *
 * In this form you can choose between the CAPL-program and/or the Nodelayer-DLL.
 *
 * @param flags
 *   Indicates the activated part of the node.
 *   - 1: Activates the CAPL-program
 *   - 2: Activates the Nodelayer
 *   - 3: Activates the CAPL-program and the Nodelayer
 *
 * @return
 *   This form returns the part of the node being online before the function call. Equal to the
 *   flags.
 */
dword canOnline(dword flags);

}
