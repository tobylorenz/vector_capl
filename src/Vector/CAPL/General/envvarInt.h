#pragma once

#include "../DataTypes.h"
#include "envvar.h"

namespace capl
{

/**
 * Environment variable of type Int
 */
class envvarInt : public envvar
{
public:
    envvarInt();
    virtual ~envvarInt();

    /* intern */
    int value;

    virtual size_t valueSize();
};

}
