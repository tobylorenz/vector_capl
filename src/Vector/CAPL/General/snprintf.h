#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Prints a formatted string to character array.
 *
 * This function corresponds to the C function sprintf. Supplementally, the parameter len
 * indicates the maximum length of the array dest.
 *
 * The format string has the same meaning as with the function write and is described there.
 *
 * @param dest
 *   Character buffer to print to.
 *
 * @param len
 *   Maximum number of characters printed to buffer.
 *
 * @param format
 *   Formatted string printed to buffer.
 *
 * @return
 *   The number of characters written.
 */
long snprintf(char * dest, long len, char * format, ...);

}
