#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the time of the Panel Designer clock control. (form 1)
 *
 * Sets the time of the Panel Designer clock control element.
 *
 * The panel is accessed by its individual panel name that is entered in the Panel Designer.
 *
 * @param panel
 *   Panel name ("" - references all opened panels)
 *
 * @param control
 *   Name of the element. You can only access the control by its name. In the property
 *   dialog of the control it's name is assigned/displayed.
 *   If you want to use the name of a symbol (signal or environment/system variable) you
 *   have to ensure that the control has no name instead of the individual control's name.
 *   The name of the environment variable, system variable or signal could be specified as following.
 *   The form for signals is: "Signal:<signal name>".
 *   The form for environment variables is: "EnvVar:<environment variable name>".
 *   The form for system variables is: "SysVar:<name of system variable>". The name space must
 *   not be used.
 *
 * @param hours
 *   Defines the hours of the time to be displayed in the clock control.
 *
 * @param minutes
 *   Defines the minutes of the time to be displayed in the clock control.
 *
 * @param seconds
 *   Defines the seconds of the time to be displayed in the clock control.
 */
void SetClockControlTime(char * panel, char * control, int hours, int minutes, int seconds);

/**
 * @ingroup General
 *
 * @brief
 *   Sets the time of the Panel Designer clock control. (form 2)
 *
 * Sets the time of the Panel Designer clock control element.
 *
 * The panel is accessed by its individual panel name that is entered in the Panel Designer.
 *
 * @param panel
 *   Panel name ("" - references all opened panels)
 *
 * @param control
 *   Name of the element. You can only access the control by its name. In the property
 *   dialog of the control it's name is assigned/displayed.
 *   If you want to use the name of a symbol (signal or environment/system variable) you
 *   have to ensure that the control has no name instead of the individual control's name.
 *   The name of the environment variable, system variable or signal could be specified as following.
 *   The form for signals is: "Signal:<signal name>".
 *   The form for environment variables is: "EnvVar:<environment variable name>".
 *   The form for system variables is: "SysVar:<name of system variable>". The name space must
 *   not be used.
 *
 * @param time
 *   Defines the time in seconds to be displayed in the clock control. The corresponding
 *   hours, minutes and seconds are calculated during runtime.
 */
void SetClockControlTime(char * panel, char * control, int time);

}
