#pragma once

#include "../DataTypes.h"
#include "../GMLAN/GMLAN.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the priority of the message.
 *
 * gmLanSetPrio sets the priority of the message.
 *
 * @param msg
 *   Message
 *
 * @param v
 *   Parameter ID, source address or priority
 */
void gmLanSetPrio(gmLanMessage msg, long v);

}
