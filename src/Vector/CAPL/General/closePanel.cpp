#include "closePanel.h"

#include <iostream>

namespace capl
{

void closePanel(char * panelName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
