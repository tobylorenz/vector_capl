#include "timeNowFloat.h"

#include <iostream>

namespace capl
{

double timeNowFloat(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
