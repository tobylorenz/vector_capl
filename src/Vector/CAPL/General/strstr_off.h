#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Searches a string in another string.
 *
 * Searches in s1 for s2.
 *
 * @param s1
 *   First string
 *
 * @param offset
 *   Offset in s1 at which the search shall be started
 *
 * @param s2
 *   Second string
 *
 * @return
 *   First position of s2 in s1, or -1 if s2 is not found in s1.
 */
long strstr_off(char * s1, long offset, char * s2);

}
