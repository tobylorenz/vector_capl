#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Starts playing the replay file.
 *
 * Starts playing the replay file with the name fileName.
 *
 * @param fileName
 *   Replay file.
 *
 * @return
 *   The returned handle is required to stop the replay file.
 */
dword StartReplayFile(char * fileName);

}
