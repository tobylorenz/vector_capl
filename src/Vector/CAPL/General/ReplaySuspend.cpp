#include "ReplaySuspend.h"

#include <iostream>

namespace capl
{

dword ReplaySuspend(char * pName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
