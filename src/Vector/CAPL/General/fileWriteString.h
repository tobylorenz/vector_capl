#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Opens a file, finds a special section and writes the variable entry with a special value.
 *
 * @deprecated
 *   Replaced by writeProfileString
 *
 * Opens the file filename, finds the section section and writes the variable entry with the value value.
 * If entry already exists, the old value is overwritten.
 * The functional result is the number of characters written, or 0 for an error.
 *
 * @param section
 *   Section of file
 *
 * @param entry
 *   Name of variable
 *
 * @param value
 *   Value
 *
 * @param filename
 *   Name of file
 *
 * @return
 *   Number of written characters of 0 if an error has occurred.
 */
long fileWriteString(char * section, char * entry, char * value, char * filename);

}
