#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates the logarithm to base 10.
 *
 * Calculates the logarithm to base 10.
 *
 * @param x
 *   Value of which the logarithm shall be calculated.
 *
 * @return
 *   Logarithm of x (to base 10).
 */
double _Log10(double x);

}
