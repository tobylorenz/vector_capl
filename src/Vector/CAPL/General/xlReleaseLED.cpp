#include "xlReleaseLED.h"

#include <iostream>

namespace capl
{

dword xlReleaseLED(dword ledBitMask)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
