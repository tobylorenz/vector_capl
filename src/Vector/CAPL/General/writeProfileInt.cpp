#include "writeProfileInt.h"

#include <iostream>

namespace capl
{

long writeProfileInt(char * section, char * entry, long value, char * filename)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
