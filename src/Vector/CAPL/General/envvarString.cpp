#include "envvarString.h"

#include <iostream>

namespace capl
{

envvarString::envvarString() :
    envvar(),
    value()
{
}

envvarString::~envvarString()
{
}

size_t envvarString::valueSize()
{
    return value.size() + 1;
}

}
