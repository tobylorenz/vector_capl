#include "runError.h"

#include <iostream>

namespace capl
{

void runError(long err, long res)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
