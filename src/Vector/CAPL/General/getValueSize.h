#pragma once

#include "../DataTypes.h"
#include "General.h"
#include "envvar.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns the size of the environment variable value in bytes. (Form 1)
 *
 * Returns the size of the environment variable value in bytes.
 *
 * @param EnvVarName
 *   Environment variable name
 *
 * @return
 *   Size of the data in bytes.
 */
int getValueSize(envvar & EnvVarName);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the size of the environment variable value in bytes. (Form 2)
 *
 * Returns the size of the environment variable value in bytes.
 *
 * @param name
 *   Environment variable name
 *
 * @return
 *   Size of the data in bytes.
 */
int getValueSize(char * name);

}
