#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 */
dword StandaloneConfigOpen(char * rtcfgFileName, dword stopCurrentMeasurement, dword startNewMeasurement, dword returnToActiveConfig); // Opens the rtcfg file with the given name as standalone configuration.

}
