#include "arctan.h"

#include <iostream>

namespace capl
{

double arctan(double x)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
