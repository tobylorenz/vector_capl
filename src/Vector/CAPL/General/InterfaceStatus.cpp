#include "InterfaceStatus.h"

#include <iostream>

namespace capl
{

void InterfaceStatus(long time, long channel, long status)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
