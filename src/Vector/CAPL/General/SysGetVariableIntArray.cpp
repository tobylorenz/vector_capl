#include "SysGetVariableIntArray.h"

#include <iostream>

namespace capl
{

long SysGetVariableIntArray(char * namespace_, char * variable, int * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysGetVariableIntArray(char * SysVarName, int * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
