#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Calculates the exponential function.
 *
 * Calculates the exponential function.
 *
 * @param x
 *   Value whose exponent is to be calculated
 *
 * @return
 *   Exponent to base e.
 */
double exp(double x);

}
