#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Searches for a variable entry in a file section.
 *
 * @deprecated
 *   Replaced by getProfileFloat
 *
 * Analogous to fileReadInt for floats.
 *
 * @param section
 *   Section of file
 *
 * @param entry
 *   Name of variable
 *
 * @param def
 *   Value
 *
 * @param file
 *   Name of file
 *
 * @return
 *   Number of characters read.
 */
double fileReadFloat(char * section, char * entry, double def, char ile);

}
