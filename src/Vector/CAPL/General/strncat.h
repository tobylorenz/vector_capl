#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Appends a string to another string.
 *
 * This function appends src to dest. len indicates the maximum length of the fit string. The
 * function ensures that there is a terminating '\0'. Thus, a maximum of len - strlen(dest) -
 * 1 characters are copied.
 *
 * @param dest
 *   Target string to which characters are appended.
 *
 * @param src
 *   Appended string.
 *
 * @param len
 *   Maximum length of composite string.
 */
void strncat(char * dest, char * src, long len);

}
