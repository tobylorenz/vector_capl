#include "_floor.h"

#include <iostream>

namespace capl
{

double _floor(double x)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
