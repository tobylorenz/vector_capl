#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets specified LEDs.
 *
 * Sets the LEDs specified by ledBitMask to the operation mode specified by ledMode. A
 * successful call of xlAcquireLED is necessary before the operation mode of an LED can be
 * set with this function.
 *
 * This function is supported by the following device: VN8900 (driver version > 7.5)
 *
 * Note that for every successful call of xlAcquireLED on a specific LED, you have to call
 * xlReleaseLED to release this LED again.
 *
 * @param ledBitMask
 *   The LEDs you want to set. You can bitwise combine the values to specify multiple LEDs.
 *   The following LEDs are available:
 *   - 0x001: Module (M)
 *   - 0x008: CAN Channel 1
 *   - 0x010: CAN Channel 2
 *   - 0x020: CAN Channel 3
 *   - 0x040: CAN Channel 4
 *   - 0x080: FlexRay Channel 1 A
 *   - 0x100: FlexRay Channel 1 B
 *   - 0x200: Keypad S1
 *   - 0x400: Keypad S2
 *
 * @param ledMode
 *   The operation mode which shall be valid for the specified LEDs. The operation modes
 *   are mutually exclusive and only one LED can be in one operation mode.
 *   The following operation modes are available:
 *   - 0x00000000: No Change
 *   - 0x00000001: LED Off
 *   - 0x00000002: LED On Red
 *   - 0x00000004: LED Blinking Red
 *   - 0x00000008: LED On Green
 *   - 0x00000010: LED Blinking Green
 *   - 0x00000020: LED On Orange
 *   - 0x00000040: LED Blinking Orange
 *
 * @return
 *   - 0: no error, function succeeded.
 *   - != 0: error, function failed. Check whether your device supports this function and you
 *     have an appropriate driver installed on the device.
 */
dword xlSetLED(dword ledBitMask, dword ledMode);

}
