#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Stops the Clock Control designed as stop watch in the Panel Designer.
 *
 * Stops the Clock Control designed as stop watch with the Panel Designer (setting Mode =
 * StopWatch).
 *
 * The displayed time stays unchanged unless the user starts the stop watch again or
 * resets it.
 *
 * If the stop watch is started again without resetting it, the start time is the current
 * displayed time (not zero).
 *
 * The panel is accessed by its individual panel name that is entered in the Panel
 * Designer.
 *
 * @param panel
 *   Panel name ("" - references all opened panels)
 *
 * @param control
 *   Name of the element. You can only access the control by its name. In the property
 *   dialog of the control it's name is assigned/displayed.
 *   If you want to use the name of a symbol (signal or environment/system variable) you
 *   have to ensure that the control has no name instead of the individual control's name.
 *   The name of the environment variable, system variable or signal could be specified as
 *   following.
 *   The form for signals is: "Signal:<signal name>".
 *   The form for environment variables is: "EnvVar:<environment variable name>".
 *   The form for system variables is: "SysVar:<name of system variable>". The name space
 *   must not be used.
 */
void ClockControlStop(char * panel, char * control);

}
