#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Determines the constant deviation when Drift is set.
 *
 * Determines the constant deviation when Drift is set.
 *
 * @return
 *   Drift in parts per thousand.
 */
int getDrift(void);

}
