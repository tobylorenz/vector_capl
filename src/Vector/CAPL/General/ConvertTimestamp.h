#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Converts a timestamp to separate parts.
 *
 * Converts a timestamp to separate parts.
 *
 * @param timestamp
 *   timestamp in 10 microseconds
 *
 * @param days
 *   Receives the days of the timestamp
 *
 * @param hours
 *   Receives the hours the timestamp (between 0 and 23)
 *
 * @param minutes
 *   Receives the minutes of the timestamp (between 0 and 59)
 *
 * @param seconds
 *   Receives the seconds of the timestamp (between 0 and 59)
 *
 * @param milliSeconds
 *   Receives the milliseconds of the timestamp (between 0 and 999)
 *
 * @param microSeconds
 *   Receives the microseconds of the timestamp (between 0 and 999)
 */
void ConvertTimestamp(dword timestamp, dword& days, byte& hours, byte& minutes, byte& seconds, word& milliSeconds, word& microSeconds);

}
