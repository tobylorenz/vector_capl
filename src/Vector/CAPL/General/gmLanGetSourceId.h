#pragma once

#include "../DataTypes.h"
#include "../GMLAN/GMLAN.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Gets the source address of the message.
 *
 * gmLanGetSourceId gets the source address of the message.
 *
 * @param msg
 *   Message
 *
 * @return
 *   Source address
 */
long gmLanGetSourceId(gmLanMessage msg);

}
