#include "msTimer.h"

#include <iostream>

namespace capl
{

void msTimer::set(long duration)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void msTimer::set(long duration, long durationNanoSec)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void msTimer::cancel(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

int msTimer::isTimerActive(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long msTimer::timeToElapse(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
