#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Opens the file, searches the section and writes the variable.
 *
 * Opens the file filename, searches the section section and writes the variable entry with
 * the value value. If entry already exists the old value is overwritten. The functional result
 * is the number of characters written or 0 in case of an error.
 *
 * @param section
 *   Section of the file as a string.
 *
 * @param entry
 *   Variable name as a string.
 *
 * @param value
 *   Value as a string.
 *
 * @param filename
 *   File path as a string.
 *
 * @return
 *   - 0: Write error
 *   - >1: Write access. Number of characters will not be returned.
 */
long writeProfileString(char * section, char * entry, char * value, char * filename);

}
