#include "getValue.h"

#include <cstring>
#include <iostream>

namespace capl
{

int getValue(envvarInt & EnvVarName)
{
    return EnvVarName.value;
}

double getValue(envvarFloat & EnvVarName)
{
    return EnvVarName.value;
}

long getValue(envvarString & EnvVarName, char * buffer)
{
    strcpy(buffer, EnvVarName.value.c_str());
    return EnvVarName.value.size() + 1;
}

long getValue(envvarData & EnvVarName, byte * buffer)
{
    memcpy((char *) buffer, (const char *) EnvVarName.value, EnvVarName.size);
    return EnvVarName.size;
}

long getValue(envvarData & EnvVarName, byte * buffer, long offset)
{
    memcpy((char *) buffer, (const char *) EnvVarName.value + offset, EnvVarName.size - offset);
    return EnvVarName.size - offset;
}

double getValue(char * name)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long getValue(char * name, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long getValue(char * name, byte * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long getValue(char * name, byte * buffer, long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
