#include "stopStatisticAcquisition.h"

#include <iostream>

namespace capl
{

void stopStatisticAcquisition(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
