#include "_min.h"

namespace capl
{

long _min(long x, long y)
{
    return y < x ? y : x;
}

dword _min(dword x, dword y)
{
    return y < x ? y : x;
}

int64 _min(int64 x, int64 y)
{
    return y < x ? y : x;
}

qword _min(qword x, qword y)
{
    return y < x ? y : x;
}

double _min(double x, double y)
{
    return y < x ? y : x;
}

}
