#include "ReplayStart.h"

#include <iostream>

namespace capl
{

dword ReplayStart(char * pName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
