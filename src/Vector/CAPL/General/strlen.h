#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Gets the length of a string.
 *
 * The functional result is the length of the string s.
 *
 * @param s
 *   string
 *
 * @return
 *   Length of the string.
 */
long strlen(char * s);

}
