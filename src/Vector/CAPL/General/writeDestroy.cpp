#include "writeDestroy.h"
#include "General_intern.h"

namespace capl
{

void writeDestroy(dword sink)
{
    caplIntern::WriteCommand wc;
    wc.command = caplIntern::WriteCommand::Command::WriteDestroy;
    wc.sink = sink;
    for (auto fct: caplIntern::on_write)
        fct(wc);
}

}
