#include "gmLanGetSourceId.h"

#include <iostream>

namespace capl
{

long gmLanGetSourceId(gmLanMessage msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
