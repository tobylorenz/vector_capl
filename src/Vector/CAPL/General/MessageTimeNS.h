#pragma once

#include "../DataTypes.h"
#include "../CAN/CAN.h"
#include "../LIN/LIN.h"
#include "../MOST/MOST.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns the time stamp in nanoseconds.
 *
 * Returns the time stamp in nanoseconds.
 *
 * The time stamp that can be polled with this function has a greater precision than the
 * msg. TIME time stamp, whereby the precision depends on the CAN or LIN hardware that is
 * being used.
 *
 * @param msg
 *   CAN message
 *
 * @return
 *   Time stamp of the message in ns
 */
double MessageTimeNS(message msg);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the time stamp in nanoseconds.
 *
 * Returns the time stamp in nanoseconds.
 *
 * The time stamp that can be polled with this function has a greater precision than the
 * msg. TIME time stamp, whereby the precision depends on the CAN or LIN hardware that is
 * being used.
 *
 * @param msg
 *   LIN message
 *
 * @return
 *   Time stamp of the message in ns
 */
double MessageTimeNS(linMessage msg);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the time stamp in nanoseconds.
 *
 * Returns the time stamp in nanoseconds.
 *
 * The time stamp that can be polled with this function has a greater precision than the
 * msg. TIME time stamp, whereby the precision depends on the CAN or LIN hardware that is
 * being used.
 *
 * @param msg
 *   MOST message
 *
 * @return
 *   Time stamp of the message in ns
 */
double MessageTimeNS(mostMessage msg);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the time stamp in nanoseconds.
 *
 * Returns the time stamp in nanoseconds.
 *
 * The time stamp that can be polled with this function has a greater precision than the
 * msg. TIME time stamp, whereby the precision depends on the CAN or LIN hardware that is
 * being used.
 *
 * @param msg
 *   MOST message
 *
 * @return
 *   Time stamp of the message in ns
 */
double MessageTimeNS(mostAMSMessage msg);

/**
 * @ingroup General
 *
 * @brief
 *   Returns the time stamp in nanoseconds.
 *
 * Returns the time stamp in nanoseconds.
 *
 * The time stamp that can be polled with this function has a greater precision than the
 * msg. TIME time stamp, whereby the precision depends on the CAN or LIN hardware that is
 * being used.
 *
 * @param msg
 *   MOST message
 *
 * @return
 *   Time stamp of the message in ns
 */
double MessageTimeNS(mostRawMessage msg);

}
