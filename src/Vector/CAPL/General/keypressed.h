#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns the key code of a currently pressed key.
 *
 * This function returns the key code of a currently pressed key. If no key is being pressed it
 * returns 0.. For example, pressing of a key can be queried in a timer function. The
 * reaction can also be to letting go of a key.
 *
 * @return
 *   Key code of pressed key.
 *   If the 8 lower bits do not equal 0, keypressed returns the ASCII code of the next key in
 *   the keyboard buffer. If the 8 lower bits do not equal 0, the 8 upper bits represent the
 *   extended key code (see IBM PC Technical Reference Manual).
 */
dword keypressed(void);

}
