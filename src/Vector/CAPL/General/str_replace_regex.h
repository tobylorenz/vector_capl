#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Replaces all occurrences of pattern in a string with another string.
 *
 * Replaces all occurrences of pattern in a string with another string.
 *
 * @param s
 *   String to be modified.
 *
 * @param pattern
 *   Regular expression which determines the parts in s which shall be replaced. For the
 *   regular expression, the same syntax is used as in the Perl programming language.
 *
 * @param replacement
 *   Replacement for the parts which match the pattern.
 *
 * @return
 *   1 if successful, 0 if the resulting string would be too long for the buffer s.
 */
long str_replace_regex(char * s, char * pattern, char * replacement);

}
