#include "writeLineEX.h"
#include "General_intern.h"

namespace capl
{

void writeLineEX(dword sink, dword severity, const char * format, ...)
{
    va_list args;

    /* format the content */
    char buf[255];
    vsnprintf(buf, sizeof(buf), format, args);
    // @todo better use boost::format

    /* define the message */
    caplIntern::WriteCommand wc;
    wc.command = caplIntern::WriteCommand::Command::WriteLineEX;
    wc.sink = sink;
    wc.severity = severity;
    wc.textMessage.assign(buf);
    for (auto fct: caplIntern::on_write)
        fct(wc);
}

}
