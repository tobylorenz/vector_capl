#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the read and write path to the directory.
 *
 * This function sets the read and write path to the directory. The path can be given as
 * absolute or relative to the currently active configuration.
 *
 * @param path
 *   Path
 *
 * @param mode
 *   - 0: Sets path for read functions
 *   - 1: Sets path for write functions (same as setWritePath)
 *   - 2: Sets path for both types of functions
 */
void setFilePath(char * path, dword mode);

}
