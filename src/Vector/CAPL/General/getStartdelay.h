#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Determines the value of the start delay configured for this network node in the Simulation Setup.
 *
 * Determines the value of the start delay configured for this network node in the Simulation
 * Setup.
 *
 * @return
 *   Start delay in ms. If no start delay was set the function returns the value zero.
 */
int getStartdelay(void);

}
