#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns x to the power of y.
 *
 * Returns x to the power of y.
 *
 * @param x
 *   base
 *
 * @param y
 *   exponent
 *
 * @return
 *   x to the power of y.
 */
double _pow(double x, double y);

}
