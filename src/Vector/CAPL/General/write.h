#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Outputs a text message to the Write window.
 *
 * Outputs a text message to the Write window. Write is based on the C function printf.
 *
 * The compiler cannot check the format string. Illegal format entries will lead to undefined
 * results. Messages output with the write function will be displayed on separate lines.
 *
 * @param format
 *   Format string, variables or expressions
 *   Legal format expressions:
 *   - "%ld","%d": decimal display
 *   - "%lx","%x": hexadecimal display
 *   - "%lX","%X": hexadecimal display (upper case)
 *   - "%lu","%u": unsigned display
 *   - "%lo","%o": octal display
 *   - "%s": display a string
 *   - "%g","%f": floating point display
 *      e.g. %5.3f means, 5 digits in total (decimal point inclusive) and 3 digits
 *      after the decimal point. 5 is the minimum of digits in this case.
 *   - "%c": display a character
 *   - "%%": display %-character
 *   - "%I64d": decimal display of a 64 bit value
 *   - "%I64x": hexadecimal display of a 64 bit value
 *   - "%I64X": hexadecimal display of a 64 bit value (upper case)
 *   - "%I64u": unsigned display of a 64 bit value
 */
void write(const char * format, ...);

}
