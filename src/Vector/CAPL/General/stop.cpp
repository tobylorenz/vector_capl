#include "stop.h"

#include <iostream>

namespace capl
{

void stop(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
