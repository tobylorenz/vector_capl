#include "atol.h"

#include <iostream>

namespace capl
{

long atol(char * s)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
