#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Returns the context of the specified bus.
 *
 * Returns the context of the specified bus.
 *
 * @param name
 *   The name of the bus.
 *
 * @return
 *   In the case of success, the context of the specified bus is returned.
 *   If the specified bus does not exist, 0 is returned.
 */
dword GetBusNameContext(char * name);

}
