#include "keypressed.h"

#include <iostream>

namespace capl
{

dword keypressed(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
