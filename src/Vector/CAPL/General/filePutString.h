#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Writes a string in the specified file.
 *
 * This function writes a string in the specified file.
 *
 * @param buff
 *   Buffer for the read-in string
 *
 * @param buffsize
 *   Number of characters
 *
 * @param fileHandle
 *   Handle to the file
 *
 * @return
 *   If an error occurs the return value is 0, else 1.
 */
long filePutString(char * buff, long buffsize, dword fileHandle);

}
