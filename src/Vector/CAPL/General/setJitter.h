#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Sets the Jitter interval for the timers of a network node.
 *
 * The Jitter interval for the timers of a network node can be set with this function. The two
 * values may lie between -10000 and 10000 (corresponds to -100.00% to 100.00%). If one of
 * the two values does not lie within this range, a message is output in the Write window.
 *
 * @param min
 *   Integer for the lower interval limit.
 *
 * @param max
 *   Integer for the upper interval limit.
 */
void setJitter(int min, int max);

}
