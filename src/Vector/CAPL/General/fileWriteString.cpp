#include "fileWriteString.h"

#include <iostream>

namespace capl
{

long fileWriteString(char * section, char * entry, char * value, char * filename)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
