#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup General
 *
 * @brief
 *   Gets the full path name for a path defined relative to the current configuration.
 *
 * Gets the absolute path of a file.
 *
 * As parameter the file should be defined with the relative path to the current
 * configuration.
 *
 * @param relPath
 *   A path (with or without a file name) defined relative to the current configuration. If this
 *   parameter is empty, then the full path of the current configuration will simply be
 *   returned.
 *
 * @param absPath
 *   Buffer to which the full path name should be copied.
 *
 * @param absPathLen
 *   Size of the buffer [in bytes] for the full path name.
 *
 * @return
 *   On success this function returns length of the full path name, otherwise -1.
 */
long getAbsFilePath(char * relPath, char * absPath, long absPathLen);

}
