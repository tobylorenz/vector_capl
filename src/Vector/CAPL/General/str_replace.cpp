#include "str_replace.h"

#include <iostream>

namespace capl
{

long str_replace(char * s, char * searched, char * replacement)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long str_replace(char * s, long startoffset, char * replacement, long length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
