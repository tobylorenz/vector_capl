#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Deletes the map object assigned to this handle in the map window.
 */
long C2xDeleteMapObject(long handle);

}
