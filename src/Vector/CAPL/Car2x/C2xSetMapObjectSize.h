#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets either the size or the height and width of a map object.
 */
long C2xSetMapObjectSize(long handle, double size);

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets either the size or the height and width of a map object.
 */
long C2xSetMapObjectSize(long handle, double width, double height);

}
