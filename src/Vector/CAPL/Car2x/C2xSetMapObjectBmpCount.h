#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the number of bitmaps in a multi bitmap file of a map object.
 */
long C2xSetMapObjectBmpCount(long handle, long numbOfBmps);

}
