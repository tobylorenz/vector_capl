#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the file path to a bitmap for a map object of the type bitmap.
 */
long C2xSetMapObjectBmpFilePath(long handle, char * filePath);

}
