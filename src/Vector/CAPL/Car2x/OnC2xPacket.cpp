#include "OnC2xPacket.h"

#include <iostream>

namespace capl
{

void OnC2xPacket(long channel, long dir, long radioChannel, long signalStrength, long signalQuality, long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
