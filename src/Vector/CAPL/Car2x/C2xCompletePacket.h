#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   completes a WLAN packet for sending
 *
 * The function completes a packet before sending it with C2xOutputPacket. It calculates the fields
 * that are marked with CompleteProtocol in the protocol overview, e.g. checksum, lengths, etc.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @return
 *   0 or error code
 */
long C2xCompletePacket(long packet);

}
