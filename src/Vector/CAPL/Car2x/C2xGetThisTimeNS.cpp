#include "C2xGetThisTimeNS.h"

#include <iostream>

namespace capl
{

qword C2xGetThisTimeNS()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
