#include "C2xResizeToken.h"

#include <iostream>

namespace capl
{

long C2xResizeToken(long packet, char * protocolDesignator, char * tokenDesignator, long newBitLength)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
