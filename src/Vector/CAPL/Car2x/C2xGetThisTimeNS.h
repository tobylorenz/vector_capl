#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets timestamp of a received WLAN packet
 *
 * The function returns the timestamp of an received WLAN packet in nanoseconds.
 *
 * It is only usable in a CAPL callback function that had been registered with C2xReceivePacket.
 *
 * @return
 *   time stamp in nanoseconds
 */
qword C2xGetThisTimeNS();

}
