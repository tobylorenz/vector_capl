#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets received data (Variant 1)
 *
 * The function gets the returned data.
 *
 * It is only usable in a CAPL callback function that had been registered with C2xReceivePacket.
 *
 * @param offset
 *   byte offset relative to the beginning of a data packet or the payload (see description above)
 *
 * @param bufferSize
 *   number of requested bytes
 *
 * @param buffer
 *   buffer in which the requested data are copied
 *
 * @return
 *   number of copied data bytes
 */
long C2xGetThisData(long offset, long bufferSize, byte * buffer);

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets received data (Variant 2)
 *
 * The function gets the returned data.
 *
 * It is only usable in a CAPL callback function that had been registered with C2xReceivePacket.
 *
 * @param offset
 *   byte offset relative to the beginning of a data packet or the payload (see description above)
 *
 * @param bufferSize
 *   number of requested bytes
 *
 * @param buffer
 *   buffer in which the requested data are copied
 *
 * @return
 *   number of copied data bytes
 */
long C2xGetThisData(long offset, long bufferSize, char * buffer);

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets received data (Variant 3)
 *
 * The function gets the returned data.
 *
 * It is only usable in a CAPL callback function that had been registered with C2xReceivePacket.
 *
 * @param offset
 *   byte offset relative to the beginning of a data packet or the payload (see description above)
 *
 * @param bufferSize
 *   number of requested bytes
 *
 * @param buffer
 *   buffer in which the requested data are copied
 *
 * @return
 *   number of copied data bytes
 */
long C2xGetThisData(long offset, long bufferSize, void * buffer);

}
