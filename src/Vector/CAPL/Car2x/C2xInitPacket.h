#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   creates a new WLAN packet (Variant 1)
 *
 * This function creates a new WLAN packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @return
 *   handle of the created packet or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xInitPacket(void);

/**
 * @ingroup Car2x
 *
 * @brief
 *   creates a new WLAN packet (Variant 2)
 *
 * This function creates a new WLAN packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param protocolDesignator
 *   designator of the protocol, which should be used for initialization
 *
 * @return
 *   handle of the created packet or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xInitPacket(char * protocolDesignator);

/**
 * @ingroup Car2x
 *
 * @brief
 *   creates a new WLAN packet (Variant 3)
 *
 * This function creates a new WLAN packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param protocolDesignator
 *   designator of the protocol, which should be used for initialization
 *
 * @param packetTypeDesignator
 *   designator of the packet type
 *
 * @return
 *   handle of the created packet or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xInitPacket(char * protocolDesignator, char * packetTypeDesignator);

/**
 * @ingroup Car2x
 *
 * @brief
 *   creates a new WLAN packet (Variant 4)
 *
 * This function creates a new WLAN packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param packetToCopy
 *   handle of a packet which was created with C2xInitPacket before or handle of a packet which has
 *   been received within a callback function (OnC2xPacket)
 *   The header and the data of this packet are copied to the new created packet.
 *
 * @return
 *   handle of the created packet or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xInitPacket(long packetToCopy);

/**
 * @ingroup Car2x
 *
 * @brief
 *   creates a new WLAN packet (Variant 5)
 *
 * This function creates a new WLAN packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param rawDataLength
 *   length of rawData in byte
 *
 * @param rawData
 *   raw data of a WLAN packet that is used to initialized the new packet
 *
 * @return
 *   handle of the created packet or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xInitPacket(long rawDataLength, byte * rawData);

/**
 * @ingroup Car2x
 *
 * @brief
 *   creates a new WLAN packet (Variant 6)
 *
 * This function creates a new WLAN packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param rawDataLength
 *   length of rawData in byte
 *
 * @param rawData
 *   raw data of a WLAN packet that is used to initialized the new packet
 *
 * @return
 *   handle of the created packet or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xInitPacket(long rawDataLength, char * rawData);

/**
 * @ingroup Car2x
 *
 * @brief
 *   creates a new WLAN packet (Variant 7)
 *
 * This function creates a new WLAN packet. Other functions can access to the newly created packet with the returned handle.
 *
 * Protocol fields that are marked as InitProtocol in the protocol description are initialized.
 *
 * @param rawDataLength
 *   length of rawData in byte
 *
 * @param rawDataStruct
 *   raw data of a WLAN packet that is used to initialized the new packet
 *
 * @return
 *   handle of the created packet or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xInitPacket(long rawDataLength, void * rawDataStruct);

}
