#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the integer value of a token (Variant 1)
 *
 * The function requests the integer value of a token.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "lpvSpeed"
 *
 * @return
 *   integer value of the token or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xGetTokenInt(long packet, char * protocolDesignator, char * tokenDesignator);

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the integer value of a token (Variant 2)
 *
 * The function requests the integer value of a token.
 *
 * Variant (2) with byte offset returns a part of the token data as integer.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "lpvSpeed"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   length of the integer value, must be 1, 2, 3 or 4 byte
 *
 * @param networkByteOrder
 *   - 0 = INTEL (little-endian)
 *   - 1 = MOTOROLA (big-endian)
 *
 * @return
 *   integer value of the token or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xGetTokenInt(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, long networkByteOrder);

}
