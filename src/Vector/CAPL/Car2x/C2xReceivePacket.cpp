#include "C2xReceivePacket.h"

#include <iostream>

namespace capl
{

long C2xReceivePacket(char * onPacketCallback)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
