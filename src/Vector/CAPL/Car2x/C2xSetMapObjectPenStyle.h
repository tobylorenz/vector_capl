#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the pen style of a map object.
 */
long C2xSetMapObjectPenStyle(long handle, double penStyle);

}
