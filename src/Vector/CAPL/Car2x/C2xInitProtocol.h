#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   initializes a protocol for a WLAN packet (Variant 1)
 *
 * The function initializes the protocol for a packet. If necessary further needed lower protocols are
 * initialized, e.g. IPv4. Already initialized higher protocols are deleted.
 *
 * Protocol fields that are marked as InitProtocol in the protocol overview are initialized.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, taken from the protocol overview
 *
 * @return
 *   0 or error code
 */
long C2xInitProtocol(long packet, char * protocolDesignator);

/**
 * @ingroup Car2x
 *
 * @brief
 *   initializes a protocol for a WLAN packet (Variant 2)
 *
 * The function initializes the protocol for a packet. If necessary further needed lower protocols are
 * initialized, e.g. IPv4. Already initialized higher protocols are deleted.
 *
 * Protocol fields that are marked as InitProtocol in the protocol overview are initialized.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, taken from the protocol overview
 *
 * @param packetTypeDesignator
 *   type of the packet, taken from the protocol overview
 *
 * @return
 *   0 or error code
 */
long C2xInitProtocol(long packet, char * protocolDesignator, char * packetTypeDesignator);

}
