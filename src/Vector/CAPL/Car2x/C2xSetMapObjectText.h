#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets text or formatted text for a map object of the type text.
 */
long C2xSetMapObjectText(long handle, char * text);

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets text or formatted text for a map object of the type text.
 */
long C2xSetMapObjectText(long handle, char * formattedText, double value);

}
