#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the value of a bit in a bit string.
 */
long C2xSetTokenBitOfBitString(long packet, char * protocolDesignatorl, char * tokenDesignator, char * namedBit, long value);

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the value of a bit in a bit string.
 */
long C2xSetTokenBitOfBitString(long packet, char * protocolDesignatorl, char * tokenDesignator, long bitPosition, long value);

}
