#include "C2xGetThisValue32.h"

#include <iostream>

namespace capl
{

long C2xGetThisValue32(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
