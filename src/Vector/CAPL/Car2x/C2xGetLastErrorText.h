#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the description of the last error code
 *
 * Gets the error code description of the last called C2x... function.
 *
 * @param bufferSize
 *   size of buffer in which the description text is copied
 *
 * @param buffer
 *   buffer in which the description text is copied
 *
 * @return
 *   number of copied bytes
 */
long C2xGetLastErrorText(dword bufferSize, char * buffer);

}
