#include "C2xRemoveToken.h"

#include <iostream>

namespace capl
{

long C2xRemoveToken(long packet, char * protocolDesignator, char * tokenDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
