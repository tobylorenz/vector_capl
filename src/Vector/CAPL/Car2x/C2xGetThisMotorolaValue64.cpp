#include "C2xGetThisMotorolaValue64.h"

#include <iostream>

namespace capl
{

long C2xGetThisMotorolaValue64(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
