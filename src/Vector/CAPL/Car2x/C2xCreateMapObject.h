#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Initializes a map object to be drawn in the map window.
 */
long C2xCreateMapObject(long objecttype);

}
