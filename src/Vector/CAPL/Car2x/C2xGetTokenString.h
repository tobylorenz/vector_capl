#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets the string value of a token
 *
 * The function copies characters from the token and adds a terminating "\0".
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "cnh"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "header"
 *
 * @param bufferSize
 *   size of buffer in byte
 *   The function adds a terminating "\0". Thus the maximum number of copied characters is bufferSize-1.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied characters or 0
 *   With C2xGetLastError you can check if the function has been processed successfully.
 */
long C2xGetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, long bufferSize, char * buffer);

}
