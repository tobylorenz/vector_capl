#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the heading of a map object.
 */
long C2xSetMapObjectHeading(long handle, double rotation);

}
