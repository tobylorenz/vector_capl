#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the line color of a map object.
 */
long C2xSetMapObjectLineColor(long handle, double lineColor);

}
