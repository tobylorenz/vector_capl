#include "C2xGetTokenData.h"

#include <iostream>

namespace capl
{

long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, byte * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, void * bufferStruct)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, byte * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long C2xGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, void * bufferStruct)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
