#include "C2xGetLastErrorText.h"

#include <iostream>

namespace capl
{

long C2xGetLastErrorText(dword bufferSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
