#include "C2xSetTokenInt64.h"

#include <iostream>

namespace capl
{

long C2xSetTokenInt64(long packet, char * protocolDesignator, char * tokenDesignator, int64 value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long C2xSetTokenInt64(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, long networkByteOrder, int64 value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
