#include "C2xOutputPacket.h"

#include <iostream>

namespace capl
{

long C2xOutputPacket(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
