#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   sets the data of a token (Variant 1)
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use C2xResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "eth"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param data
 *   data that are copied to the token
 *
 * @return
 *   0 or error code
 */
long C2xSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, char * data);

/**
 * @ingroup Car2x
 *
 * @brief
 *   sets the data of a token (Variant 2)
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use C2xResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "eth"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param data
 *   data that are copied to the token
 *
 * @return
 *   0 or error code
 */
long C2xSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, byte * data);

/**
 * @ingroup Car2x
 *
 * @brief
 *   sets the data of a token (Variant 3)
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use C2xResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "eth"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param dataStruct
 *   struct containing the data
 *
 * @return
 *   0 or error code
 */
long C2xSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, void * dataStruct);

/**
 * @ingroup Car2x
 *
 * @brief
 *   sets the data of a token (Variant 4)
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use C2xResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "eth"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param data
 *   data that are copied to the token
 *
 * @return
 *   0 or error code
 */
long C2xSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, char * data);

/**
 * @ingroup Car2x
 *
 * @brief
 *   sets the data of a token (Variant 5)
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use C2xResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "eth"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param data
 *   data that are copied to the token
 *
 * @return
 *   0 or error code
 */
long C2xSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, byte * data);

/**
 * @ingroup Car2x
 *
 * @brief
 *   sets the data of a token (Variant 6)
 *
 * The function sets the data or a part of data of a token.
 *
 * It does not resize the token. Use C2xResizeToken to change the length.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "eth"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied
 *
 * @param dataStruct
 *   struct containing the data
 *
 * @return
 *   0 or error code
 */
long C2xSetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, void * dataStruct);

}
