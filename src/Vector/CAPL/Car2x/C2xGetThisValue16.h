#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   gets received 16 bit value (Intel format)
 *
 * This function reads the data of a received packet in Intel format.
 *
 * It is only usable in a CAPL callback function that had been registered with C2xReceivePacket
 *
 * @param offset
 *   byte offset relative to the beginning of a data packet or the payload (see description above)
 *
 * @return
 *   read value
 */
long C2xGetThisValue16(long offset);

}
