#include "C2xGetThisValue16.h"

#include <iostream>

namespace capl
{

long C2xGetThisValue16(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
