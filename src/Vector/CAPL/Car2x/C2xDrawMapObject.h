#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Draws a map object in the map window with the configured parameters.
 */
long C2xDrawMapObject(long handle);

}
