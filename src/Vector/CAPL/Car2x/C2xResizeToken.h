#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   modifies the length of a token
 *
 * The function sets the length of a token.
 *
 * In case the token length is increased by full bytes, the additional bytes are initialized with 0.
 *
 * @param packet
 *   handle of a packet that has been created with C2xInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "wlan"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param newBitLength
 *   new length of the token in bits
 *
 * @return
 *   0 or error code
 */
long C2xResizeToken(long packet, char * protocolDesignator, char * tokenDesignator, long newBitLength);

}
