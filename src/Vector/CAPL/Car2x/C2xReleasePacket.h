#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   deletes a WLAN packet
 *
 * The function deletes a packet created with C2xInitPacket. The handle can not be used any
 * longer.
 *
 * @param packet
 *   handle of the packet to delete
 *
 * @return
 *   0 or error code
 */
long C2xReleasePacket(long packet);

}
