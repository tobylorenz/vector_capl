#include "C2xReleasePacket.h"

#include <iostream>

namespace capl
{

long C2xReleasePacket(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
