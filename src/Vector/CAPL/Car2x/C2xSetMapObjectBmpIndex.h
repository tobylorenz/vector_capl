#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Car2x
 *
 * @brief
 *   Sets the index of a bitmap in a multi bitmap file.
 */
long C2xSetMapObjectBmpIndex(long handle, long index);

}
