#include "RS232OnReceive.h"

#include <iostream>

namespace capl
{

dword RS232OnReceive(dword port, byte * buffer, dword number)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
