#include "RS232WriteBlock.h"

#include <iostream>

namespace capl
{

dword RS232WriteBlock(dword port, byte * buffer, dword number)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
