#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Callback handler for reception of errors at a serial port.
 *
 * Callback handler for reception of errors at a serial port.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param errorFlags
 *   Cumulative summary of what went wrong. Bits are set to flag conditions.
 *   - 0: Send operation failed.
 *   - 1: Receive operation failed.
 *   - 2: Frame error. May be caused by parity mismatch or any other frame mismatch
 *        (e.g. number of stop bits).
 *   - 3: Frame parity error. Is caused by parity mismatch.
 *   - 4: Buffer overrun. It is not specified if the driver of the sender cannot send fast
 *        enough, if it is up to the receiver which got too much data in too short time or
 *        anything else.
 *   - 5: Buffer overrun at receiver.
 *   - 6: Break state. Other end requested to pause.
 *   - 7: Timeout. May be caused by wrongly set too short timeout. See RS232SetHandshake
 *        for setting the timeout.
 */
void RS232OnError(dword port, dword errorFlags);

}
