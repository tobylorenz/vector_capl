#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Writes block of data to serial port.
 *
 * @deprecated
 *   Replaced by RS232Send
 *
 * Sends a block of bytes to a serial port.
 * - The operation starts the sending of a block.
 * - By default the function works blocking, i.e. it waits for completion.
 * - A CAN.INI switch allows for non-blocking behavior. The non-blocking behavior does not block the RT kernel and allows accurate and timely operation of the RT kernel. Therefore, it is very recommendable to use non-blocking behavior.
 *   INI entry:
 *   [RS232]
 *   BlockingWrite=0
 *   Values: integer
 *   0=non-Blocking, 1=blocking
 * - The callback handler RS232OnSend will notify the node of completion.
 * - To get informed about errors occurring in later stages of the operation use RS232OnError. There are no automatic retrials in case of error.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param buffer
 *   An array of bytes of which number will be sent.
 *
 * @param number
 *   Number of bytes to send.
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        - the serial port with the given number does not exist on the system
 *        - the port has not been opened
 *        - only relevant for non-blocking usage:
 *          if another send operation (this one,RS232WriteByte or RS232Send) has been used shortly before, then the previous send operation may not have finished which leads to an error. Use the RS232OnSend callback handler to synchronize operations.
 *          For non-blocking usage, see accoding section under RS232Send.
 *   - 1: success
 *        In contrast to RS232Send success means here that the operation has really succeeded to transmit data.
 *   - 2: time out, i.e. write access could not be completed till timeout (only relevant for blocking variant, see RS232SetHandshake for setting timeout).
 */
dword RS232WriteBlock(dword port, byte * buffer, dword number);

}
