#pragma once

/**
 * @defgroup RS232 RS232 CAPL Functions
 */

/* RS232 API */
#include "RS232Close.h"
#include "RS232Open.h"
#include "RS232Configure.h"
#include "RS232SetHandshake.h"
#include "RS232Send.h"
#include "RS232Receive.h"
#include "RS232SetSignalLine.h"

/* RS232 API Callback Handlers */
#include "RS232OnSend.h"
#include "RS232OnReceive.h"
#include "RS232OnError.h"

/* Obsolete Functions */
#include "RS232ByteCallback.h"
#include "RS232CloseHandle.h"
#include "RS232EscapeCommExt.h"
#include "RS232EscapeCommFunc.h"
#include "RS232SetCommState.h"
#include "RS232WriteBlock.h"
#include "RS232WriteByte.h"
