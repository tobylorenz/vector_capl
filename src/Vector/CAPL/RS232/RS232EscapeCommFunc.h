#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Sets signal lines on serial port.
 *
 * @deprecated
 *   Replaced by RS232SetSignalLine
 *
 * Sets signal lines on all open serial ports (opened by CANoe/CANalyzer).
 *
 * @param modemControl
 *   Signal lines and levels to bet set on all open ports (opened by CANoe/CANalyzer).
 *   Value : State of DTR : State of RTS
 *   - 0 : 0 : 0
 *   - 1 : 1 : 0
 *   - 2 : 0 : 1
 *   - 3 : 1 : 1
 *   DTR: Data-Terminal-Ready (from sender)
 *   RTS: Request-To-Send (from sender)
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        - the serial port with the given number does not exist on the system
 *        - the port has not been opened
 *   - 1: success
 */
dword RS232EscapeCommFunc(dword modemControl);

}
