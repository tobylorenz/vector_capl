#include "RS232SetHandshake.h"

#include <iostream>

namespace capl
{

dword RS232SetHandshake(dword port, dword handshake, dword XonLimit, dword XoffLimit, dword XonChar, dword XoffChar)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword RS232SetHandshake(dword port, dword handshake, dword XonLimit, dword XoffLimit, dword XonChar, dword XoffChar, dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
