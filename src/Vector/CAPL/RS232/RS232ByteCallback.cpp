#include "RS232ByteCallback.h"

#include <iostream>

namespace capl
{

void RS232ByteCallback(dword port, dword datum, dword note)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
