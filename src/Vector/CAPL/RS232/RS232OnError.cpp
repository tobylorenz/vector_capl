#include "RS232OnError.h"

#include <iostream>

namespace capl
{

void RS232OnError(dword port, dword errorFlags)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
