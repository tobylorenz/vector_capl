#include "RS232Open.h"

#include <iostream>

namespace capl
{

dword RS232Open(dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
