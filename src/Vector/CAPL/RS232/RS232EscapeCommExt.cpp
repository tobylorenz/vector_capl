#include "RS232EscapeCommExt.h"

#include <iostream>

namespace capl
{

dword RS232EscapeCommExt(dword modemControl, dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
