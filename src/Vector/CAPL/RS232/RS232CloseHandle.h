#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Closes serial port.
 *
 * @deprecated
 *   Replaced by RS232Close
 *
 * Closes a serial port.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @return
 *   - 0: error.
 *     The error occurs if the serial port with the given number does not exist on the system.
 *   - 1: success
 */
dword RS232CloseHandle(dword port);

}
