#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Configures serial port. (Form 1)
 *
 * Configures serial port.
 *
 * Without setting up a configuration explicitly, the default configuration is used.
 *
 * Default baud rate: 9600, 8 data bits, 1 stop bit, no parity.
 *
 * If a deprecated INI file exists, then the data of the INI file will be used.
 *
 * With form 2 of this function you can configure a serial port to use parity without
 * letting the operating system verify parity correctness. This form should only be used
 * if the communication to a device was not possible when using parity and form 1 of this function.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param baudrate
 *   The symbol rate to use for reception and transmission.
 *   Typically, 9600 is used. There is a list of possible values which depends on the serial port.
 *   Normally, 115.200 is the allowed maximum.
 *
 * @param numberOfDataBits
 *   The number of data bits within a transmission frame.
 *   8 is used at most. Only values between 5 and 8 are possible. Not all values and not all
 *   combinations with other frame parameters may be supported by the serial port.
 *
 * @param numberOfStopBits
 *   A code which sets the number of stop bits within a transmission frame.
 *   - 1: 1 stop bit is used
 *        (1 has changed meaning compared to obsolete variant RS232SetCommState)
 *   - 2: 2 stop bits are used
 *
 * @param parity
 *   A code which identifies the parity mode to use.
 *   - 0: no parity used, i.e. frame contains no parity bit
 *   - 1: odd parity
 *   - 2: even parity
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        the serial port with the given number does not exist on the system
 *        the port has not been opened
 *   - 1: success
 */
dword RS232Configure(dword port, dword baudrate, dword numberOfDataBits, dword numberOfStopBits, dword parity); // Configures serial port (everything besides handshake).

/**
 * @ingroup RS232
 *
 * @brief
 *   Configures serial port. (Form 2)
 *
 * Configures serial port.
 *
 * Without setting up a configuration explicitly, the default configuration is used.
 *
 * Default baud rate: 9600, 8 data bits, 1 stop bit, no parity.
 *
 * If a deprecated INI file exists, then the data of the INI file will be used.
 *
 * With form 2 of this function you can configure a serial port to use parity without
 * letting the operating system verify parity correctness. This form should only be used
 * if the communication to a device was not possible when using parity and form 1 of this function.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param baudrate
 *   The symbol rate to use for reception and transmission.
 *   Typically, 9600 is used. There is a list of possible values which depends on the serial port.
 *   Normally, 115.200 is the allowed maximum.
 *
 * @param numberOfDataBits
 *   The number of data bits within a transmission frame.
 *   8 is used at most. Only values between 5 and 8 are possible. Not all values and not all
 *   combinations with other frame parameters may be supported by the serial port.
 *
 * @param numberOfStopBits
 *   A code which sets the number of stop bits within a transmission frame.
 *   - 1: 1 stop bit is used
 *        (1 has changed meaning compared to obsolete variant RS232SetCommState)
 *   - 2: 2 stop bits are used
 *
 * @param parity
 *   A code which identifies the parity mode to use.
 *   - 0: no parity used, i.e. frame contains no parity bit
 *   - 1: odd parity
 *   - 2: even parity
 *
 * @param enableParityCheck
 *   Indicates whether to enable or disable the parity verification.
 *   This parameter is ignored if the parameter parity is set to 0 (no parity used).
 *   - 0: parity verification disabled
 *   - !=0: parity verification enabled
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        the serial port with the given number does not exist on the system
 *        the port has not been opened
 *   - 1: success
 */
dword RS232Configure(dword port, dword baudrate, dword numberOfDataBits, dword numberOfStopBits, dword parity, dword enableParityCheck); // Configures serial port (everything besides handshake).

}
