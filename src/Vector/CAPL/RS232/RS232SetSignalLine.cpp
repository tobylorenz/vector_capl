#include "RS232SetSignalLine.h"

#include <iostream>

namespace capl
{

dword RS232SetSignalLine(dword port, dword line, dword state)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
