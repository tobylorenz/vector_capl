#include "RS232Close.h"

#include <iostream>

namespace capl
{

dword RS232Close(dword port)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
