#include "RS232Receive.h"

#include <iostream>

namespace capl
{

dword RS232Receive(dword port, byte * buffer, dword size)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
