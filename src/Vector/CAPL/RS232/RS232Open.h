#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Opens a serial port.
 *
 * Opens a serial port.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        the serial port with the given number does not exist on the system
 *        another program uses the serial port according to the port number
 *   - 1: success
 */
dword RS232Open(dword port);

}
