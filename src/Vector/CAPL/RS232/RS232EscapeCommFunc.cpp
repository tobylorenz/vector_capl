#include "RS232EscapeCommFunc.h"

#include <iostream>

namespace capl
{

dword RS232EscapeCommFunc(dword modemControl)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
