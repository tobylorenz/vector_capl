#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup RS232
 *
 * @brief
 *   Sets handshake parameters on serial port.
 *
 * Sets handshake parameters on serial port.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param handshake
 *   Sets variant of handshake to use.
 *   Allowed values (internal bit field leads to strange values):
 *   - 0: no handshake at all
 *   - 10: hardware handshake, use DTR<->DSR
 *   - 33: hardware handshake, use RTS<->CTS
 *   - 65: hardware handshake, use RTS<->CTS, use "toggle" variant
 *   - 128: software handshake, use Xon and Xoff characters
 *   DTR: Data-Terminal-Ready (from sender).
 *   RTS: Request-To-Send (from sender).
 *   DSR: Data-Set-Ready (from receiver).
 *   CTS: Clear-To-Send (from receiver).
 *
 * @param XonLimit
 *   Parameter for software flow control. It specifies a limit after which flow control shall
 *   become active (send XonChar). It relates to the receiver buffer (of the driver) and defines
 *   the buffer level in bytes for starting transfer requests.
 *
 * @param XoffLimit
 *   Parameter for software flow control. It specifies a limit after which flow control shall
 *   become active (send XoffChar). It relates to the receiver buffer (of the driver) and
 *   defines the level margin from the buffer size for starting hold requests.
 *
 * @param XonChar
 *   Parameter for software flow control. It the symbol sent to signalize start of operations
 *   (for reception as well as transmission).
 *
 * @param XoffChar
 *   Parameter for software flow control. It the symbol sent to signalize start of operations
 *   (for reception as well as transmission).
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        the serial port with the given number does not exist on the system
 *        the port has not been opened
 *   - 1: success
 */
dword RS232SetHandshake(dword port, dword handshake, dword XonLimit, dword XoffLimit, dword XonChar, dword XoffChar); // Configures handshake parameters.

/**
 * @ingroup RS232
 *
 * @brief
 *   Sets handshake parameters on serial port. (Obsolete)
 *
 * Sets handshake parameters on serial port.
 *
 * @param port
 *   A number between 1 and 255 identifying a serial port.
 *
 * @param handshake
 *   Sets variant of handshake to use.
 *   Allowed values (internal bit field leads to strange values):
 *   - 0: no handshake at all
 *   - 10: hardware handshake, use DTR<->DSR
 *   - 33: hardware handshake, use RTS<->CTS
 *   - 65: hardware handshake, use RTS<->CTS, use "toggle" variant
 *   - 128: software handshake, use Xon and Xoff characters
 *   DTR: Data-Terminal-Ready (from sender).
 *   RTS: Request-To-Send (from sender).
 *   DSR: Data-Set-Ready (from receiver).
 *   CTS: Clear-To-Send (from receiver).
 *
 * @param XonLimit
 *   Parameter for software flow control. It specifies a limit after which flow control shall
 *   become active (send XonChar). It relates to the receiver buffer (of the driver) and defines
 *   the buffer level in bytes for starting transfer requests.
 *
 * @param XoffLimit
 *   Parameter for software flow control. It specifies a limit after which flow control shall
 *   become active (send XoffChar). It relates to the receiver buffer (of the driver) and
 *   defines the level margin from the buffer size for starting hold requests.
 *
 * @param XonChar
 *   Parameter for software flow control. It the symbol sent to signalize start of operations
 *   (for reception as well as transmission).
 *
 * @param XoffChar
 *   Parameter for software flow control. It the symbol sent to signalize start of operations
 *   (for reception as well as transmission).
 *
 * @param timeout
 *   Timeout in milliseconds to be used for all send and receive operations.
 *   - -1: infinite
 *   - <10: is not allowed
 *   If this parameter isn't set, the timeout default value will be used: 5 sec.
 *
 * @return
 *   - 0: error
 *        The error occurs if
 *        the serial port with the given number does not exist on the system
 *        the port has not been opened
 *   - 1: success
 */
dword RS232SetHandshake(dword port, dword handshake, dword XonLimit, dword XoffLimit, dword XonChar, dword XoffChar, dword timeout); // Configures handshake parameters. (obsolete)

}
