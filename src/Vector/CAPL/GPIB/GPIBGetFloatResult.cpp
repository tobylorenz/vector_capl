#include "GPIBGetFloatResult.h"

#include <iostream>

namespace capl
{

double GPIBGetFloatResult(char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
