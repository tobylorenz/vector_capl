#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Helps functions for extracting numeric values from GPIB response strings.
 *
 * Helper functions for extracting numeric values from GPIB response strings.
 *
 * @param buffer
 *   The string in buffer, which may have been obtained by the GPIBResponse function, is
 *   searched for a floating-point number, using the function strtod of C/C++. If a space
 *   character is present in the string, the search for the number starts at the first such
 *   character.
 *
 * @return
 *   Extracted float value.
 *   0: no representation of a value has been found.
 *   -1: buffer is an empty string.
 */
double GPIBGetFloatResult(char * buffer);

}
