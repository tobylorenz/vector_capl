#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Returns the number of bytes transferred by the last GPIB operation.
 *
 * Returns the number of bytes transferred by the last GPIB operation.
 *
 * @return
 *   Number of bytes transmitted.
 *   -1: on error, if no GPIB driver is available
 */
long GPIBGetCnt(void);

}
