#include "GPIBGetStatus.h"

#include <iostream>

namespace capl
{

long GPIBGetStatus(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
