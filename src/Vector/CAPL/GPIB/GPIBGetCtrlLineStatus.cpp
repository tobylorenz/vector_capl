#include "GPIBGetCtrlLineStatus.h"

#include <iostream>

namespace capl
{

long GPIBGetCtrlLineStatus(long boardIdx)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
