#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Changes the secondary address of a device.
 *
 * Changes the secondary address of a device.
 *
 * @param deviceDescriptor
 *   Device ID
 *
 * @param newAddr
 *   New secondary address
 *
 * @return
 *   Status
 *   -1: on error, if no GPIB driver is available
 */
long GPIBDevChangeSecAddr(long deviceDescriptor, long newAddr);

}
