#include "GPIBWriteNum.h"

#include <iostream>

namespace capl
{

long GPIBWriteNum(long deviceDescriptor, char * cmdStr, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
