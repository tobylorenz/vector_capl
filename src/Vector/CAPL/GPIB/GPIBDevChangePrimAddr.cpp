#include "GPIBDevChangePrimAddr.h"

#include <iostream>

namespace capl
{

long GPIBDevChangePrimAddr(long deviceDescriptor, long newAddr)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
