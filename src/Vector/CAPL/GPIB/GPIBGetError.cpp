#include "GPIBGetError.h"

#include <iostream>

namespace capl
{

long GPIBGetError(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
