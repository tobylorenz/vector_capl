#include "GPIBGetCnt.h"

#include <iostream>

namespace capl
{

long GPIBGetCnt(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
