#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Returns the status word.
 *
 * Returns the status word.
 *
 * @return
 *   Current status
 *   -1: on error, if no GPIB driver is available
 */
long GPIBGetStatus(void);

}
