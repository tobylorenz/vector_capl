#include "GPIBResponse.h"

#include <iostream>

namespace capl
{

void GPIBResponse(long deviceDescriptor, char * queryString, char * resultString)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
