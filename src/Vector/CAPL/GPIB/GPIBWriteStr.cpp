#include "GPIBWriteStr.h"

#include <iostream>

namespace capl
{

long GPIBWriteStr(long deviceDescriptor, char * cmdStr)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
