#include "GPIBDevOpen.h"

#include <iostream>

namespace capl
{

long GPIBDevOpen(long boardIdx, long primAddr, long secAddr, long timeout, long eot, long eos)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
