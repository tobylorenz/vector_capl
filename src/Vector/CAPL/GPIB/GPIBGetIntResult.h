#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Helps functions for extracting numeric values from GPIB response strings.
 *
 * Helper functions for extracting numeric values from GPIB response strings.
 *
 * @param buffer
 *   The string in buffer, which may have been obtained by the GPIBResponse function, is
 *   searched for an integer number: All non-digit characters in the string as well as all digits
 *   found after the first comma character or decimal point are ignored, when assembling the
 *   integer number.
 *
 * @return
 *   Extracted integer value
 */
long GPIBGetIntResult(char * buffer);

}
