#include "GPIBOnError.h"

#include <iostream>

namespace capl
{

void GPIBOnError(long deviceDescriptor, char * query, char * response, long status, long error)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
