#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Writes GPIB command to the device.
 *
 * Writes cmdStr to the device.
 *
 * The function returns immediately and does not wait for the end of the command
 * transmission.
 *
 * @param deviceDescriptor
 *   Device ID
 *
 * @param cmdStr
 *   GPIB command
 *
 * @return
 *   Status
 *   -1: on error, if no GPIB driver is available
 */
long GPIBWriteStr(long deviceDescriptor, char * cmdStr);

}
