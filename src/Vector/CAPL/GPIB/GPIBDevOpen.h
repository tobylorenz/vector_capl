#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Opens a GPIB device and returns a device ID.
 *
 * Opens a GPIB device and returns a device ID.
 *
 * @param boardIdx
 *   Index of the GPIB board (e.g.0)
 *
 * @param primAddr
 *   Primary address of the GPIB device (0..30)
 *
 * @param secAddr
 *   Secondary address of the GPIB device (96..126, typical: 0)
 *
 * @param timeout
 *   Timeout of the device.
 *   This indicates the maximum duration of an I/O operation. If this time expires before the
 *   operation has ended the function rolls back with a timeout error in the status word.
 *   Attention: The value for Timeout must be > 0! (typically: 10 s)
 *   - 0: Timeout disabled. Do not use!
 *   - 1: Timeout = 10 us
 *   - 2: Timeout = 30 us
 *   - 3: Timeout = 100 us
 *   - 4: Timeout = 300 us
 *   - 5: Timeout = 1 ms
 *   - 6: Timeout = 3 ms
 *   - 7: Timeout = 10 ms
 *   - 8: Timeout = 30 ms
 *   - 9: Timeout = 100 ms
 *   - 10: Timeout = 300 ms
 *   - 11: Timeout = 1 s
 *   - 12: Timeout = 3 s
 *   - 13: Timeout = 10 s
 *   - 14: Timeout = 30 s
 *   - 15: Timeout = 100 s
 *   - 16: Timeout = 300 s
 *   - 17: Timeout = 1000 s
 *
 * @param eot
 *   Indicates whether the GPIB EOI line should be set at the end of a write operation.
 *   Values: 0, 1 (typical: 1)
 *
 * @param eos
 *   Configuration of "end-of-string" behavior.
 *   For further information please refer to documentation for your GPIB controller.
 *   Typical value: 0
 *
 * @return
 *   Device ID
 *   -1: on error
 *     - if no GPIB driver is available, or
 *     - if the driver is not properly installed, or configured incorrectly, or
 *     - if the function is called outside the PreStart function of a CAPL program.
 */
long GPIBDevOpen(long boardIdx, long primAddr, long secAddr, long timeout, long eot, long eos);

}
