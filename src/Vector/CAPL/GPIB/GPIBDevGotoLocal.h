#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup GPIB
 *
 * @brief
 *   Sets a device into local mode.
 *
 * Sets a device into local mode. A successor GPIB command sets the device back into
 * remote mode.
 *
 * @param deviceDescriptor
 *   Device ID
 *
 * @return
 *   Status
 *   -1: on error, if no GPIB driver is available
 */
long GPIBDevGotoLocal(long deviceDescriptor);

}
