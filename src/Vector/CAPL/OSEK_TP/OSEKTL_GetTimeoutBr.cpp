#include "OSEKTL_GetTimeoutBr.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTimeoutBr(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
