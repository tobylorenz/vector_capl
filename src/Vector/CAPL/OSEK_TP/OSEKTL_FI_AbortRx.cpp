#include "OSEKTL_FI_AbortRx.h"

#include <iostream>

namespace capl
{

void OSEKTL_FI_AbortRx(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
