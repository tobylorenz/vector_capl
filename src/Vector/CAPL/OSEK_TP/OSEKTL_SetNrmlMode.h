#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Toggles to normal addressing
 */
void OSEKTL_SetNrmlMode(void);

}
