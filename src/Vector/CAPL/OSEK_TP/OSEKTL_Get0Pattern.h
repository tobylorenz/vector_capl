#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the pattern, default is 0x00
 */
long OSEKTL_Get0Pattern(void);

}
