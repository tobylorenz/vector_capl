#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Error message from transport layer
 *
 * @param error
 *   - 1: Timeout waiting for CF
 *   - 2: Timeout waiting for FC
 *   - 3: Received Wrong SN
 *   - 4: TxBusy, only one transmission possible at
 *        the same time
 *   - 5: Received unexpected PDU
 *   - 6: Timeout while trying to send a CAN frame
 *   - 7: Too many FC.WFT sent
 *   - 8: Receiver buffer overflow
 *   - 9: Wrong parameter
 *   - 10: Invalid flow status received
 *   - 11: Transfer has been aborted explicitly
 *   - <0: Unknown Error
 */
void OSEKTL_ErrorInd(int error);

}
