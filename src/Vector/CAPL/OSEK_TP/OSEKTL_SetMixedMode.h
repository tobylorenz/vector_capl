#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Toggles to mixed addressing
 */
void OSEKTL_SetMixedMode(void);

}
