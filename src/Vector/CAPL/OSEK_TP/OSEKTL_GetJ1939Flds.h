#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   The "Get" function returns the 10 bit from the CAN
 *   ID, i.e. bits 16 to 25.
 */
long OSEKTL_GetJ1939Flds(void);

}
