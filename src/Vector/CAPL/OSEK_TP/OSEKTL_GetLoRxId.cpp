#include "OSEKTL_GetLoRxId.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetLoRxId(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
