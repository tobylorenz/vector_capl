#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @return
 *   Flow Control is
 *   - 1: used
 *   - 0: not used
 */
long OSEKTL_IsUseFC(void);

}
