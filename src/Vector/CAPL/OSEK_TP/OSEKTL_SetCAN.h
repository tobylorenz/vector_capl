#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the channel on which a node receives and sends
 *   frames. Only works properly in 'PreStart'
 */
void OSEKTL_SetCAN(long can);

}
