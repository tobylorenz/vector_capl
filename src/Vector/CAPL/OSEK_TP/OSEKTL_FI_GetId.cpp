#include "OSEKTL_FI_GetId.h"

#include <iostream>

namespace capl
{

dword OSEKTL_FI_GetId(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
