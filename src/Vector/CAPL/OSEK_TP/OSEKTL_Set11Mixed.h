#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Toggles to 11 bit mixed addressing
 */
void OSEKTL_Set11Mixed(void);

}
