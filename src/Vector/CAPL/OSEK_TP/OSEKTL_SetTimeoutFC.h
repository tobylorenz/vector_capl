#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the timeout for flow control messages
 */
void OSEKTL_SetTimeoutFC(long t);

}
