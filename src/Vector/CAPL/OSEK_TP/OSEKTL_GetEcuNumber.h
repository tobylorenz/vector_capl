#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the active address (Number) of the controller
 */
long OSEKTL_GetEcuNumber(void);

}
