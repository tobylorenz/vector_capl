#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the address extension of the most recent reception
 *   for (11 bit) Mixed mode
 *   (available after reception of the FirstFrame).
 *   -1 indicates that no such value is available.
 */
long OSEKTL_GetRecentAE(void);

}
