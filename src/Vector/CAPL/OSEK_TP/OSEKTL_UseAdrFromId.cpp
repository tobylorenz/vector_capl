#include "OSEKTL_UseAdrFromId.h"

#include <iostream>

namespace capl
{

void OSEKTL_UseAdrFromId(long val)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
