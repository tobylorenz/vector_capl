#include "OSEKTL_GetMsgCount.h"

#include <iostream>

namespace capl
{

dword OSEKTL_GetMsgCount(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
