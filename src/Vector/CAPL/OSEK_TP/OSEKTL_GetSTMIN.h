#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the active Stmin
 */
long OSEKTL_GetSTMIN(void);

}
