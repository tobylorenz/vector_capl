#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Retrieves the received data
 */
void OSEKTL_GetRxData(byte* rxData, int bufferSize);
}
