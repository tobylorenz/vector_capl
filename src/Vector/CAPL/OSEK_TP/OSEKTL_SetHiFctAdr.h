#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the highest functional address (Diagnosis)
 */
void OSEKTL_SetHiFctAdr(long h);

}
