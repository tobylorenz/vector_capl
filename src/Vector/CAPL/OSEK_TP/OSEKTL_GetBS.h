#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the active block size (requested as receiver)
 */
long OSEKTL_GetBS(void);

}
