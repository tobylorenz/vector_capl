#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Reads the timeout value Bs [ms]
 *   (described in ISO/TF2)
 */
long OSEKTL_GetTimeoutBs(void);

}
