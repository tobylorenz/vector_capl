#include "OSEKTL_GetTxPrio.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTxPrio(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
