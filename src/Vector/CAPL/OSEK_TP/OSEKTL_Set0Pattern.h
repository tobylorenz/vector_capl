#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the pattern to be used for zero padding
 */
void OSEKTL_Set0Pattern(long pattern);

}
