#include "OSEKTL_SetEvalAllFC.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetEvalAllFC(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
