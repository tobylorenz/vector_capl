#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Switches zero padding off. This is the default
 */
void OSEKTL_Clr0Padding(void);

}
