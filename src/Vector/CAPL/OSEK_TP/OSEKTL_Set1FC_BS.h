#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   According to ISO, BS=0 means that there is only one
 *   FC (the one after the FF).
 *   Some manufacturers decided, to give BS=255 this
 *   meaning.
 */
void OSEKTL_Set1FC_BS(byte bs);

}
