#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the highest functional address (Diagnosis)
 */
long OSEKTL_GetHiFctAdr(void);

}
