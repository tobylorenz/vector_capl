#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Priority for FC will be used
 *
 * @return
 *   - 0: as configured
 *   - 1: as received in FF
 */
void OSEKTL_SetUseFFPrio(long prio);

}
