#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Set the delay of all FC frames to this value.
 *   (default: 0)
 */
void OSEKTL_SetFCDelay(dword milliseconds);

}
