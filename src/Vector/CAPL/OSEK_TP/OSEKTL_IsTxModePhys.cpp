#include "OSEKTL_IsTxModePhys.h"

#include <iostream>

namespace capl
{

long OSEKTL_IsTxModePhys(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
