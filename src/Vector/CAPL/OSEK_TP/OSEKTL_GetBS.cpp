#include "OSEKTL_GetBS.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetBS(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
