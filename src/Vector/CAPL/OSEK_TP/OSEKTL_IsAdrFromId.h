#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @return
 *   - 1: target address calculated from CAN identifier
 *   - 0: target address from OSEKTL_SetTgtAdr(ta)
 */
long OSEKTL_IsAdrFromId(void);

}
