#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the active Target Address
 */
long OSEKTL_GetTgtAdr(void);

}
