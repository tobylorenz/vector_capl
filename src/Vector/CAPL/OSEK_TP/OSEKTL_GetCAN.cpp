#include "OSEKTL_GetCAN.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetCAN(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
