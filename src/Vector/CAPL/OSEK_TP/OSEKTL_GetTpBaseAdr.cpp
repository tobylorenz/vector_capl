#include "OSEKTL_GetTpBaseAdr.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTpBaseAdr(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
