#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Length (dlc) of the CAN frames is always 8, independent
 *   of the amount of data to be transmitted. This is the
 *   default behavior.
 */
void OSEKTL_SetDLC8(void);

}
