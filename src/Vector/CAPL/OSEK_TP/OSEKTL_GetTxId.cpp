#include "OSEKTL_GetTxId.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTxId(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
