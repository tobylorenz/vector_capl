#include "OSEKTL_GetAdrExt.h"

#include <iostream>

namespace capl
{

byte OSEKTL_GetAdrExt(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
