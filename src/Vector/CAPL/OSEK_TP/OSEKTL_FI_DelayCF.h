#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Delay a Consecutive Frame, where 1 is
 *   the first one, etc.
 */
void OSEKTL_FI_DelayCF(
        long NumberOfDelayedCF,
        unsigned long DelayInMsForThisCF);

}
