#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @return
 *   - 1: Id calculated from ECU + BaseAdr
 *   - 0: Id from OSEKTL_SetTxId
 */
long OSEKTL_IsIdFromAdr(void);

}
