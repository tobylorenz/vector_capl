#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   The call will only affect future transmissions
 *   and receptions!
 *
 * @param flag
 *   - 0: Use USDT mode (default)
 *   - 1: Activate ASDT mode
 */
void OSEKTL_ActivateAck(long flag);

}
