#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Return the maximum TP message length the node will
 *   accept without sending an overflow frame. (default:
 *   4095)
 */
long OSEKTL_GetMaxMsgLen(void);

}
