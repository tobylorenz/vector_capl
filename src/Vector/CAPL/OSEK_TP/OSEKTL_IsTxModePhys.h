#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @return
 *   1 if Tx mode is physical or else 0
 */
long OSEKTL_IsTxModePhys(void);

}
