#include "OSEKTL_UseBSOfFC.h"

#include <iostream>

namespace capl
{

void OSEKTL_UseBSOfFC(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
