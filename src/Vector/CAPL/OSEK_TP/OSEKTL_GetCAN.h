#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Reads the channel on which a node receives and sends
 *   frames
 */
long OSEKTL_GetCAN(void);

}
