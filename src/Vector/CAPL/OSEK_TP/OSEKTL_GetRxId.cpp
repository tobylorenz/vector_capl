#include "OSEKTL_GetRxId.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetRxId(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
