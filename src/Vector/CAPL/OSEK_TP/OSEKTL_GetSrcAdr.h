#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the Source Address of the last received
 *   message
 */
long OSEKTL_GetSrcAdr(void);

}
