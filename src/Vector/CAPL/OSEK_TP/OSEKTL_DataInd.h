#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the number of data received
 */
void OSEKTL_DataInd(long rxLength);

}
