#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Set the block size used by the sender to a fixed value,
 *   i.e. the BS returned in the FC.CTS is ignored.
 */
void OSEKTL_SetFixedBS(long blocksize);

}
