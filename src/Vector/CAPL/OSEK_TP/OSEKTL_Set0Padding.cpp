#include "OSEKTL_Set0Padding.h"

#include <iostream>

namespace capl
{

void OSEKTL_Set0Padding(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
