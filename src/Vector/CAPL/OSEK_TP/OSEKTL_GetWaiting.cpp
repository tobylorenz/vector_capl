#include "OSEKTL_GetWaiting.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetWaiting(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
