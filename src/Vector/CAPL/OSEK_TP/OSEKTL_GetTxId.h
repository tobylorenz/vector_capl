#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the active transmit ID
 */
long OSEKTL_GetTxId(void);

}
