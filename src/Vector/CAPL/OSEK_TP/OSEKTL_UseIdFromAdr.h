#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Usually, the CAN-ID is derived from the sender address
 *   (and the base address). After the call of this function
 *   with val=0 the CAN-ID is not automatically calculated
 *   and can be set using the function
 *   OSEKTL_SetTxId(canId), (like in normal addressing
 *   mode).
 *   1: Switches back to default behavior, that is the CANID
 *   is derived from the base address and the sender address.
 */
void OSEKTL_UseIdFromAdr(long val);

}
