#include "OSEKTL_SetDlcVar.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetDlcVar(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
