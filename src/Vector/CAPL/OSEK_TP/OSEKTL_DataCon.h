#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the number of data successfully sent
 */
void OSEKTL_DataCon(long txLength);

}
