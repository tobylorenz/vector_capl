#include "OSEKTL_FI_IsFC.h"

#include <iostream>

namespace capl
{

long OSEKTL_FI_IsFC(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
