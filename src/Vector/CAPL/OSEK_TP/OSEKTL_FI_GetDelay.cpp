#include "OSEKTL_FI_GetDelay.h"

#include <iostream>

namespace capl
{

dword OSEKTL_FI_GetDelay(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
