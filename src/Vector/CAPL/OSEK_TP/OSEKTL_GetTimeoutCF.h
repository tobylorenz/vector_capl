#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the time up to which a Consecutive Frame (CF)
 *   must have arrived
 */
long OSEKTL_GetTimeoutCF(void);

}
