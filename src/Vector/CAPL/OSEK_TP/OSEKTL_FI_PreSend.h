#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Length of message and message content
 *
 * @param dlc
 *   length 1
 *
 * @param message
 *   length 8 byte
 */
void OSEKTL_FI_PreSend(word * dlc, byte * message);

}
