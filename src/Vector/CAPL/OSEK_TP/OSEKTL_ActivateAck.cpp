#include "OSEKTL_ActivateAck.h"

#include <iostream>

namespace capl
{

void OSEKTL_ActivateAck(long flag)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
