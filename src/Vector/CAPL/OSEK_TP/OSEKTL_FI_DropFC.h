#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Do not send one FlowControl frame,
 *   where 0 is the first one, etc.
 */
void OSEKTL_FI_DropFC(long NumberOfDroppedFC);

}
