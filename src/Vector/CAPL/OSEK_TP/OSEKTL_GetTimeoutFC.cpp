#include "OSEKTL_GetTimeoutFC.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTimeoutFC(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
