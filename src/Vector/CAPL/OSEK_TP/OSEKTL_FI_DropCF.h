#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Do not send one ConsecutiveFrame
 */
void OSEKTL_FI_DropCF(long NumberOfCFToBeLost);

}
