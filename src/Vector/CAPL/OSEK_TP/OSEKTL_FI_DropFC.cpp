#include "OSEKTL_FI_DropFC.h"

#include <iostream>

namespace capl
{

void OSEKTL_FI_DropFC(long NumberOfDroppedFC)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
