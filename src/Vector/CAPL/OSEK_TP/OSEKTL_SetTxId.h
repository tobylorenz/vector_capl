#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the transmit ID
 */
void OSEKTL_SetTxId(long id);

}
