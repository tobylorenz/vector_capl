#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Get the number of messages considered.
 */
dword OSEKTL_GetMsgCount(void);

}
