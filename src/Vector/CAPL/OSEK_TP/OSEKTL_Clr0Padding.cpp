#include "OSEKTL_Clr0Padding.h"

#include <iostream>

namespace capl
{

void OSEKTL_Clr0Padding(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
