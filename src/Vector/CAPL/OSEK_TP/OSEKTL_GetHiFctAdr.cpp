#include "OSEKTL_GetHiFctAdr.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetHiFctAdr(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
