#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @return
 *   - 0: Minimum DLC for CAN frames used
 *   - x=1-8: At least DLC x used
 *
 */
long OSEKTL_GetFixedDLC(void);

}
