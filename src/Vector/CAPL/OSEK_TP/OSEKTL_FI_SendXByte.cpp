#include "OSEKTL_FI_SendXByte.h"

#include <iostream>

namespace capl
{

void OSEKTL_FI_SendXByte(
        long NumberOfBytesToSendTillStop,
        unsigned long MinLenLastFrame,
        long FillValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
