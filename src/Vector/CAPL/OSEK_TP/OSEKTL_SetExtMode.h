#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Toggles to extended addressing
 */
void OSEKTL_SetExtMode(void);

}
