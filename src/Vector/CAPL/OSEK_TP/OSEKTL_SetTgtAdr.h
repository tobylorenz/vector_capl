#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the Target Address
 */
void OSEKTL_SetTgtAdr(long ta);

}
