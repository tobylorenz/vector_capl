#include "OSEKTL_GetJ1939Flds.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetJ1939Flds(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
