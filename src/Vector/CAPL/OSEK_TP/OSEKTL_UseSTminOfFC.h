#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   STmin of Flow Control messages is used. This is the
 *   default behavior
 */
void OSEKTL_UseSTminOfFC(void);

}
