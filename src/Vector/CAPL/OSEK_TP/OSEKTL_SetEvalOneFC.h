#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Only the First FC is evaluated, as specified in ISO.
 */
void OSEKTL_SetEvalOneFC(void);

}
