#include "OSEKTL_GetTimeoutBs.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetTimeoutBs(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
