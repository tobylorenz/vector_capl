#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Double a frame, where 0 is the
 *   FirstFrame, 1 the first ConsecutiveFrame
 *   etc.
 */
void OSEKTL_FI_DoubleTx(long NumberOfFrameToBeDoubled);

}
