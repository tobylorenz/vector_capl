#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Set the maximum number of FC.WT frames the receiver
 *   will send before aborting the reception. (default: 10)
 */
void OSEKTL_SetWFTMax(dword WFTmax);

}
