#include "OSEKTL_FI_GetPCIOff.h"

#include <iostream>

namespace capl
{

dword OSEKTL_FI_GetPCIOff(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
