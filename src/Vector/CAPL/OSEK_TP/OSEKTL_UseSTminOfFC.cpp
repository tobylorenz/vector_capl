#include "OSEKTL_UseSTminOfFC.h"

#include <iostream>

namespace capl
{

void OSEKTL_UseSTminOfFC(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
