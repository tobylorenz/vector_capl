#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Returns the delay (us) the message will
 *   experience if the value is not changed with
 *   OSEKTL_FI_SetDelay.
 */
dword OSEKTL_FI_GetDelay(void);

}
