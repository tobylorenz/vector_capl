#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Prohibit Fault Injection
 */
void OSEKTL_FI_Disable(void);

}
