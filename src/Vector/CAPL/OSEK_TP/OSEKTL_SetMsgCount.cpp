#include "OSEKTL_SetMsgCount.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetMsgCount(dword n)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
