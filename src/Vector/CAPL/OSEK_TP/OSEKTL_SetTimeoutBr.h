#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the timeout value Br
 *   (described in ISO/TF2) to val ms
 */
void OSEKTL_SetTimeoutBr(long val);

}
