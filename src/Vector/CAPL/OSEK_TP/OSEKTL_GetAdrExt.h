#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Gets the address extension
 */
byte OSEKTL_GetAdrExt(void);

}
