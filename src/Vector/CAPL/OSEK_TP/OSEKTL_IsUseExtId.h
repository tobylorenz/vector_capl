#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @return
 *   - 1: Nodes use Extended CAN IDs
 *   - 0: Nodes use Standard CAN IDs
 */
long OSEKTL_IsUseExtId(void);

}
