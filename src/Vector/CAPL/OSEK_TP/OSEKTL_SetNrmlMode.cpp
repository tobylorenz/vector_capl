#include "OSEKTL_SetNrmlMode.h"

#include <iostream>

namespace capl
{

void OSEKTL_SetNrmlMode(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
