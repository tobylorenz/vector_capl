#include "OSEKTL_GetUseAllAE.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetUseAllAE(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
