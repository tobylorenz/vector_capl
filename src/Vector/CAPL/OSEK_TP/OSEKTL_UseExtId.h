#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @param val
 *   - 1: Nodes use Extended CAN IDs
 *   - 0: Nodes use Standard CAN IDs
 */
void OSEKTL_UseExtId(long val);

}
