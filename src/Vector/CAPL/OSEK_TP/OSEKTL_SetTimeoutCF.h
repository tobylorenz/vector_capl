#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Sets the timeout for Consecutive Frames
 */
void OSEKTL_SetTimeoutCF(long t);

}
