#include "OSEKTL_GetLoFctAdr.h"

#include <iostream>

namespace capl
{

long OSEKTL_GetLoFctAdr(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
