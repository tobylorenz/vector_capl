#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Switches zero padding on
 */
void OSEKTL_Set0Padding(void);

}
