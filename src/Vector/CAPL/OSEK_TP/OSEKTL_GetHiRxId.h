#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   Highest CAN ID which passes Rx Filter
 */
long OSEKTL_GetHiRxId(void);

}
