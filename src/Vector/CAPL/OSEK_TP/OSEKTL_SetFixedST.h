#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_TP
 *
 * @brief
 *   st is used as separation time instead of STmin.
 *   If st < STmin, this results in a protocol violation
 */
void OSEKTL_SetFixedST(long st);

}
