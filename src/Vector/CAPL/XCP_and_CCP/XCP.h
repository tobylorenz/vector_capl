#pragma once

/**
 * @ingroup XCP_and_CCP
 * @{
 * @defgroup XCP XCP CAPL Functions
 * @}
 */

#include "OnXCPConnect.h"
#include "OnXcpError.h"
#include "OnXcpGetCalPage.h"
#include "OnXcpSetCalPage.h"
#include "OnXcpUpload.h"
#include "OnXcpUserCommand.h"
#include "xcpActivate.h"
#include "xcpConnect.h"
#include "xcpDeactivate.h"
#include "xcpDisconnect.h"
#include "xcpGetCalPage.h"
#include "xcpIsConnected.h"
#include "xcpSetCalPage.h"
#include "xcpUpload.h"
#include "xcpUserCommand.h"
