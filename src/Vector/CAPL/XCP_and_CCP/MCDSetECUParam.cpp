#include "MCDSetECUParam.h"

#include <iostream>

namespace capl
{

long MCDSetECUParam(char * moduleName, char * parameterName, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
