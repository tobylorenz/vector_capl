#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Initialisation of the MCD server. CANape will be started in non modal mode.
 *
 * Initialisation of the MCD server. CANape will be started in non modal mode.
 *
 * This function must only be called during the initialisation phase of the measurement (MeasurementInit).
 *
 * @param driverType
 *   - 0: Driver initialisation as CCP session
 *   - 1: Driver initialisation as XCP session
 *
 * @return
 *   - 0: OK
 *   - -1: Error
 */
long MCDInitEx(long driverType);

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Initialisation of the MCD server. CANape will be started in non modal mode.
 *
 * Initialisation of the MCD server. CANape will be started in non modal mode.
 *
 * This function must only be called during the initialisation phase of the measurement (MeasurementInit).
 *
 * @param driverType
 *   - 0: Driver initialisation as CCP session
 *   - 1: Driver initialisation as XCP session
 *
 * @param autoCreate
 *   - 0: Only the MCD server is started; the module is not created implicitly.
 *   - 1: The module is created implicitly; MCDCreateModule is not needed afterwards.
 *
 * @return
 *   - 0: OK
 *   - -1: Error
 */
long MCDInitEx(long driverType, long autoCreate);

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Initialisation of the MCD server. CANape will be started in non modal mode.
 *
 * Initialisation of the MCD server. CANape will be started in non modal mode.
 *
 * This function must only be called during the initialisation phase of the measurement (MeasurementInit).
 *
 * @param driverType
 *   - 0: Driver initialisation as CCP session
 *   - 1: Driver initialisation as XCP session
 *
 * @param autoCreate
 *   - 0: Only the MCD server is started; the module is not created implicitly.
 *   - 1: The module is created implicitly; MCDCreateModule is not needed afterwards.
 *
 * @param sampleSize
 *   Number of values of the data acquisition. The default in the other functions is 32.
 *
 * @return
 *   - 0: OK
 *   - -1: Error
 */
long MCDInitEx(dword driverType, dword autoCreate, dword sampleSize);

}
