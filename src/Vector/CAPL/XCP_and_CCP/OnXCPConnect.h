#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   Is called after establishing the connection with xcpConnect and before start of the DAQ measurement.
 *
 * After establishing the connection and before start of the DAQ measurement the callback function OnXcpConnect is called.
 *
 * @param ecuQualifier
 *   Name of the device – configured within the XCP/CCP configuration dialog.
 *
 * @return
 *   - 0: OK
 *   - -1: Device with this name is not existing
 *   - -2: Operation not allowed – already connected
 */
void OnXcpConnect(char * ecuQualifier);

}
