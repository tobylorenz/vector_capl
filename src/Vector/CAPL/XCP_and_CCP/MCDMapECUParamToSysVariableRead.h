#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Maps an ECU parameter to a system variable so that it the system variable changes whenever the parameter is updated.
 *
 * Maps an ECU parameter to a system variable so that it the system variable changes whenever the parameter is updated-
 * The MCD data acquisition must already be started.
 *
 * @param moduleName
 *   Name of the module that is configured in the global options dialog External Programs|MCD 3D Server.
 *
 * @param parameterName
 *   Name of the parameter.
 *
 * @param SysVarName
 *   System variable.
 *
 * @return
 *   - 0: No error.
 *   - 1: The system variable is already mapped as read target of a different parameter.
 *   - 2: The system variable is already mapped as read target of this parameter.
 *   - 3: The parameter is already mapped as read source of a different system variable.
 *   - 4: The system variable is already mapped as write source of a different parameter.
 *   - 5: The system variable is not of type float.
 *   - 6: The system variable is read-only.
 *   - 7: The MCD data acquisition is not currently active.
 *   - -1: An internal error occurred.
 */
long MCDMapECUParamToSysVariableRead(char * moduleName, char * parameterName, char * SysVarName);

}
