#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   Sends user defined data to the XCP slave.
 *
 * Sends user defined data to the XCP slave.
 *
 * The callback returns the response on the user command.
 *
 * @param ecuQualifier
 *   Name of the device, configured within the XCP/CCP Configuration dialog.
 *
 * @param data
 *   Byte array for user defined data.
 *
 * @param dataSize
 *   Size of the user defined data.
 */
void xcpUserCommand(char * ecuQualifier, byte * data, long dataSize);

}
