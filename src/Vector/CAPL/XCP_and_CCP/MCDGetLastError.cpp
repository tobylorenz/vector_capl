#include "MCDGetLastError.h"

#include <iostream>

namespace capl
{

long MCDGetLastError(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
