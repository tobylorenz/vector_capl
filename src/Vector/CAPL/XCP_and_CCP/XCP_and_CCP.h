#pragma once

/**
 * @defgroup XCP_and_CCP XCP and CCP
 */

/* ASAM-MCD CAPL Functions */
#include "ASAM_MCD.h"

/* XCP CAPL Functions */
#include "XCP.h"
