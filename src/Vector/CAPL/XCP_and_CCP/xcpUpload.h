#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   Upload of the system variable.
 *
 * Initiates an upload of the XCP signal from ECU and updates the dedicated system variable.
 *
 * After finishing of the upload the callback function OnXcpUpload is called to indicate the upload status.
 *
 * @param namespace_
 *   Namespace of the corresponding system variable
 *
 * @param variable
 *   Name of the corresponding system variable.
 *
 * @return
 *   - 0: OK
 *   - -1: System variable of the signal was not found
 *   - -2: Operation not allowed
 *   - -3: System variable of the signal is inactive
 *   - -4: Device is not connected
 */
long xcpUpload(char * namespace_, char * variable);

/**
 * @ingroup XCP
 *
 * @brief
 *   Upload of the system variable.
 *
 * Initiates an upload of the XCP signal from ECU and updates the dedicated system variable.
 *
 * After finishing of the upload the callback function OnXcpUpload is called to indicate the upload status.
 *
 * @param sysvar
 *   Name of the fully qualified name of the system variable, including all name spaces, separated by "::".
 *   The name must be preceded by "sysvar::"
 *
 * @return
 *   - 0: OK
 *   - -1: System variable of the signal was not found
 *   - -2: Operation not allowed
 *   - -3: System variable of the signal is inactive
 *   - -4: Device is not connected
 */
long xcpUpload(char * sysvar);

}
