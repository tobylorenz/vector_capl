#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Configure and start the data acquisition.
 *
 * Configure and start the data acquisition.
 * After a data acquisition is started the configured parameters are transferred to CANoe with the specified polling rate.
 * The values can be obtained with MCDGetCurrentValue.
 *
 * Using MCDStartDataAcqAsync the data acquisition is started asynchronous.
 * This means that the current measuring is not interrupted during the call of this function.
 *
 * @param moduleName
 *   Name of the module that is configured in the global options dialog External Programs|MCD 3D Server.
 *
 * @param taskId
 *   Id of the task which is available.
 *
 * @param pollingRate
 *   Cycle time with which the values are reported to CANoe internally.
 *
 * @param parameters
 *   A string that specifies the parameters which are cyclically reported to CANoe.
 *   The parameters are separated with ';'.
 *   Note: the size of the string must contain the null terminator.
 *   Example for a string that specifies a0, b0 and c0 as parameters:
 *   char paramStr[9] = "a0;b0;c0";
 *
 * @return
 *   0
 */
long MCDStartDataAcq(char * moduleName, int taskId, int pollingRate, char * parameters);

}
