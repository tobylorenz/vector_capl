#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Get one of the values which was configured with the data acquisition.
 *
 * Get one of the values which was configured with the data acquisition.
 *
 * @param moduleName
 *   Name of the module that is configured in the global options dialog External Programs|MCD 3D Server.
 *
 * @param parameterName
 *   Name of the parameter to be set.
 *
 * @return
 *   The value of the requested parameter.
 */
double MCDGetCurrentValue(char * moduleName, char * parameterName);

}
