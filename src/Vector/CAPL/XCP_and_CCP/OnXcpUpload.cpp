#include "OnXcpUpload.h"

#include <iostream>

namespace capl
{

void OnXcpUpload(char * namespace_, char * variable, long errorIndication)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
