#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   Returns the connection status of a XCP/CCP device.
 *
 * Returns the current connection status of a XCP/CCP device.
 *
 * @param ecuQualifier
 *   Name of the device – configured within the XCP/CCP configuration dialog.
 *
 * @return
 *   - 0: Not connected
 *   - 1: Connected
 *   - -2: Device with this name is not existing
 */
long xcpIsConnected(char * ecuQualifier);

}
