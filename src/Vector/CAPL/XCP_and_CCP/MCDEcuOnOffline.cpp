#include "MCDEcuOnOffline.h"

#include <iostream>

namespace capl
{

long MCDEcuOnOffline(char * moduleName, long ecuState)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
