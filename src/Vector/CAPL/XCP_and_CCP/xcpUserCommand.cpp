#include "xcpUserCommand.h"

#include <iostream>

namespace capl
{

void xcpUserCommand(char * ecuQualifier, byte * data, long dataSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
