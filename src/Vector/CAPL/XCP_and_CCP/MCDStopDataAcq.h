#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Stops the data acquisition explicitly.
 *
 * Stops the data acquisition explicitly.
 * An active data acquisition is stopped implicitly on measurement stop.
 *
 * @return
 *   0
 */
long MCDStopDataAcq(void);

}
