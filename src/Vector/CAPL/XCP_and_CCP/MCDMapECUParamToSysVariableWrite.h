#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Maps an ECU parameter to a system variable so that it is changed whenever the system variable changes.
 *
 * Maps an ECU parameter to a system variable so that it is changed whenever the system variable changes.
 *
 * @param moduleName
 *   Name of the module that is configured in the global options dialog External Programs|MCD 3D Server.
 *
 * @param parameterName
 *   Name of the parameter.
 *
 * @param SysVarName
 *   System variable.
 *
 * @return
 *   - 0: No error.
 *   - 1: The parameter is already mapped as write target of another system variable.
 *   - 2: The parameter is already mapped as write target of this system variable.
 *   - 3: The system variable is already mapped as write source of a different parameter.
 *   - 4: The system variable is already mapped as read target of a different parameter.
 *   - 5: The system variable is not of type float.
 *   - -1: An internal error occurred.
 */
long MCDMapECUParamToSysVariableWrite(char * moduleName, char * parameterName, char * SysVarName);

}
