#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Returns the last occurred error.
 *
 * Returns the last occurred error.
 *
 * @return
 *   Error number: See Error Codes for the description.
 */
long MCDGetLastError(void);

}
