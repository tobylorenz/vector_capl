#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   Is called after finishing of the upload with xcpUpload to indicate the upload status.
 *
 * After finishing of the upload the callback function OnXcpUpload is called to indicate the upload status.
 *
 * Use the errorIndication parameter of the OnXcpUpload callback function to check for errors occurred during the upload,
 * if an error during call of xcpUpload occurs OnXcpUpload is not called.
 *
 * @param namespace_
 *   Namespace of the corresponding system variable
 *
 * @param variable
 *   Name of the corresponding system variable.
 *
 * @param errorIndication
 *   - 0: OK
 *   - -4: Device is not connected
 */
void OnXcpUpload(char * namespace_, char * variable, long errorIndication);

}
