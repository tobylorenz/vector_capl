#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Writing of a parameter.
 *
 * Writing of a parameter.
 *
 * @param moduleName
 *   Name of the module that is configured in the global options dialog External Programs|MCD 3D Server.
 *
 * @param parameterName
 *   Name of the parameter to be set.
 *
 * @param value
 *   New value of the parameter to be set.
 *
 * @return
 *   - 0: OK
 */
long MCDSetECUParam(char * moduleName, char * parameterName, double value);

}
