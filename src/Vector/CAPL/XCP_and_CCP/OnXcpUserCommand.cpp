#include "OnXcpUserCommand.h"

#include <iostream>

namespace capl
{

void OnXcpUserCommand(char * ecuQualifier, byte * data, long dataSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
