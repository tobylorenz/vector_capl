#include "xcpUpload.h"

#include <iostream>

namespace capl
{

long xcpUpload(char * namespace_, char * variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long xcpUpload(char * sysvar)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
