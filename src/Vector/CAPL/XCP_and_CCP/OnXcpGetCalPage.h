#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   Is called to get the currently active calibration data page of an ECU.
 *
 * The callback returns the currently active calibration data page of the ECU.
 *
 * @param ecuQualifier
 *   Name of the device, configured within the XCP/CCP Configuration dialog.
 *
 * @param reserved0
 *   Reserved
 *
 * @param reserved1
 *   Reserved
 *
 * @param pageNr
 *   Logical data page number.
 *   Usually 0 identifies the page in RAM and 1 the page in FLASH memory.
 *   CANoe.XCP can only work on parameters stored in RAM.
 */
void OnXcpGetCalPage(char * ecuQualifier, byte reserved0, byte reserved1, byte pageNr);

}
