#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup ASAM_MCD
 *
 * @brief
 *   Creates a module.
 *
 * Creates a module.
 *
 * @param moduleName
 *   Module name (has to be defined in the module list).
 *
 * @return
 *   - 0: OK
 *   - -1: Error
 */
long MCDCreateModule(char * moduleName);

}
