#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup XCP
 *
 * @brief
 *   Is called whenever the XCP slaves answers with a negative response.
 *
 * Whenever the XCP slaves answers with a negative response (i.e. not 0xFF) the OnXCPError callback function is called.
 *
 * @param ecuQualifier
 *   Name of the device, configured within the XCP/CCP Configuration dialog.
 *
 * @param xcpCmd
 *   XCP command sent by CANoe.
 *
 * @param xcpErrorCode
 *   Error code returned by the XCP slave.
 */
void OnXcpError(char * ecuQualifier, byte xcpCmd, byte xcpErrorCode);

}
