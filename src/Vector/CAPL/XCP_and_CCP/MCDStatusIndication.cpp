#include "MCDStatusIndication.h"

#include <iostream>

namespace capl
{

void MCDStatusIndication(char * moduleName, long operation, long status)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
