#include "MCDInitEx.h"

#include <iostream>

namespace capl
{

long MCDInitEx(dword driverType)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long MCDInitEx(dword driverType, dword autoCreate)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long MCDInitEx(dword driverType, dword autoCreate, dword sampleSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
