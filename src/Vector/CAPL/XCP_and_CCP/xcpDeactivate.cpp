#include "xcpDeactivate.h"

#include <iostream>

namespace capl
{

long xcpDeactivate(char * namespace_, char * variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long xcpDeactivate(char * sysvar)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
