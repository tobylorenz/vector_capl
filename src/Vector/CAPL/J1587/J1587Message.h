#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup J1587
 *
 * @brief
 *   Receipt of a J1587 message.
 *
 * Defines the event handler for a valid J1708 message, the thispointer is of type J1587Message.
 *
 * For passing this message to other node, output(this) must be called inside the event handler.
 */
class J1587Message
{
public:
    /** @todo
    dword TIME;
    word MsgChannel;
    byte J1587_MID;
    byte J1587_ReceivedMID;
    word Error;
    byte PRIO;
    word DLC;
    byte DATA[3825];
    */
};

}
