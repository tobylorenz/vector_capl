#pragma once

#include "../DataTypes.h"
#include "J1587.h"

namespace capl
{

/**
 * @ingroup J1587
 *
 * @brief
 *   Gets a J1587 parameter inside a J1708 message at a given index.
 *
 * Gets a J1587 parameter inside a J1708 message at a given index.
 *
 * @param[in] msg
 *   J1708 message
 *
 * @param[out] param
 *   returned parameter at the given index
 *
 * @param[in] index
 *   index of parameter to retrieve
 *
 * @return
 *   0 or error code
 */
byte J1587GetParameter(J1587Message msg, J1587Param param, word index);

}
