#pragma once

#include "../DataTypes.h"
#include "J1587.h"

namespace capl
{

/**
 * @ingroup J1587
 *
 * @brief
 *   Appends a J1587 parameter to a J1708 message.
 *
 * Appends a J1587 parameter to a J1708 message.
 *
 * @param msg
 *   J1708 message
 *
 * @param param
 *   returned parameter at the given index
 *
 * @return
 *   - 0: no error
 *   - 0x1: A J1587Param of type PCMD not specifying a PPID cannot be appended to a J1587 message with DLC > 0 in any case (a "non PPID" PCMD always has its own message, there are no multiple PCMD messages!).
 *   - 0x2: The parameter to append is of a different page than the first parameter (if yet any = DLC > 0) of the J1708 message.
 *   - 0x3: The parameter to append is of a different type (parameter, proprietary parameter, PCMD data) than the first parameter (if yet any = DLC > 0) of the J1708 message.
 *   - 0x4: The DLC of the resulting J1708 message would exceed 3825 bytes (limit for the TP!).
 *   - 0x5: The receiver of the parameter to append to the message is different from the receiver of the message. A (proprietary) J1708 message can only have one receiver.
 *   - 0x6: The data content of the J1708 message is invalid. This can be due to a mismatch between the DLC and the actual content of the message or due to a message content, from which valid parameter data cannot be deduced. Therefore it is impossible to evaluate if the append operation is possible at all and if yes to find the position for appending an additional parameter to the message.
 *   - 0xFF: general error
 */
byte J1587AppendParameter(J1587Message msg, J1587Param param);

}
