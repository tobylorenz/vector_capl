#include "J1587AppendParameter.h"

#include <iostream>

namespace capl
{

byte J1587AppendParameter(J1587Message msg, J1587Param param)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
