#pragma once

#include "../DataTypes.h"
#include "J1587.h"

namespace capl
{

/**
 * @ingroup J1587
 *
 * @brief
 *   Gets the count of J1587 parameters inside a given J1708 message.
 *
 * Gets the count of J1587 parameters inside a given J1708 message that can be used to further calls to J1587GetParameter().
 *
 * @param[in] msg
 *   J1708 message
 *
 * @return
 *   number of parameters
 */
word J1587GetParameterCount(J1587Message msg);

}
