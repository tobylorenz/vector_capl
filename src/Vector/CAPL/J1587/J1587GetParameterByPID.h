#pragma once

#include "../DataTypes.h"
#include "J1587.h"

namespace capl
{

/**
 * @ingroup J1587
 *
 * @brief
 *   Gets a J1587 parameter inside a J1708 message given a specific PID.
 *
 * Gets a J1587 parameter inside a J1708 message given a specific PID.
 *
 * The function returns an error if a parameter with the specified dbPID is not found inside the J1708 message.
 *
 * @param[in] msg
 *   J1708 message
 *
 * @param[out] param
 *   returned parameter at the given index
 *
 * @param[in] dbPID
 *   database PID
 *
 * @return
 *   0 or error code
 */
byte J1587GetParameterByPID(J1587Message msg, J1587Param param, dword dbPID);

}
