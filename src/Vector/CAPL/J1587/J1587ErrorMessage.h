#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup J1587
 *
 * @brief
 *   Receipt of an error message.
 *
 * Defines the event handler for an erroneous J1708 message. The error code is contained in J1587ErrorMessage::J1587_Error.
 *
 * For passing this message to other node, output(this) must be called inside the event handler.
 */
class J1587ErrorMessage
{
public:
    /** @todo
    J1587_MID
    */
};

}
