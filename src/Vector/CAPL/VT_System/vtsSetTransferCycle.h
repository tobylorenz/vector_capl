#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup VT_System
 *
 * @brief
 *   Sets the cycle time for retrieving the corresponding value from the VT System.
 *
 * Sets the cycle time for retrieving the measured value of a system variable from the VT System and writing it to the corresponding system variable.
 *
 * @param Target
 *   Name of the system variable/namespace that will be affected by this function call.
 *
 * @param CycleTime
 *   Defines the cycle time in seconds.
 *   The only available cycle times are 1 ms (= 0,001), 2 ms, 5 ms, 10 ms, 20 ms, 50 ms, 100 ms, 200 ms, 500 ms, 1 s, 2 s, 5 s and 10 s. Please note that not all cycle times are available for all measurement values. See possible settings in the VT System configuration dialog.
 *   If an invalid cycle time is specified, the call will fail with a feedback value of -1.
 *
 * @return
 *   - 0: Successful call
 *   - -1: Call error, e.g., system variable does not belong to the VT System or cycle time is invalid.
 *   - -4: Invalid system variable
 */
long vtsSetTransferCycle(char * Target, double CycleTime);

}
