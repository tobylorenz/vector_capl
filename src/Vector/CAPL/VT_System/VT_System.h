#pragma once

/** @todo to be implemented */

/**
 * @defgroup VT_System VT System
 */

/* General Methods/Functions */
#include "vtsSetTransferCycle.h"

/* VT1004 related Methods/Functions */
#include "vtsResetMinMax.h"
#include "vtsSetLoadMode.h"
#include "vtsSetMeasurementMode.h"
#include "vtsSetLoadControlTimeout.h"
#include "vtsSetPWMMeasurementDuration.h"
#include "vtsSetPWMThreshold.h"
#include "vtsSetImpedanceMode.h"
#include "vtsSetIntegrationTime.h"

/* VT2004 related Methods/Functions */
#include "vtsLoadWFResistance.h"
#include "vtsLoadWFVoltage.h"
#include "vtsSetCurveType.h"
#include "vtsSetStimulationMode.h"
#include "vtsStartStimulation.h"
#include "vtsStopStimulation.h"
#include "vtsSetPWMResistanceLow.h"
#include "vtsSetPWMStartDelay.h"
#include "vtsSetPWMResistanceHigh.h"
#include "vtsSetPWMVoltageLow.h"
#include "vtsSetPWMVoltageHigh.h"
#include "vtsSetPWMRepeats.h"
#include "vtsSetWFParams.h"

/* VT2516 related Methods/Functions */
#include "vtsLoadWFBitStream.h"
#include "vtsSetCurveType.h"
#include "vtsSetIntegrationTime.h"
#include "vtsSetPWMMeasurementDuration.h"
#include "vtsSetPWMRepeats.h"
#include "vtsSetPWMVoltageHigh.h"
#include "vtsSetPWMVoltageLow.h"
#include "vtsSetStimulationMode.h"
#include "vtsSetThreshold1_8.h"
#include "vtsSetThreshold9_16.h"
#include "vtsSetWFParams.h"
#include "vtsStartStimulation.h"
#include "vtsStopStimulation.h"

/* VT2816 related Methods/Functions */
#include "vtsLoadWFVoltage.h"
#include "vtsResetMinMax.h"
#include "vtsSetCurveType.h"
#include "vtsSetIntegrationTime.h"
#include "vtsSetMeasurementMode.h"
#include "vtsSetOutputRange.h"
#include "vtsSetWFParams.h"
#include "vtsStartStimulation.h"
#include "vtsStopStimulation.h"

/* VT2848 related Methods/Functions */
#include "vtsLoadWFBitStream.h"
#include "vtsSetCurveType.h"
#include "vtsSetOutputMode.h"
#include "vtsSetOutputSource.h"
#include "vtsSetPWMMeasurementDuration.h"
#include "vtsSetPWMRepeats.h"
#include "vtsSetPWMStartDelay.h"
#include "vtsSetThreshold.h"
#include "vtsSetWFParams.h"
#include "vtsStartStimulation.h"
#include "vtsStopStimulation.h"

/* VT7001 related Methods/Functions */
#include "vtsLoadWFVoltage.h"
#include "vtsResetMinMax.h"
#include "vtsSerialClose.h"
#include "vtsSerialConfigure.h"
#include "vtsSerialOpen.h"
#include "vtsSerialReceive.h"
#include "vtsSerialSend.h"
#include "vtsSetIntegrationTime.h"
#include "vtsSetInterconnectionMode.h"
#include "vtsSetMaxCurrentMode.h"
#include "vtsSetMinCurrentMeasurementRange.h"
#include "vtsSetRefVoltageMode.h"
#include "vtsSerialSetOnErrorHandler.h"
#include "vtsSerialSetOnReceiveHandler.h"
#include "vtsSerialSetOnSendHandler.h"
#include "vtsSetWFParams.h"
#include "vtsStartStimulation.h"
#include "vtsStopStimulation.h"

/* Trigger Methods/Functions */
#include "vtsSetTriggerParams.h"
#include "vtsSetTriggerParamsEx.h"
#include "vtsStartTrigger.h"
