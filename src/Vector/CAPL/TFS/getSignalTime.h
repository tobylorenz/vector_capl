#pragma once

#include "../DataTypes.h"
#include "../CAN/dbSig.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Gets the time point relative to the measurement start at which the signal was last sent on the bus.
 *
 * Gets the time point relative to the measurement start at which the signal was last sent on
 * the bus.
 *
 * @param aSignal
 *   The signal to be polled.
 *
 * @return
 *   Time point or 0 if the signal was not sent yet.
 */
dword getSignalTime(dbSignal & aSignal);

}
