#include "RegisterSignalDriver.h"

#include <iostream>

namespace capl
{

long RegisterSignalDriver(dbSignal & aSignal, char * aCallbackFunction)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
