#include "TestWaitForSignal.h"

#include <iostream>

namespace capl
{

long TestWaitForSignal(dbSignal & aSignal, dword aTimeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
