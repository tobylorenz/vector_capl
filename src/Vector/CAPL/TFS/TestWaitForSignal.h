#pragma once

#include "../DataTypes.h"
#include "../CAN/dbSig.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Tests the availability of a specific signal.
 *
 * @deprecated
 *   Replaced by TestWaitForSignalAvailable
 *
 * Tests the availability of a specific signal and waits if necessary until its availability.
 * A signal that is received at least once from the bus after the measurement starts is classified as "available".
 *
 * This function is useful when you want to assure that single signals are available before starting a signal-oriented test, i.e. to synchronize the tester with the bus.
 *
 * @param aSignal
 *   Signal whose availability is being tested or for which is waited
 *
 * @param aTimeout
 *   Maximum waiting time
 *
 * @return
 *   - -2: Wait state is exited due to a constraint/condition violation
 *   - -1: General error, e.g. functionality is unavailable
 *   - 0: Wait state is exited due to a timeout
 *   - 1: Wait state is exited; signal is available for further tests
 */
long TestWaitForSignal(dbSignal & aSignal, dword aTimeout);

}
