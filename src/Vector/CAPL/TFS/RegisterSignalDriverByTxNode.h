#pragma once

#include "../DataTypes.h"
#include "../CAN/dbSig.h"
#include "../CAN/dbNode.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Registers the given callback as a 'signal driver' for the signal. (Form 2)
 *
 * @deprecated
 *   No replacement
 *
 * Registers the given callback as a 'signal driver' for the signal.
 *
 * @param aSignal
 *   DB signal to be queried.
 *
 * @param aTxNode
 *   DB node that should send the signal. It is only necessary if several send nodes are
 *   approved for a message.
 *
 * @param aCallbackFunction
 *   Name of a callback function that should be defined as follows: void function(double value).
 *
 * @return
 *   - 1: Correct functionality
 *   - 0: CAPL 'signal driver' could not be registered
 */
long RegisterSignalDriverByTxNode(dbSignal & aSignal, dbNode & aTxNode, char * aCallbackFunction);

}
