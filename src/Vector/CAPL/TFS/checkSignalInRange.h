#pragma once

#include "../DataTypes.h"
#include "../CAN/dbSig.h"
#include "../General/envvar.h"
#include "../System_Variables/sysvar.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks the value of a signal or an environment variable against a condition. (Form 1)
 *
 * Checks the signal value or the environment variable value or the system variable value
 * against the condition:
 *
 * aLowLimit <= Value <= aHighLimit
 *
 * @param aSignal
 *   The signal to be polled.
 *
 * @param aLowLimit
 *   Lower limit of the signal value.
 *
 * @param aHighLimit
 *   Upper limit of the signal value.
 *
 * @return
 *   -  1: If the condition is TRUE
 *   -  0: If the condition is violated or the signal is unavailable, i.e. was not on the bus yet
 *   - -1: General error
 *   - -2: Type of the environment or system variable is not valid - only float or integer are valid -
 *         or invalid limits of the given range.
 */
long checkSignalInRange(dbSig & aSignal, double aLowLimit, double aHighLimit);

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks the value of a signal or an environment variable against a condition. (Form 2)
 *
 * Checks the signal value or the environment variable value or the system variable value
 * against the condition:
 *
 * aLowLimit <= Value <= aHighLimit
 *
 * @param EnvVarName
 *   Environment variable to be queried.
 *
 * @param aLowLimit
 *   Lower limit of the signal value.
 *
 * @param aHighLimit
 *   Upper limit of the signal value.
 *
 * @return
 *   -  1: If the condition is TRUE
 *   -  0: If the condition is violated or the signal is unavailable, i.e. was not on the bus yet
 *   - -1: General error
 *   - -2: Type of the environment or system variable is not valid - only float or integer are valid -
 *         or invalid limits of the given range.
 */
long checkSignalInRange(envvar & EnvVarName, double aLowLimit, double aHighLimit);

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks the value of a signal or an environment variable against a condition. (Form 3)
 *
 * Checks the signal value or the environment variable value or the system variable value
 * against the condition:
 *
 * aLowLimit <= Value <= aHighLimit
 *
 * @param aSysVar
 *   System Variable to be queried.
 *
 * @param aLowLimit
 *   Lower limit of the signal value.
 *
 * @param aHighLimit
 *   Upper limit of the signal value.
 *
 * @return
 *   -  1: If the condition is TRUE
 *   -  0: If the condition is violated or the signal is unavailable, i.e. was not on the bus yet
 *   - -1: General error
 *   - -2: Type of the environment or system variable is not valid - only float or integer are valid -
 *         or invalid limits of the given range.
 */
long checkSignalInRange(sysvar & aSysVar, double aLowLimit, double aHighLimit);

}
