#pragma once

#include "../DataTypes.h"
#include "../CAN/dbSig.h"
#include "../General/envvar.h"
#include "../System_Variables/sysvar.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks the signal value against a condition.
 *
 * Checks the value of the signal, the environment variable value or the system variable value against the condition:
 *
 * aLowLimit <= Value <= aHighLimit
 *
 * The test step is then evaluated as passed or failed, depending on the results
 *
 * @param aTestStep
 *   Name of the test step for the test report
 *
 * @param aSignal
 *   The signal to be polled
 *
 * @param aLowLimit
 *   Lower limit of the signal value
 *
 * @param aHighLimit
 *   Upper limit of the signal value
 *
 * @return
 *   - -1: General error
 *   - 0: Correct functionality
 */
long testValidateSignalInRange(char * aTestStep, dbSignal & aSignal, double aLowLimit, double aHighLimit);

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks the signal value against a condition.
 *
 * Checks the value of the signal, the environment variable value or the system variable value against the condition:
 *
 * aLowLimit <= Value <= aHighLimit
 *
 * The test step is then evaluated as passed or failed, depending on the results
 *
 * @param aTestStep
 *   Name of the test step for the test report
 *
 * @param EnvVarName
 *   Environment variable to be queried
 *
 * @param aLowLimit
 *   Lower limit of the signal value
 *
 * @param aHighLimit
 *   Upper limit of the signal value
 *
 * @return
 *   - -1: General error
 *   - 0: Correct functionality
 */
long testValidateSignalInRange(char * aTestStep, envvar & EnvVarName, double aLowLimit, double aHighLimit);

/**
 * @ingroup TFS
 *
 * @brief
 *   Checks the signal value against a condition.
 *
 * Checks the value of the signal, the environment variable value or the system variable value against the condition:
 *
 * aLowLimit <= Value <= aHighLimit
 *
 * The test step is then evaluated as passed or failed, depending on the results
 *
 * @param aTestStep
 *   Name of the test step for the test report
 *
 * @param aSysVar
 *   System Variable to be queried
 *
 * @param aLowLimit
 *   Lower limit of the signal value
 *
 * @param aHighLimit
 *   Upper limit of the signal value
 *
 * @return
 *   - -1: General error
 *   - 0: Correct functionality
 */
long testValidateSignalInRange(char * aTestStep, sysvar & aSysVar, double aLowLimit, double aHighLimit);

}
