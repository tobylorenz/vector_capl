#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup TFS
 *
 * @brief
 *   This function annotates a logging file to a report.
 *
 * @deprecated
 *   This command is still retained for compatibility reasons since the logging files used are automatically annotated in the XML log after version 5.2.
 *
 * This function annotates a logging file to a report, e.g. to HTML reports created with extendedNavigation.xslt.
 *
 * @param dataName
 *   Path of the logging file.
 *   This must always be relative to the XML test report, e.g. "logfile.asc" or "log/logfile.asc".
 *   If a logging file is not displayed correctly, you should check the specified path since it is not automatically checked whether the specified file actually exists.
 *   Use "/" as a separator to prevent problems in some browsers.
 */
void TestReportSetLoggingBlock(char * dataName);

}
