#include "RegisterSignalDriverByTxNode.h"

#include <iostream>

namespace capl
{

long RegisterSignalDriverByTxNode(dbSignal & aSignal, dbNode & aTxNode, char * aCallbackFunction)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
