#pragma once

/**
 * @defgroup TFS Test Feature Set (TFS) Functions
 */

/* Collection of Event Information */
#include "TestGetLastWaitElapsedTimeNS.h"

/* Composed Wait Points */

/* Constraints and Conditions */

/* Diagnostic Test Support */

/* Fault Injection Functions */

/* OEM Package based Fault Injection Functions */

/* Signaling of User-defined Events */

/* Signal Oriented Tests */
#include "checkSignalInRange.h"
#include "testValidateSignalInRange.h"
#include "testValidateSignalOutsideRange.h"
#include "CheckSignalMatch.h"
#include "getSignal.h"
#include "getSignalTime.h"
#include "RegisterSignalDriver.h"
#include "setSignal.h"

/* Test Controlling */

/* Test Report */

/* Test Structuring */

/* Transmission Commands */

/* Verdict Interaction */

/* Visualization */

/* Wait Instructions */

/* Test Feature Set for K-Line */

/* Obsolete Functions */
// checkSignalInRangeByTxNode
// getSignalByTxNode
// getSignalTimeByTxNode
#include "RegisterSignalDriverByTxNode.h"
// SetSignalTimeByTxNode
#include "TestReportSetLoggingBlock.h"
#include "testValidateSignalInRangeByTxNode.h"
#include "testValidateSignalOutsideRangeByTxNode.h"
#include "TestWaitForSignal.h"
#include "TestWaitForSignals.h"
