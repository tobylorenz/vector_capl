#include "testValidateSignalOutsideRangeByTxNode.h"

#include <iostream>

namespace capl
{

long testValidateSignalOutsideRangeByTxNode(char * aTestStep, char * aSignal, char * aTxNode, double aLowLimit, double aHighLimit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
