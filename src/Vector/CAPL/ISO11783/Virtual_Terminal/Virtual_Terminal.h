#pragma once

/** @todo to be implemented */

/**
 * @ingroup ISO11783
 * @{
 * @defgroup Virtual_Terminal Virtual Terminal API
 * @}
 */

/* General Functions */
#include "VTCreate.h"
#include "VTDestroy.h"
#include "VTGetLastError.h"
#include "VTGetLastErrorText.h"
#include "VTGoOnline.h"
#include "VTSetTP.h"
#include "VTSetProprietaryCharSet.h"

/* Network Management */
#include "VTGetECUTableTime.h"
#include "VTQueryAddress.h"
#include "VTQueryDevice.h"
#include "VTUpdateECUTable.h"

/* Object Pool */
#include "VTOPGetAttribute.h"
#include "VTOPGetAttributeFloat.h"
#include "VTOPGetHandle.h"
#include "VTOPGetNumericValue.h"
#include "VTOPGetStringValue.h"

/* Callbacks */
#include "VTAddrClaimedIndication.h"
#include "VTOnError.h"
#include "VTOnStatus.h"
