#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup ISO11783_IL
 *
 * @brief
 *   Suppress the auto-start function of the IL.
 *
 * This function can only be used in on prestart, to suppress the auto-start function of the IL.
 *
 * If the device name is not specified, the node attributes with the device name must be defined (Exception: If NMT is not activate, the device name is not needed).
 *
 * @return
 *   0 - Success or error code
 */
long Iso11783IL_ControlInit(void);

/**
 * @ingroup ISO11783_IL
 *
 * @brief
 *   Suppress the auto-start function of the IL.
 *
 * This function can only be used in on prestart, to suppress the auto-start function of the IL.
 *
 * If the device name is not specified, the node attributes with the device name must be defined (Exception: If NMT is not activate, the device name is not needed).
 *
 * @param deviceName
 *   ISO11783 64-Bit device name (optional)
 *
 * @return
 *   0 - Success or error code
 */
long Iso11783IL_ControlInit(byte * deviceName);

}
