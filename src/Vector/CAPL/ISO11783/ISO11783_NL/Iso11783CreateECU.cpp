#include "Iso11783CreateECU.h"

#include <iostream>

namespace capl
{

dword Iso11783CreateECU(dword busHandle, char * deviceName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword Iso11783CreateECU(char * busName, char * deviceName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
