#include "scopeTriggerNow.h"

#include <iostream>

namespace capl
{

long scopeTriggerNow(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
