#include "scopeConnect.h"

#include <iostream>

namespace capl
{

long scopeConnect(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
