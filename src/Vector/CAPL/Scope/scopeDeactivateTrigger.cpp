#include "scopeDeactivateTrigger.h"

#include <iostream>

namespace capl
{

long scopeDeactivateTrigger(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
