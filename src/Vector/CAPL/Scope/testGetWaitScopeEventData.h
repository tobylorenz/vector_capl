#pragma once

#include "../DataTypes.h"
#include "Scope.h"

namespace capl
{

/**
 * @ingroup Scope
 *
 * @brief
 *   Retrieves the data of CANoe Scope event.
 *
 * Retrieves the data of CANoe Scope event triggered by the last wait instruction.
 *
 * @param aScopeEvent
 *   Data structure to be filled.
 *
 * @return
 *   - 0: Data access successful.
 *   - -1: Data access could not be executed, the last event was not an awaited event.
 */
long testGetWaitScopeEventData(ScopeEvent aScopeEvent);

}
