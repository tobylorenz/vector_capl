#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Scope
 *
 * @brief
 *   Performs Trigger Now action for Scope window.
 *
 * Performs Trigger Now action for Scope window. This action is equivalent to immediate triggering via the GUI.
 *
 * The completion of this action is reported with an internal event which can be awaited via TFS-function testWaitForScopeEvent() in CAPL programs for test modules.
 *
 * @return
 *   - 1 (success): Trigger signal has been generated. On trigger completion an internal Scope event will be generated (see above). Failure can be recognized implicitly by not receiving the corresponding Scope event during certain timeout, e.g. during one second.
 *   - -1 (failure): The Scope connection is not established.
 *   - -2 (failure): The data fetching is not completed yet.
 *   - -3 (failure): Memory overflow.
 */
long scopeTriggerNow(void);

}
