#pragma once

#include "../DataTypes.h"
#include "Scope.h"

namespace capl
{

/**
 * @ingroup Scope
 *
 * @brief
 *   Waits for the occurrence of CANoe Scope event.
 *
 * Waits for the occurrence of CANoe Scope event. Should the event not occur before the expiration of the time aTimeout, the wait condition is resolved nevertheless.
 *
 * @param aTimeout
 *   Timeout in ms
 *
 * @return
 *   - 1: Resume due to event occurred
 *   - 0: Resume due to timeout
 *   - -1: General error, for example, functionality is not available
 *   - -2: Resume due to constraint violation
 */
long testWaitForScopeEvent(dword aTimeout);

/**
 * @ingroup Scope
 *
 * @brief
 *   Waits for the occurrence of CANoe Scope event.
 *
 * Waits for the occurrence of CANoe Scope event. Should the event not occur before the expiration of the time aTimeout, the wait condition is resolved nevertheless.
 *
 * @param aTimeout
 *   Timeout in ms
 *
 * @param scopeEvent
 *   Type of the Scope event to be awaited.
 *   If the type of the Scope event is not specified the function will resume on any Scope event and the exact event type can be queried using testGetWaitScopeEventData() function.
 *   Supported values:
 *   - scopeConnected: Occurs when the Connect Scope action initiated with the scopeConnect() CAPL call is successfully completed.
 *   - scopeDisconnected: Occurs when the Disconnect Scope action initiated with the scopeDisconnect() CAPL call is successfully completed.
 *   - scopeTriggerActivated: Occurs when the Scope trigger activation initiated with the scopeActivateTrigger() CAPL call is successfully completed.
 *   - scopeTriggerDeactivated: Occurs when the Scope trigger deactivation initiated with the scopeDeactivateTrigger() CAPL call is successfully completed.
 *   - scopeTriggered: Occurs when the Scope triggering action initiated with the scopeTriggerNow() CAPL call is successfully completed.
 *
 * @return
 *   - 1: Resume due to event occurred
 *   - 0: Resume due to timeout
 *   - -1: General error, for example, functionality is not available
 *   - -2: Resume due to constraint violation
 */
long testWaitForScopeEvent(ScopeEventType scopeEvent, dword aTimeout);

}
