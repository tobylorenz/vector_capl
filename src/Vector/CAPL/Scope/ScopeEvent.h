#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Scope
 */
enum class ScopeEventType
{
    eScopeConnected, /**< Request to establish connection to Scope hardware successfully completed. */
    eScopeDisconnected, /**< Request to interrupt connection to Scope hardware successfully completed. */
    eScopeTriggerActivated, /**< Request to activate Scope trigger conditions successfully completed. */
    eScopeTriggerDeactivated, /**< Request to deactivate Scope trigger conditions successfully completed. */
    eScopeTriggered /**< Request to trigger on Scope hardware successfully completed. */
};

/**
 * @ingroup Scope
 */
class ScopeEvent
{
public:
    /**
     * Type of scope event
     */
    ScopeEventType Type;

    /**
     * Data identifier of the captured data created after re-quest to trigger on Scope hardware completed.
     */
    int DataId;

    /**
     * Time stamp of the captured data created after request to trigger on Scope hardware completed.
     */
    int64 Time;
};

}
