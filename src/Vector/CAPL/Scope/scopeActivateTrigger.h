#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Scope
 *
 * @brief
 *   Performs Activate Trigger action for Scope window.
 *
 * Performs Activate Trigger action for Scope window. This action is equivalent to activating the trigger via the GUI.
 *
 * The completion of this action is reported with an internal event which can be awaited via TFS-function testWaitForScopeEvent() in CAPL programs for test modules.
 *
 * @return
 *   - 2 (success): Trigger is already active. This might be a case when the trigger has been activated by a previous CAPL call or manually.
 *   - 1 (success): Trigger activation process started. On success an internal Scope event will be generated (see above). Failure can be recognized implicitly by not receiving the corresponding Scope event during certain timeout, e.g. during one second.
 *   - -1 (failure): There is no valid Scope hardware device configured.
 *   - -2 (failure): The Scope connection is not established.
 */
long scopeActivateTrigger(void);

}
