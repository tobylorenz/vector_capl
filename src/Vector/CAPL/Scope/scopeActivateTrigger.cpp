#include "scopeActivateTrigger.h"

#include <iostream>

namespace capl
{

long scopeActivateTrigger(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
