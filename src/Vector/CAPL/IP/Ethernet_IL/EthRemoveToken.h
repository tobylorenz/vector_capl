#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Removes a token from a protocol header.
 *
 * The function removes a token from a protocol header
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "dhcpv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "option1"
 *
 * @return
 *   0 or error code
 */
long EthRemoveToken(long packet, char * protocolDesignator, char * tokenDesignator);

}
