#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets timestamp of an received Ethernet packet.
 *
 * The function returns the timestamp of an received Ethernet packet in nanoseconds.
 *
 * It is only usable in a CAPL callback function that had been registered with EthReceiveRawPacket or EthReceivePacket. It depends on the callback function, which part of the packet data is accessed by the function:
 *
 *   - Callback of EthReceiveRawPacket:
 *     The function handles the raw Ethernet packet data.
 *     The offset 0 is the beginning of the Ethernet packet.
 *   - Callback of EthReceivePacket:
 *     The function handles the payload data of the highest interpretable protocol, i.e. in an UDP packet the functions handles the payload data of the UDP protocol.
 *
 * @return
 *   time stamp in nanoseconds
 */
qword EthGetThisTimeNS();

}
