#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Deletes an Ethernet packet.
 *
 * The function deletes a packet created with EthInitPacket.
 * The handle can not be used any longer.
 *
 * @param packet
 *   handle of the packet to delete
 *
 * @return
 *   0 or error code
 */
long EthReleasePacket(long packet);

}
