#include "EthGetLastError.h"

#include <iostream>

namespace capl
{

long EthGetLastError(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
