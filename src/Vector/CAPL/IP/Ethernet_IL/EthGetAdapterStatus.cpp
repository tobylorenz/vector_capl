#include "EthGetAdapterStatus.h"

#include <iostream>

namespace capl
{

long EthGetAdapterStatus()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
