#include "EthResizeToken.h"

#include <iostream>

namespace capl
{

long EthResizeToken(long packet, char * protocolDesignator, char * tokenDesignator, long newBitLength)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
