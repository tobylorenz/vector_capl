#pragma once

/**
 * @ingroup IP
 * @{
 * @defgroup Ethernet_IL Ethernet Interaction Layer
 * @}
 */

/* Callback Functions */
#include "OnEthPacket.h"
#include "OnEthRawPacket.h"
#include "OnEthAdapterStatus.h"

/* General Functions */
#include "EthGetLastError.h"
#include "EthGetLastErrorText.h"
#include "EthGetMacId.h"
#include "EthGetAdapterStatus.h"
#include "EthSetVerbosity.h"

/* Raw Ethernet */
#include "EthOutputRawPacket.h"
#include "EthReceiveRawPacket.h"
#include "EthGetThisData.h"
#include "EthGetThisValue8.h"
#include "EthGetThisValue16.h"
#include "EthGetThisMotorolaValue16.h"
#include "EthGetThisValue32.h"
#include "EthGetThisMotorolaValue32.h"
#include "EthGetThisValue64.h"
#include "EthGetThisMotorolaValue64.h"
#include "EthGetThisTimeNS.h"

/* Packet API */
#include "EthInitPacket.h"
#include "EthCompletePacket.h"
#include "EthReleasePacket.h"
#include "EthReceivePacket.h"
#include "EthOutputPacket.h"
#include "EthIsPacketValid.h"
#include "EthInitProtocol.h"
#include "EthResizeToken.h"
#include "EthGetTokenData.h"
#include "EthSetTokenData.h"
#include "EthGetTokenInt.h"
#include "EthGetTokenInt64.h"
#include "EthSetTokenInt.h"
#include "EthSetTokenInt64.h"
#include "EthGetTokenString.h"
#include "EthGetTokenSubString.h"
#include "EthSetTokenString.h"
#include "EthAddToken.h"
#include "EthRemoveToken.h"
#include "EthGetTokenLengthBit.h"

/* Control API */
#include "EthControlInit.h"
#include "EthControlStart.h"
#include "EthControlStop.h"
