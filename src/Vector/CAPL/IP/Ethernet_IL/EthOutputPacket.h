#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Sends an Ethernet packet.
 *
 * This function sends an Ethernet packet.
 *
 * @param packet
 *   handle of the packet to send
 *
 * @return
 *   0 or error code
 */
long EthOutputPacket(long packet);

}
