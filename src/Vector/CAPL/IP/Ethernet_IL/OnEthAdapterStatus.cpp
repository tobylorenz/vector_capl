#include "OnEthAdapterStatus.h"

#include <iostream>

namespace capl
{

void OnEthAdapterStatus(long channel, long status)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
