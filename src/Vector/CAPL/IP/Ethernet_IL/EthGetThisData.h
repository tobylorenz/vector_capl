#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets received data.
 *
 * The function gets the returned data.
 *
 * It is only usable in a CAPL callback function that had been registered with EthReceiveRawPacket or EthReceivePacket.
 * It depends on the callback function, which part of the packet data is accessed by the function:
 *   - Callback of EthReceiveRawPacket:
 *     The function handles the raw Ethernet packet data.
 *     The offset 0 is the beginning of the Ethernet packet.
 *   - Callback of EthReceivePacket:
 *     The function handles the payload data of the highest interpretable protocol, i.e. in an UDP packet the functions handles the payload data of the UDP protocol.
 *
 * @param offset
 *   byte offset relative to the beginning of a data packet or the payload (see description above)
 *
 * @param bufferSize
 *   number of requested bytes
 *
 * @param buffer
 *   buffer in which the requested data are copied
 *
 * @return
 *   number of copied data bytes
 */
long EthGetThisData(long offset, long bufferSize, byte * buffer);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets received data.
 *
 * The function gets the returned data.
 *
 * It is only usable in a CAPL callback function that had been registered with EthReceiveRawPacket or EthReceivePacket.
 * It depends on the callback function, which part of the packet data is accessed by the function:
 *   - Callback of EthReceiveRawPacket:
 *     The function handles the raw Ethernet packet data.
 *     The offset 0 is the beginning of the Ethernet packet.
 *   - Callback of EthReceivePacket:
 *     The function handles the payload data of the highest interpretable protocol, i.e. in an UDP packet the functions handles the payload data of the UDP protocol.
 *
 * @param offset
 *   byte offset relative to the beginning of a data packet or the payload (see description above)
 *
 * @param bufferSize
 *   number of requested bytes
 *
 * @param buffer
 *   buffer in which the requested data are copied
 *
 * @return
 *   number of copied data bytes
 */
long EthGetThisData(long offset, long bufferSize, char * buffer);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets received data.
 *
 * The function gets the returned data.
 *
 * It is only usable in a CAPL callback function that had been registered with EthReceiveRawPacket or EthReceivePacket.
 * It depends on the callback function, which part of the packet data is accessed by the function:
 *   - Callback of EthReceiveRawPacket:
 *     The function handles the raw Ethernet packet data.
 *     The offset 0 is the beginning of the Ethernet packet.
 *   - Callback of EthReceivePacket:
 *     The function handles the payload data of the highest interpretable protocol, i.e. in an UDP packet the functions handles the payload data of the UDP protocol.
 *
 * @param offset
 *   byte offset relative to the beginning of a data packet or the payload (see description above)
 *
 * @param bufferSize
 *   number of requested bytes
 *
 * @param buffer
 *   buffer in which the requested data are copied
 *
 * @return
 *   number of copied data bytes
 */
long EthGetThisData(long offset, long bufferSize, void * buffer);

}
