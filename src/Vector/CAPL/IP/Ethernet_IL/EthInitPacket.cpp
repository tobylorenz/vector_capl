#include "EthInitPacket.h"

#include <iostream>

namespace capl
{

long EthInitPacket()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthInitPacket(char * protocolDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthInitPacket(char * protocolDesignator, char * packetTypeDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthInitPacket(void * srcMacIdStruct, void * dstMacIdStruct, long ethernetType)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthInitPacket(byte srcMacId[6], byte dstMacId[6], long ethernetType)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthInitPacket(long packetToCopy)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthInitPacket(long rawDataLength, byte * rawData)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
