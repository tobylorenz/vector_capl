#include "EthReleasePacket.h"

#include <iostream>

namespace capl
{

long EthReleasePacket(long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
