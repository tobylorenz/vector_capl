#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   CAPL handler to receive a state change of the Ethernet interface.
 *
 * This callback function is called when the state of the Ethernet adapter has changed.
 *
 * @param channel
 *   Ethernet channel number of the adapter that has been changed
 *
 * @param status
 *   new state of the adapter
 *   - 0 - unknown state
 *   - 1 - not connected
 *   - 2 - connected
 */
void OnEthAdapterStatus(long channel, long status);

}
