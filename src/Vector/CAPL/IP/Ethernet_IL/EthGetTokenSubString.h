#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets a part of a string value of a token.
 *
 * The function copies a specified number of characters from a given position inside the token and adds a terminating "\0".
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "udp"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "data"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of characters to be copied
 *
 * @param buffer
 *   buffer in which the data are copied.
 *   The function adds a terminating "\0".
 *   Thus, the size of the buffer must be at least one byte larger than length.
 *
 * @return
 *   number of copied characters or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthGetTokenString(long packet, char * protocolDesignator, char * tokenDesignator, long bufferSize, char * buffer);

}
