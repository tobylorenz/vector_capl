#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param length
 *   number of bytes to be copied.
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, char * buffer);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param length
 *   number of bytes to be copied.
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, byte * buffer);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param length
 *   number of bytes to be copied.
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param bufferStruct
 *   struct in which the data are copied
 *
 * @return
 *   number of copied bytes or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long length, void * bufferStruct);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied.
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, char * buffer);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied.
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param buffer
 *   buffer in which the data are copied
 *
 * @return
 *   number of copied bytes or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, byte * buffer);

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the data of a token.
 *
 * The function requests the data or a part of date of a token.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @param protocolDesignator
 *   name of the protocol, e.g. "IPv4"
 *
 * @param tokenDesignator
 *   name of the token, e.g. "source"
 *
 * @param byteOffset
 *   offset from the beginning of the token in byte
 *
 * @param length
 *   number of bytes to be copied.
 *   Make sure size of destination (buffer or bufferStruct) is equal or larger than length.
 *
 * @param bufferStruct
 *   struct in which the data are copied
 *
 * @return
 *   number of copied bytes or 0.
 *   With EthGetLastError you can check if the function has been processed successfully.
 */
long EthGetTokenData(long packet, char * protocolDesignator, char * tokenDesignator, long byteOffset, long length, void * bufferStruct);

}
