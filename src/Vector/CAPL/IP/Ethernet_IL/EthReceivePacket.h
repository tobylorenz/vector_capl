#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Registers a CAPL handler function to receive Ethernet packets.
 *
 * Use this function to register a CAPL callback function to receive IP packets.
 * The callback has a packet handle as parameter and the functions to access the tokens can be used.
 * The EthGetThis-functions can be used to access the payload of the highest interpretable protocol.
 *
 * The callback must have the following signature:
 *
 * void OnEthPacket(long channel, long dir, long packet)
 *
 * The difference to EthReceiveRawPacket is, that the callback function gets a packet handle as parameter and the EthGetThis-Functions access the payload of the highest protocol instead the raw Ethernet data.
 *
 * @param onPacketCallback
 *   name of CAPL callback function
 *
 * @return
 *   0 or error code
 */
long EthReceivePacket(char * onPacketCallback);

}
