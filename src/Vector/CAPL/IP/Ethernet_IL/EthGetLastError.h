#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the last error code.
 *
 * Returns the error code of the last called Eth... function.
 *
 * @return
 *   error code
 */
long EthGetLastError(void);

}
