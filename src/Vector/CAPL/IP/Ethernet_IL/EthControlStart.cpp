#include "EthControlStart.h"

#include <iostream>

namespace capl
{

long EthControlStart()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
