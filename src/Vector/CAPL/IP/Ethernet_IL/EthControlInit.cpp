#include "EthControlInit.h"

#include <iostream>

namespace capl
{

long EthControlInit()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthControlInit(char * protocolDesignator)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
