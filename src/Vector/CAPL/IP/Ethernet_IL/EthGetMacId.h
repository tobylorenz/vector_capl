#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the MAC-ID of the Ethernet interface.
 *
 * This function reads the MAC-ID of the Ethernet interface.
 *
 * @param macId
 *   destination array for the read MAC-ID with six elements
 *
 * @return
 *   0 or error code
 */
long EthGetMacId(byte * macId);

}
