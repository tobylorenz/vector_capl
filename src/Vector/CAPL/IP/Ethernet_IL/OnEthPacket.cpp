#include "OnEthPacket.h"

#include <iostream>

namespace capl
{

void OnEthPacket(long channel, long dir, long packet)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
