#include "EthGetThisData.h"

#include <iostream>

namespace capl
{

long EthGetThisData(long offset, long bufferSize, byte * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthGetThisData(long offset, long bufferSize, char * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthGetThisData(long offset, long bufferSize, void * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
