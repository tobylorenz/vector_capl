#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Gets the state of the Ethernet adapter.
 *
 * This function reads the state of the Ethernet interface.
 *
 * @return
 *   - 0 - unknown state
 *   - 1 - not connected
 *   - 2 - connected
 *   or error code
 */
long EthGetAdapterStatus();

}
