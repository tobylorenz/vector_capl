#include "EthGetThisTimeNS.h"

#include <iostream>

namespace capl
{

qword EthGetThisTimeNS()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
