#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Completes an Ethernet packet for sending.
 *
 * The function completes a packet before sending it with EthOutputPacket.
 * It calculates the fields that are marked with CompleteProtocol in the protocol overview, e.g. checksum, lengths, etc.
 *
 * @param packet
 *   handle of a packet that has been created with EthInitPacket
 *
 * @return
 *   0 or error code
 */
long EthCompletePacket(long packet);

}
