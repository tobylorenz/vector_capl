#include "EthGetThisValue16.h"

#include <iostream>

namespace capl
{

long EthGetThisValue16(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
