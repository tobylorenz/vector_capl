#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   CAPL handler to receive raw data of Ethernet packets.
 *
 * The function is called when a registered raw Ethernet packet is received.
 *
 * Within this callback function the following functions can be used to retrieve the received packet data:
 *   - EthGetThisData
 *   - EthGetThisTimeNS
 *   - EthGetThisValue8
 *   - EthGetThisValue16
 *   - EthGetThisValue32
 *   - EthGetThisValue64
 *   - EthGetThisMotorolaValue16
 *   - EthGetThisMotorolaValue32
 *   - EthGetThisMotorolaValue64
 *
 * The functions access the raw Ethernet data.
 * Offset 0 starts at the beginning of the Ethernet header.
 *
 * @param channel
 *   Ethernet channel on which the packet was received
 *
 * @param dir
 *   direction of the packet
 *     - 0 – RX
 *     - 1 - TX
 *
 * @param packetLength
 *   number of bytes of the received packet (see Packet Length)
 */
void OnEthRawPacket(long channel, long dir, long packetLength);

}
