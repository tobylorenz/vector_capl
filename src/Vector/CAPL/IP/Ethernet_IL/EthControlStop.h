#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   Stops the Ethernet Interaction Layer.
 *
 * This function stops the Ethernet Interaction Layer.
 *
 * @return
 *   0 or error code
 */
long EthControlStop();

}
