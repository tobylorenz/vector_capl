#include "EthOutputRawPacket.h"

#include <iostream>

namespace capl
{

long EthOutputRawPacket(long packetLength, char packetData)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthOutputRawPacket(long packetLength, byte packetData)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long EthOutputRawPacket(long packetLength, void * packetDataStruct)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
