#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup Ethernet_IL
 *
 * @brief
 *   CAPL handler to receive data of Ethernet packets.
 *
 * The function is called when a registered Ethernet packet is received.
 *
 * Within this callback function the following functions can be used to retrieve the received packet data:
 *   - EthGetThisData
 *   - EthGetThisTimeNS
 *   - EthGetThisValue8
 *   - EthGetThisValue16
 *   - EthGetThisValue32
 *   - EthGetThisValue64
 *   - EthGetThisMotorolaValue16
 *   - EthGetThisMotorolaValue32
 *   - EthGetThisMotorolaValue64
 *
 * The functions access the payload of the highest interpretable protocol.
 * Offset 0 starts at the beginning of the payload.
 *
 * @param channel
 *   Ethernet channel on which the packet was received
 *
 * @param dir
 *   direction of the packet
 *     - 0 – RX
 *     - 1 - TX
 *
 * @param packet
 *   handle of the received packet
 */
void OnEthPacket(long channel, long dir, long packet);

}
