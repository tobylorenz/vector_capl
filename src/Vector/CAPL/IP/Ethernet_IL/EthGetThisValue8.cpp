#include "EthGetThisValue8.h"

#include <iostream>

namespace capl
{

long EthGetThisValue8(long offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
