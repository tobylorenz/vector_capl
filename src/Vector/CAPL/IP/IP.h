#pragma once

/**
 * @defgroup IP IP CAPL Functions
 */

/* Ethernet Interaction Layer */
#include "Ethernet_IL/Ethernet_IL.h"

/* SOME/IP Interaction Layer */
#include "SOME_IP_IL/SOME_IP_IL.h"
