#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup SOME_IP_IL
 *
 * @brief
 *   Triggers sending of an event.
 *
 * This function triggers sending of an event.
 *
 * Afterwards, the CAPL callback function is called in OnSomeIpPrepareEvent,
 * and the application has the opportunity to change or update the values of the SOME/IP event
 * before it is sent to its subscriber and by Multicast.
 *
 * @param pevHandle
 *   handle of the event to be sent
 *
 * @return
 *   0 or error code
 */
long SomeIpTriggerEvent(dword pevHandle);

}
