#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup SOME_IP_IL
 *
 * @brief
 *   Informs the local SD that the Remote Service is no longer needed.
 *
 * @todo
 */

}
