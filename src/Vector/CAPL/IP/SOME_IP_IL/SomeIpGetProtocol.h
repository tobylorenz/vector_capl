#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup SOME_IP_IL
 *
 * @brief
 *   Returns the protocol (UDP/TCP) the SOME/IP message was transmitted with.
 *
 * @todo
 */

}
