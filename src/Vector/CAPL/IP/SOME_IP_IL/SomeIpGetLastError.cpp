#include "SomeIpGetLastError.h"

#include <iostream>

namespace capl
{

long SomeIpGetLastError(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
