#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup SOME_IP_IL
 *
 * @brief
 *   Interface to retrieve the last error which occurs in the SOME/IP IL.
 *
 * Interface to retrieve the last error which occurs in the SOME/IP IL.
 *
 * @return
 *   0 or error code
 */
long SomeIpGetLastError(void);

}
