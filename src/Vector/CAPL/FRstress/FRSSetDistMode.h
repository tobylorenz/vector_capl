#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Configures the disturbance type.
 *
 * @deprecated
 *
 * Sets the disturbance type.
 * A bitstream disturbance generates the disturbance sequence after the trigger is detected.
 * The frame disturbance changes individual bits within a frame with bit accuracy.
 *
 * @param triggerCondition
 *   Values: 1-4
 *
 * @param mode
 *   Values:
 *   - 1=Bitstream disturbance (analog & digital mode), the disturbance is configured using FRSSetBitstreamDist.
 *   - 2=Frame disturbance (only digital mode), the disturbance is configured using FRSSetFrmDistElem, FRSSetFrmDist and FRSSetDistPayload.
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetDistMode(int triggerCondition, int mode);

}
