#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Adds frame elements (numerical input) to a trigger condition.
 *
 * Adds a numerical trigger value to the respective trigger condition.
 * A complex trigger condition can be created through multiple calls.
 *
 * @param triggerCondition
 *   Values: 1-4
 *
 * @param triggerElement
 *   Values:
 *   - 0: reservedBit
 *   - 1: PPI
 *   - 2: NFI
 *   - 3: SFI
 *   - 4: SUPFI
 *   - 5: FrameId
 *   - 6: PayloadLength
 *   - 7: HeaderCRC
 *   - 8: cycleCount
 *   - 9: BSS1
 *   - 10: BSS2
 *   - 11: BSS3
 *   - 12: BSS4
 *   - 13: BSS5
 *   - 14: TrailerBSS1
 *   - 15: TrailerBSS2
 *   - 16: TrailerBSS3
 *   - 17: TrailerCRCByte1
 *   - 18: TrailerCRCByte2
 *   - 19: TrailerCRCByte3
 *   - 20: FES
 *
 * @param triggerValue
 *   Numerical value of the respective frame field.
 *   The respective maximum values must be noted.
 *   For example, the Payload Length must not exceed the maximum value of 127 words.
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetTrigElem(int triggerCondition, int triggerElement, int triggerValue);

}
