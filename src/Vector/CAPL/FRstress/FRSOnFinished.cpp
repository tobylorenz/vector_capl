#include "FRSOnFinished.h"

#include <iostream>

namespace capl
{

long FRSOnFinished(char * CallbackName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
