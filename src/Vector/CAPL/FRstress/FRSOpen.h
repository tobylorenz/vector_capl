#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Opens an FRstress configuration file.
 *
 * Opens an FRstress configuration file.
 *
 * @param sFileName
 *   Name of the FRstress configuration file.
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSOpen(char * sFileName);

}
