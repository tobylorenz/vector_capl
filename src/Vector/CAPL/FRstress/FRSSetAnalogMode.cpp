#include "FRSSetAnalogMode.h"

#include <iostream>

namespace capl
{

long FRSSetAnalogMode(double baudrate, int channel, int payloadLength)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
