#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Activates the Scope mode.
 *
 * Activates the Scope mode of FRstress.
 * Configuration functions that belong to another operating mode are ignored.
 *
 * @param baudrate
 *   Values: 10, 5, 2.5, 1.25 Mbit/s
 *
 * @param channel
 *   Values: 1=A, 2=B
 *
 * @param payloadLength
 *   Values: 0-254 bytes
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetScopeMode(double baudrate, int channel, int payloadLength);

}
