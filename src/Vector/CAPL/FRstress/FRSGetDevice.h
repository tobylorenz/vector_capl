#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Indicates the device number of the available FRstress hardware.
 *
 * Indicates the device number of the FRstress hardware, set with FRSSetDevice.
 *
 * @return
 *   Returns the device number of the currently active hardware unit.
 */
long FRSGetDevice();

}
