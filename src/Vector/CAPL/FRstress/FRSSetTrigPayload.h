#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Adds frame payload areas to a trigger condition.
 *
 * Adds a bit sequence to the respective trigger condition as a trigger value.
 * A complex trigger condition can be created through multiple calls.
 *
 * @param triggerCondition
 *   Values: 1-4
 *
 * @param triggerPayloadElement
 *   Values: 0=Payload Byte, 1=BSS
 *
 * @param byteIndex
 *   Values: 0...254 start index in the payload
 *
 * @param triggerValue
 *   Bit sequence of the respective Payload byte.
 *   The possible elements of the sequence are 0,1,x (don't care).
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetTrigPayload(int triggerCondition, int triggerPayloadElement, int byteIndex, char * triggerValue);

}
