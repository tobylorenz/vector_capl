#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Activates the automatic CRC computation.
 *
 * Activates the automatic CRC computation based on the disturbance data
 * (FRSSetFrmDist, FRSSetFrmDistElem, FRSSetDistPayload) and the real frame.
 *
 * @param index
 *   Values: 1-4
 *
 * @param calculateCRC
 *   Values:
 *   - 0: no calculation
 *   - 1: calculate header crc
 *   - 2: calculate frame crc
 *   - 3: calculate header and frame crc
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSActivateCRCCalc(int index, int calculateCRC);

}
