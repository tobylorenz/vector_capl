#include "FRSClear.h"

#include <iostream>

namespace capl
{

long FRSClear()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
