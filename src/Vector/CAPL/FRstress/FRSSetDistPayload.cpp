#include "FRSSetDistPayload.h"

#include <iostream>

namespace capl
{

long FRSSetDistPayload(int triggerCondition, int disturbancePayloadElement, int byteIndex, char * disturbanceValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
