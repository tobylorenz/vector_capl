#include "FRSSetTrigPayload.h"

#include <iostream>

namespace capl
{

long FRSSetTrigPayload(int triggerCondition, int triggerPayloadElement, int byteIndex, char * triggerValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
