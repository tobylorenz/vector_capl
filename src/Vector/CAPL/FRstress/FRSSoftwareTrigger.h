#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Activates the FRstress software trigger.
 *
 * Activates the FRstress software trigger.
 *
 * @param iEnable
 *   - Enable = 1
 *   - Disable = 0
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSoftwareTrigger(int iEnable);

}
