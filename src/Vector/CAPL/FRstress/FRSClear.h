#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Clears the content of the current configuration.
 *
 * Clears the content of the current configuration.
 * For a defined initial state, the function has to be called as the first configuration function.
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSClear();

}
