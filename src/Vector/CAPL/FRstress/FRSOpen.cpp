#include "FRSOpen.h"

#include <iostream>

namespace capl
{

long FRSOpen(char * sFileName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
