#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Adds frame elements (bit sequence) to a trigger condition.
 *
 * Adds a bit sequence to the respective trigger condition as a trigger value.
 * A complex trigger condition can be created through multiple calls.
 *
 * @param triggerCondition
 *   Values: 1-4
 *
 * @param triggerElement
 *   Values:
 *   - 0: reservedBit
 *   - 1: PPI
 *   - 2: NFI
 *   - 3: SFI
 *   - 4: SUPFI
 *   - 5: FrameId
 *   - 6: PayloadLength
 *   - 7: HeaderCRC
 *   - 8: cycleCount
 *   - 9: BSS1
 *   - 10: BSS2
 *   - 11: BSS3
 *   - 12: BSS4
 *   - 13: BSS5
 *   - 14: TrailerBSS1
 *   - 15: TrailerBSS2
 *   - 16: TrailerBSS3
 *   - 17: TrailerCRCByte1
 *   - 18: TrailerCRCByte2
 *   - 19: TrailerCRCByte3
 *   - 20: FES
 *
 * @param triggerValue
 *   Bit sequence of the respective frame field.
 *   The possible elements of the sequence are 0,1,x (don't care).
 *   The respective maximum lengths must be noted.
 *   For example, the PayloadLength field must not exceed the maximum length of 7 bits.
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSSetTrig(int triggerCondition, int triggerElement, int triggerValue);

}
