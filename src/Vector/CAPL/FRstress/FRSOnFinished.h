#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Registers a callback.
 *
 * Registers a callback. The callback will be executed when the session ends. The callback function must be added in the CAPL section callback.
 *
 * @param CallbackName
 *   Any desired name can be used as name of the callback function.
 *
 * @return
 *   -  0: successful
 *   - -1: in case of error
 */
long FRSOnFinished(char * CallbackName);

}
