#include "FRSInit.h"

#include <iostream>

namespace capl
{

long FRSInit()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
