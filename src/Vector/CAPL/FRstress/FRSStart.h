#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FRstress
 *
 * @brief
 *   Orders the FRstress COM server to activate the hardware for the error disturbance activity.
 *
 * Orders the FRstress COM server to activate the hardware for the error disturbance activity.
 *
 * @return
 *   - 0: successful
 *   - -1: in case of error
 */
long FRSStart();

}
