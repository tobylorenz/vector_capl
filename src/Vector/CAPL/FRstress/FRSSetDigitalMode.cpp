#include "FRSSetDigitalMode.h"

#include <iostream>

namespace capl
{

long FRSSetDigitalMode(double baudrate, int channel, int payloadLength, double macrotickLengthUs, int TSSLengthBit, int cycleLengthUs, int numberStaticSlots, int actionPointOffsetMT, int staticSlotLengthMT, int TSSExtension)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
