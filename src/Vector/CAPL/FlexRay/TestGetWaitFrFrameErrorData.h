#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   If a FlexRay erroneous frame is the last event that triggers a wait instruction, the event's content can be called up.
 *
 * @todo
 */

}
