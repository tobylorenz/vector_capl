#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Initializes the FlexRay Communication Controller (CC) and generates the specified wakeup pattern before reintegration in the cluster or the startup.
 *
 * This function initializes the FlexRay Communication Controller (CC) and generates the specified wakeup pattern before reintegration in the cluster or the startup.
 *
 * @param channel
 *   FlexRay channel (cluster number)
 *
 * @param wuChMask
 *   Channel for the wakeup pattern.
 *   - 1: Channel A
 *   - 2: Channel B
 *
 * @param wuCount
 *   Number of repetitions (2 – 63) of the wakeup symbol in a wakeup pattern.
 *
 * @param wuTxIdle
 *   This number designates the number of idle bits in a wakeup symbol.
 *   According to protocol specification, this should result in 18 µs.
 *
 * @param wuTxLow
 *   This number designates the number of low bits in a wakeup symbol.
 *   According to protocol specification, this should result in 6 µs.
 *
 * @param cfg
 *  Character data array.
 *  This is currently not used and is of no importance.
 */
void ResetFlexRayCCEx(int channel, int wuChMask, int wuCount, int wuTxIdle, int wuTxLow, char * cfg);

}
