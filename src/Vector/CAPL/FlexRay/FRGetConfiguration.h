#pragma once

#include "../DataTypes.h"
#include "FlexRay.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Copies the FlexRay protocol parameters to a FlexRay parameter object.
 *
 * This function copies the FlexRay protocol parameters to configurationVar.
 *
 * @param channel
 *   FlexRay channel (cluster number).
 *
 * @param configurationVar
 *   Name of the variable referenced by the configuration object.
 *   The variable name was defined when the object was created using FRConfiguration.
 */
void FRGetConfiguration(int channel, FRConfiguration & configurationVar);

}
