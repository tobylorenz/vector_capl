#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   If a FlexRay Null Frame is the last event that triggers a wait instruction, the frame's content can be called up.
 *
 * @todo
 */

}
