#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Controls the routing for the dynamic frame part.
 *
 * Activates/deactivates the automatic routing for the dynamic part.
 * The function can not be used during a measurement.
 *
 * Settings from the Network Hardware Configuration dialog are overwritten with this function.
 *
 * @param bypass
 *   - 0 = Routing deactivated
 *   - 1 = Routing activated
 *
 * @param channel
 *   On of the both gateway channels.
 *   The routing direction is irrelevant.
 *
 * @return
 *   - 0 = Error
 *   - 1 = OK
 */
long FrGwBypassDynamic(long bypass, long channel);

}
