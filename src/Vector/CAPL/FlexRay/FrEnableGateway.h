#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Activates a gateway.
 *
 * Activates the gateway.
 *
 * Settings from the Network-Hardware-Configuration dialog will be overwritten with this function.
 *
 * @param channelA
 *   Channel 1 of the gateway
 *
 * @param channelB
 *   Channel 2 of the gateway
 *
 * @return
 *   - 0 = Error
 *   - 1 = OK
 */
long FrEnableGateway(long channelA, long channelB);

}
