#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Puts the FlexRay Communication Controller into the desired Protocol Operation Mode.
 *
 * This function puts the FlexRay Communication Controller (CC) into the desired Protocol Operation Mode (POC state).
 * The function is non-blocking.
 * That means, the function will return before the CC has reached the desired POC-state.
 *
 * All E-Ray POC-state changes can be monitored with the status- or POC-state-events. A status event is generated as soon as the second CC has reached the desired POC-state.
 *
 * @param channel
 *   FlexRay channel (cluster number).
 *
 * @param ccNumber
 *   - 1: E-Ray
 *   - 2: Second Startup Controller/Coldstart helper (Fujitsu)
 *
 * @param pocState
 *   Desired POC state:
 *   - 0: wakeup
 *   - 1: normal active
 *   - 2: halt
 *   - 3: ready
 *
 * @return
 *   - 0: Error (wrong parameter, or the POC state can not be reached)
 *   - 1: No error
 */
long FrSetPOCState(long channel, long ccNumber, long pocState);

}
