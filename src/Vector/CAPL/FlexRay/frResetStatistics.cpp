#include "frResetStatistics.h"

#include <iostream>

namespace capl
{

void frResetStatistics()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void frResetStatistics(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
