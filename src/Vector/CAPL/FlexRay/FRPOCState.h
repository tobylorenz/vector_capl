#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Is called whenever there is a change of state on the FlexRay Communication Controller's protocol operation state machine.
 *
 * @todo
 */
class FRPOCState
{
public:
};

}
