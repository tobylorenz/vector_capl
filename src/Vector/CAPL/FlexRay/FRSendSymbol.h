#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Sends an MTS symbol in the next possible symbol window if the Communication Controller is in normal mode (synchronized).
 *
 * This function sends an MTS symbol in the next possible symbol window if the Communication Controller is in normal mode (synchronized).
 *
 * @param type
 *   Not used at this time. Reserved for future expansions. Should always be equal to 0.
 *
 * @param param
 *   Not used at this time. Reserved for future expansions. Should always be equal to 0.
 *
 * @param channel
 *   Channel number (or cluster number)
 */
void FRSendSymbol(long type, long param, int channel);

}
