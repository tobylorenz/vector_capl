#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Controls the routing for static frames.
 *
 * Activates/deactivates the automatic routing for the given slot.
 * The function can be used during measurement.
 *
 * @param bypass
 *   - 0 = Routing deactivated
 *   - 1 = Routing activated
 *
 * @param channel
 *   On of the both gateway channels.
 *   The routing direction is irrelevant.
 *
 * @param slotId
 *   Slot number
 *
 * @param baseCycle
 *   Base cycle
 *
 * @param cycleRepetition
 *   Repetition factor
 *
 * @return
 *   - 0 = Error
 *   - 1 = OK
 */
long FrGwBypassStatic(long bypass, long channel, long slotId, long baseCycle, long cycleRepetition);

}
