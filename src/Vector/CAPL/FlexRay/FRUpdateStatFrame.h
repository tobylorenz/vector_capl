#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Updates the FlexRay Communication Controller's (CC) send buffer with the current data from the send object.
 *
 * @todo
 */

}
