#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Is called if a Null Frame is received in the specified slot and cycle.
 *
 * @todo
 */
class FRNullFrame
{
public:
};

}
