#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   The callback occurs when the FlexRay Communication Controller (CC) has synchronized with the bus or the synchronization fails.
 *
 * This function is a callback and is called by the tool!
 *
 * The callback occurs when the FlexRay Communication Controller (CC) has synchronized with the bus or the synchronization fails.
 *
 * @param msgTime
 *   Event time stamp.
 *
 * @param channel
 *   Channel in the tool that selects the CC.
 *   Should be set to 1 by default (since the tool currently only supports one FlexRay CC).
 *
 * @param statusType
 *   The bits signal the type of status event concerned.
 *   At the moment only bus synchronization type is available. This means the bits of this mask define which bit is valid in infomask1.
 *   - 0x0001: "Bus Synchronization" status is included.
 *   - 0x0002: Received symbol
 *
 *
 * @param infomask1
 *   If statusType == 1:
 *   The bits signal the value of an special status event.
 *   - 0: CC unable to synchronize with the bus.
 *   - 1: CC is synchronized to the bus.
 *   If statusType == 2:
 *   Symbol type (in bits 0-15):
 *   - 1: CAS
 *   - 2: MTS
 *   - 3: Wakeup
 *   - 4: Not specified
 *   Symbol length (in bits 16-30) in bit times.
 *
 * @param infomask2
 *   If statusType == 1:
 *   Not used.
 *   If statusType == 2:
 *   Symbol receiving cycle.
 *
 * @param infomask3
 *   If statusType == 1:
 *   Not used.
 *   If statusType == 2:
 *   Channel mask (A=1 / B=2 / AB = 3): Channel the symbol was received.
 */
void FlexRayRcvStatusEvent(long msgTime, int channel, long statusType, long infomask1, long infomask2, long infomask3);

}
