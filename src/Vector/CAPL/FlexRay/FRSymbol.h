#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Is called whenever a symbol (wakeup, CAS, MTS) is received.
 *
 * @todo
 */
class FRSymbol
{
public:
};

}
