#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Waits for the occurrence of change of state on the FlexRay Communication Controller's protocol operation state machine.
 *
 * @todo
 */

}
