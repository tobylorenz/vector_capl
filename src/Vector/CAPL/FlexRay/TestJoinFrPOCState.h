#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Completes the current set of "joined events" with the change of state event on the FlexRay Communication Controller's protocol operation state machine.
 *
 * @todo
 */

}
