#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   This function configures one of two possible key slots that are to be sent for a FlexRay bus.
 *
 * Configures one of two possible key slots that are to be sent for a FlexRay bus.
 *
 * The change will be applied with the next reset of the interface hardware (e.g. by ResetFlexRayCC or ResetFlexRayCCEx).
 *
 * @param channel
 *   Channel number (or cluster number)
 *
 * @param channelMask
 *   Channel of the transmission frame.
 *   Values:
 *   - 1: Channel A
 *   - 2: Channel B
 *   - 3: Channel A+B
 *
 * @param keySlotIndex
 *   Addresses the desired key slot (1 or 2).
 *
 * @param keySlotId
 *   This number designates a specific FlexRay slot in the static segment.
 *   Its value must be between 1 and 1023.
 *
 * @param keySlotUsage
 *   Defines the mode of the key slot:
 *   - 0: Off
 *   - 1: Startup/Sync (Allowing Leading Coldstart)
 *   - 2: Sync
 *   - 3: Startup/Sync
 */
void FrSetKeySlot(long channel, long channelMask, long keySlotIndex, long keySlotId, long keySlotUsage);

}
