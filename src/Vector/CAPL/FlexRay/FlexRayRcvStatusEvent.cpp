#include "FlexRayRcvStatusEvent.h"

#include <iostream>

namespace capl
{

void FlexRayRcvStatusEvent(long msgTime, int channel, long statusType, long infomask1, long infomask2, long infomask3)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
