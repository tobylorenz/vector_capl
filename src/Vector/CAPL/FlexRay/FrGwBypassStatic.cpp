#include "FrGwBypassStatic.h"

#include <iostream>

namespace capl
{

long FrGwBypassStatic(long bypass, long channel, long slotId, long baseCycle, long cycleRepetition)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
