#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup FlexRay
 *
 * @brief
 *   Creates a FlexRay PDU object.
 *   Is called when a PDU with the corresponding name is received.
 *
 * @todo
 */
class FrPDU
{
public:
};

}
