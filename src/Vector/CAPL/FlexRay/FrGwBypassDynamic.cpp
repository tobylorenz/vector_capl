#include "FrGwBypassDynamic.h"

#include <iostream>

namespace capl
{

long FrGwBypassDynamic(long bypass, long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
