#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * Sets flags[i] to 1 if node (i + startIndex) is active and does not signal sleepreadiness.
 * The number of such nodes is returned (arguments cf. above).
 */
long NMGetAwakeConfig(unsigned long arraySize, byte * flags, unsigned long startIndex = 0);

}
