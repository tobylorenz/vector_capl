#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * @brief
 *   OsekNMTalk
 *
 * Network Management is switched to active, so that it can participate in the
 * ring.
 */
void TalkNM(void);

}
