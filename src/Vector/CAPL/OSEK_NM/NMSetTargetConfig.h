#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * Set the target NM configuration, i.e. flags[i] is the expected state of the node
 * (startIndex + i). The arguments have a meaning corresponding to 6.
 */
void NMSetTargetConfig(unsigned long arraySize, byte * flags, unsigned long startIndex = 0);

/**
 * @ingroup OSEK_NM
 *
 * @brief
 *   OsekNMSetConfigSoll
 *
 * Sets the desired configuration. The meaning of the bits is the same as for NMSetTargetConfig.
 */
void NMSetTargetConfig(unsigned long ConfigTarget);

}
