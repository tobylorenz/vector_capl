#include "SilentNM.h"

#include <iostream>

namespace capl
{

void SilentNM(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
