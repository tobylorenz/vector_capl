#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * This function transfers the ring data to the Network Management DLL. If the
 * node possesses currently the token, the application is permitted to write the
 * ring data, otherwise, the write access will be denied. The successful access
 * to the ring data will be indicated by returning a "1"; unsuccessful access returns
 * a "0".
 * The first argument (ArraySize) specifies the number of ring data that shall be
 * transmitted. The ringdata itself are contained in the array where the second
 * argument points to.
 */
unsigned int TransmitRingData(unsigned long ArraySize, byte * array);

}
