#pragma once

/**
 * @defgroup OSEK_NM OSEK Network Management CAPL Functions
 */

#include "TalkNM.h"
#include "SilentNM.h"
#include "GotoMode_BusSleep.h"
#include "GotoMode_Awake.h"
#include "NMGetStatus.h"
#include "NMGetConfig.h"
#include "GetConfig.h"
#include "NMSetTargetConfig.h"
#include "NMSetConfigMask.h"
#include "CmpConfig.h"
#include "NMGetAwakeConfig.h"
#include "NMSetWakeUpInfo.h"
#include "TransmitRingData.h"
#include "ReadRingData.h"

#include "ReinitSleepTimer.h"
