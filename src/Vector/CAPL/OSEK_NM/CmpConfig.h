#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * @brief
 *   OsekNMCmpConfig
 *
 * Checks to see if all the ECUs in the target configuration are participating in
 * Network Management, and considers the mask. Yields a value of 1 when all
 * desired ECUs are participating or 0 when one or more are absent.
 */
unsigned long CmpConfig(void);

}
