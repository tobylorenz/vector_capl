#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * All ECUs not marked with a 0 are relevant for the target configuration. Meaning
 * of arguments corresponding to NMGetConfig.
 */
void NMSetConfigMask(unsigned long arraySize, byte * flags, unsigned long startIndex = 0);

/**
 * @ingroup OSEK_NM
 *
 * @brief
 *   OsekNMSetConfigMask
 *
 * All ECUs designated with 1 are relevant for the target configuration.
 */
void NMSetConfigMask(unsigned long ConfigMask);

}
