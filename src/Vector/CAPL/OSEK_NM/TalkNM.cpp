#include "TalkNM.h"

#include <iostream>

namespace capl
{

void TalkNM(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
