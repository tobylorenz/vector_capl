#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup OSEK_NM
 *
 * The reason for wake-up is communicated to Network Management with this
 * function. This function should be called only in the function ApplReqWakeUpInfo()
 * (CALLBACK Function GetConfig).
 */
void NMSetWakeUpInfo(unsigned long WakeUpInfo);

}
