#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Allows changing the baud rate during the measurement.
 *
 * Allows changing the baudrate during the measurement.
 *
 * @param baudrate
 *   Value range: 200 baud - 115200 baud.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SetBaudrate(dword baudrate);

}
