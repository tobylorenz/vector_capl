#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Sets the P3 time which the tester waits until transmitting a further request.
 *
 * Sets the P3 time which the tester waits until transmitting a further request.
 *
 * @param P3_us
 *   P3 time in us.
 *   Value range: 0 – 10000000
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SetP3Tester(dword P3_us);

}
