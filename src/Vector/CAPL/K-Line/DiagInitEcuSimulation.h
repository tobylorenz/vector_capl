#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Initialize CAPL node to represent a diagnostics ECU simulation.
 *
 * Initialize CAPL node to represent a diagnostics ECU simulation.
 * From now on the ECU can interpret and use all diagnostic objects in the CAPL code of this node as defined by the respective diagnostic description.
 *
 * CANoe will initialize the CAPL callback interface for diagnostics during the call.
 *
 * @param ecuQualifier
 *   Qualifier of the ECU as set in the diagnostic configuration dialog for the respective diagnostic description.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long DiagInitEcuSimulation(char * ecuQualifier);

}
