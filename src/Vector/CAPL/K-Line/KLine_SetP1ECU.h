#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Sets the interbyte time of a response.
 *
 * Sets the interbyte time of a response.
 *
 * @param P1_us
 *   P1 in us
 *   Value range: 0 – 650000.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SetP1ECU(dword P1_us);

}
