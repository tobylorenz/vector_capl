#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Sets the timings of the fast init wake-up pattern.
 *
 * Sets the timings of the fast init wake-up pattern.
 *
 * @param TiniL_us
 *   Duration of the low phase in us.
 *   Value range: 2000 - 650000
 *
 * @param Twup_us
 *   Duration of the wakeup pattern.
 *   Value range: 2000 - 650000
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SetWakeUpTimesTester(dword TiniL_us, dword Twup_us);

}
