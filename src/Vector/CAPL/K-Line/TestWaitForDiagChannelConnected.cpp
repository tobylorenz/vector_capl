#include "TestWaitForDiagChannelConnected.h"

#include <iostream>

namespace capl
{

long TestWaitForDiagChannelConnected(dword timeout_ms)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
