#include "KLine_SetInitType.h"

#include <iostream>

namespace capl
{

long KLine_SetInitType(long initType)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
