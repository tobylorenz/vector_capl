#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Sets the address of the tester.
 *
 * Sets the address of the tester.
 *
 * @param testerAddress
 *   Address of the tester.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SetTesterAddress(byte testerAddress);

}
