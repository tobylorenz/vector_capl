#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Configures the used header format.
 *
 * Configures the used header format.
 *
 * @param useAddresses
 *   - 0: Address bytes are not used.
 *   - 1: Address bytes are used.
 *
 * @param forceLengthByte
 *   - 0: No Length byte will be sent.
 *   - 1: Length byte will always be sent.
 *   - 2: Length byte will be sent for frames greater than 63 bytes.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SetHeaderFormat(long useAddresses, long forceLengthByte);

}
