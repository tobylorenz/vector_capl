#include "DiagSendResponsePDU.h"

#include <iostream>

namespace capl
{

long DiagSendResponsePDU(word rawPDULength, byte * rawPDU)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
