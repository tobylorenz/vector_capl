#include "DiagIsChannelConnected.h"

#include <iostream>

namespace capl
{

long DiagIsChannelConnected()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
