#include "KLine_SetW4Tester.h"

#include <iostream>

namespace capl
{

long KLine_SetW4Tester(dword W4_us)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
