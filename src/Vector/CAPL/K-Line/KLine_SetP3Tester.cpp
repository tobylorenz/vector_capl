#include "KLine_SetP3Tester.h"

#include <iostream>

namespace capl
{

long KLine_SetP3Tester(dword P3_us)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
