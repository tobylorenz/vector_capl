#include "KLine_SetECUAddress.h"

#include <iostream>

namespace capl
{

long KLine_SetECUAddress(byte ecuAddress)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
