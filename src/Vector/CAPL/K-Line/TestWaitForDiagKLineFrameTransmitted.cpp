#include "TestWaitForDiagKLineFrameTransmitted.h"

#include <iostream>

namespace capl
{

long TestWaitForDiagKLineFrameTransmitted(dword timeout_ms)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
