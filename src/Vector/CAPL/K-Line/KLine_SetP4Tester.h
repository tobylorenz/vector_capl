#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Sets the interbyte time of a request.
 *
 * Sets the interbyte time of a request.
 *
 * @param P4_us
 *   P4 in us
 *   Value range: 0 – 650000.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SetP4Tester(dword P4_us);

}
