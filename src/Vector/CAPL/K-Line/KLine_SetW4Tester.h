#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Sets the W4 timing of the tester between keybyte2 and keybyte2Not.
 *
 * Sets the W4 timing of the tester between keybyte2 and keybyte2Not.
 *
 * @param W4_us
 *   W4 time in us.
 *   Value range: 0 - 650000
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_SetW4Tester(dword W4_us);

}
