#include "TestWaitForDiagKLineFrameReceived.h"

#include <iostream>

namespace capl
{

long TestWaitForDiagKLineFrameReceived(dword timeout_ms)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
