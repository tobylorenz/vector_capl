#include "KLine_SetBaudrate.h"

#include <iostream>

namespace capl
{

long KLine_SetBaudrate(dword baudrate)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
