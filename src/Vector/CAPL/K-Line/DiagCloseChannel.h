#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Closes a channel.
 *
 * Closes a diagnostic channel. State of the channel is Closed afterwards.
 *
 * The channel the function refers to is set with the last call of DiagSetTarget.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long DiagCloseChannel(void);

}
