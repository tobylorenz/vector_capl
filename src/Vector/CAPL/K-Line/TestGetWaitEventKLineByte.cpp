#include "TestGetWaitEventKLineByte.h"

#include <iostream>

namespace capl
{

long TestGetKLineByte()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TestGetKLineByte(int64 timeStampOut)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
