#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   If a K-Line frame is the last event that triggers a wait instruction, the content can be called up with this function.
 *
 * If a K-Line frame is the last event that triggers a wait instruction, the content can be called up with the first function.
 *
 * The second function allows to call the content and the end of byte time stamps.
 *
 * @param bufferOut
 *   Byte data buffer of the frame
 *
 * @param bufferLen
 *   Size of the data buffer.
 *
 * @return
 *   On success a value greater 0, otherwise a value lesser 0.
 */
long TestGetWaitEventKLineFrame(byte * bufferOut, dword bufferLen);

/**
 * @ingroup K-Line
 *
 * @brief
 *   If a K-Line frame is the last event that triggers a wait instruction, the content can be called up with this function.
 *
 * If a K-Line frame is the last event that triggers a wait instruction, the content can be called up with the first function.
 *
 * The second function allows to call the content and the end of byte time stamps.
 *
 * @param bufferOut
 *   Byte data buffer of the frame
 *
 * @param bufferLen
 *   Size of the data buffer.
 *
 * @param timeStampOut
 *   Buffer of the end of byte stamps.
 *
 * @return
 *   On success a value greater 0, otherwise a value lesser 0.
 */
long TestGetWaitEventKLineFrame(byte * bufferOut, dword bufferLen, int64 * timeStampOut);

}
