#include "KLine_Set5BaudAddressTester.h"

#include <iostream>

namespace capl
{

long KLine_Set5BaudAddressTester(byte fiveBaudAddress)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
