#include "KLine_SetP4Tester.h"

#include <iostream>

namespace capl
{

long KLine_SetP4Tester(dword P4_us)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
