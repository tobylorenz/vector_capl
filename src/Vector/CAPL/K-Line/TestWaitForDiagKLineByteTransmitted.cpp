#include "TestWaitForDiagKLineByteTransmitted.h"

#include <iostream>

namespace capl
{

long TestWaitForDiagKLineByteTransmitted(dword timeout_ms)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
