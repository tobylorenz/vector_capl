#include "DiagDisconnectChannel.h"

#include <iostream>

namespace capl
{

long DiagDisconnectChannel()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
