#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Retrieves the different parameters of the init patterns.
 *
 * Retrieves the different parameters of the init patterns.
 *
 * @param parameterID
 *   Value range: 0-10
 *   - 0: KeyByte1
 *   - 1: Keybyte2
 *   - 2: TiniL
 *   - 3: Twup
 *   - 4: W4 between key byte 2 and inverted key byte 2
 *   - 5: W1
 *   - 6: W2
 *   - 7: W3
 *   - 8: W4
 *   - 9: W4 between inverted keybyte 2 and inverted address byte
 *   - 10: BaudRate from sync pattern
 *
 * @return
 *   Parameter value
 */
long KLine_GetMeasuredValue(dword parameterID);

}
