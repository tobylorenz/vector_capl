#include "KLine_SetW5Tester.h"

#include <iostream>

namespace capl
{

long KLine_SetW5Tester(dword W5_us)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
