#include "KLine_CreateECURepresentation.h"

#include <iostream>

namespace capl
{

long KLine_CreateECURepresentation(dword channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
