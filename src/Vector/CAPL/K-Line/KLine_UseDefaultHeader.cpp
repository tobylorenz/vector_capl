#include "KLine_UseDefaultHeader.h"

#include <iostream>

namespace capl
{

long KLine_UseDefaultHeader()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
