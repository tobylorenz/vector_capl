#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   If a byte is the last event that triggers a wait instruction, the content can be called up with this function.
 *
 * If a byte is the last event that triggers a wait instruction, the content can be called up with the first function.
 *
 * The second function allows to call the content and the end of byte time stamp.
 *
 * @return
 *   Value of the byte
 */
long TestGetKLineByte();

/**
 * @ingroup K-Line
 *
 * @brief
 *   If a byte is the last event that triggers a wait instruction, the content can be called up with this function.
 *
 * If a byte is the last event that triggers a wait instruction, the content can be called up with the first function.
 *
 * The second function allows to call the content and the end of byte time stamp.
 *
 * @param timeStampOut
 *   End of byte time stamp.
 *
 * @return
 *   Value of the byte
 */
long TestGetKLineByte(int64 timeStampOut);

}
