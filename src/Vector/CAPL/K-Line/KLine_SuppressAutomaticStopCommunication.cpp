#include "KLine_SuppressAutomaticStopCommunication.h"

#include <iostream>

namespace capl
{

long KLine_SuppressAutomaticStopCommunication(long doSuppress)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
