#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup K-Line
 *
 * @brief
 *   Sets the 5 baud address for the 5 baud init pattern.
 *
 * Sets the 5 baud address for the 5 baud init pattern.
 *
 * @param fiveBaudAddress
 *   Five baud address of the ECU.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long KLine_Set5BaudAddressTester(byte fiveBaudAddress);

}
