#include "TestGetWaitEventKLineFrame.h"

#include <iostream>

namespace capl
{

long TestGetWaitEventKLineFrame(byte * bufferOut, dword bufferLen)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TestGetWaitEventKLineFrame(byte * bufferOut, dword bufferLen, int64 * timeStampOut)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
