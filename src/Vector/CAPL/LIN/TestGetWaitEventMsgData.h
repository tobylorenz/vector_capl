#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Calls up the frame content if a LIN frame with a valid response is the last event that triggers a wait instruction.
 *
 * @todo
 */

}
