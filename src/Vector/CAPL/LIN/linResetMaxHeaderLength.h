#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Resets the maximum header length to the protocol version dependent default.
 *
 * Resets the maximum header length to the protocol version dependent default.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linResetMaxHeaderLength();

}
