#include "linSetChecksumError.h"

#include <iostream>

namespace capl
{

long linSetChecksumError(long frameId, long activate)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
