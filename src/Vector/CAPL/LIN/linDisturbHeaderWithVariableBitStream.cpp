#include "linDisturbHeaderWithVariableBitStream.h"

#include <iostream>

namespace capl
{

dword linDisturbHeaderWithVariableBitStream(dword byteIndex, dword bitIndex, byte * bitStream, int64 * lengthInNS, dword numberOfBits, dword roundUp, dword timeoutPrevention)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
