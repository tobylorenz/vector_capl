#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Converts specified bit time to an absolute time.
 *
 * Converts specified bit time to an absolute time.
 *
 * The absolute time is calculated using the current baud rate on the channel determined by the CAPL program context.
 *
 * @param bitTimes
 *   Time in bits.
 *
 * @return
 *   Absolute time in ns.
 */
int64 linBits2Time_ns(dword bitTimes);

/**
 * @ingroup LIN
 *
 * @brief
 *   Converts specified bit time to an absolute time.
 *
 * Converts specified bit time to an absolute time.
 *
 * The absolute time is calculated using the current baud rate on the channel determined by the CAPL program context.
 *
 * @param bitTimes
 *   Time in bits.
 *
 * @param channel
 *   Channel number, whose baud rate will be used.
 *
 * @return
 *   Absolute time in ns.
 */
int64 linBits2Time_ns(dword channel, dword bitTimes);

}
