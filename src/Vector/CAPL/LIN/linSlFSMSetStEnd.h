#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Ends the configuration of the state.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
