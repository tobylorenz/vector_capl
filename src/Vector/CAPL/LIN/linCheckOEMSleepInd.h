#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Checks the sleep indication bits of all slave nodes defined in the LIN network.
 *
 * This function checks the sleep indication bits of all slave nodes defined in the LIN network (the channel is determined by the CAPL program context).
 *
 * @return
 *   Returns non-zero if all queried sleep indication bits are set, otherwise zero.
 */
long linCheckOEMSleepInd();

}
