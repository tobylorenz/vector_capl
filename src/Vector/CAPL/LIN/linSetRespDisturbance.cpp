#include "linSetRespDisturbance.h"

#include <iostream>

namespace capl
{

long linSetRespDisturbance (long frameId, long lengthInBits, long level, long offsetInSixteenthBits)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
