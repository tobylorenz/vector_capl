#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets/resets the response error flag for a calling slave node.
 *
 * Sets/resets the response error flag for a calling slave node.
 *
 * @param activate
 *   - 0: reset
 *   - 1: set
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetRespError(long activate);

}
