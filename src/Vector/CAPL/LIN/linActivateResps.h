#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Reactivates all frame responses published by the calling Slave node according to the LIN database file.
 *
 * Reactivates all frame responses published by the calling Slave node according to the LIN database file (LDF),
 * after having been previously deactivated by linDeactiveResps() or linResetSlave().
 *
 * Per default all frame responses are activated for Slave nodes on measurement start i.e. it is assumed that Slave nodes have non-volatile memory.
 *
 * LIN2.0 Slave nodes will automatically activate responses for re-configurable frames on receiving valid reconfiguration commands i.e. AssignFrameID.
 *
 * Individual frame responses can be activated manually using the function linSetRespCounter.
 *
 * @return
 *   Number of activated frame responses or -1 if an error occurs.
 */
int linActivateResps();

}
