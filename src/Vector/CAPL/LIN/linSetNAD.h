#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Changes the node address of the calling simulated slave.
 *
 * Changes the node address of the calling simulated slave.
 *
 * @param nad
 *   The new nad to be used by the simulated slave.
 *
 * @return
 *   On success zero, otherwise -1.
 */
dword linSetNAD(dword nad);

}
