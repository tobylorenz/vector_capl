#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets an inter-byte space for a specified frame and a specified byte filed.
 *
 * With this function it is possible to set an inter-byte space for a specified frame and a specified byte filed.
 * The byte field may belong to the frame header as well as to the frame response.
 *
 * Inter-byte space is the period between the end of the stop bit of the preceding byte and the start bit of the following byte.
 *
 * By default the inter-byte space is 0.
 *
 * @param frameID
 *   LIN frame identifier whose inter-byte space should be changed
 *   Value range: 0…63
 *
 * @param index
 *   Index of the byte field, in front of which the inter-byte space should be inserted.
 *   Value range: 0..N+1, where N=frame length
 *   - 0: Inter-byte is inserted between the Synch byte and the Identifier byte.
 *   - 1: Inter-byte is inserted in front of the first data byte.
 *   - N+1: Inter-byte is inserted in front of the Checksum byte.
 *
 * @param sixteenthBits
 *   Length of the inter-byte space to set [in units of 1/16th of bit time].
 *   Value range: 0..65535. This corresponds to 0..4095.93 bit times.
 *   For a LIN standard compliance: The maximum space between the bytes cannot exceed 40% duration compared to nominal transmission time.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSetInterByteSpace(long frameID, long index, dword sixteenthBits);

}
