#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Detailed description of the linHeader selectors.
 */
class linHeader
{
public:
    /**
     * LIN Frame identifier (6 bits)
     *
     * Value range: 0..63
     */
    byte ID;

    /**
     * Number of data bytes contained in the frame (Data Length Code).
     *
     * Value range: 0..8
     */
    byte DLC;
};

}
