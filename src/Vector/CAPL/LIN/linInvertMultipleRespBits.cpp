#include "linInvertMultipleRespBits.h"

#include <iostream>

namespace capl
{

dword linInvertMultipleRespBit(long frameID, dword * byteIndices, dword * bitIndices, dword * numberOfDisturbedBits, dword * levels, dword arrSize, dword numberOfExecutions)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
