#pragma once

#include "../DataTypes.h"
#include "LIN.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets a checksum (that is usually not a correct one) for a LIN frame.
 *
 * Sets a checksum (that is usually not a correct one) for a LIN frame.
 *
 * @param linFrame
 *   LIN frame for which a checksum will be set.
 *
 * @param csValue
 *   The checksum value to be set.
 */
void linSetManualChecksum(linMessage linFrame, byte csValue);

}
