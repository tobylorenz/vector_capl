#pragma once

#include "../DataTypes.h"
#include "../LIN/LIN.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a start time stamp of a LIN bus event.
 *
 * @deprecated
 *   Replaced by linMessage selectors
 *
 * This function can be used to retrieve a start time stamp of a LIN bus event.
 * The resulting time stamp is a time elapsed since the measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type frame.
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetStartOfFrame(linMessage busEvent);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a start time stamp of a LIN bus event.
 *
 * @deprecated
 *   Replaced by linCsError selectors
 *
 * This function can be used to retrieve a start time stamp of a LIN bus event.
 * The resulting time stamp is a time elapsed since the measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type checksum error.
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetStartOfFrame(linCsError busEvent);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a start time stamp of a LIN bus event.
 *
 * @deprecated
 *   Replaced by linReceiveError selectors
 *
 * This function can be used to retrieve a start time stamp of a LIN bus event.
 * The resulting time stamp is a time elapsed since the measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type receive error.
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetStartOfFrame(linReceiveError busEvent);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a start time stamp of a LIN bus event.
 *
 * @deprecated
 *   Replaced by linTransmError selectors
 *
 * This function can be used to retrieve a start time stamp of a LIN bus event.
 * The resulting time stamp is a time elapsed since the measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type transmission error.
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetStartOfFrame(linTransmError busEvent);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a start time stamp of a LIN bus event.
 *
 * @deprecated
 *   Replaced by linSyncError selectors
 *
 * This function can be used to retrieve a start time stamp of a LIN bus event.
 * The resulting time stamp is a time elapsed since the measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type synchronization error.
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetStartOfFrame(linSyncError busEvent);

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves a start time stamp of a LIN bus event.
 *
 * @deprecated
 *   Replaced by linWakeupFrame selectors
 *
 * This function can be used to retrieve a start time stamp of a LIN bus event.
 * The resulting time stamp is a time elapsed since the measurement start [in units of 10 µsec].
 *
 * @param busEvent
 *   LIN bus event of type wakeup signal.
 *
 * @return
 *   Returns the retrieved time stamp [in units of 10 µsec] or 0 on failure.
 */
dword linGetStartOfFrame(linWakeupFrame busEvent);

}
