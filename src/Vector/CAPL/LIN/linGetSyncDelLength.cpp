#include "linGetSyncDelLength.h"

#include <iostream>

namespace capl
{

dword linGetSyncDelLength(linMessage busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetSyncDelLength(linCsError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetSyncDelLength(linReceiveError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetSyncDelLength(linTransmError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetSyncDelLength(linSyncError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
