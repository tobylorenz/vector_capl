#include "linStartDisturbance.h"

#include <iostream>

namespace capl
{

long linStartDisturbance(dword length, dword level)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
