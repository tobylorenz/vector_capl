#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Returns the current transmission speed of the RS232 port to the hardware.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
