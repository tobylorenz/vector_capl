#include "linETFSetDirtyFlag.h"

#include <iostream>

namespace capl
{

long linETFSetDirtyFlag(long assocId, long dirty)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
