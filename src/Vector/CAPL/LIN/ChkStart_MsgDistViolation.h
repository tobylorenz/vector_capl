#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   This check is useful for spontaneous messages where one message depends to another message; e.g. for token-ring initializations for network management.
 *
 * @todo
 */

}
