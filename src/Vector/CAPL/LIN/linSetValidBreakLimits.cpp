#include "linSetValidBreakLimits.h"

#include <iostream>

namespace capl
{

dword linSetValidBreakLimits(dword breakMin16thBits, dword breakMax16thBits, dword delMin16thBits, dword delMax16thBits)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
