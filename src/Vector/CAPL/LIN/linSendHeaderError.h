#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sends a header with the current sync break, sync delimiter and interbyte space settings and the specified sync byte and id byte.
 *
 * This function sends a header with the current sync break, sync delimiter and interbyte space settings and the specified sync byte and id byte.
 *
 * @param syncByte
 *   Data value to be sent in the Synch Byte field.
 *   Correct value is 0x55
 *   Value range: 0..0xFF
 *
 * @param idWithParity
 *   Data value to be sent in the Protected Identifier field.
 *   The lower 6 bits specify the identifier to be used.
 *   The upper 2 bits specify the identifier parity to be used.
 *   The correct value can be calculated using the linGetProtectedID function.
 *   Value range: 0..0xFF
 *
 * @param StopAfterError
 *   Specifies whether the transmission should be stopped after an error.
 *   If an incorrect synch byte is set by this function and StopAfterError is 1, the identifier will not be sent.
 *   A simulated message response (has to be activated independently) will generally not be sent if there is an error in the header.
 *   Value range: 0..1
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSendHeaderError(byte syncByte, byte idWithParity, byte StopAfterError);

}
