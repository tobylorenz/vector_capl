#include "linDeactivateResps.h"

#include <iostream>

namespace capl
{

int linDeactivateResps()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
