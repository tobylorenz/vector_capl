#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets the dirty flag for the LIN master request frame.
 *
 * Sets the dirty flag for the LIN master request frame (frame identifier=0x3C).
 * The master request only gets transmitted when the dirty flag is set.
 *
 * @param dirty
 *   - 0: clear the dirty flag
 *   - 1: set the dirty flag
 *
 * @return
 *   On success a value unequal to zero, otherwise zero.
 */
dword linSetMasterRequestDirtyFlag(dword dirty);

}
