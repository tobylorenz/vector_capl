#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets/resets the wakeup indication bit for a calling slave node.
 *
 * Sets/resets the wakeup indication bit for a calling slave node.
 *
 * @param active
 *   - 0: set
 *   - 1: reset
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetOEMWakeupInd(long active);

}
