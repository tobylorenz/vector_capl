#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Changes the wake-up setting.
 *
 * This function changes the wake-up setting.
 *
 * @param restartScheduler
 *   - 0: After wakeup the current schedule table is started with the slot before entering sleep mode.
 *   - 1: After wakeup the current schedule table is started from the beginning.
 *   In the case no schedule tables are defined this parameter is ignored.
 *
 * @param wakeupIdentifier
 *   LIN frame identifier to be sent additionally directly after sending a wakeup signal.
 *   If an invalid identifier is specified i.e. not in the range 0..63:
 *   when schedule tables are defined no special wakeup identifier is sent;
 *   when no schedule tables are defined a SynchBreak / SynchField pair without an identifier is sent;
 *   Value range: 0..0xFF
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linChangeWakeupSettings(byte restartScheduler, long wakeupIdentifier);

}
