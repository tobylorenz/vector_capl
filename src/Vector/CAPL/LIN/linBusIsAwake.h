#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts a specified bit when the next bus event for a specified ID occurs.
 *
 * Queries the awake state of the LIN bus.
 *
 * @return
 *   Returns a value unequal to zero if the bus is awake, otherwise zero.
 */
long linBusIsAwake();

}
