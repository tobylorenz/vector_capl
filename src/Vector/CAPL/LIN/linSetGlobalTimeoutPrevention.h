#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Activates/deactivates the global timeout prevention for transceivers with a dominant timeout.
 *
 * Activates/deactivates the global timeout prevention for transceivers with a dominant timeout.
 * If active, the cab/piggy will use an inbuilt transistor to keep the bus signal at a dominant level
 * after the dominant timeout stopped the transceiver from transmitting a dominant signal.
 *
 * @param on
 *   - 0: Disable
 *   - 1: Enable
 *
 * @return
 *  On success, a value unequal to zero, otherwise zero.
 */
dword linSetGlobalTimeoutPrevention(dword on);

}
