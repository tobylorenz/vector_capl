#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets the maximum header length in bit times.
 *
 * Sets the maximum header length in bit times.
 *
 * @param bitTimes
 *   The new maximum header length in bit times.
 *   Headers longer than the maximum header length will be considered illegal.
 *   CANoe will not generate any response to these headers.
 *   Value range: 31-255
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSetMaxHeaderLength(long bitTimes);

}
