#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Converts specified system time to bit time.
 *
 * This function converts specified system times to bit times.
 * The conversion is done using the  current baud rate on the channel determined by the CAPL program context.
 *
 * @param time
 *   System time to be converted [in ns].
 *
 * @return
 *   Resulting bit time.
 */
dword linTime2Bits_ns(int64 time);

/**
 * @ingroup LIN
 *
 * @brief
 *   Converts specified system time to bit time.
 *
 * This function converts specified system times to bit times.
 * The conversion is done using the  current baud rate on the channel determined by the CAPL program context.
 *
 * @param time
 *   System time to be converted [in ns].
 *
 * @param channel
 *   Channel number, whose baud rate will be used.
 *
 * @return
 *   Resulting bit time.
 */
dword linTime2Bits_ns(dword channel, int64 time);

}
