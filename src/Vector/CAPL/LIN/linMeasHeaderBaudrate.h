#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets request to measure the baud rate value for next LIN header event.
 *
 * With this function it is possible to set request to measure the baud rate value for next LIN header event.
 * Channel, where the baud rate is measured, is determined by the CAPL program context.
 *
 * This function only initiates the baud rate measurement.
 * The result becomes available when the next LIN header occurs and it remains valid until next measurement request.
 * To retrieve the result the function linGetMeasBaudrate() shall be used.
 *
 * @param index
 *   This parameter specifies a field of LIN header which will be used for baud rate measurement.
 *   - 0: Synch field
 *   - 1: Identifier field
 *
 * @return
 *   On successful request returns zero, otherwise -1.
 */
long linMeasHeaderBaudrate(int index);

}
