#include "linDisturbRespWithBitStream.h"

#include <iostream>

namespace capl
{

dword linDisturbRespWithBitStream(long disturbedFrameId, dword byteIndex, dword bitIndex, byte * bitStream, dword numberOfBits, dword timeoutPrevention)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
