#include "linCheckRespError.h"

#include <iostream>

namespace capl
{

long linCheckRespError()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
