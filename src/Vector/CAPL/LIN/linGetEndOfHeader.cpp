#include "linGetEndOfHeader.h"

#include <iostream>

namespace capl
{

dword linGetEndOfHeader(linMessage busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetEndOfHeader(linCsError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linGetEndOfHeader(linReceiveError busEvent)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
