#include "linSetRespBitStream.h"

#include <iostream>

namespace capl
{

long linSetRespBitStream(long frameId, byte * bitStream, long numberOfBits)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long linSetRespBitStream(long frameId, byte * bitStream, long numberOfBits, long timeoutPrevention)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long linSetRespBitStream(long frameId, byte * bitStream, long numberOfBits, long timeoutPrevention, long count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
