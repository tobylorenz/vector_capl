#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * This procedure is called for SlaveTimeouts.
 * The timeout of an FSM status can be set with LINSlFSMSetStTO.
 */
class linSlaveTimeout
{
public:
    /**
     * The time stamp of the timeout with which the global time base was synchronized on the PC (CAN hardware or PC system clock).
     * The time stamp must be used if you want to take into consideration time relations with events from other sources.
     * This time stamp is also generated in the Trace Window.
     *
     * Unit:10 microseconds.
     */
    dword Time;

    /**
     * Time stamp generated by the LIN hardware.
     * It will only prove useful in isolated cases to use this time stamp that is unsynchronized and thus still original.
     * The time stamp should only be used to compare the time of two LIN hardware generated events.
     *
     * Unit: 10 microseconds.
     */
    dword MsgOrigTime;

    /**
     * Status of MsgOrigTime:
     * - Bit 0 set: Time stamp is valid
     * - Bit 0 not set: Time stamp is invalid
     * - Bit 1 set: Time stamp was generated by software
     * - Bit 1 not set: Time stamp was generated by hardware
     * - Bit 4: Has a bus-specific meaning; not currently in use for LIN
     */
    byte MsgTimeStampStatus;

    /**
     * The channel through which the timeout was received.
     */
    word MsgChannel;

    /**
     * The Identifier of the FSM that initiated the timeout.
     */
    byte lin_FsmID;

    /**
     * The status of the FSM in which the timeout occurred.
     */
    byte lin_FsmState;

    /**
     * The status to which the FSM switched after the timeout.
     */
    byte lin_FollowStateID;
};

}
