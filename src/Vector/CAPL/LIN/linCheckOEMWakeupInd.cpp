#include "linCheckOEMWakeupInd.h"

#include <iostream>

namespace capl
{

long linCheckOEMWakeupInd()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
