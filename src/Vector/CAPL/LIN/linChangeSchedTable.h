#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Switches from the current schedule table to another one.
 *
 * This function switches from the current schedule table to another one.
 *
 * By calling this function in the event procedure on preStart, it is possible to specify in which schedule table the measurement should start.
 *
 * @param tableIndex
 *   Index of the schedule table to be changed to.
 *   Value range: 0..N-1, where N is a total number of  defined schedule tables
 *
 * @param slotIndex
 *   Index of slot to be started within the new schedule table.
 *   Value range: 0..Y-1, where Y is a total number of slots in the new schedule table
 *
 * @param onSlotIndex
 *   Index of last slot in the current schedule table to be sent before changing to the new schedule table.
 *   Value range: -2..X-1, where X is a total number of slots in the current schedule table.
 *   Value: -1 - makes change on reaching the end of current schedule table.
 *   Value: -2 - makes change immediately.
 *
 * @return
 *   Index of the current schedule table or -1 if no active schedule table exists and on failure.
 */
long linChangeSchedTable(dword tableIndex, dword slotIndex = 0, dword onSlotIndex = -2);

}
