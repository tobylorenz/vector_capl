#include "linSetInterByteSpaces.h"

#include <iostream>

namespace capl
{

dword linSetInterByteSpaces(dword * arrayOfSixteenthBits)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linSetInterByteSpaces(long frameID, dword * arrayOfSixteenthBits)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
