#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Deactivates the bit stream response of the specified frame.
 *
 * Deactivates the bit stream response of the specified frame.
 *
 * @param frameId
 *   LIN frame identifier in the range 0 .. 63
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linResetRespBitStream(long frameId);

}
