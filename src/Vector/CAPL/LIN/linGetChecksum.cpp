#include "linGetChecksum.h"

#include <iostream>

namespace capl
{

byte linGetChecksum(linMessage linFrame)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

byte linGetChecksum(linCsError linCsError)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
