#include "linActivateSlaveNetworkManagement.h"

#include <iostream>

namespace capl
{

long linActivateSlaveNetworkManagement(long active)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
