#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Stops the internal scheduler.
 *
 * This function stops the internal scheduler.
 * This way a cyclical traversal of the current schedule table is stopped.
 *
 * Scheduler can be started again by calling linStartScheduler() function.
 */
void linStopScheduler();

}
