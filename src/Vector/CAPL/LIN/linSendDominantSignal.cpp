#include "linSendDominantSignal.h"

#include <iostream>

namespace capl
{

dword linSendDominantSignal(dword lengthInMicroseconds)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
