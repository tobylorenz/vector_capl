#include "linSetScopeTrigger.h"

#include <iostream>

namespace capl
{

long linSetScopeTrigger (long ID, long Position)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long linSetScopeTrigger (long eventCount, long triggerCount, long triggerMask, byte * idMask)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
