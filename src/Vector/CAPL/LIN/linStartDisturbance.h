#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Starts a disturbance on the bus.
 *
 * Starts a disturbance on the bus.
 *
 * Note that disturbances shorter than 64 bit times
 * - cannot be stopped with linStopDisturbance().
 * - may fail, if the hardware simultaneously transmits on this channel.
 *
 * @param length
 *   Length of the disturbance in bit times.
 *   - -1: permanent disturbance
 *
 * @param level
 *   The level of the disturbance.
 *   0: dominant disturbance (inverts recessive bits)
 *   1: recessive disturbance (inverts dominant bits - requires mag-Cab/Piggy and external power supply)
 *
 * @return
 *   On success a value unequal to zero, otherwise zero.
 */
long linStartDisturbance(dword length, dword level);

}
