#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Reports the flash mode state on high speed capable transceivers.
 *
 * This function reports the flash mode state on high speed capable transceivers.
 *
 * @return
 *   Returns 1 if flash mode is active, otherwise 0.
 */
dword linIsFlashModeActive();

}
