#include "linSetGlobalInterByteSpace.h"

#include <iostream>

namespace capl
{

dword linSetGlobalInterByteSpace(dword sixteenthBits)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
