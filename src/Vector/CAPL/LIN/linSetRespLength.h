#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Configures how many data bytes of the frame response should be sent.
 *
 * This function can be used to configure how many data bytes of the frame response should be sent.
 * The frame's length is not changed by this function.
 *
 * To send a correct response the numberOfBytes should be set equal to DLC+1 i.e for the frame data bytes and their checksum.
 *
 * If the numberOfBytes is to set to a value larger than DLC + 1, then a complete response is also sent.
 *
 * If the numberOfBytes is set to a value smaller than DLC+1, then a receiving error will occur or if numberOfBytes = 0 a transmission error.
 *
 * @param frameID
 *   Identifier of the LIN frame to be configured.
 *   Value range: 0…63
 *
 * @param numberOfBytes
 *   Number of bytes to be sent in the frame response (including checksum).
 *   Value range: 0..DLC+1
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetRespLength(long frameID, long numberOfBytes);

}
