#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Limits the number of responses sent for the specified frame identifier.
 *
 * With this function it is possible to limit the number of responses sent for the specified frame identifier.
 *
 * By setting count = 0 it is possible to deactivate the frame response completely.
 *
 * With count = -1 a frame response is always sent when a correct frame header is received.
 * This is the default configuration for newly configured frames.
 *
 * @param frameID
 *   Identifier of the LIN frame to be configured
 *   Value range: 0…63
 *
 * @param count
 *   Number of responses to be sent.
 *   n-times: 0..254
 *   unlimited: -1
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetRespCounter(long frameID, long count);

}
