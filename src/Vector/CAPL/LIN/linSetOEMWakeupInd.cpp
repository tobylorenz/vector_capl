#include "linSetOEMWakeupInd.h"

#include <iostream>

namespace capl
{

long linSetOEMWakeupInd(long active)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
