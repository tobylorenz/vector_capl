#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Retrieves time stamps of all falling edges in the disturbed byte or in the pseudo-byte caused by the last bit inversion.
 *
 * @todo
 */

}
