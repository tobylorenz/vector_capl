#include "linSetOEMSleepInd.h"

#include <iostream>

namespace capl
{

long linSetOEMSleepInd(long active)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
