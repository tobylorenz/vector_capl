#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Changes the Data Length Code of a LIN frame during the measurement.
 *
 * Changes the Data Length Code (i.e. length in bytes) of a LIN frame during the measurement.
 *
 * Per default the DLC of LIN frames is initialized according to the LIN Description File (LDF).
 * This function is therefore only needed if you are simulating LIN nodes without using a LDF and
 * need to change the DLC of a LIN frame during the measurement.
 *
 * To initialize the DLC of a LIN frame the function linSetDlc should be used in the event procedure on preStart.
 *
 * @param frameID
 *   LIN frame identifier in the range 0 .. 63
 *
 * @param dlc
 *   Frame length in bytes in the range 1 .. 8
 *
 * @return
 *   If successful a value unequal to zero.
 */
long linChangeDlc(long frameID, long dlc);

}
