#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Returns the currently set checksum of a LIN frame or LIN checksum error.
 *
 * @deprecated
 *
 * @todo
 */

}
