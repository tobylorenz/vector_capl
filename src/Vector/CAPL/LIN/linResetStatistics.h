#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Resets LIN channel statistics.
 *
 * Resets LIN channel statistics.
 */
void linResetStatistics();

/**
 * @ingroup LIN
 *
 * @brief
 *   Resets LIN channel statistics.
 *
 * Resets LIN channel statistics.
 *
 * @return
 *   LIN channel (1-based)
 */
void linResetStatistics(long channel);

}
