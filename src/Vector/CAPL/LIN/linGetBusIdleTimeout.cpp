#include "linGetBusIdleTimeout.h"

#include <iostream>

namespace capl
{

dword linGetBusIdleTimeout()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
