#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Converts specified system time to bit time.
 *
 * @deprecated
 *   Replaced by linTime2Bits_ns
 *
 * This function converts specified system times to bit times.
 * The conversion is done using the current baud rate on the channel determined by the CAPL program context.
 *
 * @param time
 *   System time to be converted [in units of 10 µsec].
 *
 * @return
 *   Resulting bit time.
 */
dword linTime2Bits(dword time);

/**
 * @ingroup LIN
 *
 * @brief
 *   Converts specified system time to bit time.
 *
 * @deprecated
 *   Replaced by linTime2Bits_ns
 *
 * This function converts specified system times to bit times.
 * The conversion is done using the current baud rate on the channel determined by the CAPL program context.
 *
 * @param time
 *   System time to be converted [in units of 10 µsec].
 *
 * @param channel
 *   Channel number, whose baud rate will be used.
 *
 * @return
 *   Resulting bit time.
 */
dword linTime2Bits(dword channel, dword time);

}
