#include "linResetRespDisturbance.h"

#include <iostream>

namespace capl
{

long linResetRespDisturbance(long frameId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
