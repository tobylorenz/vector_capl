#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Activates the measurement of the falling edges of the specified bytes in the next message.
 *
 * This function activates the measurement of the falling edges of the specified bytes in the next message with the correct id
 * if specified or in the next message if no id was specified.
 * The measured values can be queried with LINGetMeasEdgeTimeDiffs.
 *
 * @param numIndices
 *   The number of specified byte indices.
 *
 * @param indices
 *   An array containing the indices of the bytes to be measured.
 *   Note that the minimum size for this array is numIndices.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linMeasEdgeTimeDiffs(dword numIndices, long * indices);

/**
 * @ingroup LIN
 *
 * @brief
 *   Activates the measurement of the falling edges of the specified bytes in the next message.
 *
 * This function activates the measurement of the falling edges of the specified bytes in the next message with the correct id
 * if specified or in the next message if no id was specified.
 * The measured values can be queried with LINGetMeasEdgeTimeDiffs.
 *
 * @param numIndices
 *   The number of specified byte indices.
 *
 * @param indices
 *   An array containing the indices of the bytes to be measured.
 *   Note that the minimum size for this array is numIndices.
 *
 * @param id
 *   The lin-identifier of the message to be measured.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linMeasEdgeTimeDiffs(dword numIndices, long * indices, dword id);

}
