#include "linInvertRespBit.h"

#include <iostream>

namespace capl
{

dword linInvertRespBit (long frameID, dword byteIndex, dword bitIndex)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linInvertRespBit (long frameID, dword byteIndex, dword bitIndex, dword level)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linInvertRespBit (long frameID, dword byteIndex, dword bitIndex, dword level, dword numberOfExecutions)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linInvertRespBit (long frameID, dword byteIndex, dword bitIndex, dword level, dword numberOfExecutions, dword reportFallingEdges)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
