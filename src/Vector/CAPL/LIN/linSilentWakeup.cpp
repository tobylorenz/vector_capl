#include "linSilentWakeup.h"

#include <iostream>

namespace capl
{

long linSilentWakeup()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
