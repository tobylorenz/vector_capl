#include "linSetGlobalTimeoutPrevention.h"

#include <iostream>

namespace capl
{

dword linSetGlobalTimeoutPrevention(dword on)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
