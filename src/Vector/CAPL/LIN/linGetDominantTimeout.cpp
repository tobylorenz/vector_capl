#include "linGetDominantTimeout.h"

#include <iostream>

namespace capl
{

int64 linGetDominantTimeout()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
