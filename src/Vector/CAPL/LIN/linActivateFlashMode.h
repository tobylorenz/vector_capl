#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Activates the flash mode on high speed capable transceivers.
 *
 * This function activates flash mode on high speed capable transceivers.
 * Note that in flash mode such a transceiver will use faster rising and falling edges and will disregard the EMC limitations of the LIN network.
 * Note also, that the activation of the flash mode cannot be done while the channel is transmitting or the scheduler is running.
 *
 * @param activate
 *   - 1 will activate the flash mode,
 *   - 0 will deactivated it.
 *
 * @return
 *   On successful request returns 1, otherwise 0.
 */
dword linActivateFlashMode(byte activate);

}
