#include "linBusIsAwake.h"

#include <iostream>

namespace capl
{

long linBusIsAwake()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
