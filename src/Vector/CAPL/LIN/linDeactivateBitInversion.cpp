#include "linDeactivateBitInversion.h"

#include <iostream>

namespace capl
{

long LINDeactivateBitInversion()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
