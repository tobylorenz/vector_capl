#include "linSetRespError.h"

#include <iostream>

namespace capl
{

long linSetRespError(long activate)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
