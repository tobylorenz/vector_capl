#include "linActivateResps.h"

#include <iostream>

namespace capl
{

int linActivateResps()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
