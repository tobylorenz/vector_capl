#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Queries the Data Length Code of a LIN frame.
 *
 * Queries the Data Length Code (i.e. length in bytes) of a LIN frame.
 *
 * Per default the DLC of LIN frames is initialized according to the LIN Description File (LDF).
 *
 * If no LDF is used, this function returns the DLC initialized in on preStart e.g. using linSetDlc, for LIN frames sent by simulated LIN nodes.
 *
 * For LIN frames whose DLC has not been initialized, this function will return the default DLC for the selected ID as defined in the LIN1.1 protocol specification.
 *
 * For LIN frames sent by external LIN nodes, the measured DLC is returned.
 *
 * @param frameID
 *   LIN frame identifier in the range 0 .. 63.
 *
 * @return
 *   If successful the frame length [in bytes] or -1 on failure.
 */
long linGetDlc(long frameID);

}
