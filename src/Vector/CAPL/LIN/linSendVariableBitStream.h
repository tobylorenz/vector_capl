#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sends an arbitrary bit stream with bits of variable length.
 *
 * Sends an arbitrary bit stream with bits of variable length on the LIN bus.
 *
 * @param dataBuffer
 *   The bit stream to be sent.
 *   Maximum number of bits: 2^31-1.
 *
 * @param lengthInNS
 *   The length of each bit in nanoseconds.
 *
 * @param numberOfBits
 *   The number of bits in the bitStream-array.
 *   Note that while the dataBuffer-array will usually have a size of ceil(numberOfBits / 8),
 *   the size of lengthInNS will need to be at least numberOfBits
 *
 * @param roundUp
 *   If true, the lengths specified in lengthInNS will be rounded up to the next possible length that can be transmitted by the LIN hardware,
 *   otherwise the lengths will be rounded down.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSendVariableBitStream(byte * dataBuffer, int64 * lengthInNS, dword numberOfBits, dword roundUp);

/**
 * @ingroup LIN
 *
 * @brief
 *   Sends an arbitrary bit stream with bits of variable length.
 *
 * Sends an arbitrary bit stream with bits of variable length on the LIN bus.
 *
 * @param dataBuffer
 *   The bit stream to be sent.
 *   Maximum number of bits: 2^31-1.
 *
 * @param lengthInNS
 *   The length of each bit in nanoseconds.
 *
 * @param numberOfBits
 *   The number of bits in the bitStream-array.
 *   Note that while the dataBuffer-array will usually have a size of ceil(numberOfBits / 8),
 *   the size of lengthInNS will need to be at least numberOfBits
 *
 * @param roundUp
 *   If true, the lengths specified in lengthInNS will be rounded up to the next possible length that can be transmitted by the LIN hardware,
 *   otherwise the lengths will be rounded down.
 *
 * @param timeoutPrevention
 *   - 0: deactivates the timeout prevention for the 7259-transceiver.
 *   - 1: activates the timeout prevention for the 7259-transceiver.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSendVariableBitStream(byte * dataBuffer, int64 * lengthInNS, dword numberOfBits, dword roundUp, dword timeoutPrevention);

}
