#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Inverts the multiple bits in specified locations in the next LIN header.
 *
 * Inverts the multiple bits in specified locations in the next LIN header.
 *
 * @param frameID
 *   ID of the bus event to be manipulated.
 *   Value range: 0..63
 *
 * @param byteIndices
 *   An array of the indices of the bytes to be manipulated (use 0 for the first byte).
 *   If an index is equal to the frame’s length, then the checksum byte will be manipulated.
 *   An index larger than the frame length is invalid.
 *   Value range: 0..N, where N is frame length
 *
 * @param bitIndices
 *   An array of the indices of the bits to be manipulated.
 *   An index in the range 0-7 will manipulate a data bit, while the index 8 will manipulate the stop bit.
 *   Higher index values will cause the interbyte-space after the data byte to be manipulated.
 *   In this case, the user should make sure that the interbyte-space is large enough.
 *   Value range: 0..255
 *
 * @param numberOfDisturbedBits
 *   An array specifying how many consecutive bits shall be inverted at the specified locations.
 *
 * @param levels
 *   An array of the levels of the disturbance
 *   - 0: Dominant (inverts recessive bit)
 *   - 1: Recessive (inverts dominant bit – requires mag-Cab/Piggy and external power supply)
 *
 * @param arrSize
 *   The number of elements in the above arrays.
 *   This value should not be greater than the size of the smallest array.
 *
 * @param numberOfExecutions
 *   The number of consecutive frames with the specified identifier in which the bit inversions will be executed.
 *   Default: 1 (single shot).
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linInvertMultipleRespBit(long frameID, dword * byteIndices, dword * bitIndices, dword * numberOfDisturbedBits, dword * levels, dword arrSize, dword numberOfExecutions = 1);


}
