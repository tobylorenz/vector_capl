#include "linIsFlashModeActive.h"

#include <iostream>

namespace capl
{

dword linIsFlashModeActive()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
