#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Deactivates the specified slot of schedule table.
 *
 * This function deactivates the specified slot of schedule table.
 * For example, it can be used to turn slots off in a diagnostic table.
 *
 * By calling this function in the event procedure on preStart, it is possible to configure initial state of schedule table.
 *
 * @param tableIndex
 *   Index of the schedule table to be configured.
 *   Value range: 0..N-1, where N is a total number of defined schedule tables
 *
 * @param slotIndex
 *   Index of slot to be deactivated within the specified schedule table.
 *   Value range: 0..Y-1, where Y is a total number of slots in the specified schedule table
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linDeactivateSlot(dword tableIndex, dword slotIndex);

}
