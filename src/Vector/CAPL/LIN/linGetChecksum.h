#pragma once

#include "../DataTypes.h"
#include "LIN.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Returns the checksum of a LIN frame.
 *
 * Returns the checksum of a LIN frame.
 *
 * Checksum model (classic/enhanced) is determined automatically.
 *
 * @param linFrame
 *   LIN frame object
 *
 * @return
 *   Calculated checksum
 */
byte linGetChecksum(linMessage linFrame);

/**
 * @ingroup LIN
 *
 * @brief
 *   Returns the checksum of a LIN checksum error.
 *
 * Returns the checksum of a LIN checksum error.
 *
 * Checksum model (classic/enhanced) is determined automatically.
 *
 * @param linCsError
 *   LIN checksum error object
 *
 * @return
 *   Calculated checksum
 */
byte linGetChecksum(linCsError linCsError);

}
