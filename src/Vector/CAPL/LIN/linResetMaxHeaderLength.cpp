#include "linResetMaxHeaderLength.h"

#include <iostream>

namespace capl
{

dword linResetMaxHeaderLength()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
