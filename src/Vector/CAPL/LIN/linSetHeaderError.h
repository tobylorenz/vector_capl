#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets invalid parameters in a transmitted LIN header.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
