#include "linInvertHeaderBit.h"

#include <iostream>

namespace capl
{

dword linInvertHeaderBit(dword byteIndex, dword bitIndex)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linInvertHeaderBit(dword byteIndex, dword bitIndex, dword level)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linInvertHeaderBit(dword byteIndex, dword bitIndex, dword level, dword numberOfExecutions)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linInvertHeaderBit(dword byteIndex, dword bitIndex, dword level, dword numberOfExecutions, dword reportFallingEdges)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword linInvertHeaderBit(dword byteIndex, dword bitIndex, dword level, dword numberOfExecutions, dword reportFallingEdges, long disturbAfterHeaderID, dword waitForHeaders)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
