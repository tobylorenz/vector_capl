#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Changes length of break/synch symbol parts.
 *
 * With this function it is possible to change length of break/synch symbol parts.
 * Particularly the length of its synch break (dominant bits) and its synch delimiter (recessive bits).
 *
 * The version using float parameters allows setting the break and delimiter lengths in increments of 1/16th bits,
 * but the unit still is bit times (i.e. linSetBreakLength(14.5, 2.5) will set a break length of 14 8/16th bit times and
 * a delimiter length of 2 8/16th bit times).
 * Note that setting non-integer break and delimiter lengths is not supported by every hardware.
 *
 * @param syncBreakLen
 *   Synch break length in bit times.
 *   Default value: 18
 *   Value range: 10..30
 *   Values required for a LIN compliant header: >= 13
 *
 * @param syncDelLen
 *   Synch delimiter length in bit times.
 *   Default value: 2
 *   Value range: 1..30
 *   Values required for a LIN compliant header: >= 1
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSetBreakLength(dword syncBreakLen = 18, dword syncDelLen = 2);

/**
 * @ingroup LIN
 *
 * @brief
 *   Changes length of break/synch symbol parts.
 *
 * With this function it is possible to change length of break/synch symbol parts.
 * Particularly the length of its synch break (dominant bits) and its synch delimiter (recessive bits).
 *
 * The version using float parameters allows setting the break and delimiter lengths in increments of 1/16th bits,
 * but the unit still is bit times (i.e. linSetBreakLength(14.5, 2.5) will set a break length of 14 8/16th bit times and
 * a delimiter length of 2 8/16th bit times).
 * Note that setting non-integer break and delimiter lengths is not supported by every hardware.
 *
 * @param syncBreakLen
 *   Synch break length in bit times.
 *   Default value: 18
 *   Value range: 10..30
 *   Values required for a LIN compliant header: >= 13
 *
 * @param syncDelLen
 *   Synch delimiter length in bit times.
 *   Default value: 2
 *   Value range: 1..30
 *   Values required for a LIN compliant header: >= 1
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
dword linSetBreakLength(double syncBreakLen = 18.0, double syncDelLen = 2.0);

}
