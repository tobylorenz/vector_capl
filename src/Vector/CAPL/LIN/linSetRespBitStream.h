#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets up a bit stream as the response to the specified frame.
 *
 * Sets up a bit stream as the response to the specified frame.
 *
 * @param frameId
 *   LIN frame identifier in the range 0 .. 63
 *
 * @param bitStream
 *   The bit stream array to be sent as a response.
 *   Maximum number of bits: 2^31-1.
 *
 * @param numberOfBits
 *   Number of bits in the bitStream array to be sent.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetRespBitStream(long frameId, byte * bitStream, long numberOfBits);

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets up a bit stream as the response to the specified frame.
 *
 * Sets up a bit stream as the response to the specified frame.
 *
 * @param frameId
 *   LIN frame identifier in the range 0 .. 63
 *
 * @param bitStream
 *   The bit stream array to be sent as a response.
 *   Maximum number of bits: 2^31-1.
 *
 * @param numberOfBits
 *   Number of bits in the bitStream array to be sent.
 *
 * @param timeoutPrevention
 *   - 0: deactivates the timeout prevention for the 7259-transceiver.
 *   - 1: activates the timeout prevention for the 7259-transceiver.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetRespBitStream(long frameId, byte * bitStream, long numberOfBits, long timeoutPrevention);

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets up a bit stream as the response to the specified frame.
 *
 * Sets up a bit stream as the response to the specified frame.
 *
 * @param frameId
 *   LIN frame identifier in the range 0 .. 63
 *
 * @param bitStream
 *   The bit stream array to be sent as a response.
 *   Maximum number of bits: 2^31-1.
 *
 * @param numberOfBits
 *   Number of bits in the bitStream array to be sent.
 *
 * @param timeoutPrevention
 *   - 0: deactivates the timeout prevention for the 7259-transceiver.
 *   - 1: activates the timeout prevention for the 7259-transceiver.
 *
 * @param count
 *   Number of times, the bit stream shall be sent as a response before deactivating itself.
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetRespBitStream(long frameId, byte * bitStream, long numberOfBits, long timeoutPrevention, long count);

}
