#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Deactivates frame responses for all frames published by the calling slave node.
 *
 * This function deactivates frame responses for all frames published by the calling slave node.
 * The frame responses can be deactivated individually using the function linSetRespCounter().
 *
 * @return
 *   Number of deactivated frame responses or -1 on failure.
 */
int linDeactivateResps();

}
