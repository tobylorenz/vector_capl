#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Deactivates the disturbance in the response space of the specified frame.
 *
 * Deactivates the disturbance in the response space of the specified frame.
 *
 * @param frameId
 *   LIN frame identifier in the range 0 .. 63
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linResetRespDisturbance(long frameId);

}
