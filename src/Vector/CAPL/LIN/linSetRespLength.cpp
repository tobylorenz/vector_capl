#include "linSetRespLength.h"

#include <iostream>

namespace capl
{

long linSetRespLength(long frameID, long numberOfBytes)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
