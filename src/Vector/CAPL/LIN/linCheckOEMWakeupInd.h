#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Checks the wakeup indication bits of all slave nodes defined in the LIN network.
 *
 * This function checks the wakeup indication bits of all slave nodes defined in the LIN network (the channel is determined by the CAPL program context).
 *
 * @return
 *   Returns non-zero if at least one of the queried wakeup indication bits is set, otherwise zero.
 */
long linCheckOEMWakeupInd();

}
