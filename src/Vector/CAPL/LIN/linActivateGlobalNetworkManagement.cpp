#include "linActivateGlobalNetworkManagement.h"

#include <iostream>

namespace capl
{

long linActivateGlobalNetworkManagement(long active)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
