#include "linDeactivateSlot.h"

#include <iostream>

namespace capl
{

long linDeactivateSlot(dword tableIndex, dword slotIndex)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
