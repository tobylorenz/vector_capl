#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets request to measure the baud rate value according to a certain data byte of a certain LIN frame.
 *
 * With this function it is possible to set request to measure the baud rate value according to a certain data byte of a certain LIN frame.
 * Channel, where the baud rate is measured, is determined by the CAPL program context.
 *
 * This function only initiates the baud rate measurement.
 * The result becomes available when the next bus event for the specified ID occurs and it remains valid until next measurement request.
 * To retrieve the result the function linGetMeasBaudrate() shall be used.
 *
 * @param frameID
 *   LIN frame identifier whose baud rate will be measured.
 *   Value range: 0…63
 *
 * @param index
 *   Data byte index.
 *   Value range: 0..N, where N = length [in bytes] of specified LIN frame
 *   - [0..N-1]: data bytes 1..N
 *   - N: checksum byte
 *
 * @return
 *   On successful request returns zero, otherwise -1.
 */
long linMeasRespBaudrate(long frameID, int index);

}
