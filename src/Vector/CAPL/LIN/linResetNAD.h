#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Resets NAD of the Slave node determined by the CAPL program context to its initial NAD.
 *
 * This function resets NAD (Node Address for Diagnostic) of the Slave node determined by the CAPL program context to its initial NAD.
 *
 * See Option .LIN: Notes to the way initial NAD is determined to understand how initial NAD is determined in CANoe.
 *
 * @return
 *   On success this function returns a value unequal to -1, otherwise -1.
 */
long linResetNAD();

}
