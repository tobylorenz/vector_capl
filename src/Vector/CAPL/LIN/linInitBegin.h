#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Starts the CAPL controlled LIN initialization procedure.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
