#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Waits for the occurrence of a LIN Event-triggered frame with a single response for the specified associated frame.
 *
 * @todo
 */

}
