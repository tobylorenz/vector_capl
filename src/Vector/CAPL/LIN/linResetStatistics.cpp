#include "linResetStatistics.h"

#include <iostream>

namespace capl
{

void linResetStatistics()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void linResetStatistics(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
