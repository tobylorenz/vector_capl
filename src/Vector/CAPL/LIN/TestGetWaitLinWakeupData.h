#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Calls up the frame content if LIN Wakeup frame is the last event that triggers a wait instruction.
 *
 * @todo
 */

}
