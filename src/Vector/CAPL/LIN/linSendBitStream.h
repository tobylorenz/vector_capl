#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sends an arbitrary bit stream on the LIN bus.
 *
 * Sends an arbitrary bit stream on the LIN bus.
 *
 * @param bitStream
 *   The bit stream to be sent.
 *   Maximum number of bits: 2^31-1.
 *
 * @param numberOfBits
 *   The number of bits in the bitStream-array.
 *
 * @return
 *   On success a value unequal to zero, otherwise zero.
 */
long linSendBitStream(byte * bitStream, long numberOfBits);

/**
 * @ingroup LIN
 *
 * @brief
 *   Sends an arbitrary bit stream on the LIN bus.
 *
 * Sends an arbitrary bit stream on the LIN bus.
 *
 * @param bitStream
 *   The bit stream to be sent.
 *   Maximum number of bits: 2^31-1.
 *
 * @param numberOfBits
 *   The number of bits in the bitStream-array.
 *
 * @param timeoutPrevention
 *   - 0: deactivates the timeout prevention for the 7259-transceiver.
 *   - 1: activates the timeout prevention for the 7259-transceiver.
 *
 * @return
 *   On success a value unequal to zero, otherwise zero.
 */
long linSendBitStream(byte * bitStream, long numberOfBits, long timeoutPrevention);

}
