#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Determines the conditions under which the LIN hardware can be reactivated.
 *
 * This command determines the conditions under which the LIN hardware can be reactivated (leaves the Sleep mode).
 *
 * Default wakeup delimiter depends on the hardware settings (see Hardware Configuration: LIN) while the number of expected wakeup frames is 1.
 *
 * Main use case for this function is to simulate slow Master, which does not wake-up until an expected Wakeup frame burst.
 *
 * @param wakeupDelimiter
 *   This parameter specifies the wakeup delimiter length, i.e. the time between the end of wakeup frame and the first sent header.
 *   Units of this parameter as well as default value depend on the hardware settings (see Hardware Configuration: LIN).
 *   Value range:: 3..255 (ms or bit times)
 *
 * @param numberOfWakeupFrames
 *   This parameter specifies the number of wakeup frames, after that the LIN hardware is reactivated. If the value 0 is set, the LIN hardware will never leave the Sleep mode.
 *   Value range: 0 .. 31
 *   Default value: 1
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetWakeupParams(long wakeupDelimiter, long numberOfWakeupFrames);

}
