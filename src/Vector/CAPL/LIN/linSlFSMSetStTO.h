#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets a timeout for the state specified by the Slave.
 *
 * @deprecated
 *   ...
 *
 * @todo
 */

}
