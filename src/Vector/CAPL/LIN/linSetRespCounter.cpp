#include "linSetRespCounter.h"

#include <iostream>

namespace capl
{

long linSetRespCounter(long frameID, long count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
