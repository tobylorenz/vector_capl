#include "linResetSlave.h"

#include <iostream>

namespace capl
{

long linResetSlave()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long linResetSlave(dword resetToLastConfiguration)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
