#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Sets a response baud rate for a specified frame.
 *
 * Sets a response baud rate for a specified frame.
 * This baud rate will be used for the response, regardless of the master baud rate.
 *
 * @param frameId
 *   The identifier of the frame for which the response baud rate will be set.
 *
 * @param baudrate
 *   The baud rate to be used for the response.
 *   Value range: 1000-20000
 *
 * @return
 *   On success, a value unequal to zero, otherwise zero.
 */
long linSetRespBaudrate(long frameId, long baudrate);

}
