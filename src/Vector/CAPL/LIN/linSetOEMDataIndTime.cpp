#include "linSetOEMDataIndTime.h"

#include <iostream>

namespace capl
{

long linSetOEMDataIndTime(long timeInMS)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
