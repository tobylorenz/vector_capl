#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup LIN
 *
 * @brief
 *   Calls up the event content, if LIN Event-triggered frame with a single response for the specified associated frame is the last event that triggers a wait instruction.
 *
 * @todo
 */

}
