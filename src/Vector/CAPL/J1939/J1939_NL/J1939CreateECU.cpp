#include "J1939CreateECU.h"

#include <iostream>

namespace capl
{

dword J1939CreateECU(dword busHandle, char * deviceName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword J1939CreateECU(char * busName, char * deviceName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
