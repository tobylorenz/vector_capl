#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup J1939_TSL
 *
 * @brief
 *   Sends a Request for Address Claim from Null Address (0xFE) and updates the NMT table.
 *
 * Sends a ‘Request for Address Claim' from Null Address (0xFE) and updates the NMT table. The function waits until the specified time is elapsed.
 *
 * @param waitTime
 *   wait until this time elapses, in milliseconds
 *
 * @return
 *   - 0 - successful
 *   - <0 - error code
 */
long J1939TestNmtRefresh(dword waitTime);

}
