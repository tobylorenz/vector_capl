#pragma once

/** @todo to be implemented */

/**
 * @ingroup J1939
 * @{
 * @defgroup J1939_TSL J1939 Test Service Library
 * @}
 */

/* Wait Functions */
#include "J1939TestWaitForPG.h"
#include "J1939TestJoinPGEvent.h"
#include "J1939TestWaitForAnyJoinedEvent.h"
#include "J1939TestWaitForAllJoinedEvents.h"
#include "J1939TestGetWaitEventPGData.h"
#include "J1939TestJoinDMWithSPNEvent.h"
#include "J1939TestJoinDMWithoutSPNEvent.h"
#include "J1939TestWaitForDMWithSPN.h"
#include "J1939TestWaitForDMWithoutSPN.h"

/* Network Management */
#include "J1939TestNmtRefresh.h"
#include "J1939TestNmtQueryAddress.h"
#include "J1939TestNmtQueryDeviceNameForAddress.h"
#include "J1939TestNmtQueryDeviceName.h"
#include "J1939TestNmtQueryECU.h"
#include "J1939TestOnNMTChange.h"

/* Test Pattern */
#include "J1939TestRequest.h"
#include "J1939TestCommand.h"
#include "J1939TestRequestACL.h"
#include "J1939TestConflictACL.h"
#include "J1939TestCommandedAddress.h"
#include "J1939TestOutputBAM.h"
#include "J1939TestOutputTPCM.h"
#include "J1939TestReceiveTPCM.h"

/* Check Functions */
#include "J1939TestChkCreate_RequestViolation.h"
#include "J1939TestChkCreate_ACLViolation.h"
#include "J1939TestChkCreate_BAMViolation.h"
#include "J1939TestChkCreate_RTSCTSViolation.h"
#include "J1939TestChkCreate_AbsCycleTimeViolation.h"
#include "J1939TestChkCreate_RelCycleTimeViolation.h"
#include "J1939TestChkCreate_PGDistanceViolation.h"
#include "J1939TestChkCreate_SignalRangeViolation.h"
#include "J1939TestChkControl_Start.h"
#include "J1939TestChkControl_Stop.h"
#include "J1939TestChkControl_Reset.h"
#include "J1939TestChkControl_Destroy.h"
#include "J1939TestChkQuery_NumEvents.h"
#include "J1939TestChkQuery_StatMin.h"
#include "J1939TestChkQuery_StatMax.h"
#include "J1939TestChkQuery_StatAvg.h"
#include "J1939TestChkQuery_Count.h"
#include "J1939TestChkQuery_Last.h"

/* Stimulus Functions */
#include "J1939TestStmCreate_Toggle.h"
#include "J1939TestStmCreate_CSV.h"
#include "J1939TestStmControl_Start.h"
#include "J1939TestStmControl_Stop.h"
#include "J1939TestStmControl_Reset.h"
#include "J1939TestStmControl_Destroy.h"

/* Device Name Functions */
#include "J1939TestMakeDeviceName.h"
#include "J1939TestGetAAC.h"
#include "J1939TestSetAAC.h"
#include "J1939TestGetIG.h"
#include "J1939TestSetIG.h"
#include "J1939TestGetVSInstance.h"
#include "J1939TestSetVSInstance.h"
#include "J1939TestGetVS.h"
#include "J1939TestSetVS.h"
#include "J1939TestGetFct.h"
#include "J1939TestSetFct.h"
#include "J1939TestGetFctInstance.h"
#include "J1939TestSetFctInstance.h"
#include "J1939TestGetECUInstance.h"
#include "J1939TestSetECUInstance.h"
#include "J1939TestGetMC.h"
#include "J1939TestSetMC.h"
#include "J1939TestGetIN.h"
#include "J1939TestSetIN.h"

/* Other Functions */
#include "J1939TestCaseComment.h"
#include "J1939TestChkConfig_SetTitle.h"
