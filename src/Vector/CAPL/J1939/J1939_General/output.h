#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup J1939
 *
 * @brief
 *   Outputs a parameter group onto the CAN bus.
 *
 * Outputs a parameter group onto the CAN bus.
 *
 * @param pg_variable
 *   Variable of the type "pg".
 */
void output(pg pg_variable);

}
