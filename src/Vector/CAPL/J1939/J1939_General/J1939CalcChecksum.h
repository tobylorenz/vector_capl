#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup J1939
 *
 * @brief
 *   Calculates the checksum of a parameter group.
 *
 * This function calculates the checksum of a parameter group.
 *
 * PGNs, for which the checksum should be calculated, as well as the computational method are defined in the J1939/71 specification.
 *
 * This function can be used for sending a message (to calculate the checksum) and for receiving a message (to validate the transmitted checksum).
 *
 * @param parameterGroup
 *   parameter group for which the checksum should be calculated
 *   Per the J1939/71 specification, the function is only used for the following PGNs:
 *   - 1024 (External Brake Request)
 *   - 61469 (Steering Angle Sensor Information)
 *   - 61483 (Crash Notification)
 *   - PGN 2816 (Advanced Emergency Braking System 2)
 *   - 0 (Torque/Speed Control 1)
 *
 * @return
 *   The calculated checksum, if a computational method is defined (see J1939/71 specification), else "-1".
 *   range of valid values:
 *   - 0..7 (for "Torque/Speed Control 1")
 *   - 0..15 (for all other PGNs)
 */
dword J1939CalcChecksum(pg * parameterGroup);

}
