cmake_minimum_required(VERSION 2.8)

set(function_group J1939_General)

set(source_files
  getThisMessage.cpp
  J1939CalcChecksum.cpp
  output.cpp)

set(header_files
  getThisMessage.h
  J1939CalcChecksum.h
  J1939_General.h
  output.h
  pg.h)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

add_library(${PROJECT_NAME}_${function_group} OBJECT ${source_files} ${header_files})

if(OPTION_USE_GCOV)
  target_link_libraries(${PROJECT_NAME}_${function_group} gcov)
  add_definitions(-g -O0 -fprofile-arcs -ftest-coverage)
endif()

if(CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif()

install(
  FILES ${header_files}
  DESTINATION include/Vector/CAPL/J1939/${function_group})
