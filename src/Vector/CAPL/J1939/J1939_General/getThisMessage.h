#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup J1939
 *
 * @brief
 *   Transfers a parameter group to an indicated parameter group.
 *
 * Transfers the parameter group to the indicated parameter group pg. The number of bytes of the transmitted parameter group data is given by length.
 *
 * This function must be used exclusively within the program block.
 *
 * @param pg_variable
 *   variable of the type "pg"
 *
 * @param length
 *   length entry of the type "int"
 */
void getThisMessage(pg pg_variable, int length);

}
