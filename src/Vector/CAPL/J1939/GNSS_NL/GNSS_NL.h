#pragma once

/** @todo to be implemented */

/**
 * @ingroup J1939
 * @{
 * @defgroup GNSS_NL GNSS Node Layer
 * @}
 */

/* Node Control */
#include "GNSSGetName.h"
#include "GNSSGetAddress.h"
#include "GNSSMakeName.h"
#include "GNSSStartUp.h"
#include "GNSSShutDown.h"
#include "GNSSUpdateTable.h"

/* Events */
#include "GNSSAppErrorIndication.h"
#include "GNSSAppAddrClaim.h"
#include "GNSSAppCmdAddrIndication.h"
#include "GNSSOnWpBeforeLastReached.h"
#include "GNSSOnLastWpReached.h"
#include "GNSSOnSetSpeed.h"

/* Simulation */
#include "GNSSStartSimulation.h"
#include "GNSSStopSimulation.h"
#include "GNSSSetSpeed.h"
#include "GNSSGetCurSpeed.h"
#include "GNSSPauseSpeed.h"
#include "GNSSContinueSpeed.h"
#include "GNSSSetCourse.h"
#include "GNSSGetCurCourse.h"
#include "GNSSGetCurGradient.h"

/* Settings */
#include "GNSSSetUnits.h"
#include "GNSSSetPGSettings.h"
#include "GNSSSetPosDataVal.h"
#include "GNSSSetCOGSOGVal.h"
#include "GNSSSetJitterLatLon.h"
#include "GNSSSetJitterAlt.h"

/* Waypoints */
#include "GNSSAddWp.h"
#include "GNSSRemoveWp.h"
#include "GNSSAddWpRel.h"
#include "GNSSAddWpRef.h"
#include "GNSSAddWpS.h"
#include "GNSSAddWpRelS.h"
#include "GNSSAddWpRefS.h"
#include "GNSSAddWpLine.h"
#include "GNSSAddWpRect.h"
#include "GNSSAddWpMeander.h"
#include "GNSSAddWpFile.h"
#include "GNSSSetRefPoint.h"
#include "GNSSGetCurLat.h"
#include "GNSSGetCurLon.h"
#include "GNSSGetCurAlt.h"

/* Other Functions */
#include "GNSSGetLastError.h"
#include "GNSSGetAbsFilePath.h"
