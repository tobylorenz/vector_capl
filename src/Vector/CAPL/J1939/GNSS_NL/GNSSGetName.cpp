#include "GNSSGetName.h"

#include <iostream>

namespace capl
{

dword GNSSGetName(dword addr, byte * name)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
