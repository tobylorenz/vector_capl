#include "CANstressSetDevice.h"

#include <iostream>

namespace capl
{

long CANstressSetDevice(dword deviceId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
