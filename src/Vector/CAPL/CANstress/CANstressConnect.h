#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Establishes a connection between the CANstress software and the CANstress hardware.
 *
 * Establishes a connection between the CANstress software and the CANstress hardware.
 *
 * @return
 *   - 0: If successful.
 *   - -1: In case of error (due to an internal error).
 *   - -2: In case of error (due to an internal timeout during the connection establishment).
 */
long CANstressConnect(void);

}
