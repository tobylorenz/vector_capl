#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Opens a CANstress configuration file.
 *
 * Opens a CANstress configuration file.
 *
 * @param fileName
 *   If the passed configuration file is configured as a User File, only the file name is passed here.
 *   Otherwise either an absolute path or a relative path relating to the folder of the CANoe configuration must be passed.
 */
void CANstressOpen(char * fileName);

}
