#include "CANstressConnect.h"

#include <iostream>

namespace capl
{

long CANstressConnect(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
