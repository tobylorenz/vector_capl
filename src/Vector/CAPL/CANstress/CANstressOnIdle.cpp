#include "CANstressOnIdle.h"

#include <iostream>

namespace capl
{

long CANstressOnIdle(char * fnctCallback)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
