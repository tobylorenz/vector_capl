#include "CANstressWaitForFinished.h"

#include <iostream>

namespace capl
{

long CANstressWaitForFinished(dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
