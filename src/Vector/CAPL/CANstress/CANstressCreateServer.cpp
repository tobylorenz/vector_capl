#include "CANstressCreateServer.h"

#include <iostream>

namespace capl
{

long CANstressCreateServer(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long CANstressCreateServer(char * deviceAlias)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long CANstressCreateServer(dword deviceNr)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
