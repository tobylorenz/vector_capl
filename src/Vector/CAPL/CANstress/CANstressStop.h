#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Orders the CANstress COM server to end the current disturbance activity of the hardware.
 *
 * Orders the CANstress COM server to end the current disturbance activity of the hardware.
 */
void CANstressStop(void);

}
