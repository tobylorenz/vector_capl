#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Orders the CANstress COM server to activate the hardware for the error disturbance activity.
 *
 * Orders the CANstress COM server to activate the hardware for the error disturbance activity.
 * If there is no connection between the CANstress software and the CANstress hardware, this is established previously.
 *
 * @return
 *   - 0: If successful.
 *   - -1: In case of error (due to an internal error).
 *   - -2: In case of error (due to an internal timeout).
 */
long CANstressStart(void);

}
