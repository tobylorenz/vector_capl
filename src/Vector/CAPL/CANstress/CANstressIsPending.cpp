#include "CANstressIsPending.h"

#include <iostream>

namespace capl
{

long CANstressIsPending(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
