#include "CANstressAvailableDevices.h"

#include <iostream>

namespace capl
{

dword CANstressAvailableDevices(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
