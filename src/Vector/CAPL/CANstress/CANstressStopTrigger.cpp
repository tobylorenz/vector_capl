#include "CANstressStopTrigger.h"

#include <iostream>

namespace capl
{

long CANstressStopTrigger(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
