#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Serves to query whether the CANstress hardware is in the state Finished.
 *
 * Serves to query whether the CANstress hardware is in the state Finished.
 *
 * @return
 *   - 1: If the CANstress hardware is in the state Finished.
 *   - 0: If the hardware is in another state (Idle or Pending)
 *   - -1: On occurrence of an error.
 */
long CANstressIsFinished(void);

}
