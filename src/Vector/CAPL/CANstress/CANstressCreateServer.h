#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Starts the CANstress software and establishes a connection via the COM interface to this COM server.
 *
 * Starts the CANstress software and establishes a connection via the COM interface to this COM server.
 * If no registered CANstress COM server is found, the ongoing measurement is stopped.
 *
 * @return
 *   - 0: If successful.
 *   - -1: In case of error.
 */
long CANstressCreateServer(void);

/**
 * @ingroup CANstress
 *
 * @brief
 *   Starts the CANstress software and establishes a connection to this COM server via the COM interface.
 *
 * Starts the CANstress software and establishes a connection to this COM server via the COM interface.
 * The COM server generated only establishes a connection to the CANstress device defined in deviceAlias.
 * Connected CANstress devices are mapped to an alias using the canstress.ini file.
 * You will find this file in the CANstress software's installation directory.
 * If the call is successful, the device defined in deviceAlias will be set as the current device.
 *
 * @param deviceAlias
 *   Device alias
 *
 * @return
 *   - > 0: If successful.
 *     If the attempt to establish a connection to a COM server is successful,
 *     a handle required for the CANstressSetDevice function will be returned for the device.
 *     This handle will be a value greater than 0.
 *   - -1: In case of error.
 *     If the call is not successful (because the CANstress software has not been registered as a COM server, for example),
 *     the value -1 will be returned. The result of the test currently in progress will also be set to failed.
 */
long CANstressCreateServer(char * deviceAlias);

/**
 * @ingroup CANstress
 *
 * @brief
 *   Starts the CANstress software and establishes a connection to this COM server via the COM interface.
 *
 * Starts the CANstress software and establishes a connection to this COM server via the COM interface.
 * The COM server generated only establishes a connection to the CANstress device defined in deviceNr.
 * Connected CANstress devices are mapped to a number using the canstress.ini file.
 * You will find this file in the CANstress software's installation directory.
 * If the call is successful, the device defined in deviceNr will be set as the current device.
 *
 * @param deviceNr
 *   Device number
 *
 * @return
 *   - > 0: If successful.
 *     If the attempt to establish a connection to a COM server is successful,
 *     a handle required for the CANstressSetDevice function will be returned for the device.
 *     This handle will be a value greater than 0.
 *   - -1: In case of error.
 *     If the call is not successful (because the CANstress software has not been registered as a COM server, for example),
 *     the value -1 will be returned. The result of the test currently in progress will also be set to failed.
 */
long CANstressCreateServer(dword deviceNr);

}
