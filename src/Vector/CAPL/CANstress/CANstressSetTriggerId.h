#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Sets the message ID, which will activate the trigger.
 *
 * Sets the message ID, which will activate the trigger.
 *
 * @param id
 *   Message ID for trigger.
 *   Both simple and complex CAN message IDs can be specified.
 *   However, the corresponding mode must be set in the basic configuration!
 */
void CANstressSetTriggerId(dword id);

}
