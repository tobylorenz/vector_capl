#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Sends back the handle for the current CANstress device.
 *
 * Sends back the handle for the current CANstress device.
 *
 * @return
 *   - > 0: If successful.
 *     Handle for the current CANstress device.
 *   - -1: In case of error.
 *     DeviceId does not contain a valid handle for a CANstress device. You might get this result, for example, if CANstressCreateServer has not yet been called.
 */
long CANstressGetDevice(void);

}
