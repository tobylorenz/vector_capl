#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Serves to query whether the CANstress hardware is in the state Pending.
 *
 * Serves to query whether the CANstress hardware is in the state Pending.
 *
 * @return
 *   - 1: If the CANstress hardware is in the state Pending.
 *   - 0: If the hardware is in another state (Idle or Finished).
 *   - -1: On occurrence of an error.
 */
long CANstressIsPending(void);

}
