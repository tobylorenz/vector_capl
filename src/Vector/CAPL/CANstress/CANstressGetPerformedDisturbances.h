#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Acquires the number of disturbances that were executed by CANstress.
 *
 * Acquires the number of disturbances that were executed by CANstress.
 *
 * @return
 *   Number of disturbances that were executed since the last call to disturb.
 */
long CANstressGetPerformedDisturbances(void);

}
