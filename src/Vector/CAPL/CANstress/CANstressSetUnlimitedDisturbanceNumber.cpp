#include "CANstressSetUnlimitedDisturbanceNumber.h"

#include <iostream>

namespace capl
{

void CANstressSetUnlimitedDisturbanceNumber(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
