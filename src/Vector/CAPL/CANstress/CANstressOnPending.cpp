#include "CANstressOnPending.h"

#include <iostream>

namespace capl
{

long CANstressOnPending(char * fnctCallback)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
