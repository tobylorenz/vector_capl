#include "CANstressWaitForIdle.h"

#include <iostream>

namespace capl
{

long CANstressWaitForIdle(dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
