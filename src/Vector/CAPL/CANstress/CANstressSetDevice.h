#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Defines the active device for further functions.
 *
 * Defines the active device for further functions.
 *
 * @param deviceId
 *   Handle for the CANstress device on which further functions are to run.
 *   This handle is sent back by the CANstressCreateServer function.
 *
 * @return
 *   - 0: If successful.
 *   - -1: In case of error.
 *     This means that deviceId is not a valid handle for a CANstress device.
 *     Only handles sent back by the CANstressCreateServer function and for which the connection to the COM server has not yet been terminated with CANstressQuit are valid.
 */
long CANstressSetDevice(dword deviceId);

}
