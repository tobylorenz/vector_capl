#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANstress
 *
 * @brief
 *   Deactivates the CANstress software triggers if this is still active.
 *
 * Deactivates the CANstress software triggers if this is still active.
 *
 * @return
 *   0: On successful call.
 */
long CANstressStopTrigger(void);

}
