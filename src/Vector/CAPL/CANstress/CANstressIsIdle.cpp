#include "CANstressIsIdle.h"

#include <iostream>

namespace capl
{

long CANstressIsIdle(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
