#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Executes a SDO download, followed by a SDO upload.
 *   The data received are checked against predefined data.
 *
 * @todo
 */

}
