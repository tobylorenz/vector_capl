#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Converts a string value in a dword value.
 *
 * The function converts a string in a dword value.
 *
 * The string contains either a decimal value, e.g. "1234" and "-1234" or a hexadecimal value, e.g. "0x4321". The prefix 0x is automatically detected and the function converts the string.
 *
 * @param inValueBuf[]
 *   buffer containing the string
 *
 * @param valueBufSize
 *   buffer size in byte of inValueBuf
 *
 * @return
 *    converted string as dword value
 */
dword coTfsatoxD(char inValueBuf[], dword valueBufSize);

}
