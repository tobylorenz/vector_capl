#include "coTfsSDOBlockDownloadSegmentRequest.h"

#include <iostream>

namespace capl
{

long coTfsSDOBlockDownloadSegmentRequest(dword cont, dword seqNumber, byte inValueBuf[], dword valueBufSize, dword isLastSegment)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
