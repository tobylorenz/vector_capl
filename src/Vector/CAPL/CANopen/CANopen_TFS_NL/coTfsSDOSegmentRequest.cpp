#include "coTfsSDOSegmentRequest.h"

#include <iostream>

namespace capl
{

long coTfsSDOSegmentRequest(dword ccs, dword cont, dword toggleBit, dword notUsed, byte inValueBuf[], dword valueBufSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long coTfsSDOSegmentRequest(dword ccs, dword cont, dword toggleBit, dword notUsed, qword inValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
