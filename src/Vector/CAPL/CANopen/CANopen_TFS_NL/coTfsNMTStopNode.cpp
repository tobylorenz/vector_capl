#include "coTfsNMTStopNode.h"

#include <iostream>

namespace capl
{

long coTfsNmtStopNode(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long coTfsNmtStopNode(dword broadcastFlag)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
