#include "coTfsHeartbeatProducer.h"

#include <iostream>

namespace capl
{

long coTfsHeartbeatProducer(dword duration, dword producerTime, dword tolerance)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
