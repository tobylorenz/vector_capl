#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sends an individual SDO abort message.
 *
 * This function sends out a SDO abort message.
 *
 * @param index
 *   object index
 *
 * @param subIndex
 *   object sub index
 *
 * @param abortCode
 *   error code
 *
 * @return
 *   error code
 */
long coTfsSDOAbort(dword index, dword subIndex, dword abortCode);

}
