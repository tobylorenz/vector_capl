#include "coTfsActivateGuardingReqMonitor.h"

#include <iostream>

namespace capl
{

long coTfsActivateGuardingReqMonitor(dword nodeID, dword guardTime, dword tolerance)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
