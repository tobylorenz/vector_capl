#include "coTfsHeartbeatProducerCheckIfActive.h"

#include <iostream>

namespace capl
{

long coTfsHeartbeatProducerCheckIfActive(dword duration, dword producerTime, dword tolerance)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
