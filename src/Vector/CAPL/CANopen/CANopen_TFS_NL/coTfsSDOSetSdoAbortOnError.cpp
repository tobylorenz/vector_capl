#include "coTfsSDOSetSdoAbortOnError.h"

#include <iostream>

namespace capl
{

long coTfsSDOSetSdoAbortOnError(dword setSdoAbort)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
