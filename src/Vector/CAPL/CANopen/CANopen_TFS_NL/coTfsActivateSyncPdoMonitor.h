#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Checks the temporally correct occurrence of SYNC PDO messages and calls the corresponding callback functions.
 *
 * This function checks if the DUT sends a synchronous PDO with the given CAN-ID. The check is only available if the sync check was activated with coTfsActivateSyncMonitor.
 *
 * For each correct occurrence, the callback void coTfsOnSyncPdoMsg(dword canId) is called. The synchronous PDO must occur in the defined time slot. If the time is not adhered to, then the callback void coTfsOnSyncPdoFail(dword canId) is called once. After an error has occurred, the callback system is automatically disabled.
 *
 * This check can be switched off again with coTfsDeactivateSyncPdoMonitor.
 *
 * @param syncCanId
 *   sync CAN-ID to be used
 *
 * @param pdoCanId
 *   CAN-ID of the PDO to be monitored
 *
 * @param transmissionType
 *   PDO transmission type, range 1..240
 *
 * @return
 *   error code
 */
long coTfsActivateSyncPdoMonitor(dword syncCanId, dword pdoCanId, dword transmissionType);

}
