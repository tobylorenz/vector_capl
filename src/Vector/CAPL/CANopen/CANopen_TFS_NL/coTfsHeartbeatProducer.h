#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   The heartbeat producer of a CANopen device is checked.
 *
 * This function activates the heartbeat producer of the DUT. For this the object 0x1017 with producerTime is described via a SDO write access.  After this, the regularity of the heartbeat message is checked and then the heartbeat producer is switched off again (0x1017 = 0). The test duration must be greater than the permitted time deviation.
 *
 * At least two heartbeat messages of the DUT are awaited, even if the test duration is set shorter.
 *
 * @param duration
 *   test duration in ms, how long the regularity of the heartbeat producer should be tested
 *
 * @param producerTime
 *   heartbeat producer time in ms
 *
 * @param tolerance
 *   permitted time deviation of the DUT in ms
 *   It is recommended that you use an even value. The tolerated time-frame within which a message is still accepted is: x - (tolerance/2) <= x <= x + (tolerance/2)
 *
 * @return
 *   error code
 */
long coTfsHeartbeatProducer(dword duration, dword producerTime, dword tolerance);

}
