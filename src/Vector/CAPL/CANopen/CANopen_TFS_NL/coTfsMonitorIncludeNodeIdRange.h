#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Includes a range of node-IDs for monitoring.
 *
 * This function includes the specified range of node-IDs for monitoring. To include a single node-ID use the function coTfsMonitorIncludeNodeId.
 *
 * You can start the monitoring with coTfsMonitorActivate and stop it with coTfsMonitorDeactivate.
 *
 * To exclude node-IDs or ranges of node-IDs from monitoring you can use the functions coTfsMonitorExcludeNodeId and coTfsMonitorExcludeNodeIdRange. With coTfsMonitorSetNmtTimeout and coTfsMonitorSetSdoTimeout you can set NMT and SDO timeouts.
 *
 * @param minNodeId
 *   lower limit
 *
 * @param maxNodeId
 *   upper limit
 *
 * @return
 *   error code
 */
long coTfsMonitorIncludeNodeIdRange(dword minNodeId, dword maxNodeId);

}
