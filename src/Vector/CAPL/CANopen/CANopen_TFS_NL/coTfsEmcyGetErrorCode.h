#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Returns received emergency messages (of a node).
 *
 * This function transmits as return value the number of received emergency messages for the CANopen® node labeled with nodeID.
 *
 * Each emergency message can only be called up once. The return value contains the number of received messages including the emergency message returned with the function call. All data pointers are only valid if there is at least one message present.
 *
 * It is possible to omit the nodeID parameter. In this case, the internally-stored value set with coTfsSetNodeId is used.
 *
 * @param nodeID

The function transmits emergency messages from the selected node-ID. If node-ID is set to 0, the messages of all nodes are transmitted.
 *
 * @param outCanId
 *   CAN-ID data field
 *
 * @param outTimeStamp
 *   data field on the time stamp of the transmitted emergency message in ms
 *
 * @param outEmcyCode
 *   emergency code data field
 *
 * @param outErrorReg
 *   error register data field
 *
 * @param outMsCodeBuf[]
 *   manufacturer-specific error code (data field)
 *
 * @param msCodeBufSize
 *   size of buffer in byte of outMsCodeBuf
 *
 * @return
 *   number of available emergency messages
 */
dword coTfsEmcyGetErrorCode(dword nodeID, dword outCanId[1], dword outTimeStamp[1], dword outEmcyCode[1], dword outErrorReg[1], byte outMsCodeBuf[5], dword msCodeBufSize);

}
