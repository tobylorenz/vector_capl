#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Transfers the internally-stored node-ID.
 *
 * This function returns the internally-stored node-ID that is used for the simplified test functions.
 *
 * @return
 *   node-ID
 */
dword coTfsGetNodeId(void);

}
