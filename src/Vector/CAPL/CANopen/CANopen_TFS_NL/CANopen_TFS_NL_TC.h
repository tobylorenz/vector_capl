#pragma once

/* Parameter control functions */
#include "coTfsGetNodeId.h"
#include "coTfsSetSdoCANid.h"
#include "coTfsSetNodeId.h"

/* Test control functions */
#include "coTfsatoxD.h"
#include "coTfsatoxL.h"
#include "coTfsGetVersion.h"
#include "coTfsSetFailControl.h"
#include "coTfsSetTimeoutValue.h"
#include "coTfsSetTestModuleNodeId.h"
#include "coTfsGetTestModuleNodeId.h"
#include "coTfsSetApplProfile.h"
#include "coTfsGetApplProfile.h"
#include "coTfsSetReportBehaviour.h"
#include "coTfsSDOSetSdoAbortOnError.h"
#include "coTfsSDOSetAbortControl.h"
#include "coTfsArrToD.h"
#include "coTfsArrToQ.h"
#include "coTfsArrToW.h"
#include "coTfsDToArr.h"
#include "coTfsWToArr.h"
#include "coTfsClearObjectName.h"
#include "coTfsLoadDeviceDescription.h"
#include "coTfsResetTfs.h"

/* Check functions */
#include "coTfsActivateSyncPdoMonitor.h"
#include "coTfsDeactivateSyncPdoMonitor.h"
#include "coTfsActivateGuardingReqMonitor.h"
#include "coTfsDeactivateGuardingReqMonitor.h"
#include "coTfsActivateHeartbeatMonitor.h"
#include "coTfsDeactivateHeartbeatMonitor.h"
#include "coTfsActivateSyncMonitor.h"
#include "coTfsDeactivateSyncMonitor.h"
