#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Starts a freely-configurable SYNC producer test.
 *   This test requires the existence of the optional sync counter.
 *
 * @todo
 */

}
