#include "coTfsLoadDeviceDescription.h"

#include <iostream>

namespace capl
{

long coTfsLoadDeviceDescription(dword nodeId, char fileName[])
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
