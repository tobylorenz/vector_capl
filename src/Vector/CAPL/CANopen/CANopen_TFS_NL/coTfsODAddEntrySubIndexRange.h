#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Adds a list of object entries to the internal list, which only differ in the given sub index.
 *
 * @todo
 */

}
