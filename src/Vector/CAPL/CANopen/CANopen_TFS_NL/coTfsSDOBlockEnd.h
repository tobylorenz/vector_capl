#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Stops a SDO block up/download.
 *
 * This function sends a SDO block end response/request message (depending on a SDO block upload and SDO block download) and awaits the corresponding response.
 *
 * @param ccs (client command specifier)
 *   - 5: block upload
 *   - 6: block download
 *
 * @param cs (client subcommand)
 *   - 1: end block download
 *
 * @param notUsed
 *   number of bytes in the last segment that contains no data
 *
 * @param crc
 *   CRC checksum, can be calculated with coTfsCalcSdoCrc, is 0 if client and/or server offer no CRC support
 *
 * @param crcSetting
 *   - 0: no CRC support, the expected CRC is 0
 *   - 1: CRC is supported, the expected CRC must be specified in parameter crc
 *   - 2: no information about CRC support, received CRC are not evaluated, default setting for syntax (1)
 *
 * @return
 *   error code
 */
long coTfsSDOBlockEnd(dword ccs, dword cs, dword notUsed, dword crc);

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Stops a SDO block up/download.
 *
 * This function sends a SDO block end response/request message (depending on a SDO block upload and SDO block download) and awaits the corresponding response.
 *
 * The parameter crcSetting is used for SDO block upload only.
 *
 * @param ccs (client command specifier)
 *   - 5: block upload
 *   - 6: block download
 *
 * @param cs (client subcommand)
 *   - 1: end block download
 *
 * @param notUsed
 *   number of bytes in the last segment that contains no data
 *
 * @param crc
 *   CRC checksum, can be calculated with coTfsCalcSdoCrc, is 0 if client and/or server offer no CRC support
 *
 * @param crcSetting
 *   - 0: no CRC support, the expected CRC is 0
 *   - 1: CRC is supported, the expected CRC must be specified in parameter crc
 *
 * @return
 *   error code
 */
long coTfsSDOBlockEnd(dword ccs, dword cs, dword notUsed, dword crc, dword crcSetting);

}
