#include "coTfsEmcyGetErrorCode.h"

#include <iostream>

namespace capl
{

dword coTfsEmcyGetErrorCode(dword nodeID, dword outCanId[1], dword outTimeStamp[1], dword outEmcyCode[1], dword outErrorReg[1], byte outMsCodeBuf[5], dword msCodeBufSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
