#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   The DUT is commanded to execute a "Reset Communication".
 *
 * This call triggers a NMT message that resets the communication functionality of the DUT. After sending out the message, the boot-up message of the DUT is awaited. The reliable wait time corresponds to the general wait time, which is set with coTfsSetTimeoutValue.
 *
 * @return
 *   error code
 */
long coTfsNmtResetCommunication(void);

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   The DUT is commanded to execute a "Reset Communication".
 *
 * This call triggers a NMT message that resets the communication functionality of the DUT. After sending out the message, the boot-up message of the DUT is awaited. The reliable wait time corresponds to the general wait time, which is set with coTfsSetTimeoutValue.
 *
 * @param broadcastFlag
 *   - !=0: NMT message is sent to all nodes (broadcast)
 *
 * @return
 *   error code
 */
long coTfsNmtResetCommunication(dword broadcastFlag);

}
