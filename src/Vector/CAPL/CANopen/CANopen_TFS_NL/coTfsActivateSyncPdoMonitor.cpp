#include "coTfsActivateSyncPdoMonitor.h"

#include <iostream>

namespace capl
{

long coTfsActivateSyncPdoMonitor(dword syncCanId, dword pdoCanId, dword transmissionType)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
