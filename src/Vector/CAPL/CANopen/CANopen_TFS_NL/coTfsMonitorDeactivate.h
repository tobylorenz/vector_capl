#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Deactivates the passive communication monitor.
 *
 * This function deactivates the passive communication monitor that has been activated with coTfsMonitorActivate ands adds the timings statistics to the report.
 *
 * @return
 *   - 0: at least one time violation has occurred
 *   - 1: all values have been received in time
 */
long coTfsMonitorDeactivate(void);

}
