#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Checks standard entries of the object dictionary.
 *
 * The function executes a standard test of the object dictionary.
 *
 * For a description of the object check, see coTfsODChkSingleEntry.
 *
 * @return
 *   error code
 */
long coTfsODChkStdEntries(void);

}
