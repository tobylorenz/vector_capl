#include "coTfsODCheckStdEntries.h"

#include <iostream>

namespace capl
{

long coTfsODChkStdEntries(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
