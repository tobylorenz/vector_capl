#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Removes all entries (of a node) from the internally-stored list.
 *
 * This function deletes the internal list on which occurring emergency messages are stored.
 *
 * It is possible to omit the nodeID parameter. In this case, the internally-stored value set with coTfsSetNodeId is used.
 *
 * @return
 *   error code
 */
long coTfsEmcyResetList(void);

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Removes all entries (of a node) from the internally-stored list.
 *
 * This function deletes the internal list on which occurring emergency messages are stored.
 *
 * It is possible to omit the nodeID parameter. In this case, the internally-stored value set with coTfsSetNodeId is used.
 *
 * @param nodeID
 *   all messages of the associated node-ID are deleted; for nodeID=0, all emergency messages of all nodes are deleted
 *
 * @return
 *   error code
 */
long coTfsEmcyResetList(dword nodeID);

}
