#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Checks the temporally correct occurrence of the SYNC message and calls the corresponding callback functions.
 *
 * This function checks whether the DUT as sync producer makes the sync available within the defined times.
 *
 * For each correct occurrence, the callback void coTfsOnSyncMsg (dword nodeId) is called. If the time is not adhered to, then the callback void coTfsOnSyncFail(dword nodeId, dword cause) is called. After an error has occurred, the callback system is automatically disabled. The reason for the error is specified in the parameter cause:
 *   - 1: message distance too small
 *   - 2: message distance too large or message is missing
 *
 * This check can be switched off again with coTfsDeactivateSyncMonitor. Only one DUT can be monitored at a time.
 *
 * The optional sync counter is checked. maxCounter contains the highest allowed number. See DS301 V4.1 for more information.
 *
 * The description of coTfsActivateHeartbeatMonitor contains a temporal representation that can be applied sensibly to this function.
 *
 * @param canID
 *   CANopen® CAN-ID of the DUT
 *
 * @param producerTime
 *   sync time in micro seconds
 *
 * @param tolerance
 *   permitted time deviation of the target device in micro seconds
 *   It is recommended that you use an even value. The tolerated time frame within a message is still accepted is: x - (delta/2) <= x <= x + (delta/2)
 *
 * @param windowLength
 *   synchronous PDO window length in micro seconds
 *
 * @param maxCounter
 *   1 < maxCounter < 241
 *
 * @return
 *   error code
 */
long coTfsActivateSyncMonitor(dword canId, dword producerTime, dword tolerance, dword windowLength, dword maxCounter);

}
