#include "coTfsatoxL.h"

#include <iostream>

namespace capl
{

long coTfsatoxL(char inValueBuf[], dword valueBufSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
