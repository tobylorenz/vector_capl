#include "coTfsSetTestModuleNodeId.h"

#include <iostream>

namespace capl
{

long coTfsSetTestModuleNodeId(dword nodeId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
