#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Waits for the occurrence of a SDO abort message from a particular node.
 *
 * @todo
 */

}
