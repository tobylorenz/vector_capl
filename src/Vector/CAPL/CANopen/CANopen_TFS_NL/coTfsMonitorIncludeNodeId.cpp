#include "coTfsMonitorIncludeNodeId.h"

#include <iostream>

namespace capl
{

long coTfsMonitorIncludeNodeId(dword nodeId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
