#include "coTfsDeactivateGenericMonitor.h"

#include <iostream>

namespace capl
{

long coTfsDeactivateGenericMonitor(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long coTfsDeactivateGenericMonitor(dword canId, dword dlc)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
