#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Expands the allowed value range of the last created object entry with a given value.
 *
 * @todo
 */

}
