#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Returns the current device state.
 *
 * This function returns the current device status of the DUT. The current state is checked with heartbeat and guarding mechanisms.
 *
 * For this, there is first an attempt to configure a heartbeat producer in the DUT (object 0x1017). The heartbeat message contains the current device status. If the DUT should not make a heartbeat producer available, a remote guarding frame is sent to the DUT. The DUT responds with the corresponding guarding response. The procedure is repeated again. The second response is evaluated and contains the device status. The CANopen® specification provides that a CANopen® device supports at least one of the two modes.
 *
 * @param nmtState
 *   current device status (data field)
 *   - 0: boot-up
 *   - 4: stopped
 *   - 5: operational
 *   - 127: pre-operational
 *
 * @return
 *   error code
 */
long coTfsNmtGetCurrentState(dword nmtState[1]);

}
