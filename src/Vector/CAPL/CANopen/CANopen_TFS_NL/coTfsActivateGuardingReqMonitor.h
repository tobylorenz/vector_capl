#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Checks the temporally correct occurrence of guarding RTRs and controls the corresponding callback functions.
 *
 * When this function is called, it is checked whether the DUT makes available the remote frame for the guarding within the defined times.
 *
 * For each correct occurrence, the callback void coTfsOnGuardingRTRMsg(dword nodeId) is called. If the time is not adhered to, then the callback void coTfsOnGuardingRTRFail(dword nodeId, dword cause) is called once. After an error has occurred, the callback system is automatically disabled. The reason for the error is specified in the parameter cause:
 *   - 1: message distance too small
 *   - 2: message distance too large or message is missing
 *
 * This check can be switched off again with coTfsDeactivateGuardingReqMonitor. Only one DUT can be monitored at a time.
 *
 * The description of coTfsActivateHeartbeatMonitor contains a temporal representation that can be applied sensibly to this function.
 *
 * @param nodeID
 *   node-ID of the DUT
 *
 * @paramguardTime
 *   guarding time in us
 *
 * @paramtolerance
 *   permitted time deviation of the target device in us
 *   It is recommended that you use an even value. The tolerated time frame within which a message is still accepted is: x - (delta/2) <= x <= x + (delta/2)
 *
 * @return
 *   error code
 */
long coTfsActivateGuardingReqMonitor(dword nodeID, dword guardTime, dword tolerance);

}
