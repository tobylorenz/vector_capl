#include "coTfsCbGetReferenceTime.h"

#include <iostream>

namespace capl
{

long coTfsCbGetReferenceTime(dword type, dword nodeId, dword refTime[1], dword minTime[1], dword minOccTime[1], dword maxTime[1], dword maxOccTime[1], dword averageTime[1])
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
