#include "coTfsActivateHeartbeatMonitor.h"

#include <iostream>

namespace capl
{

long coTfsActivateHeartbeatMonitor(dword nodeID, dword producerTime, dword tolerance)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
