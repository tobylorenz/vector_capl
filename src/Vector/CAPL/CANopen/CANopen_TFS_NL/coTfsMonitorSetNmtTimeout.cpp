#include "coTfsMonitorSetNmtTimeout.h"

#include <iostream>

namespace capl
{

long coTfsMonitorSetNmtTimeout(dword nmtTimeoutInMs)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
