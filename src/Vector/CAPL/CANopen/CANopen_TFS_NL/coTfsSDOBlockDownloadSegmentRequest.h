#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sends an individual segment of a SDO block download.
 *
 * This function sends a single segmented SDO block download message and waits for the corresponding response.
 *
 * @param cont
 *   - 0: more segments will follow
 *   - 1: last segment
 *
 * @param seqNumber
 *   sequence number
 *
 * @param inValueBuf
 *   data field for transfer data
 *
 * @param valueBufSize
 *   buffer size in byte of inValueBuf
 *
 * @param isLastSegment
 *   - 1: last segment of transmission
 *   - 0: more segments follow
 *
 * @return
 *   error code
 */
long coTfsSDOBlockDownloadSegmentRequest(dword cont, dword seqNumber, byte inValueBuf[], dword valueBufSize, dword isLastSegment);

}
