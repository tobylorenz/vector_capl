#include "coTfsSetNodeId.h"

#include <iostream>

namespace capl
{

long coTfsSetNodeId(dword nodeID)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
