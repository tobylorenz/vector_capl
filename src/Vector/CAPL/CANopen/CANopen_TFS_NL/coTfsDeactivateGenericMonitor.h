#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Stops a generic message monitor and evaluate monitoring results.
 *
 * This function stops monitoring and evaluates the monitoring results.
 *
 * @return
 *   error code
 */
long coTfsDeactivateGenericMonitor(void);

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Stops a generic message monitor and evaluate monitoring results.
 *
 * This function stops monitoring and evaluates the monitoring results.
 *
 * @param canId
 *   CAN-ID of the monitored message
 *
 * @param dlc
 *   message length in byte, [0..8]
 *
 * @return
 *   error code
 */
long coTfsDeactivateGenericMonitor(dword canId, dword dlc);

}
