#include "coTfsEmcyWaitForMessage.h"

#include <iostream>

namespace capl
{

long coTfsEmcyWaitForMessage(dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
