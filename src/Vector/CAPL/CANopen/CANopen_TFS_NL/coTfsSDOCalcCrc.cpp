#include "coTfsSDOCalcCrc.h"

#include <iostream>

namespace capl
{

dword coTfsSDOCalcCrc(byte inValueBuf[], dword valueBufSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
