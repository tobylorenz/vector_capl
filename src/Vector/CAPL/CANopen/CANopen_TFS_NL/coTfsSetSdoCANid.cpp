#include "coTfsSetSdoCANid.h"

#include <iostream>

namespace capl
{

long coTfsSetSdoCANid(dword sdoClnRxId, dword sdoClnTxId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
