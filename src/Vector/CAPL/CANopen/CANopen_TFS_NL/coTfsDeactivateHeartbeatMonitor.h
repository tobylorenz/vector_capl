#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Switches off the checking of the heartbeat producer.
 *
 * This function switches off the calling of the heartbeat callbacks if these are switched on.
 *
 * The callbacks can be switched on with coTfsActivateHeartbeatMonitor.
 *
 * @param nodeId
 *   identifier of the node for which the heartbeat callbacks are to deactivate
 *
 * @return
 *   error code
 */
long coTfsDeactivateHeartbeatMonitor(dword nodeId);

}
