#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Stops a test after a specified number of CAN messages.
 *
 * @todo
 */

}
