#include "coTfsLifeGuarding.h"

#include <iostream>

namespace capl
{

long coTfsLifeGuarding(dword guardTime, dword retryFactor, dword tolerance)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
