#include "coTfsNMTStartNode.h"

#include <iostream>

namespace capl
{

long coTfsNmtStartNode(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long coTfsNmtStartNode(dword broadcastFlag)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
