#include "coTfsNMTRequest.h"

#include <iostream>

namespace capl
{

long coTfsNmtRequest(dword command)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long coTfsNmtRequest(dword command, dword broadcastFlag)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
