#include "coTfsNMTResetNode.h"

#include <iostream>

namespace capl
{

long coTfsNmtResetNode(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long coTfsNmtResetNode(dword broadcastFlag)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
