#include "coTfsSDOSetAbortControl.h"

#include <iostream>

namespace capl
{

long coTfsSDOSetAbortControl(dword controlValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
