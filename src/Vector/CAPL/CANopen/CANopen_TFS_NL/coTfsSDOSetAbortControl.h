#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Defines which values are allowed for successful test completion.
 *
 * The function defines which values are allowed for successful test completion.
 *
 * @param controlValue
 *   - 1: correct response or accepted SDO abort code (see coTfsSDOAddAccAbortCode) is allowed for successful test completion, default
 *   - 2: correct response or any SDO abort code is allowed
 *   - 3: correct response is not allowed
 *   - 4: correct response is not allowed but any SDO abort code
 *
 * @return
 *   error code
 */
long coTfsSDOSetAbortControl(dword controlValue);

}
