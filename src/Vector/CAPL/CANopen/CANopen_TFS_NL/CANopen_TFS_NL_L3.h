#pragma once

/* Guarding */
#include "coTfsGuarding.h"

/* Heartbeat */
#include "coTfsHeartbeat.h"

/* NMT */
#include "coTfsNMT.h"

/* Object Dictionary */
#include "coTfsODCheckStdEntries.h"

/* SDO */
#include "coTfsSDO.h"

/* SYNC */
#include "coTfsSyncProducer.h"
