#include "coTfsSDOGetUploadData.h"

#include <iostream>

namespace capl
{

long coTfsSDOGetUploadData(byte outValueBuf[], dword valueBufSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
