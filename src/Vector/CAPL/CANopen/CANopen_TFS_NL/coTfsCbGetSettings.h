#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Gets parameter of an active monitor.
 *
 * This function gets the set parameter of an active monitor.
 *
 * @param type
 *   - 0: Heartbeat message
 *   - 1: Sync message
 *   - 2: Sync PDO message
 *   - 3: Guarding RTR message for request
 *
 * @param nodeId
 *   monitored node-ID for request
 *
 * @param cycleTime
 *   set cycle time
 *
 * @param tolerance
 *   set tolerance
 *
 * @return
 *   error code
 */
long coTfsCbGetSettings(dword type, dword nodeId, dword cycleTime[1], dword tolerance[1]);

}
