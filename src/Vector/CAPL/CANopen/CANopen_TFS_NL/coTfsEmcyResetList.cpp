#include "coTfsEmcyResetList.h"

#include <iostream>

namespace capl
{

long coTfsEmcyResetList(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long coTfsEmcyResetList(dword nodeID)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
