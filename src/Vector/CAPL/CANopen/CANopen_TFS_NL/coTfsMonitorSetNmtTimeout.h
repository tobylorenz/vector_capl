#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sets the NMT timeout for the passive communication monitor.
 *
 * This function sets the NMT timeout for the passive communication monitor.
 *
 * You can start the monitoring with coTfsMonitorActivate and stop it with coTfsMonitorDeactivate.
 *
 * To include node-IDs or ranges of node-IDs for monitoring you can use the functions coTfsMonitorIncludeNodeId and coTfsMonitorIncludeNodeIdRange. For exclusion you can use coTfsMonitorExcludeNodeId and coTfsMonitorExcludeNodeIdRange.
 *
 * @param nmtTimeoutInMs
 *   timeout in ms, default value: 500 ms
 *
 * @return
 *   error code
 */
long coTfsMonitorSetNmtTimeout(dword nmtTimeoutInMs);

}
