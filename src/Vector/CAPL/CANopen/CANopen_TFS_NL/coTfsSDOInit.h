#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Starts an expedited/segmented SDO data transfer.
 *
 * This function sends an initialization message for
 *   - SDO Expedited Upload
 *   - SDO Segmented Upload
 *   - SDO Expedited Download
 *   - SDO Segmented Download
 * The protocol is described in detail in DS-301.
 *
 * @param index
 *   object index
 *
 * @param subIndex
 *   object sub index
 *
 * @param ccs (client command specifier)
 *   - 1: download
 *   - 2: upload, other values can cause unexpected behavior
 *
 * @param expedited
 *   - 0: normal transfer
 *   - 1: expedited transfer
 *
 * @param sizeIndicated
 *   - 0: size unknown
 *   - 1: size known (recommended value)
 *
 * @param notUsed
 *   number of data bytes that contain no reference data
 *
 * @param inValueBuf
 *   data field, at least 4 bytes large reference data assigned
 *
 * @param valueBufSize
 *   buffer size in byte of inValueBuf
 *
 * @return
 *   error code
 */
long coTfsSDOInit(dword index, dword subIndex, dword ccs, dword expedited, dword sizeIndicated, dword notUsed, byte  inValueBuf[], dword valueBufSize);

}
