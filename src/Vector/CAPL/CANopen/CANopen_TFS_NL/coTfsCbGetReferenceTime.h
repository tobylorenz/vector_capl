#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Gets the maximum, minimum, and average value of an active monitor.
 *
 * This function gets the maximum, minimum, and average value of an active monitor.
 *
 * @param type
 *   - 0 - Heartbeat message
 *   - 1 - Sync message
 *   - 2 - Sync PDO message
 *   - 3 - Guarding RTR message for request
 *
 * @param nodeId
 *   monitored node-ID for request
 *
 * @param refTime
 *   reference time of the last executed callback [ms]
 *
 * @param minTime
 *   minimum time difference [ms]
 *
 * @param minOccTime
 *   minimum time stamp of occurrence [ms]
 *
 * @param maxTime
 *   maximum time difference [ms]
 *
 * @param maxOccTime
 *   maximum time stamp of occurrence [ms]
 *
 * @param averageTime
 *   average time of all callbacks
 *
 * @return
 *   error code
 */
long coTfsCbGetReferenceTime(dword type, dword nodeId, dword refTime[1], dword minTime[1], dword minOccTime[1], dword maxTime[1], dword maxOccTime[1], dword averageTime[1]);

}
