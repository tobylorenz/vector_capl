#include "coTfsMonitorSetLssTimeout.h"

#include <iostream>

namespace capl
{

long coTfsMonitorSetLssTimeout(dword lssTimeoutInMs)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
