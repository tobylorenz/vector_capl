#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sends an Inquire revision number request and waits for the response.
 *
 * @todo
 */

}
