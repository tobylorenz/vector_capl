#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sets the CAN identifier to be used for the SDO tests.
 *
 * With this function the internal the CAN identifiers to be used for the SDO tests are set. All test functions, which uses SDO access, will use the specified identifiers. This setting will override the application profile specific SDO id settings.
 *
 * @param sdoClnRxId
 *   CAN-ID for receive SDOs
 *
 * @param sdoClnTxId
 *   CAN-ID for send SDOs
 *
 * @return
 *   error code
 */
long coTfsSetSdoCANid(dword sdoClnRxId, dword sdoClnTxId);

}
