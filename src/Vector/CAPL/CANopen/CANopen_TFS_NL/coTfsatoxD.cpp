#include "coTfsatoxD.h"

#include <iostream>

namespace capl
{

dword coTfsatoxD(char inValueBuf[], dword valueBufSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
