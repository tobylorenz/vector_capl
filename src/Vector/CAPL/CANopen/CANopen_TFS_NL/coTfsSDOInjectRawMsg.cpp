#include "coTfsSDOInjectRawMsg.h"

#include <iostream>

namespace capl
{

long coTfsSDOInjectRawMsg(dword canId, dword dlc, byte buf[], dword bufSize, dword isRtr, dword transmitCounter, dword replace)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long coTfsSDOInjectRawMsg(dword canId, dword dlc, qword buf, dword isRtr, dword transmitCounter, dword replace)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
