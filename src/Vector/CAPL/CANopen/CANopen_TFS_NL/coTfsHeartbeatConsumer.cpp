#include "coTfsHeartbeatConsumer.h"

#include <iostream>

namespace capl
{

long coTfsHeartbeatConsumer(dword virtProducerId, dword consumerTime, dword tolerance)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
