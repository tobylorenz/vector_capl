#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Inserts a SDO abort message to the following command.
 *
 * This function inserts a SDO Abort Code message during the following cotfs command. Only one message is supported and the insertion is only possible for the directly following command. The inserted message is sent directly after the x.message, whereas x is defined with transmitCounter.
 *
 * If less messages than expected are sent by the following command (transmitCounter > number of sent messages), the insertion of the message is canceled.
 *
 * @param canId
 *   CAN identifier, highest bit for extended and standard CAN identifier message
 *
 * @param index
 *   object index
 *
 * @param subIndex
 *   object sub index
 *
 * @param abortCode
 *   SDO abort code
 *
 * @param transmitCounter
 *   specifies the number of necessary sent messages by the CANopen® test module after that the message will be inserted or which message number will be replaced
 *   If not enough messages were sent by the following cotfs command, no message will be inserted/replaced at all (transmitCounter = 1..x).
 *
 * @param replace
 *   - 0: the message to be injected is sent after the regular planned CAN message
 *   - 1: the regular planned CAN message is replaced with the message to be injected
 *
 * @return
 *   error code
 */
long coTfsSDOInjectAbortMsg(dword canId, dword index, dword subIndex, dword abortCode, dword transmitCounter, dword replace);


}
