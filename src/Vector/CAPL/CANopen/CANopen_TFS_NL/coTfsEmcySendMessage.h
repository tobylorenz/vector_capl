#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sends an emergency message.
 *
 * This function sends out an emergency message.
 *
 * @param nodeID
 *   node-ID of the node that is sender of the message
 *
 * @param emcyCode
 *   emergency code
 *
 * @param errorReg
 *   error register
 *
 * @param inMsCodeBuf[]
 *   manufacturer-specific code (data field)
 *
 * @param msCodeBufSize
 *   size of buffer in byte of inMsCodeBuf
 *
 * @return
 *   error code
 */
long coTfsEmcySendMessage(dword nodeID, dword emcyCode, dword errorReg, byte inMsCodeBuf[5], dword msCodeBufSize);

}
