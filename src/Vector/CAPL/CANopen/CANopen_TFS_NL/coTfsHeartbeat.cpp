#include "coTfsHeartbeat.h"

#include <iostream>

namespace capl
{

long coTfsHeartbeat(dword virtProducerId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
