#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Switches off the checking of guarding RTRs.
 *
 * This function switches off the calling of the RTR callbacks if these are switched on.
 *
 * The callbacks can be switched on with coTfsActivateGuardingReqMonitor.
 *
 * @param nodeId
 *   identifier of the node for which the RTR callbacks are to deactivate
 *
 * @return
 *   error code
 */
long coTfsDeactivateGuardingReqMonitor(dword nodeId);

}
