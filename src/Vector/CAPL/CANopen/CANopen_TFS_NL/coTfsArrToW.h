#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Converts a byte array to a word value.
 *
 * The function converts the content of a byte array into a word value.
 *
 * @param inByteArray[]
 *   array containing the bytes to be converted, recommended array size: 2 byte
 *
 * @param arraysize
 *   size of the array
 *
 * @return
 *   converted value or 0 if parameters are invalid
 */
word coTfsArrToW(byte inByteArray[], dword arraysize);

}
