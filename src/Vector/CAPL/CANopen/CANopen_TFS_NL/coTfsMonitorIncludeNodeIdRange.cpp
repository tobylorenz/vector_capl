#include "coTfsMonitorIncludeNodeIdRange.h"

#include <iostream>

namespace capl
{

long coTfsMonitorIncludeNodeIdRange(dword minNodeId, dword maxNodeId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
