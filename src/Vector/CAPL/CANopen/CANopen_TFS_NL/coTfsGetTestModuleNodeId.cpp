#include "coTfsGetTestModuleNodeId.h"

#include <iostream>

namespace capl
{

dword coTfsGetTestModuleNodeId(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
