#include "coTfsDeactivateSyncMonitor.h"

#include <iostream>

namespace capl
{

long coTfsDeactivateSyncMonitor(dword canId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
