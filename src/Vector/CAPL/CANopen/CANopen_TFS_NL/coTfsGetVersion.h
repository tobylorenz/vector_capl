#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Returns the version of the CANopen test feature set node layer.
 *
 * This function returns the internal version number of this CANopen Test Feature Set node layer.
 *
 * @return
 *   version number
 */
dword coTfsGetVersion(void);

}
