#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Waits for an emergency message of a particular node.
 *
 * This function waits until either a time-out occurs or an emergency message for the selected DUT is received.
 *
 * @param timeout
 *   time-out in ms
 *
 * @return
 *   - 0: a time-out has occurred
 *   - 1: an emergency message was received
 */
long coTfsEmcyWaitForMessage(dword timeout);

}
