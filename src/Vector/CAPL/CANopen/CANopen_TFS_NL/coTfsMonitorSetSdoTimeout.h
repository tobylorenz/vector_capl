#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sets the SDO timeout for the passive communication monitor.
 *
 * This function sets the SDO timeout for the passive communication monitor.
 *
 * You can start the monitoring with coTfsMonitorActivate and stop it with coTfsMonitorDeactivate.
 *
 * To include node-IDs or ranges of node-IDs for monitoring you can use the functions coTfsMonitorIncludeNodeId and coTfsMonitorIncludeNodeIdRange. For exclusion you can use coTfsMonitorExcludeNodeId and coTfsMonitorExcludeNodeIdRange.
 *
 * @param sdoTimeoutInMs
 *   timeout in ms, default value: 200 ms
 *
 * @return
 *   error code
 */
long coTfsMonitorSetSdoTimeout(dword sdoTimeoutInMs);

}
