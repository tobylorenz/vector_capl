#include "coTfsMonitorExcludeNodeIdRange.h"

#include <iostream>

namespace capl
{

long coTfsMonitorExcludeNodeIdRange(dword minNodeId, dword maxNodeId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
