#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Deletes all objects of the internal test object list.
 *
 * @todo
 */

}
