#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Checks a single object dictionary entry of existence and size via a SDO upload.
 *
 * @todo
 */

}
