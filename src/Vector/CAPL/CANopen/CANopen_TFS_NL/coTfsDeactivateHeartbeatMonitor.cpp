#include "coTfsDeactivateHeartbeatMonitor.h"

#include <iostream>

namespace capl
{

long coTfsDeactivateHeartbeatMonitor(dword nodeId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
