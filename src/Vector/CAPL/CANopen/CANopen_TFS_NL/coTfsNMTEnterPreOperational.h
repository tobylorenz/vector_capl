#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   The DUT is set to state "Pre-Operational".
 *
 * This call triggers a NMT message that sets the DUT into the pre-operational state.
 *
 * The set state is checked with heartbeat and guarding mechanisms.
 *
 * For this, there is first an attempt to configure a heartbeat producer in the DUT (object 0x1017). The heartbeat message contains the current device status. If the DUT should not make a heartbeat producer available, a remote guarding frame is sent to the DUT which responses with the corresponding guarding response. The procedure is repeated again. The second response is evaluated and contains the device status. The CANopen® specification provides that a CANopen® device supports at least one of the two modes.
 *
 * @return
 *   error code
 */
long coTfsNmtEnterPreOperational(void);

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   The DUT is set to state "Pre-Operational".
 *
 * This call triggers a NMT message that sets the DUT into the pre-operational state.
 *
 * The set state is checked with heartbeat and guarding mechanisms.
 *
 * For this, there is first an attempt to configure a heartbeat producer in the DUT (object 0x1017). The heartbeat message contains the current device status. If the DUT should not make a heartbeat producer available, a remote guarding frame is sent to the DUT which responses with the corresponding guarding response. The procedure is repeated again. The second response is evaluated and contains the device status. The CANopen® specification provides that a CANopen® device supports at least one of the two modes.
 *
 * @param broadcastFlag
 *   - !=0: NMT message is sent to all nodes (broadcast)
 *
 * @return
 *   error code
 */
long coTfsNmtEnterPreOperational(dword broadcastFlag);

}
