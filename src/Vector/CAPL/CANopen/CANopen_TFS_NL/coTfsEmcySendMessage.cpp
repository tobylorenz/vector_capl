#include "coTfsEmcySendMessage.h"

#include <iostream>

namespace capl
{

long coTfsEmcySendMessage(dword nodeID, dword emcyCode, dword errorReg, byte inMsCodeBuf[5], dword msCodeBufSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
