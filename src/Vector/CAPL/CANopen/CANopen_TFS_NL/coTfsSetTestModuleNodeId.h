#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sets the node-ID of the tester.
 *
 * The function set the node-ID of the tester. This is particularly necessary for the application profile. The function has to be called before the first use of SDOs.
 *
 * @param nodeId
 *   node-ID of the tester, if application profile 447 is used the valid range is 1..16
 *
 * @return
 *   error code
 */
long coTfsSetTestModuleNodeId(dword nodeId);

}
