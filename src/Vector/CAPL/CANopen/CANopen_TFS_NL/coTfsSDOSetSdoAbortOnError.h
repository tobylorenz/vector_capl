#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Enables or disables the sending of SDO abort codes on failed SDO accesses.
 *
 * This function enables/disables sending a SDO abort code by the test module if a SDO access by SDO Level 2 functions or other test functions, that implicitly uses the SDO functionality, has failed. The setting is used until a new call of this function. For this the node-ID of the test module and the SDO abort code 0x8000 0000 (general error) is used.
 *
 * @param setSdoAbort
 *   - 0: no SDO abort message is sent in error cases (default)
 *   - 1: a SDO abort message is sent in error cases
 *
 * @return
 *   error code
 */
long coTfsSDOSetSdoAbortOnError(dword setSdoAbort);

}
