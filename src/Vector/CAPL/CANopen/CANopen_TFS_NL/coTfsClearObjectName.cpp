#include "coTfsClearObjectName.h"

#include <iostream>

namespace capl
{

long coTfsClearObjectName(dword nodeId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
