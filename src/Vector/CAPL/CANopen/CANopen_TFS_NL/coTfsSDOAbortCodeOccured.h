#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Checks for successful execution of a SDO command with a valid SDO abort code.
 *
 * @todo
 */

}
