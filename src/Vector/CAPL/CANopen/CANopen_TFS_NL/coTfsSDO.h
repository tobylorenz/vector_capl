#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Executes an expedited/segmented/block download/upload test.
 *
 * This function executes the following transfers:
 *   - SDO Block Upload
 *   - SDO Expedited Upload
 *   - SDO Segmented Upload
 *   - SDO Block Download
 *   - SDO Expedited Download
 *   - SDO Segmented Download
 *
 * In each case, first a value is written with a download and this value is read back with an upload and checked. The test is destructive, the object in the DUT is overwritten. The object's data is overwritten during the test and not restored. The block transfers are transmitted with a CRC checksum if the CANopen® device supports these.
 *
 * The test counts as passed if all 6 data transmissions were completed successfully.
 *
 * @param index
 *   index of a domain or string object
 *
 * @param subIndex
 *   sub index of a domain or string object
 *
 * @return
 *   error code
 */
long coTfsSDO(dword index, dword subIndex);

}
