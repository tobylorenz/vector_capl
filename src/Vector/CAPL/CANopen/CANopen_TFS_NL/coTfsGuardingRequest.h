#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Sends an user-definable guarding request.
 *
 * The test sets the guard time and retry factor objects in the DUT. After that, guardReqNumber remote guarding frames are sent to the target device. The target device must respond to each individual query within the tolerance delta. The test run is sequential.
 *
 * After that, the sending of the guarding remote frames is stopped. Depending on the waitForEmcy setting, it is waited for an emergency message (coTfsSetTimeoutValue ) before guard time and retry factor are reset again.
 *
 * An eventually existing active heartbeat producer is disabled at test start and enabled again at the end of the test.
 *
 * @param guardTime
 *   guard time in milliseconds
 *
 * @param retryFactor
 *   retry factor
 *
 * @param tolerance
 *   permitted time deviation of the target device in milliseconds
 *   It is recommended that you use an even value. The tolerated time-frame within which a message is still accepted is: x - (delta/2) <= x <= x + (delta/2)
 *
 * @param guardReqNumber
 *   number of the remote frames to be sent
 *
 * @param waitForEmcy
 *   If this value is set to 1, after setting the sending of the remote frames, an emergency message with the following content is awaited: EMCY code: 0x8030, Error Register 0x11. With 0, the emergency message is not awaited.
 *
 * @return
 *   error code
 */
long coTfsGuardingRequest(dword guardTime, dword retryFactor, dword tolerance, dword guardReqNumber, dword waitForEmcy);

}
