#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Activates the passive communication monitor.
 *
 * This function activates the passive communication monitor and resets the statistics. Furthermore the active nodes are written to the test report.
 *
 * You can stop the monitoring with coTfsMonitorDeactivate. To include node-IDs or ranges of node-IDs for monitoring you can use the functions coTfsMonitorIncludeNodeId and coTfsMonitorIncludeNodeIdRange. For exclusion you can use coTfsMonitorExcludeNodeId and coTfsMonitorExcludeNodeIdRange. With coTfsMonitorSetNmtTimeout and coTfsMonitorSetSdoTimeout you can set NMT and SDO timeouts.
 *
 * @return
 *   error code
 */
long coTfsMonitorActivate(void);

}
