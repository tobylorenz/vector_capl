#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Returns the current status of the heartbeat producer of the selected node.
 *   If this is switched on, the time settings are checked (passive test).
 *
 * This function tests whether the heartbeat producer on the selected DUT is active. If heartbeat messages are detected by the DUT, then the regularity is tested across a pre-defined time.
 *
 * The test fails if the heartbeat producer is not active or works outside of the set tolerance.
 *
 * This test works passively, no SDO accesses are executed on the DUT.
 *
 * @param duration
 *   test duration in milliseconds, how long the regularity of the heartbeat producer should be tested
 *
 * @param producerTime
 *   heartbeat producer time in milliseconds
 *
 * @param tolerance
 *   permitted time deviation of the target device in milliseconds
 *   It is recommended that you use an even value. The tolerated time-frame within which a message is still accepted is: x - (tolerance/2) <= x <= x + (tolerance/2)
 *
 * @return
 *   error code
 */
long coTfsHeartbeatProducerCheckIfActive(dword duration, dword producerTime, dword tolerance);

}
