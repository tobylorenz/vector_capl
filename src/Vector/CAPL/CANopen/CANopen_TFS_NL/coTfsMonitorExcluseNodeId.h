#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_TFS_NL
 *
 * @brief
 *   Excludes a single node-ID from monitoring.
 *
 * This function excludes a single node-ID from monitoring. To exclude a range of node-IDs use the function coTfsMonitorExcludeNodeIdRange.
 *
 * You can start the monitoring with coTfsMonitorActivate and stop it with coTfsMonitorDeactivate.
 *
 * To include node-IDs or ranges of node-IDs for monitoring you can use the functions coTfsMonitorIncludeNodeId and coTfsMonitorIncludeNodeIdRange. With coTfsMonitorSetNmtTimeout and coTfsMonitorSetSdoTimeout you can set NMT and SDO timeouts.
 *
 * @param nodeId
 *   node-ID that is to be excluded
 *
 * @return
 *   error code
 */
long coTfsMonitorExcludeNodeId(dword nodeId);

}
