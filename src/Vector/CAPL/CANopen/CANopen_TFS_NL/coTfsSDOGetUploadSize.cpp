#include "coTfsSDOGetUploadSize.h"

#include <iostream>

namespace capl
{

dword coTfsSDOGetUploadSize(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
