#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Confirms a SDO upload transfer.
 *
 * The function confirms a SDO upload transfer, which was initiated on an object with access type 7.
 * It can be used, e.g. in the event function coOnUploadIndication.
 * If the transfer should be aborted, then coODAbortTransfer must be used.
 *
 * @param size
 *   transfer size, value range 0..4.294.967.295
 *
 * @param value
 *   value of the object
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODUploadResponse(dword size, dword value, dword * errCode);

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Confirms a SDO upload transfer.
 *
 * The function confirms a SDO upload transfer, which was initiated on an object with access type 7.
 * It can be used, e.g. in the event function coOnUploadIndication.
 * If the transfer should be aborted, then coODAbortTransfer must be used.
 *
 * @param size
 *   transfer size, value range 0..4.294.967.295
 *
 * @param data
 *   data of the object
 *
 * @param dataSize
 *   number of data bytes
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODUploadResponse(dword size, char * data, dword dataSize, dword * errCode);

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Confirms a SDO upload transfer.
 *
 * The function confirms a SDO upload transfer, which was initiated on an object with access type 7.
 * It can be used, e.g. in the event function coOnUploadIndication.
 * If the transfer should be aborted, then coODAbortTransfer must be used.
 *
 * @param size
 *   transfer size, value range 0..4.294.967.295
 *
 * @param data
 *   data of the object
 *
 * @param dataSize
 *   number of data bytes
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODUploadResponse(dword size, byte * data, dword dataSize, dword * errCode);

}
