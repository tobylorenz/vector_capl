#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns a floating point value of an object.
 *
 * The function returns a floating point value of an object in the event functions coOnUploadResponse or coOnDownloadIndication.
 * The call is only allowed in these event functions.
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 *
 * @return
 *   value of the object entry
 */
double coThisGetFloat(dword * errCode);

}
