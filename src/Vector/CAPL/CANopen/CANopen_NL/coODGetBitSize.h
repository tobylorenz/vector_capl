#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the size of an object in bits.
 *
 * The function returns the size of an object in the local object dictionary in bits.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 *
 * @return
 *   size of the object in bits
 */
dword coODGetBitSize(dword index, dword subIndex, dword * errCode);

}
