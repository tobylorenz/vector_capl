#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   This function is called if a SDO download is executed.
 *
 * This function is called if an SDO download is executed on an object that was created with the access type 7.
 *
 * If the parameter size exceeds the value 4, the data must be transmitted segmented.
 * With a segmented transmission, this function is called twice.
 * The first call signals the object and the number of data bytes that should be written (dataValid is 0).
 * Then already before the transfer of the data, a length check can be made and the transfer confirmed with coODDownloadResponse or aborted with coODAbortTransfer.
 * The second call signals that the data was transferred (dataValid is 1).
 * Now the data can be accessed with coThisGetSigned, coThisGetUnsigned, coThisGetFloat, coThisGetData, and coThisGetSize.
 * Then, for example, a range check can be made and confirmed with coODDownloadResponse or aborted with coODAbortTransfer.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param size
 *   transfer size, value range 0..4.294.967.295
 *
 * @param dataValid
 *   specifies whether data is available, value range 0..1
 */
void coOnDownloadIndication(dword index, dword subIndex, dword size, dword dataValid);

}
