#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Causes the determination of the serial number of a LSS slave.
 *
 * This service causes the determination of the serial number of a LSS slave.
 * It is available in configuration mode only and there may only be one LSS slave in this mode.
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coLssInqSerialNo(dword * errCode);

}
