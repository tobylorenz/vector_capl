#include "coOnRPDOMissing.h"

#include <iostream>

namespace capl
{

void coOnRPDOMissing(dword pdoNumber)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
