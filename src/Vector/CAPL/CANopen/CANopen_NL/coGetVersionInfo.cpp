#include "coGetVersionInfo.h"

#include <iostream>

namespace capl
{

void coGetVersionInfo(char * buffer, dword bufferSize, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
