#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Creates a new object of type unsigned.
 *
 * Generates a new object in the local object dictionary of the specified type and with a start value with no leading sign.
 * If the length of the available start value (maximum 32 bits) is smaller than the size of the object, the remaining part is initialized with null.
 *
 * If the object already exists, then it is replaced by the new object.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..254
 *
 * @param dataType
 *   data type of the object
 *
 * @param access
 *   access type of the object
 *
 * @param initValue
 *   initial value of the object
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODCreateUnsigned(dword index, dword subIndex, dword dataType, dword access, dword initValue, dword * errCode);

}
