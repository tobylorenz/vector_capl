#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets the floating point value of an object.
 *
 * Sets the floating point value of an object in the local object dictionary.
 * This function can only be applied to objects with the data types 0x8 and 0x11.
 *
 * For the use of this function, a note on handling should be considered.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param value
 *   value of the object
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODSetFloat(dword index, dword subIndex, double value, dword * errCode);

}
