#include "coGetValue.h"

#include <iostream>

namespace capl
{

long coGetValue(char * envVar, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
