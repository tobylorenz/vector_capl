#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Creates a new object of type float.
 *
 * Generates a new object in the local object dictionary of the specified type and with a floating point start value.
 * If the size of the object (type 0x8 - 32 bit) is smaller than the length of the start value (64 bits), a conversion is undertaken.
 * This can cause lower precision or value falsification.
 *
 * If the object already exists, then it is replaced by the new object.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..254
 *
 * @param dataType
 *   data type of the object (limited, only type 0x8 and 0x11)
 *
 * @param access
 *   access type of the object
 *
 * @param initValue
 *   initial value of the object
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODCreateFloat(dword index, dword subIndex, dword dataType, dword access, double initValue, dword * errCode);

}
