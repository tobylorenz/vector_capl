#include "coShutDown.h"

#include <iostream>

namespace capl
{

void coShutDown(dword deleteOD, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
