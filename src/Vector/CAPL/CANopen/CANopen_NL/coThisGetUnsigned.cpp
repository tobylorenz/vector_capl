#include "coThisGetUnsigned.h"

#include <iostream>

namespace capl
{

dword coThisGetUnsigned(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
