#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   The function informs the user about the current state of the configuration process.
 *
 * During the start, a simulated node as configuration manager can supply other nodes on the network with configuration data.
 * This data is then transmitted via SDO by the simulated configuration manager.
 * The function informs the user about the current state of the configuration process.
 *
 * @param event
 *   event type
 *
 * @param nodeId
 *   node-ID, value range 0..127
 */
void coOnConfigReady(dword event, dword nodeId);

}
