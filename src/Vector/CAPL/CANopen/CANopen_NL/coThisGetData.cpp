#include "coThisGetData.h"

#include <iostream>

namespace capl
{

void coThisGetData(char * buffer, dword bufferSize, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void coThisGetData(byte * buffer, dword bufferSize, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
