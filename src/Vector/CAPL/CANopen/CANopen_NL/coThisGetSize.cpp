#include "coThisGetSize.h"

#include <iostream>

namespace capl
{

dword coThisGetSize(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
