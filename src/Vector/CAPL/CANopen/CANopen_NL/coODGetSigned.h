#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the value of an object of type signed.
 *
 * Returns the value of an object in the local object dictionary with a leading sign.
 * This function can only be applied to objects with the data types 0x2, 0x3, 0x10, and 0x4.
 *
 * For the use of this function, a note on handling should be considered.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 *
 * @return
 *   value of the object
 */
long coODGetSigned(dword index, dword subIndex, dword * errCode);

}
