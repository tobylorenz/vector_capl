#include "coThisGetFloat.h"

#include <iostream>

namespace capl
{

double coThisGetFloat(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
