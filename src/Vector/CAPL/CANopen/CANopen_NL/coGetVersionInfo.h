#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the current version number of the used node layer.
 *
 * The function returns the current version number of the node layer used.
 *
 * @param buffer
 *   buffer for the version number
 *
 * @param buffersize
 *   size of the buffer in bytes
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coGetVersionInfo(char * buffer, dword bufferSize, dword * errCode);

}
