#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets all LSS slaves to the configuration mode or leaving of the mode.
 *
 * Change of all LSS slaves into the configuration mode or leaving of the mode.
 *
 * @param mode
 *   Specifies in which mode all LSS slaves are switched
 *   - 0 - leaving of the configuration mode
 *   - 1 - transition to the configuration mode
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coLssSwitchModeGlob(dword mode, dword * errCode);

}
