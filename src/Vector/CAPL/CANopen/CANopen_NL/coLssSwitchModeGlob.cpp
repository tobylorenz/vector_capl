#include "coLssSwitchModeGlob.h"

#include <iostream>

namespace capl
{

void coLssSwitchModeGlob(dword mode, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
