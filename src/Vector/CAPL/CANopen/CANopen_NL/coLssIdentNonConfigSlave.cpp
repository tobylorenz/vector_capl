#include "coLssIdentNonConfigSlave.h"

#include <iostream>

namespace capl
{

void coLssIdentNonConfigSlave(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
