#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets the communication state of the node.
 *
 * The function sets the communication state of the node.
 * The node must previously have been started with coStartUp.
 * The node layer controls the local communication state independently.
 * Also a (configured) automatic transfer into the state Operational (Bit 2 in 1F80 not set) is caused by the node layer.
 * Should it nevertheless be necessary to intervene directly in the local state machine, this can occur via this function.
 *
 * @param newState
 *   desired state of the node:
 *   - 4 - stopped
 *   - 5 - operational
 *   - 6 - reset node
 *   - 7 - reset communication
 *   - 127 - pre-operational
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coSetLocalState(dword newState, dword * errCode);

}
