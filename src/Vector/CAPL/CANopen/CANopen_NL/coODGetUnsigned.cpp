#include "coODGetUnsigned.h"

#include <iostream>

namespace capl
{

dword coODGetUnsigned(dword index, dword subIndex, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
