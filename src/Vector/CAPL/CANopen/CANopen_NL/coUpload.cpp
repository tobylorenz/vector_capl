#include "coUpload.h"

#include <iostream>

namespace capl
{

void coUpload(dword nodeId, dword index, dword subIndex, dword flags, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
