#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Triggers the immediate transmission of a TPDO.
 *
 * Generally a simulated node is used with a defined cycle time. The default value is 5 ms, but it can be changed with the function coSetOperatingMode() for each node. The cycle time also defines the reaction time on data changes of the PDOs. Consider that a decrease of the node processing time leads to a higher load of the simulated system.
 *
 * If it is necessary to transmit the PDO directly after the data change, this function can be used. It triggers the immediate transmission of a TPDO.
 *
 * The following conditions have to be fulfilled to allow the transmission of a TPDO with this function:
 * - The relating node has to be in the communication state operational.
 * - COB-ID has to be valid (TPDO enabled).
 * - The configured mapping of the TPDO is valid.
 * - The Transmission type of the TPDO has to be 254 or 255.
 * - A potentially configured inhibit time for this TPDO is not currently active.
 * - The transmission message queue is not full.
 *
 * @param pdoNumber
 *   number of the transmitted PDO, value range 1..512
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coTriggerTPDO(dword pdoNumber, dword * errCode);

}
