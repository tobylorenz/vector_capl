#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets the data of an object.
 *
 * Sets the data of an object in the local object dictionary.
 * This function can be applied to all objects.
 * For objects with a size up to 32 bits or floating point objects, however, the functions coODSetSigned, coODSetUnsigned, and coODSetFloat are recommended.
 *
 * For the use of this function, a note on handling should be considered.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param data
 *   data of the object
 *
 * @param dataSize
 *   number of data bytes
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODSetData(dword index, dword subIndex, char * data, dword dataSize, dword * errCode);

void coODSetData(dword index, dword subIndex, byte * data, dword dataSize, dword * errCode);

}
