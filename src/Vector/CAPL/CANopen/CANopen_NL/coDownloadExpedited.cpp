#include "coDownloadExpedited.h"

#include <iostream>

namespace capl
{

void coDownloadExpedited(dword nodeId, dword index, dword subIndex, dword value, dword valueSize, dword flags, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void coDownloadExpedited(dword nodeId, dword index, dword subIndex, double value, dword flags, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
