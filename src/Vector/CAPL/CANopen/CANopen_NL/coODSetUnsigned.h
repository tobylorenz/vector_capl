#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets the value of an object of type unsigned.
 *
 * Sets the value of an object in the local object dictionary with no leading sign.
 * This function can be applied to all objects with a length up to 32 bits.
 * Exceptions are the data type 0x8, 0x9, 0xA, 0xF, and 0x11.
 *
 * For the use of this function, a note on handling should be considered.
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param value
 *   value of the object
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODSetUnsigned(dword index, dword subIndex, dword value, dword * errCode);

}
