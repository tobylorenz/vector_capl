#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Activates or deactivates an emergency error code.
 *
 * The function activates or deactivates an emergency error code.
 * The error register [0x1003,0] and the 'Predefined error field' object are set.
 * An emergency message is also sent.
 *
 * @param active
 *   specifies whether an error code should be activated or deactivated
 *   - 0 - deactivates emergency state
 *   - 1 - activates emergency state
 *   - 2 - activates and deactivates automatically after a few seconds
 *
 * @param emcyError
 *   emergency error code, value range 0..65.535
 *
 * @param specificData
 *   manufacturer-specific data (maximum 5 bytes are copied)
 *
 * @param dataSize
 *   length of the specific data in bytes
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coEmergency(dword active, dword emcyError, byte * specificData, dword dataSize, dword * errCode);

}
