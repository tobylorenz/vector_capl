#include "coODUploadResponse.h"

#include <iostream>

namespace capl
{

void coODUploadResponse(dword size, dword value, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void coODUploadResponse(dword size, char * data, dword dataSize, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void coODUploadResponse(dword size, byte * data, dword dataSize, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
