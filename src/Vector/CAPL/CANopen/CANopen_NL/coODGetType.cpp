#include "coODGetType.h"

#include <iostream>

namespace capl
{

dword coODGetType(dword index, dword subIndex, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
