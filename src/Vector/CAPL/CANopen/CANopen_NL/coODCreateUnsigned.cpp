#include "coODCreateUnsigned.h"

#include <iostream>

namespace capl
{

void coODCreateUnsigned(dword index, dword subIndex, dword dataType, dword access, dword initValue, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
