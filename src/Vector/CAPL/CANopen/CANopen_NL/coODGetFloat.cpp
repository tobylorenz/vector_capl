#include "coODGetFloat.h"

#include <iostream>

namespace capl
{

double coODGetFloat(dword index, dword subIndex, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
