#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets the value of an environment variable of type integer.
 *
 * The function sets the value of an environment variable of the type integer.
 *
 * @param envVar
 *   name of the environment variable
 *
 * @param value
 *   value of the environment variable
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field
 */
void coPutValue(char * envVar, long value, dword * errCode);

}
