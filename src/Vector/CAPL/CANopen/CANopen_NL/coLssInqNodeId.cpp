#include "coLssInqNodeId.h"

#include <iostream>

namespace capl
{

void coLssInqNodeId(dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
