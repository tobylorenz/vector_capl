#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Aborts a SDO transfer.
 *
 * The function aborts a SDO transfer, which was initiated on an object with access type 7.
 * It can be used in the event functions coOnDownloadIndication or coOnUploadIndication.
 *
 * @param abortCode
 *   abort code of the SDO transfer (see /3/ or coOnError under error class 3, except 0xFF01...)
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coODAbortTransfer(dword abortCode, dword * errCode);

}
