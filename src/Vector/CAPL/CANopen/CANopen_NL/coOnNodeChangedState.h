#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   This function is called if the state of the node changes.
 *
 * This function is called if the state of the node changes.
 * This can be triggered by the functions coSetLocalState, coStartUp, coShutDown or via the network.
 *
 * @param newState
 *   new state of the node:
 *   - 4 - stopped
 *   - 5 - operational
 *   - 6 - reset node
 *   - 7 - reset communication
 *   - 127 - pre-operational
 */
void coOnNodeChangedState(dword newState);

}
