#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Writes an entry into the object dictionary of another node by using the expedited SDO download. (form 1)
 *
 * Writing of an entry into the object dictionary of another node.
 *
 * The function triggers a SDO download. The response of the node is returned in the event function coOnDownloadResponse or coOnError.
 *
 * @param nodeId
 *   node-ID, value range 1..127
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param value
 *   value of the object
 *
 * @param valueSize
 *   size of the value, value range 1..4
 *
 * @param flags
 *   bit field for future expansions, reserved
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coDownloadExpedited(dword nodeId, dword index, dword subIndex, dword value, dword valueSize, dword flags, dword * errCode);

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Writes an entry into the object dictionary of another node by using the expedited SDO download. (form 2)
 *
 * Writing of an entry into the object dictionary of another node.
 *
 * The function triggers a SDO download. The response of the node is returned in the event function coOnDownloadResponse or coOnError.
 *
 * If it is used with the float parameter value, this function always initiates a transfer with the length 4 bytes.
 * This corresponds to the data type Real32.
 * Since CAPL only supports 8 byte floating point numbers, it must be noted that the value range must not be left when writing to a REAL32 object.
 *
 * @param nodeId
 *   node-ID, value range 1..127
 *
 * @param index
 *   index of the object, value range 1..65.535
 *
 * @param subIndex
 *   sub index of the object, value range 0..255
 *
 * @param value
 *   value of the object
 *
 * @param flags
 *   bit field for future expansions, reserved
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coDownloadExpedited(dword nodeId, dword index, dword subIndex, double value, dword flags, dword * errCode);

}
