#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   This function is called if the response of a LSS command was received.
 *
 * This function is called if the response of a LSS command was received.
 * This can be triggered by one of the LSS functions.
 * If during the execution an error occurs, the event function coOnError is called instead of these.
 *
 * @param evType
 *   LSS event type
 *
 * @param param
 *   additional parameter - dependent on evType (see LSS event types)
 *
 * @param param2
 *   additional parameter - dependent on evType (see LSS event types)
 */
void coOnLssEvent(dword evType, dword param, dword param2);

}
