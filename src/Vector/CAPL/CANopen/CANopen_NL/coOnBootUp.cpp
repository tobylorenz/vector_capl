#include "coOnBootUp.h"

#include <iostream>

namespace capl
{

void coOnBootUp(dword nodeId)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
