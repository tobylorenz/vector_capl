#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the label of the node.
 *
 * The function returns the label of the node.
 * The label is formed from the name and the node-ID of the assigned database.
 *
 * @param buffer
 *   buffer for the label
 *
 * @param bufferSize
 *   size of the buffer in bytes
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coGetNodeLabel(char * buffer, dword bufferSize, dword * errCode);

}
