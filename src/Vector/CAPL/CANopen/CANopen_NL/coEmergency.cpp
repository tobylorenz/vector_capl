#include "coEmergency.h"

#include <iostream>

namespace capl
{

void coEmergency(dword active, dword emcyError, byte * specificData, dword dataSize, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
