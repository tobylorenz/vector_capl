#include "coOnDownloadIndication.h"

#include <iostream>

namespace capl
{

void coOnDownloadIndication(dword index, dword subIndex, dword size, dword dataValid)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
