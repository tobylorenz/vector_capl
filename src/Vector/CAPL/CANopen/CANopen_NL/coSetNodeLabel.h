#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Sets the label of the node.
 *
 * Usually the node label is build automatically from the nodes name and its node-ID specified within the assigned CAN database.
 * For test nodes it is not possible to assign a node from a CAN database within the configuration dialog of the test node.
 * Furthermore the user maybe wants to determine the name of a node by itself.
 * This function can be used to determine the node label manually.
 * If the user wants to reset the manually specified label this function has to be called with an empty node label ("").
 * Afterwards the automatically build node label is used.
 *
 * @param nodeLabel
 *   new node label to be used for reports within the write window and the logging file
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coSetNodeLabel(char * nodeLabel, dword * errCode);

}
