#include "coODCreateFloat.h"

#include <iostream>

namespace capl
{

void coODCreateFloat(dword index, dword subIndex, dword dataType, dword access, double initValue, dword * errCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
