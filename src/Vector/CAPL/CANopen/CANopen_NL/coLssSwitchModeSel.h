#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Puts a single LSS slave into the configuration mode.
 *
 * This service puts the LSS slave whose LSS address matches the transmitted parameters into the configuration mode.
 *
 * @param vendorId
 *   vendor-ID of the slave
 *
 * @param productCode
 *   Product code of the slave
 *
 * @param revisionNo
 *   Revision number of the slave
 *
 * @param serialNo
 *   Serial number of the slave
 *
 * @param errCode
 *   error code buffer (is entered in index 0 of the field)
 */
void coLssSwitchModeSel(dword vendorId, dword productCode, dword revisionNo, dword serialNo, dword * errCode);

}
