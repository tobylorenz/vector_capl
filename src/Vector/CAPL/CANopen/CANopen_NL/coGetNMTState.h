#pragma once

#include "../../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANopen_NL
 *
 * @brief
 *   Returns the state of a node on the network.
 *
 * Returns the state of a node on the network.
 * The node for which this function is called must previously have been started with coStartUp.
 * Furthermore, the node must be configured as NMT master (Bit 0 in 1F80 set) and the object 1F82 must exist in the object dictionary.
 *
 * @param nodeId
 *   node-ID, value range 1..127
 *
 * @param errCode
 *   error code buffer (error code is entered in index 0 of this array)
 *
 * @return
 *   current communication state of the node
 *   - 0 - unknown or failed
 *   - 4 - stopped
 *   - 5 - operational
 *   - 127 - pre-operational
 */
dword coGetNMTState(dword nodeId, dword * errCode);

}
