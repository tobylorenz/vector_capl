#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Cyclical sending restarts.
 *
 * Cyclical sending restarts.
 * This function may only be used after ILControlWait.
 *
 * @return
 *   - 0: No error.
 *   - -1: Momentary state of the IL does not permit this query.
 *   - -50: Node layer is inactive � possibly deactivated in the node's configuration dialog.
 */
long ILControlResume(void);

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Cyclical sending restarts.
 *
 * Restarts the interaction layer of the selected simulation node.
 * This function influences a simulation node with an assigned CANoe interaction layer.
 *
 * @param aNodeName
 *   Name of the node that should restart its the interaction layer.
 *
 * @return
 *   - 0: No error.
 *   - -1: General error.
 */
long ILControlResume(char * aNodeName);

}
