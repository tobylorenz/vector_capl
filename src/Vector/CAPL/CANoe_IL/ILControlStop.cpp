#include "ILControlStop.h"

#include <iostream>

namespace capl
{

long ILControlStop(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
