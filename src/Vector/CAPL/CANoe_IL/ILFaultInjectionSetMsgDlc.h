#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Assigns a new DLC to the message.
 *
 * Assigns a new DLC to the message.
 * To set the DLC back to its original value, use ILFaultInjectionResetMsgDlc.
 *
 * @param msg
 *   Message that should get a new DLC.
 *
 * @param dlc
 *   New DLC of the message.
 *
 * @return
 *   - 0: No error.
 *   - -1: Momentary state of the IL does not permit this function.
 *   - -50: Nodelayer is inactive � possibly deactivated in the node�s configuration dialog.
 *   - -100: Signal or message was not found in the database or is not mapped to this node.
 */
long ILFaultInjectionSetMsgDlc(char * msg, dword dlc);

}
