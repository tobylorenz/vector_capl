#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Resets the cycle time of the message to the database cycle time.
 *
 * Resets the cycle time of the message to the database cycle time,
 * after having manipulated the cycle time with ILFaultInjectionSetMsgCycleTime.
 *
 * @param msg
 *   Message that should get the database cycle time.
 *
 * @return
 *   - 0: No error.
 *   - -1: Momentary state of the IL does not permit this function.
 *   - -50: Nodelayer is inactive � possibly deactivated in the node�s configuration dialog.
 *   - -100: Signal or message was not found in the database or is not mapped to this node.
 */
long ILFaultInjectionResetMsgCycleTime(char * msg);

}
