#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Stops the simulation of the IL.
 *
 * The simulation of the IL is stopped.
 * After that no other function to control the IL has an effect to the IL.
 *
 * To restart the simulation of the IL use ILControlSimulationOn.
 *
 * @return
 *   - 0: No error.
 */
long ILControlSimulationOff(void);

}
