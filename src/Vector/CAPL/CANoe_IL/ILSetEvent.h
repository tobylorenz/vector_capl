#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Sends the transferred signal directly to the bus if the network is active.
 *
 * Sends the transferred signal directly to the bus if the network is active.
 *
 * The send model is ignored.
 *
 * @param sig
 *   Signal that should be sent. Specify as follows 'Message::Signal'.
 *
 * @return
 *   - 0: No error.
 *   - 1: Momentary state of the IL does not permit this query.
 *   - -50: Nodelayer is inactive � possibly deactivated in the node�s configuration dialog.
 *   - -100: Signal or message was not found in the database or is not mapped to this node
 *   - -104: General error for invalid calls.
 */
long ILSetEvent(char * sig);

}
