#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Prevents all sending of the message except the sending by calling the function ILSetMsgEvent.
 *
 * Disables the sending of the message except by calling the function ILSetMsgEvent.
 *
 * @param msg
 *   Message that should be disabled.
 *
 * @return
 *   - 0: No error.
 *   - -1: Momentary state of the IL does not permit this function.
 *   - -50: Nodelayer is inactive � possibly deactivated in the node�s configuration dialog.
 *   - -100: Signal or message was not found in the database or is not mapped to this node.
 */
long ILFaultInjectionDisableMsg(char * msg);

}
