#include "ILDisturbPduUpdateBit.h"

#include <iostream>

namespace capl
{

long ILDisturbPduUpdateBit(char * aPduName, int disturbanceCount, int updateBit)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
