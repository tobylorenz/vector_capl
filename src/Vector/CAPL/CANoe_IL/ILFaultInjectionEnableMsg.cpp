#include "ILFaultInjectionEnableMsg.h"

#include <iostream>

namespace capl
{

long ILFaultInjectionEnableMsg(char * msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
