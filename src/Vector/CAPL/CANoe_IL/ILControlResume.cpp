#include "ILControlResume.h"

#include <iostream>

namespace capl
{

long ILControlResume(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long ILControlResume(char * aNodeName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
