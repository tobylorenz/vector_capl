#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Returns the last CANoe IL error code.
 *
 * Returns the last CANoe IL error code.
 *
 * @return
 *   Error Code of the last call.
 *   - -50: Nodelayer is inactive � possibly deactivated in the node�s configuration dialog.
 */
long ILErrno(void);

}
