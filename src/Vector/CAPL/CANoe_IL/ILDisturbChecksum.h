#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Disturbs the checksum a configurable value.
 *
 * This function modifies the checksum. Different fault injections are possible.
 * This function influences a simulation node with an assigned CANoe interaction layer.
 *
 * @param pduName
 *   Name of the node that should be modified.
 *
 * @param checksumType
 *   The possible values are described in the corresponding OEM Package manual.
 *
 * @param disturbanceMode
 *   Identifies the disturbance mode:
 *   - 0: Value. The disturbance uses the value in disturbanceValue as counter.
 *   - 1: Freeze. The current counter value is transmitted.
 *   - 2: Random. A random value is transmitted as counter.
 *   - 3: Offset. The counter is incremented with the value in disturbanceValue.
 *
 * @param disturbanceCount
 *   - -1: Infinite.
 *   - 0: Stops the disturbance, e.g.a infinite disturbance.
 *   - n: Number of disturbances.
 *
 * @param disturbanceValue
 *   This value is used in combination with the disturbanceMode.
 *
 * @return
 *   - 0: No error.
 *   - -1: General error.
 */
long ILDisturbChecksum(char * pduName, long checksumType, long disturbanceMode, long disturbanceCount, long disturbanceValue);

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Disturbs the checksum a configurable value.
 *
 * This function modifies the checksum. Different fault injections are possible.
 * This function influences a simulation node with an assigned CANoe interaction layer.
 *
 * @param pduName
 *   Name of the node that should be modified.
 *
 * @param sigGroupName
 *   Some systems assign a counter to signal group.
 *   With this variant you can apply the disturbance to a dedicated signal group within a PDU.
 *
 * @param checksumType
 *   The possible values are described in the corresponding OEM Package manual.
 *
 * @param disturbanceMode
 *   Identifies the disturbance mode:
 *   - 0: Value. The disturbance uses the value in disturbanceValue as counter.
 *   - 1: Freeze. The current counter value is transmitted.
 *   - 2: Random. A random value is transmitted as counter.
 *   - 3: Offset. The counter is incremented with the value in disturbanceValue.
 *
 * @param disturbanceCount
 *   - -1: Infinite.
 *   - 0: Stops the disturbance, e.g.a infinite disturbance.
 *   - n: Number of disturbances.
 *
 * @param disturbanceValue
 *   This value is used in combination with the disturbanceMode.
 *
 * @return
 *   - 0: No error.
 *   - -1: General error.
 */
long ILDisturbChecksum(char * pduName, char * sigGroupName, long checksumType, long disturbanceMode, long disturbanceCount, long disturbanceValue);

}
