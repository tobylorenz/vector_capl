#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Disturbs the signal update bit with a configurable value.
 *
 * This function modifies the update bit of a signal. Different fault injections are possible.
 * This function influences a simulation node with an assigned CANoe interaction layer.
 *
 * @param aPduName
 *   Name of the node that should be modified.
 *
 * @param aSignalName
 *   Name of the signal using an update bit.
 *
 * @param disturbanceMode
 *   Identifies the disturbance mode:
 *   - 0: Value. The disturbance uses the value in disturbanceValue as counter.
 *   - 1: Freeze. The current counter value is transmitted.
 *   - 2: Random. A random value is transmitted as counter.
 *
 * @param disturbanceCount
 *   - -1: Infinite.
 *   - 0: Stops the disturbance, e.g.a infinite disturbance.
 *   - n: Number of disturbances.
 *
 * @param disturbanceValue
 *   This value is used in combination with the disturbanceMode.
 *
 * @return
 *   - 0: No error.
 *   - -1: General error.
 */
long ILDisturbSignalUpdateBit(char * aPduName, char * aSignalName, long disturbanceMode, long disturbanceCount, long disturbanceValue);

}
