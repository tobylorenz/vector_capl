cmake_minimum_required(VERSION 2.8)

set(function_group CANoe_IL)

set(source_files
  ILActivateClamp15.cpp
  ILControlInit.cpp
  ILControlResume.cpp
  ILControlSimulationOff.cpp
  ILControlSimulationOn.cpp
  ILControlStart.cpp
  ILControlStop.cpp
  ILControlWait.cpp
  ILDeactivateClamp15.cpp
  ILDisturbChecksum.cpp
  ILDisturbCounter.cpp
  ILDisturbPduUpdateBit.cpp
  ILDisturbSignalUpdateBit.cpp
  ILEnableTimingCyclic.cpp
  ILEnableTimingEvtTrg.cpp
  ILEnableTimingImmed.cpp
  ILErrno.cpp
  ILFaultInjectionDisableMsg.cpp
  ILFaultInjectionEnableMsg.cpp
  ILFaultInjectionResetAllFaultInjection.cpp
  ILFaultInjectionResetMsgCycleTime.cpp
  ILFaultInjectionResetMsgDlc.cpp
  ILFaultInjectionSetMsgCycleTime.cpp
  ILFaultInjectionSetMsgDlc.cpp
  ILSetEvent.cpp
  ILSetMsgEvent.cpp
  ILSetResultString.cpp
  ILSetSignal.cpp
  ILSetSignalRaw.cpp
  ILSetSignalRawField.cpp)

set(header_files
  CANoe_IL.h
  ILActivateClamp15.h
  ILControlInit.h
  ILControlResume.h
  ILControlSimulationOff.h
  ILControlSimulationOn.h
  ILControlStart.h
  ILControlStop.h
  ILControlWait.h
  ILDeactivateClamp15.h
  ILDisturbChecksum.h
  ILDisturbCounter.h
  ILDisturbPduUpdateBit.h
  ILDisturbSignalUpdateBit.h
  ILEnableTimingCyclic.h
  ILEnableTimingEvtTrg.h
  ILEnableTimingImmed.h
  ILErrno.h
  ILFaultInjectionDisableMsg.h
  ILFaultInjectionEnableMsg.h
  ILFaultInjectionResetAllFaultInjection.h
  ILFaultInjectionResetMsgCycleTime.h
  ILFaultInjectionResetMsgDlc.h
  ILFaultInjectionSetMsgCycleTime.h
  ILFaultInjectionSetMsgDlc.h
  ILSetEvent.h
  ILSetMsgEvent.h
  ILSetResultString.h
  ILSetSignal.h
  ILSetSignalRawField.h
  ILSetSignalRaw.h)

include_directories(${CMAKE_CURRENT_BINARY_DIR})

add_library(${PROJECT_NAME}_${function_group} OBJECT ${source_files} ${header_files})

if(OPTION_USE_GCOV)
  target_link_libraries(${PROJECT_NAME}_${function_group} gcov)
  add_definitions(-g -O0 -fprofile-arcs -ftest-coverage)
endif()

if(CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif()

install(
  FILES ${header_files}
  DESTINATION include/Vector/CAPL/${function_group})
