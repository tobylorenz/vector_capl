#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Cyclical sending is stopped; setting signals is now no longer possible.
 *
 * Cyclical sending is stopped; setting signals is now no longer possible.
 *
 * @return
 *   - 0: No error.
 *   - 1: Momentary state of the IL does not permit this query.
 *   - 50: Node layer is inactive � possibly deactivated in the node's configuration dialog.
 */
long ILControlStop(void);

}
