#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Starts the simulation of the IL.
 *
 * Start the simulation of the IL.
 * The IL is in the same state as it was before stopping it by the function ILControlSimulationOff.
 *
 * @return
 *   - 0: No error.
 *   - -1: Momentary state of the IL does not permit this query.
 */
long ILControlSimulationOn(void);

}
