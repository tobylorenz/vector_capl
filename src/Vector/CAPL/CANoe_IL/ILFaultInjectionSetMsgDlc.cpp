#include "ILFaultInjectionSetMsgDlc.h"

#include <iostream>

namespace capl
{

long ILFaultInjectionSetMsgDlc(char * msg, dword dlc)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
