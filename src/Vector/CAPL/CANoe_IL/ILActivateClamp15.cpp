#include "ILActivateClamp15.h"

#include <iostream>

namespace capl
{

long ILActivateClamp15(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
