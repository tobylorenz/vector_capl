#include "ILDeactivateClamp15.h"

#include <iostream>

namespace capl
{

long ILDeactivateClamp15(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
