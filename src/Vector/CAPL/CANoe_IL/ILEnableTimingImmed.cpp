#include "ILEnableTimingImmed.h"

#include <iostream>

namespace capl
{

long ILEnableTimingImmed(char * pduName, int enable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
