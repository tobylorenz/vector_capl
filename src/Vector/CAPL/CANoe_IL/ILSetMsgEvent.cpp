#include "ILSetMsgEvent.h"

#include <iostream>

namespace capl
{

long ILSetMsgEvent(char * msg)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
