#include "ILErrno.h"

#include <iostream>

namespace capl
{

long ILErrno(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
