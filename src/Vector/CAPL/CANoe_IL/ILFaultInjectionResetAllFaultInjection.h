#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Reset all fault injection settings.
 *
 * Resets the fault injection settings for all messages of the node.
 *
 * @return
 *   - 0: No error.
 *   - -1: Momentary state of the IL does not permit this function.
 *   - -50: Nodelayer is inactive � possibly deactivated in the node�s configuration dialog.
 */
long ILFaultInjectionResetAllFaultInjections(void);

}
