#include "ILControlSimulationOn.h"

#include <iostream>

namespace capl
{

long ILControlSimulationOn(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
