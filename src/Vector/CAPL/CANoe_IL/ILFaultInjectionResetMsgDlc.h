#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup CANoe_IL
 *
 * @brief
 *   Resets the DLC of the message to the database DLC.
 *
 * Resets the DLC of the message to the database DLC,
 * after having manipulated the DLC with ILFaultInjectionSetMsgDlc.
 *
 * @param msg
 *   Message that should get the database DLC.
 *
 * @return
 *   - 0: No error.
 *   - -1: Momentary state of the IL does not permit this function.
 *   - -50: Nodelayer is inactive � possibly deactivated in the node�s configuration dialog.
 *   - -100: Signal or message was not found in the database or is not mapped to this node.
 */
long ILFaultInjectionResetMsgDlc(char * msg);

}
