#pragma once

/**
 * @defgroup CANoe_IL CANoe IL CAPL Functions
 */

/* Setting of Signals */
#include "ILSetSignal.h"
#include "ILSetSignalRaw.h"
#include "ILSetSignalRawField.h"

/* Test Functions */
#include "ILSetEvent.h"
#include "ILSetMsgEvent.h"

/* Controlling of the CANoe IL */
#include "ILControlInit.h"
#include "ILControlResume.h"
#include "ILControlSimulationOff.h"
#include "ILControlSimulationOn.h"
#include "ILControlStart.h"
#include "ILControlStop.h"
#include "ILControlWait.h"

/* NM Control */
#include "ILActivateClamp15.h"
#include "ILDeactivateClamp15.h"

/* Intepretation of the Return Values of the API Functions (Error Codes) */
#include "ILErrno.h"
#include "ILSetResultString.h"

/* Fault Injection Functions */
#include "ILFaultInjectionDisableMsg.h"
#include "ILFaultInjectionEnableMsg.h"
#include "ILFaultInjectionResetMsgCycleTime.h"
#include "ILFaultInjectionResetAllFaultInjection.h"
#include "ILFaultInjectionResetMsgDlc.h"
#include "ILFaultInjectionSetMsgCycleTime.h"
#include "ILFaultInjectionSetMsgDlc.h"

/* OEM Package based on Fault Injection Functions */
#include "ILDisturbChecksum.h"
#include "ILDisturbCounter.h"
#include "ILDisturbPduUpdateBit.h"
#include "ILDisturbSignalUpdateBit.h"
#include "ILEnableTimingCyclic.h"
#include "ILEnableTimingEvtTrg.h"
#include "ILEnableTimingImmed.h"
