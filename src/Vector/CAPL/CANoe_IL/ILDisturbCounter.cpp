#include "ILDisturbCounter.h"

#include <iostream>

namespace capl
{

long ILDisturbCounter(char * pduName, long counterType, long disturbanceMode, long disturbanceCount, long disturbanceValue, long continueMode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long ILDisturbCounter(char * pduName, char * sigGroupName, long counterType, long disturbanceMode, long disturbanceCount, long disturbanceValue, long continueMode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
