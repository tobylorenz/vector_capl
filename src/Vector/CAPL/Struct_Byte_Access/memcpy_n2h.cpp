#include "memcpy_n2h.h"

#include <iostream>

namespace capl
{

void memcpy_n2h(void * dest, byte * source)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy_n2h(void * dest, byte * source, int offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
