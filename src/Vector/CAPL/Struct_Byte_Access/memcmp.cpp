#include "memcmp.h"

#include <iostream>

namespace capl
{

int memcmp(void * s, byte * buffer)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

int memcmp(byte * buffer, void * s)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

int memcmp(void * s, void * t)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
