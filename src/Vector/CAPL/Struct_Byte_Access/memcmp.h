#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Compares the bytes of the parameters. (Form 1)
 *
 * In form 3, both structs must have the same type.
 *
 * @param s
 *   A struct
 *
 * @param buffer
 *   A byte-array
 *
 * @return
 *   0 if the bytes are equal; a value different from 0 if they are unequal
 */
int memcmp(void * s, byte * buffer);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Compares the bytes of the parameters. (Form 2)
 *
 * In form 3, both structs must have the same type.
 *
 * @param s
 *   A struct
 *
 * @param buffer
 *   A byte-array
 *
 * @return
 *   0 if the bytes are equal; a value different from 0 if they are unequal
 */
int memcmp(byte * buffer, void * s);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Compares the bytes of the parameters. (Form 3)
 *
 * In form 3, both structs must have the same type.
 *
 * @param s
 *   A struct
 *
 * @param t
 *   Another struct
 *
 * @return
 *   0 if the bytes are equal; a value different from 0 if they are unequal
 */
int memcmp(void * s, void * t);

}
