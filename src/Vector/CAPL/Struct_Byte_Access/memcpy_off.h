#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to destination, giving a destination start offset. (Form 1)
 *
 * Copies bytes from a source to destination, giving a destination start offset. The size of the
 * destination must be at least destOffset + length.
 *
 * @param dest
 *   (form 1, 2): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param source
 *   (form 3, 4): Struct from which the bytes shall be copied.
 *   (other forms): Array from which the bytes shall be copied.
 *
 * @param destOffset
 *   Start offset in the destination struct or array.
 *
 * @param sourceOffset
 *   Start offset int the source struct or array.
 *
 * @param length
 *   Number of bytes which shall be copied.
 *
 */
void memcpy_off(void * dest, dword destOffset, byte * source, dword sourceOffset, dword length);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to destination, giving a destination start offset. (Form 2)
 *
 * Copies bytes from a source to destination, giving a destination start offset. The size of the
 * destination must be at least destOffset + length.
 *
 * @param dest
 *   (form 1, 2): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param source
 *   (form 3, 4): Struct from which the bytes shall be copied.
 *   (other forms): Array from which the bytes shall be copied.
 *
 * @param destOffset
 *   Start offset in the destination struct or array.
 *
 * @param sourceOffset
 *   Start offset int the source struct or array.
 *
 * @param length
 *   Number of bytes which shall be copied.
 *
 */
void memcpy_off(void * dest, dword destOffset, char * source, dword sourceOffset, dword length);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to destination, giving a destination start offset. (Form 3)
 *
 * Copies bytes from a source to destination, giving a destination start offset. The size of the
 * destination must be at least destOffset + length.
 *
 * @param dest
 *   (form 1, 2): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param source
 *   (form 3, 4): Struct from which the bytes shall be copied.
 *   (other forms): Array from which the bytes shall be copied.
 *
 * @param destOffset
 *   Start offset in the destination struct or array.
 *
 * @param sourceOffset
 *   Start offset int the source struct or array.
 *
 * @param length
 *   Number of bytes which shall be copied.
 *
 */
void memcpy_off(byte * dest, dword destOffset, void * source, dword sourceOffset, dword length);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to destination, giving a destination start offset. (Form 4)
 *
 * Copies bytes from a source to destination, giving a destination start offset. The size of the
 * destination must be at least destOffset + length.
 *
 * @param dest
 *   (form 1, 2): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param source
 *   (form 3, 4): Struct from which the bytes shall be copied.
 *   (other forms): Array from which the bytes shall be copied.
 *
 * @param destOffset
 *   Start offset in the destination struct or array.
 *
 * @param sourceOffset
 *   Start offset int the source struct or array.
 *
 * @param length
 *   Number of bytes which shall be copied.
 *
 */
void memcpy_off(char * dest, dword destOffset, void * source, dword sourceOffset, dword length);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to destination, giving a destination start offset. (Form 5)
 *
 * Copies bytes from a source to destination, giving a destination start offset. The size of the
 * destination must be at least destOffset + length.
 *
 * @param dest
 *   (form 1, 2): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param source
 *   (form 3, 4): Struct from which the bytes shall be copied.
 *   (other forms): Array from which the bytes shall be copied.
 *
 * @param destOffset
 *   Start offset in the destination struct or array.
 *
 * @param sourceOffset
 *   Start offset int the source struct or array.
 *
 * @param length
 *   Number of bytes which shall be copied.
 *
 */
void memcpy_off(byte * dest, dword destOffset, byte * source, dword sourceOffset, dword length);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to destination, giving a destination start offset. (Form 6)
 *
 * Copies bytes from a source to destination, giving a destination start offset. The size of the
 * destination must be at least destOffset + length.
 *
 * @param dest
 *   (form 1, 2): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param source
 *   (form 3, 4): Struct from which the bytes shall be copied.
 *   (other forms): Array from which the bytes shall be copied.
 *
 * @param destOffset
 *   Start offset in the destination struct or array.
 *
 * @param sourceOffset
 *   Start offset int the source struct or array.
 *
 * @param length
 *   Number of bytes which shall be copied.
 *
 */
void memcpy_off(char * dest, dword destOffset, byte * source, dword sourceOffset, dword length);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to destination, giving a destination start offset. (Form 7)
 *
 * Copies bytes from a source to destination, giving a destination start offset. The size of the
 * destination must be at least destOffset + length.
 *
 * @param dest
 *   (form 1, 2): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param source
 *   (form 3, 4): Struct from which the bytes shall be copied.
 *   (other forms): Array from which the bytes shall be copied.
 *
 * @param destOffset
 *   Start offset in the destination struct or array.
 *
 * @param sourceOffset
 *   Start offset int the source struct or array.
 *
 * @param length
 *   Number of bytes which shall be copied.
 *
 */
void memcpy_off(byte * dest, dword destOffset, char * source, dword sourceOffset, dword length);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to destination, giving a destination start offset. (Form 8)
 *
 * Copies bytes from a source to destination, giving a destination start offset. The size of the
 * destination must be at least destOffset + length.
 *
 * @param dest
 *   (form 1, 2): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param source
 *   (form 3, 4): Struct from which the bytes shall be copied.
 *   (other forms): Array from which the bytes shall be copied.
 *
 * @param destOffset
 *   Start offset in the destination struct or array.
 *
 * @param sourceOffset
 *   Start offset int the source struct or array.
 *
 * @param length
 *   Number of bytes which shall be copied.
 *
 */
void memcpy_off(char * dest, dword destOffset, char * source, dword sourceOffset, dword length);

}
