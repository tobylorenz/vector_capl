#include "memcpy.h"

#include <iostream>

namespace capl
{

void memcpy(byte * dest, void * source)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy(byte * dest, int offset, void * source)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy(void * dest, byte * source)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy(void * dest, byte * source, int offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy(void * dest, void * source)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy(void * dest, char * source)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy(void * dest, char * source, dword offset)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy(byte * dest, byte * source, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy(char * dest, byte * source, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy(byte * dest, char * source, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void memcpy(char * dest, char * source, dword length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
