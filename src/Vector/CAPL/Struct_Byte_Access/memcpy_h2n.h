#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies the bytes from the struct into the array. (Form 1)
 *
 * Copies the bytes from the struct into the array, and translates the byte order of the
 * elements from little-endian to big-endian (h2n stands for "host to network").
 *
 * @param source
 *   Struct whose bytes shall be copied
 *
 * @param dest
 *   Array into which the bytes shall be copied
 */
void memcpy_h2n(byte * dest, void * source);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies the bytes from the struct into the array. (Form 2)
 *
 * Copies the bytes from the struct into the array, and translates the byte order of the
 * elements from little-endian to big-endian (h2n stands for "host to network").
 *
 * @param source
 *   Struct whose bytes shall be copied
 *
 * @param dest
 *   Array into which the bytes shall be copied
 *
 * @param offset
 *   Offset into the array
 */
void memcpy_h2n(byte * dest, int offset, void * source);

}
