#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to a destination. (Form 1)
 *
 * In form 5, both structs must have the same
 * type. In other forms with structs, the arrays must be large enough to contain the struct
 * data.
 *
 * @param source
 *   (form 1, 2, 5): Struct whose bytes shall be copied.
 *   (other forms): Array whose bytes shall be copied.
 *
 * @param dest
 *   (form 3, 4, 5, 6, 7): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 */
void memcpy(byte * dest, void * source);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to a destination. (Form 2)
 *
 * In form 5, both structs must have the same
 * type. In other forms with structs, the arrays must be large enough to contain the struct
 * data.
 *
 * @param source
 *   (form 1, 2, 5): Struct whose bytes shall be copied.
 *   (other forms): Array whose bytes shall be copied.
 *
 * @param dest
 *   (form 3, 4, 5, 6, 7): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param offset
 *   (form 2, 4, 7): Offset in the array which marks the start of the data.
 */
void memcpy(byte * dest, int offset, void * source);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to a destination. (Form 3)
 *
 * In form 5, both structs must have the same
 * type. In other forms with structs, the arrays must be large enough to contain the struct
 * data.
 *
 * @param source
 *   (form 1, 2, 5): Struct whose bytes shall be copied.
 *   (other forms): Array whose bytes shall be copied.
 *
 * @param dest
 *   (form 3, 4, 5, 6, 7): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 */
void memcpy(void * dest, byte * source);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to a destination. (Form 4)
 *
 * In form 5, both structs must have the same
 * type. In other forms with structs, the arrays must be large enough to contain the struct
 * data.
 *
 * @param source
 *   (form 1, 2, 5): Struct whose bytes shall be copied.
 *   (other forms): Array whose bytes shall be copied.
 *
 * @param dest
 *   (form 3, 4, 5, 6, 7): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param offset
 *   (form 2, 4, 7): Offset in the array which marks the start of the data.
 */
void memcpy(void * dest, byte * source, int offset);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to a destination. (Form 5)
 *
 * In form 5, both structs must have the same
 * type. In other forms with structs, the arrays must be large enough to contain the struct
 * data.
 *
 * @param source
 *   (form 1, 2, 5): Struct whose bytes shall be copied.
 *   (other forms): Array whose bytes shall be copied.
 *
 * @param dest
 *   (form 3, 4, 5, 6, 7): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 */
void memcpy(void * dest, void * source);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to a destination. (Form 6)
 *
 * In form 5, both structs must have the same
 * type. In other forms with structs, the arrays must be large enough to contain the struct
 * data.
 *
 * @param source
 *   (form 1, 2, 5): Struct whose bytes shall be copied.
 *   (other forms): Array whose bytes shall be copied.
 *
 * @param dest
 *   (form 3, 4, 5, 6, 7): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 */
void memcpy(void * dest, char * source);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to a destination. (Form 7)
 *
 * In form 5, both structs must have the same
 * type. In other forms with structs, the arrays must be large enough to contain the struct
 * data.
 *
 * @param source
 *   (form 1, 2, 5): Struct whose bytes shall be copied.
 *   (other forms): Array whose bytes shall be copied.
 *
 * @param dest
 *   (form 3, 4, 5, 6, 7): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param offset
 *   (form 2, 4, 7): Offset in the array which marks the start of the data.
 */
void memcpy(void * dest, char * source, dword offset);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to a destination. (Form 8)
 *
 * In form 5, both structs must have the same
 * type. In other forms with structs, the arrays must be large enough to contain the struct
 * data.
 *
 * @param source
 *   (form 1, 2, 5): Struct whose bytes shall be copied.
 *   (other forms): Array whose bytes shall be copied.
 *
 * @param dest
 *   (form 3, 4, 5, 6, 7): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param length
 *   (form 8, 9, 10, 11): number of bytes which shall be copied.
 */
void memcpy(byte * dest, byte * source, dword length);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to a destination. (Form 9)
 *
 * In form 5, both structs must have the same
 * type. In other forms with structs, the arrays must be large enough to contain the struct
 * data.
 *
 * @param source
 *   (form 1, 2, 5): Struct whose bytes shall be copied.
 *   (other forms): Array whose bytes shall be copied.
 *
 * @param dest
 *   (form 3, 4, 5, 6, 7): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param length
 *   (form 8, 9, 10, 11): number of bytes which shall be copied.
 */
void memcpy(char * dest, byte * source, dword length);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to a destination. (Form 10)
 *
 * In form 5, both structs must have the same
 * type. In other forms with structs, the arrays must be large enough to contain the struct
 * data.
 *
 * @param source
 *   (form 1, 2, 5): Struct whose bytes shall be copied.
 *   (other forms): Array whose bytes shall be copied.
 *
 * @param dest
 *   (form 3, 4, 5, 6, 7): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param length
 *   (form 8, 9, 10, 11): number of bytes which shall be copied.
 */
void memcpy(byte * dest, char * source, dword length);

/**
 * @ingroup Struct_Byte_Access
 *
 * @brief
 *   Copies bytes from a source to a destination. (Form 11)
 *
 * In form 5, both structs must have the same
 * type. In other forms with structs, the arrays must be large enough to contain the struct
 * data.
 *
 * @param source
 *   (form 1, 2, 5): Struct whose bytes shall be copied.
 *   (other forms): Array whose bytes shall be copied.
 *
 * @param dest
 *   (form 3, 4, 5, 6, 7): Struct into which the bytes shall be copied.
 *   (other forms): Array into which the bytes shall be copied.
 *
 * @param length
 *   (form 8, 9, 10, 11): number of bytes which shall be copied.
 */
void memcpy(char * dest, char * source, dword length);

}
