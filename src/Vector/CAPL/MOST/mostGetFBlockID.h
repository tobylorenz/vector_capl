#pragma once

#include "../DataTypes.h"
#include "MOST.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Returns the FBlockID of the message.
 *
 * @deprecated
 *   Please use the mostMessage selector FBlockID instead.
 *
 * These functions return the FBlockID of the message.
 *
 * @param msg
 *   Message variable of type mostMessage
 *
 * @return
 *   FblockId
 */
long mostGetFBlockID(mostMessage msg);

/**
 * @ingroup MOST
 *
 * @brief
 *   Returns the FBlockID of the message.
 *
 * @deprecated
 *   Please use the mostAMSMessage selector FBlockID instead.
 *
 * These functions return the FBlockID of the message.
 *
 * @param msg
 *   Message variable of type mostAMSMessage
 *
 * @return
 *   FblockId
 */
long mostGetFBlockID(mostAMSMessage msg);

}
