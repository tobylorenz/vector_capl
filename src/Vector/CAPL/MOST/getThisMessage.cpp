#include "getThisMessage.h"

#include <iostream>

namespace capl
{

void GetThisMessage(mostAMSMessage msg, long count)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
