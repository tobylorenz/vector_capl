#include "mostSetOpType.h"

#include <iostream>

namespace capl
{

void mostSetOpType(mostMessage msg, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

void mostSetOpType(mostAMSMessage msg, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
