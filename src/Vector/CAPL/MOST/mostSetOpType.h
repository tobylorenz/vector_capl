#pragma once

#include "../DataTypes.h"
#include "MOST.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Sets the OpType of the message.
 *
 * @deprecated
 *   The following functions are obsolete!
 *   Please use the mostMessage selector OpType instead.
 *
 * Set the OpType of the message.
 *
 * @param msg
 *   Variable of type mostMessage.
 *
 * @param value
 *   Value to be set for OpType.
 */
void mostSetOpType(mostMessage msg, long value);

/**
 * @ingroup MOST
 *
 * @brief
 *   Sets the OpType of the message.
 *
 * @deprecated
 *   The following functions are obsolete!
 *   Please use the mostAMSMessage selector OpType instead.
 *
 * Set the OpType of the message.
 *
 * @param msg
 *   Variable of type mostAMSMessage.
 *
 * @param value
 *   Value to be set for OpType.
 */
void mostSetOpType(mostAMSMessage msg, long value);

}
