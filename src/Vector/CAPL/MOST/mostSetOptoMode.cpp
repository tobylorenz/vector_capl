#include "mostSetOptoMode.h"

#include <iostream>

namespace capl
{

long mostSetOptoMode(long channel, long optoMode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
