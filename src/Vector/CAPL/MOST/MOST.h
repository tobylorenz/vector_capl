#pragma once

/** @todo to be implemented */

/**
 * @defgroup MOST MOST CAPL Functions
 */

/* Selectors */
#include "mostMessage.h"
#include "mostAMSMessage.h"
#include "mostRawMessage.h"
#include "mostLightLockError.h"

/* Event Procedures */

/* MOST50 */
#include "output.h"
#include "mostAmsOutput.h"
#include "outputMostPkt.h"
#include "mostAllocTableGetCL.h"
#include "mostAllocTableGetSize.h"
#include "mostAllocTableGetWidth.h"
#include "mostAmsSetSizePrefixed.h"
#include "mostGetSpecVersion.h"
#include "mostGetSpeedGrade.h"
#include "mostSetSyncAudio.h"
#include "mostSetSyncSpdif.h"
#include "mostShutDown.h"

/* MOST150 */
#include "output.h"
#include "mostAmsOutput.h"
#include "outputMostPkt.h"
#include "outputMostEthPkt.h"
#include "outputMostEthPktThis.h"
#include "mostAllocTableGetCL.h"
#include "mostAllocTableGetSize.h"
#include "mostAllocTableGetWidth.h"
#include "mostAmsSetSizePrefixed.h"
#include "mostConfigureEclSequence.h"
#include "mostEthPktSetTraceColors.h"
#include "mostGenerateBypassToggle.h"
#include "mostGenerateEclSequence.h"
#include "mostGetShutdownReason.h"
#include "mostGetShutdownFlag.h"
#include "mostGetSpecVersion.h"
#include "mostGetSpeedGrade.h"
#include "mostGetSystemLock.h"
#include "mostSetEclGlitchFilter.h"
#include "mostSetMacAdr.h"
#include "mostGetMacAdr.h"
#include "mostSetMasterMode.h"
#include "mostGetMasterMode.h"
#include "mostSetRxBufferAsync.h"
#include "mostSetStressNodeParameter.h"
#include "mostGetStressNodeParameter.h"
#include "mostSetShutdownReason.h"
#include "mostSetStressNodeParameterData.h"
#include "mostSetSyncAudio.h"
#include "mostSetSyncSpdif.h"
#include "mostSetSystemLockFlagUsage.h"
#include "mostGetSystemLockFlagUsage.h"
#include "mostShutDown.h"
#include "mostSetShutDownFlagUsage.h"
#include "mostGetShutDownFlagUsage.h"

/* Evaluation of Event Procedures */
#include "mostEventChannel.h"
#include "mostEventTime.h"
#include "mostEventOrigTime.h"
#include "mostPktMsgChannel.h"
#include "mostPktMsgTime.h"
#include "mostPktOrigTime.h"
#include "mostPktSrcAdr.h"
#include "mostPktDestAdr.h"
#include "mostPktDir.h"
#include "mostPktArbitration.h"
#include "mostPktDlc.h"
#include "mostPktGetData.h"
#include "mostPktGetSelData.h"
#include "mostPktTelID.h"
#include "mostPktTelLen.h"
#include "mostPktIsSpy.h"
#include "mostRegChip.h"
#include "mostRegOffset.h"
#include "mostRegDataLen.h"
#include "mostRegGetData.h"
#include "mostRegGetByteAbs.h"
#include "mostRegGetWordAbs.h"

/* Hardware API */
#include "mostConfigureEclSequence.h"
#include "mostGenerateEclSequence.h"
#include "mostGetEcl.h"
#include "mostSetEcl.h"
#include "mostSetEclGlitchFilter.h"
#include "mostGetEclTermination.h"
#include "mostSetEclTermination.h"
#include "mostSetNodeAdr.h"
#include "mostSetGroupAdr.h"
#include "mostSetAltPktAdr.h"
#include "mostSetOwnAdr.h"
#include "mostSetMacAdr.h"
#include "mostGetMacAdr.h"
#include "mostGetNodeAdr.h"
#include "mostGetGroupAdr.h"
#include "mostGetAltPktAdr.h"
#include "mostGetNodePosAdr.h"
#include "mostGetLockEx.h"
#include "mostGetSBC.h"
#include "mostSetSBC.h"
#include "mostGetMPR.h"
#include "mostGetNPR.h"
#include "mostWakeup.h"
#include "mostGetRxLight.h"
#include "mostSetTxLight.h"
#include "mostGetTxLight.h"
#include "mostSetShutdownReason.h"
#include "mostGetShutdownReason.h"
#include "mostGetLock.h"
#include "mostGetAllocTable.h"
#include "mostGetCodingErrors.h"
#include "mostGetNceType.h"
#include "mostSetCorrectStartupSBC.h"
#include "mostShutDown.h"
#include "mostReadReg.h"
#include "mostWriteReg.h"
#include "mostSyncAlloc.h"
#include "mostSyncDealloc.h"
#include "mostSetAllBypass.h"
#include "mostGetAllBypass.h"
#include "mostSetMasterMode.h"
#include "mostGetMasterMode.h"
#include "mostSetTimingMode.h"
#include "mostGetTimingMode.h"
#include "mostSetSpyAsync.h"
#include "mostSetSpyCtrl.h"
#include "mostSetSpyEthPkt.h"
#include "mostGetSpyAsync.h"
#include "mostGetSpyCtrl.h"
#include "mostGetSpyEthPkt.h"
#include "mostConfigureBusloadAsync.h"
#include "mostGenerateBusloadAsync.h"
#include "mostConfigureBusloadCtrl.h"
#include "mostGenerateBusloadCtrl.h"
#include "mostConfigureBusloadEthPkt.h"
#include "mostGenerateBusloadEthPkt.h"
#include "mostGenerateBypassToggle.h"
#include "mostSetRxBufferAsync.h"
#include "mostSetShutDownFlagUsage.h"
#include "mostGetShutDownFlagUsage.h"
#include "mostSetStressNodeParameter.h"
#include "mostGetStressNodeParameter.h"
#include "mostSetSystemLockFlagUsage.h"
#include "mostGetSystemLockFlagUsage.h"
#include "mostSetTxLightPower.h"
#include "mostSetRxBufferCtrl.h"
#include "mostGenerateLightError.h"
#include "mostGenerateLockError.h"
#include "mostSetRetryParameter.h"
#include "mostGetRetryParameter.h"
#include "mostSetSyncAudio.h"
#include "mostSetSyncMute.h"
#include "mostSetSyncVolume.h"
#include "mostGetSyncMute.h"
#include "mostGetSyncVolume.h"
#include "mostSetSyncSpdif.h"
#include "mostSetSyncSpdifLock.h"
#include "mostSetSyncSpdifMode.h"
#include "mostGetSyncSpdifMode.h"
#include "mostSetClockSource.h"
#include "mostGetClockSource.h"
#include "mostSetHWFilter.h"
#include "mostGetHWFilter.h"
#include "mostSetAndFilter.h"
#include "mostGetAndFilter.h"
#include "mostSetXorFilter.h"
#include "mostGetXorFilter.h"
#include "mostGetHWCapability.h"
#include "mostGetHWInfo.h"
#include "mostTwinklePowerLed.h"

/* Application Socket API */
#include "mostApRegister.h"
#include "mostApRegisterEx.h"
#include "mostApUnregister.h"
#include "mostApUnregisterEx.h"
#include "mostApIsAddressed.h"
#include "mostApIsRegistered.h"
#include "mostApIsRegisteredEx.h"
#include "mostApGetFBlockID.h"
#include "mostApGetInstID.h"
#include "mostAsRgGetSize.h"
#include "mostAsRgGetRxTxLog.h"
#include "mostAsRgGetFBlockID.h"
#include "mostAsRgGetInstID.h"
#include "mostGetNetState.h"
#include "mostPMResetOverTemperature.h"
#include "mostPMSetOverTemperature.h"
#include "mostPMShutDownStart.h"
#include "mostPMShutDownCancel.h"
#include "mostPMTempShutdownWakeupTimeout.h"
#include "mostAsNtfDeviceIDListGetSize.h"
#include "mostAsNtfDeviceIDListGetDeviceID.h"
#include "mostAsNtfEnable.h"
#include "mostAsNtfDisable.h"
#include "mostAsNtfEnableEx.h"
#include "mostAsNtfDisableEx.h"
#include "mostAsNtfFunctionCheck.h"
#include "mostAsNtfFunctionClear.h"
#include "mostAsNtfFunctionClearAll.h"
#include "mostAsNtfFunctionEnable.h"
#include "mostAsNtfFunctionDisable.h"
#include "mostAsNtfFunctionIsEnabled.h"
#include "mostAsNtfFunctionEnableEx.h"
#include "mostAsNtfFunctionDisableEx.h"
#include "mostAsNtfFunctionIsEnabledEx.h"
#include "mostAsNtfFunctionListGetSize.h"
#include "mostAsNtfFunctionListGetFunction.h"
#include "mostAsNtfFunctionSet.h"
#include "mostAsNtfOutput.h"
#include "mostAsShdEnable.h"
#include "mostAsShdDisable.h"
#include "mostAsNtfShdFunctionEnable.h"
#include "mostAsNtfShdFunctionDisable.h"
#include "mostAsFsEnable.h"
#include "mostAsFsDisable.h"
#include "mostAsFsEnableEx.h"
#include "mostAsFsDisableEx.h"
#include "mostAsFsFunctionEnable.h"
#include "mostAsFsFunctionEnableEx.h"
#include "mostNBSetAbilityToWake.h"
#include "mostNwmFunctionEnable.h"

/* Test Functions */
#include "TestGetWaitEventMostAMSMsgData.h"
#include "TestGetWaitEventMostPkt.h"
#include "TestGetWaitEventMostRawMsgData.h"
#include "TestGetWaitEventMostMsgData.h"
#include "TestJoinMostAMSMessageEvent.h"
#include "TestJoinMostAMSReportEvent.h"
#include "TestJoinMostAMSSpyMessageEvent.h"
#include "TestJoinMostAMSSpyReportEvent.h"
#include "TestJoinMostMessageEvent.h"
#include "TestJoinMostPktEvent.h"
#include "TestJoinMostReportEvent.h"
#include "TestJoinMostSpyMessageEvent.h"
#include "TestJoinMostSpyPktEvent.h"
#include "TestJoinMostSpyReportEvent.h"
#include "TestSendMostAMSMessage.h"
#include "TestSendMostRawMessage.h"
#include "TestWaitForMostAMSMessage.h"
#include "TestWaitForMostAMSReport.h"
#include "TestWaitForMostAMSResponse.h"
#include "TestWaitForMostAMSResult.h"
#include "TestWaitForMostAMSSpyMessage.h"
#include "TestWaitForMostAMSSpyReport.h"
#include "TestWaitForMostMessage.h"
#include "TestWaitForMostPkt.h"
#include "TestWaitForMostRawSpyMessage.h"
#include "TestWaitForMostReport.h"
#include "TestWaitForMostSpyMessage.h"
#include "TestWaitForMostSpyPkt.h"
#include "TestWaitForMostSpyReport.h"
#include "TestWaitForMostAllBypass.h"
#include "TestWaitForMostCriticalUnlock.h"
#include "TestWaitForMostGroupAdr.h"
#include "TestWaitForMostLightOff.h"
#include "TestWaitForMostMPR.h"
#include "TestWaitForMostNetState.h"
#include "TestWaitForMostNodeAdr.h"
#include "TestWaitForMostNPR.h"
#include "TestWaitForMostSBC.h"
#include "TestWaitForMostShortUnlock.h"
#include "TestWaitForMostStableLock.h"
#include "TestMostReadReg.h"
#include "TestMostRegGetData.h"
#include "TestMostWriteReg.h"

/* Transmission and Receipt Settings of Nodes */
#include "mostApplicationNode.h"
#include "mostGetChannel.h"
#include "mostRcvSpyMessagesOnly.h"
#include "mostStrictChannelMapping.h"

/* Message Access */
#include "mostPrepareReport.h"
#include "getThisMessage.h"

/* Message Transmission */
#include "output.h"
#include "outputMostPkt.h"
#include "outputMostPktThis.h"
#include "mostAmsOutput.h"
#include "mostAmsClearTXQueue.h"
#include "mostSendError.h"

/* Function Catalogue */
#include "mostParamGet.h"
#include "mostParamGetData.h"
#include "mostParamGetString.h"
#include "mostParamGetStringAscii.h"
#include "mostParamIsAvailable.h"
#include "mostParamSet.h"
#include "mostParamSetData.h"
#include "mostParamSetString.h"
#include "mostParamSetStringEnc.h"
#include "mostStringToAscii.h"
#include "mostMsgDecodeRLE.h"
#include "mostMsgEncodeRLE.h"
#include "mostMsgGetSymbols.h"
#include "mostMsgSet.h"
#include "mostAmsOutput.h"

/* Fault Injection */
#include "mostAsFiEnable.h"
#include "mostFiAmsReceive.h"
#include "mostNwmFiEnableRingScan.h"
#include "mostNwmFiSetConfigState.h"
#include "mostNwmFiSetParameter.h"
#include "mostNwmFiGetParameter.h"

/* Trace Highlighting */
#include "mostEthPktSetTraceColors.h"
#include "mostMHPBlockSetTraceColors.h"
#include "mostMHPConnectionSetTraceColors.h"
#include "mostMHPPacketSetTraceColors.h"
#include "mostPktSetTraceColors.h"
#include "traceSetEventColors.h"

/* Obsolete Functions */
#include "mostGetLight.h"
#include "mostSetLight.h"
#include "mostGetOptoMode.h"
#include "mostSetOptoMode.h"
#include "mostGetFBlockID.h"
#include "mostGetFunctionID.h"
#include "mostGetOpType.h"
#include "mostSetFBlockID.h"
#include "mostSetFunctionID.h"
#include "mostSetOpType.h"
