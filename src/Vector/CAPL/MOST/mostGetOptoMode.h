#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Returns the operation mode of the Optolyzer connected to the given channel.
 *
 * @deprecated
 *   No replacement
 *
 * Returns the operation mode of the Optolyzer connected to the given channel.
 *
 * @param channel
 *   Channel of the connected interface
 *
 * @return
 *   - <0: See error codes
 */
long mostGetOptoMode(long channel);

}
