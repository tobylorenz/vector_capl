#pragma once

#include "../DataTypes.h"
#include "MOST.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Sets the FunctionID of the message.
 *
 * @deprecated
 *   The following functions are obsolete!
 *   Please use the mostMessage selector FunctionID instead.
 *
 * Set the FunctionID of the message.
 *
 * @param msg
 *   Variable of type mostMessage.
 *
 * @param value
 *   Value to be set for FunctionID.
 */
void mostSetFunctionID(mostMessage msg, long value);

/**
 * @ingroup MOST
 *
 * @brief
 *   Sets the FunctionID of the message.
 *
 * @deprecated
 *   The following functions are obsolete!
 *   Please use the mostAMSMessage selector FunctionID instead.
 *
 * Set the FunctionID of the message.
 *
 * @param msg
 *   Variable of type mostAMSMessage.
 *
 * @param value
 *   Value to be set for FunctionID.
 */
void mostSetFunctionID(mostAMSMessage msg, long value);

}
