#pragma once

#include "../DataTypes.h"
#include "MOST.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Copies the data of an AMS message into a message variable.
 *
 * Copies the data of a AMS message into the msg variable.
 *
 * This function must be used exclusively within a an event procedure on mostAMSMessage.
 *
 * @param msg
 *   Variable of the type mostAmsMessage.
 *
 * @param count
 *   Number of use data bytes that have to be copied.
 */
void GetThisMessage(mostAMSMessage msg, long count);

}
