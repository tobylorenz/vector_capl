#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup MOST
 *
 * @brief
 *   Returns the current lock status (unlock, lock, stable lock, critical unlock).
 *
 * The function returns the lock status of the connected MOST hardware. In contrast to mostGetLock(), the additional states "Stable Lock" and "Critical Unlock" are considered according to MOST specification 2.3.
 *
 * @param channel
 *   Channel of the connected MOST hardware.
 *
 * @return
 *   - 0: Unlock
 *   - 1: Lock
 *   - 2: Stable Lock
 *   - 3: Critical Unlock
 *   - <0: See error codes
 */
long mostGetLockEx(long channel);

}
