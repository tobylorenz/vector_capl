#include "mostGetLockEx.h"

#include <iostream>

namespace capl
{

long mostGetLockEx(long channel)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
