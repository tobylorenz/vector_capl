#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Delivers the qualifier path of the object that must be specified upon generation.
 *
 * Delivers the qualifier path of the object that must be specified upon generation.
 *
 * @brief obj
 *   Diagnostics object
 *
 * @brief buffer
 *   Input/output buffer
 *
 * @brief buffersize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagGetObjectPath(DiagResponse obj, char * buffer, dword buffersize);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Delivers the qualifier path of the object that must be specified upon generation.
 *
 * Delivers the qualifier path of the object that must be specified upon generation.
 *
 * @brief obj
 *   Diagnostics object
 *
 * @brief buffer
 *   Input/output buffer
 *
 * @brief buffersize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagGetObjectPath(DiagRequest obj, char * buffer, dword buffersize);

}
