#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Checks if the given object matches its specification in the CDD.
 *
 * Returns 1 if the response received for the request matches its specification in the diagnostic description.
 * If a primitive qualifier is given, the definition of that primitive at the object's service is used as specification.
 *
 * If the primitive is not valid, 0 is returned and the optional parameter reasonOut[0] is set to the precise reason why the check failed (see below).
 *
 * @param obj
 *   Diagnostics object to check.
 *
 * @return
 *   - 1: Primitive matches the specification in the diagnostic description.
 *   - 0: Primitive does not match its specification. In this case, reasonOut[0] will indicate the problem:
 *     - 1 data too long
 *     - 2 data too short
 *     - 3 constant overwritten
 *     - 4 unknown format error
 *     - 5 illegal parameter value found
 *   - <0: Error code
 */
long DiagCheckValidPrimitive(DiagRequest obj);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Checks if the given object matches its specification in the CDD.
 *
 * Returns 1 if the response received for the request matches its specification in the diagnostic description.
 * If a primitive qualifier is given, the definition of that primitive at the object's service is used as specification.
 *
 * If the primitive is not valid, 0 is returned and the optional parameter reasonOut[0] is set to the precise reason why the check failed (see below).
 *
 * @param obj
 *   Diagnostics object to check.
 *
 * @param reasonOut
 *   Optional output parameter field for fail reason.
 *
 * @return
 *   - 1: Primitive matches the specification in the diagnostic description.
 *   - 0: Primitive does not match its specification. In this case, reasonOut[0] will indicate the problem:
 *     - 1 data too long
 *     - 2 data too short
 *     - 3 constant overwritten
 *     - 4 unknown format error
 *     - 5 illegal parameter value found
 *   - <0: Error code
 */
long DiagCheckValidPrimitive(DiagRequest obj, dword * reasonOut);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Checks if the given object matches its specification in the CDD.
 *
 * Returns 1 if the response received for the request matches its specification in the diagnostic description.
 * If a primitive qualifier is given, the definition of that primitive at the object's service is used as specification.
 *
 * If the primitive is not valid, 0 is returned and the optional parameter reasonOut[0] is set to the precise reason why the check failed (see below).
 *
 * @param obj
 *   Diagnostics object to check.
 *
 * @return
 *   - 1: Primitive matches the specification in the diagnostic description.
 *   - 0: Primitive does not match its specification. In this case, reasonOut[0] will indicate the problem:
 *     - 1 data too long
 *     - 2 data too short
 *     - 3 constant overwritten
 *     - 4 unknown format error
 *     - 5 illegal parameter value found
 *   - <0: Error code
 */
long DiagCheckValidPrimitive(DiagResponse obj);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Checks if the given object matches its specification in the CDD.
 *
 * Returns 1 if the response received for the request matches its specification in the diagnostic description.
 * If a primitive qualifier is given, the definition of that primitive at the object's service is used as specification.
 *
 * If the primitive is not valid, 0 is returned and the optional parameter reasonOut[0] is set to the precise reason why the check failed (see below).
 *
 * @param obj
 *   Diagnostics object to check.
 *
 * @param reasonOut
 *   Optional output parameter field for fail reason.
 *
 * @return
 *   - 1: Primitive matches the specification in the diagnostic description.
 *   - 0: Primitive does not match its specification. In this case, reasonOut[0] will indicate the problem:
 *     - 1 data too long
 *     - 2 data too short
 *     - 3 constant overwritten
 *     - 4 unknown format error
 *     - 5 illegal parameter value found
 *   - <0: Error code
 */
long DiagCheckValidPrimitive(DiagResponse obj, dword reasonOut[]);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Checks if the given object matches its specification in the CDD.
 *
 * Returns 1 if the response received for the request matches its specification in the diagnostic description.
 * If a primitive qualifier is given, the definition of that primitive at the object's service is used as specification.
 *
 * If the primitive is not valid, 0 is returned and the optional parameter reasonOut[0] is set to the precise reason why the check failed (see below).
 *
 * @param obj
 *   Diagnostics object to check.
 *
 * @param primitiveQualifier
 *   Qualifier of the service primitive that should be used as specification.
 *
 * @return
 *   - 1: Primitive matches the specification in the diagnostic description.
 *   - 0: Primitive does not match its specification. In this case, reasonOut[0] will indicate the problem:
 *     - 1 data too long
 *     - 2 data too short
 *     - 3 constant overwritten
 *     - 4 unknown format error
 *     - 5 illegal parameter value found
 *   - <0: Error code
 */
long DiagCheckValidPrimitive(DiagResponse obj, char * primitiveQualifier);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Checks if the given object matches its specification in the CDD.
 *
 * Returns 1 if the response received for the request matches its specification in the diagnostic description.
 * If a primitive qualifier is given, the definition of that primitive at the object's service is used as specification.
 *
 * If the primitive is not valid, 0 is returned and the optional parameter reasonOut[0] is set to the precise reason why the check failed (see below).
 *
 * @param obj
 *   Diagnostics object to check.
 *
 * @param reasonOut
 *   Optional output parameter field for fail reason.
 *
 * @param primitiveQualifier
 *   Qualifier of the service primitive that should be used as specification.
 *
 * @return
 *   - 1: Primitive matches the specification in the diagnostic description.
 *   - 0: Primitive does not match its specification. In this case, reasonOut[0] will indicate the problem:
 *     - 1 data too long
 *     - 2 data too short
 *     - 3 constant overwritten
 *     - 4 unknown format error
 *     - 5 illegal parameter value found
 *   - <0: Error code
 */
long DiagCheckValidPrimitive(DiagResponse obj, char * primitiveQualifier, dword * reasonOut);

}
