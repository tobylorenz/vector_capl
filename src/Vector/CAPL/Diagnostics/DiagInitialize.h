#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reinitializes the object for the given service or primitive.
 *
 * Reinitializes the object for the given service or primitive.
 * Diagnostics request will be initialized with the default request parameters of the service, while diagnostic responses will be initialized with the default parameters of the first or specified primitive of the service.
 * If the service is not defined, or the primitive is not defined at the given service, nothing happens and an error code is returned.
 *
 * @param object
 *   Diagnostics object to re-initialize
 *
 * @param serviceQualifier
 *   Qualifier of the service that should be used for reinterpretation
 *
 * @return
 *   - 0: No error, OK
 *   - <0: Error code
 */
long DiagInitialize(DiagResponse object, char * serviceQualifier);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reinitializes the object for the given service or primitive.
 *
 * Reinitializes the object for the given service or primitive.
 * Diagnostics request will be initialized with the default request parameters of the service, while diagnostic responses will be initialized with the default parameters of the first or specified primitive of the service.
 * If the service is not defined, or the primitive is not defined at the given service, nothing happens and an error code is returned.
 *
 * @param object
 *   Diagnostics object to re-initialize
 *
 * @param serviceQualifier
 *   Qualifier of the service that should be used for reinterpretation
 *
 * @param primitiveQualifier
 *   Qualifier of the service primitive that should be used for reinterpretation
 *
 * @return
 *   - 0: No error, OK
 *   - <0: Error code
 */
long DiagInitialize(DiagResponse object, char * serviceQualifier, char * primitiveQualifier);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reinitializes the object for the given service or primitive.
 *
 * Reinitializes the object for the given service or primitive.
 * Diagnostics request will be initialized with the default request parameters of the service, while diagnostic responses will be initialized with the default parameters of the first or specified primitive of the service.
 * If the service is not defined, or the primitive is not defined at the given service, nothing happens and an error code is returned.
 *
 * @param object
 *   Diagnostics object to re-initialize
 *
 * @param serviceQualifier
 *   Qualifier of the service that should be used for reinterpretation
 *
 * @return
 *   - 0: No error, OK
 *   - <0: Error code
 */
long DiagInitialize(DiagRequest object, char * serviceQualifier);

}
