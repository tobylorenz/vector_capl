/**
 * This class represents a diagnostic request.
 */

#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @todo
 */
class DiagRequestSent
{
public:
};

}
