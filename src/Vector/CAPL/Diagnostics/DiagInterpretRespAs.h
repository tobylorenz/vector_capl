#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Treats the data of the request's response as the specified primitive.
 *
 * Treats the data of the request's response as the specified primitive.
 * This will cause a reinterpretation of the data, but the data will not be changed.
 *
 * @param response
 *   Request that has to contain a response object received by waiting for a response.
 *
 * @param primitiveQualifier
 *   Qualifier of the service primitive that should be used for reinterpretation.
 *
 * @return
 *   - 0: No error, OK
 *   - <0: Error code
 */
long DiagInterpretRespAs(DiagRequest response, char * primitiveQualifier);

}
