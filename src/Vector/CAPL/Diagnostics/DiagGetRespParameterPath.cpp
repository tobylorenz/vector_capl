#include "DiagGetRespParameterPath.h"

#include <iostream>

namespace capl
{

long DiagGetRespParameterPath(DiagRequest object, dword paramNo, char * buffer, dword bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
