#include "DiagIsPositiveResponse.h"

#include <iostream>

namespace capl
{

long DiagIsPositiveResponse(DiagResponse obj)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
