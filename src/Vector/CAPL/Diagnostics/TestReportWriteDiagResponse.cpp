#include "TestReportWriteDiagResponse.h"

#include <iostream>

namespace capl
{

long TestReportWriteDiagResponse(DiagRequest req)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
