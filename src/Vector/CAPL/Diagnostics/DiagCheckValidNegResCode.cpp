#include "DiagCheckValidNegResCode.h"

#include <iostream>

namespace capl
{

long DiagCheckValidNegResCode(DiagRequest obj, dword negResCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagCheckValidNegResCode(DiagResponse obj, dword negResCode)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagCheckValidNegResCode(DiagResponse obj)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
