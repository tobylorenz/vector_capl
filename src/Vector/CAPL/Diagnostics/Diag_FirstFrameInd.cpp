#include "Diag_FirstFrameInd.h"

#include <iostream>

namespace capl
{

void Diag_FirstFrameInd(long source, long target, long length)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
