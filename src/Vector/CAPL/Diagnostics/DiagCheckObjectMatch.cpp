#include "DiagCheckObjectMatch.h"

#include <iostream>

namespace capl
{

long DiagCheckObjectMatch(DiagRequest request, DiagResponse response)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
