#include "DiagIsRaw.h"

#include <iostream>

namespace capl
{

long DiagIsRaw(DiagRequest request)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagIsRaw(DiagResponse response)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
