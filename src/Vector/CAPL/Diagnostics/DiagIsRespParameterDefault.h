#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Access/check a response parameter for a given request.
 *   Returns != 0 if the parameter in the response has its default value.
 *
 * @todo
 */

}
