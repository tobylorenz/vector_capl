#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Checks if a channel is already in state Connected.
 *
 * Checks if a channel is already in state Connected.
 *
 * The channel the function refers to is set with the last call of DiagSetTarget.
 *
 * @return
 *   1 if channel is connected. 0 otherwise.
 */
long DiagIsChannelConnect();

}
