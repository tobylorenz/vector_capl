#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Creates a key to execute secured diagnostic functions within devices.
 *
 * Creates a key to execute secured diagnostic functions within devices.
 *
 * The key will be defined with the Seed of the device.
 *
 * @param seedArray
 *   Seed for the definition of the key.
 *
 * @param seedArraySize
 *   Number of bytes in the SeedArray.
 *
 * @param securityLevel
 *   Security level for which the key will be created.
 *
 * @param variant
 *   Variant of the diagnostic description.
 *
 * @param ipOption
 *   Further options that will be forwarded to the customer function.
 *
 * @param keyArray
 *   Key that is created with the customer DLL.
 *
 * @param maxKeyArraySize
 *   The maximum allowed number of bytes in the keyArray.
 *
 * @param keyActualSize
 *   Actual used bytes in the keyArray.
 *
 * @return
 *   At success 0 will be returned, otherwise an error code will be returned.
 *   For further error analyses you can use the callback function _Diag_GetError.
 */
long DiagGenerateKeyFromSeed(byte * seedArray, dword seedArraySize, dword securityLevel, char * variant, char * ipOption, byte * keyArray, dword maxKeyArraySize, dword & keyActualSize);

}
