#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns if the object is stored as raw data or can be interpreted.
 *
 * Returns if the object is stored as raw data or can be interpreted.
 *
 * @param request
 *   Request
 *
 * @return
 *   - 1: Object is stored as raw data.
 *   - 0: Object can be interpreted.
 *   - <0: Error code
 */
long DiagIsRaw(DiagRequest request);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns if the object is stored as raw data or can be interpreted.
 *
 * Returns if the object is stored as raw data or can be interpreted.
 *
 * @param response
 *   Response
 *
 * @return
 *   - 1: Object is stored as raw data.
 *   - 0: Object can be interpreted.
 *   - <0: Error code
 */
long DiagIsRaw(DiagResponse response);

}
