#include "DiagSetParameterRaw.h"

#include <iostream>

namespace capl
{

long DiagSetParameterRaw(DiagResponse obj, char * parameter, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagSetParameterRaw(DiagRequest obj, char * parameterName, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
