#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Adapt size of a diagnostic object to match specified parameter iterations, or set size of bus message to given number of byte.
 *
 * Adapt size of a diagnostic object to match specified parameter iterations, or set size of bus message to given number of byte.
 *
 * @param obj
 *   Diagnostics object
 *
 * @return
 *   Error code
 */
long DiagResize(DiagResponse obj);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Adapt size of a diagnostic object to match specified parameter iterations, or set size of bus message to given number of byte.
 *
 * Adapt size of a diagnostic object to match specified parameter iterations, or set size of bus message to given number of byte.
 *
 * @param obj
 *   Diagnostics object
 *
 * @return
 *   Error code
 */
long DiagResize(DiagRequest obj);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Adapt size of a diagnostic object to match specified parameter iterations, or set size of bus message to given number of byte.
 *
 * Adapt size of a diagnostic object to match specified parameter iterations, or set size of bus message to given number of byte.
 *
 * @param obj
 *   Diagnostics object
 *
 * @param byteCount
 *   Length of data to send.
 *
 * @return
 *   Error code
 */
long DiagResize(DiagResponse obj, dword byteCount);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Adapt size of a diagnostic object to match specified parameter iterations, or set size of bus message to given number of byte.
 *
 * Adapt size of a diagnostic object to match specified parameter iterations, or set size of bus message to given number of byte.
 *
 * @param obj
 *   Diagnostics object
 *
 * @param byteCount
 *   Length of data to send.
 *
 * @return
 *   Error code
 */
long DiagResize(DiagRequest obj, dword byteCount);

}
