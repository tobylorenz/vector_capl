#include "DiagSetComplexParameterRaw.h"

#include <iostream>

namespace capl
{

long DiagSetComplexParameterRaw(DiagResponse obj, char * parameterName, dword iteration, char * subParameter, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagSetComplexParameterRaw(DiagRequest obj, char * parameterName, dword iteration, char * subParameter, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
