#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   With this function CANoe triggers the CAPL interface to transmit data.
 *
 * With this function CANoe triggers the CAPL interface to transmit data.
 *
 * @param data
 *   The data of the diagnostic primitive (byte stream of request or response)
 *   that the node should transmit on the bus. This may be several hundred or
 *   thousand bytes, therefore a suitable transport protocol has to be used
 *   here to forward the data.
 *
 * @param count
 *   The actual number of bytes to be sent.
 *
 * @param furtherSegments
 *   - 0: This is the last segment of a segmented transmission (see
 *     Diag_SetDataSegmentation), typical value for ISO TP.
 *   - 1: Further segments follow.
 *   - other: reserved
 */
void _Diag_DataRequest(byte * data, dword count, long furtherSegments);

}
