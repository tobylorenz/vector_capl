#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Waits for the completion of the automatic variant identification algorithm.
 *
 * Wait for the completion of the automatic variant identification algorithm.
 * If the qualifier of an expected variant is given, an error is returned if a different variant is identified.
 *
 * @return
 *   - 1: Identification algorithm finished successfully
 *   - 0: Timeout (10 seconds)
 *   - <0: Error code, especially:
 *   - -100: Variant identification not running - DiagStartVariantIdentification must be called!
 *   - -98: No variant was identified, i.e. the algorithm failed
 *   - -90: TestWaitFor... functions are only possible in tester modules
 *   - -83: The identified variant was not the expected one
 */
long TestWaitForDiagVariantIdentificationCompleted();

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Waits for the completion of the automatic variant identification algorithm.
 *
 * Wait for the completion of the automatic variant identification algorithm.
 * If the qualifier of an expected variant is given, an error is returned if a different variant is identified.
 *
 * @param expectedVariant
 *   Qualifier of the variant that should be identified.
 *
 * @return
 *   - 1: Identification algorithm finished successfully
 *   - 0: Timeout (10 seconds)
 *   - <0: Error code, especially:
 *   - -100: Variant identification not running - DiagStartVariantIdentification must be called!
 *   - -98: No variant was identified, i.e. the algorithm failed
 *   - -90: TestWaitFor... functions are only possible in tester modules
 *   - -83: The identified variant was not the expected one
 */
long TestWaitForDiagVariantIdentificationCompleted(char * expectedVariant);

}
