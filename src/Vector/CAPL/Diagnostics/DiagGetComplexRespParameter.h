#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   The behavior of this CAPL function depends on the used parameters.
 *     - Returns the value of a numeric complex parameter.
 *     - Returns the symbolic value of a complex parameter.
 *     - Retrieve numeric sub-parameter from a parameter iteration directly.
 *
 * @todo
 */

}
