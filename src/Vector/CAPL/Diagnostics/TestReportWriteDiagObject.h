#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Writes a test step with a textual interpretation of the specified request or response object into the test report.
 *
 * TestReportWriteDiagObject writes a test step with a textual interpretation of the specified request or response object into the test report.
 *
 * These test steps are subject to the common test step report filtering as configured in the Test Module Configuration dialog.
 *
 * @param req
 *   Request
 *
 * @return
 *   Error code
 */
long TestReportWriteDiagObject(DiagRequest req);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Writes a test step with a textual interpretation of the specified request or response object into the test report.
 *
 * TestReportWriteDiagObject writes a test step with a textual interpretation of the specified request or response object into the test report.
 *
 * These test steps are subject to the common test step report filtering as configured in the Test Module Configuration dialog.
 *
 * @param resp
 *   Response
 *
 * @return
 *   Error code
 */
long TestReportWriteDiagObject(DiagResponse resp);

}
