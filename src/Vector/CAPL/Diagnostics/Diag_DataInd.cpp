#include "Diag_DataInd.h"

#include <iostream>

namespace capl
{

void Diag_DataInd(byte * rxBuffer, long count, long sender)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
