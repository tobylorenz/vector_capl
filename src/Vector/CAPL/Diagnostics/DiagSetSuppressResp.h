#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Sets the "suppressPosRspMsgIndicationBit" ("suppress positive response message indication bit").
 *
 * Under UDS (Unified Diagnostics Services), in certain requests it is possible to set the "suppressPosRspMsgIndicationBit" ("suppress positive response message indication bit").
 * This means that the receiver must not send any positive response.
 * These two functions make it possible to poll and set this bit.
 *
 * @param req
 *   Request
 *
 * @return
 *   - 1: The bit is set.
 *   - 0: The request does not use the bit, or it is not set.
 *   - Error code or value of the parameter.
 */
long DiagGetSuppressResp(DiagRequest req);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Sets the "suppressPosRspMsgIndicationBit" ("suppress positive response message indication bit").
 *
 * Under UDS (Unified Diagnostics Services), in certain requests it is possible to set the "suppressPosRspMsgIndicationBit" ("suppress positive response message indication bit").
 * This means that the receiver must not send any positive response.
 * These two functions make it possible to poll and set this bit.
 *
 * @param req
 *   Request
 *
 * @param flag
 *   0: Clear, else: Set
 *
 * @return
 *   - 1: The bit is set.
 *   - 0: The request does not use the bit, or it is not set.
 *   - Error code or value of the parameter.
 */
long DiagSetSuppressResp(DiagRequest req, long flag);

}
