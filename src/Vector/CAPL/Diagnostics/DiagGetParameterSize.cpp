#include "DiagGetParameterSize.h"

#include <iostream>

namespace capl
{

long DiagGetParameterSize(DiagResponse obj, char * parameterName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetParameterSize(DiagRequest obj, char * parameterName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
