#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   The behavior of this CAPL function depends on the used parameters.
 *     - Sets the numeric parameter to the specified value.
 *     - Sets a parameter to the symbolically-specified value.
 *
 * @todo
 */

}
