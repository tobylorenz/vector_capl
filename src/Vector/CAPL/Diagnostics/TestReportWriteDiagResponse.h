#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Writes a test step with a textual interpretation of the received response for the specified request object into the test report.
 *
 * TestReportWriteDiagResponse writes a test step with a textual interpretation of the received response for the specified request object into the test report.
 *
 * These test steps are subject to the common test step report filtering as configured in the Test Module Configuration dialog.
 *
 * @param req
 *   Request
 *
 * @return
 *   Error code
 */
long TestReportWriteDiagResponse(DiagRequest req);

}
