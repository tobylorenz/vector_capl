#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reads one byte of the response stored for the request.
 *
 * Reads one byte of the response stored for the request.
 *
 * @brief request
 *   Request
 *
 * @brief bytePos
 *   Position of the byte in the response
 *
 * @return
 *   - >= 0: Requested value or "no error"
 *   - <0: Error code
 */
long DiagGetRespPrimitiveByte(DiagRequest request, dword bytePos);

}
