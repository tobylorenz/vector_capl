#include "DiagInterpretAs.h"

#include <iostream>

namespace capl
{

long DiagInterpretAs(DiagResponse response, char * primitiveQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
