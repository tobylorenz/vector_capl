#include "DiagSetRespPrimitiveByte.h"

#include <iostream>

namespace capl
{

long DiagSetRespPrimitiveByte(DiagRequest request, dword bytePos, dword newValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
