#include "DiagGetSuppressResp.h"

#include <iostream>

namespace capl
{

long DiagGetSuppressResp(DiagRequest req)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagSetSuppressResp(DiagRequest req, long flag)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
