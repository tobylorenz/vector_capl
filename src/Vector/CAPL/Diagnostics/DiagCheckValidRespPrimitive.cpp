#include "DiagCheckValidRespPrimitive.h"

#include <iostream>

namespace capl
{

long DiagCheckValidRespPrimitive(DiagRequest obj)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagCheckValidRespPrimitive(DiagRequest obj, dword * reasonOut)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagCheckValidRespPrimitive(DiagRequest obj, char * primitiveQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagCheckValidRespPrimitive(DiagRequest obj, char * primitiveQualifier, dword * reasonOut)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
