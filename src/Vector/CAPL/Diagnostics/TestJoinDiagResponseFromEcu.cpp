#include "TestJoinDiagResponseFromEcu.h"

#include <iostream>

namespace capl
{

long TestJoinDiagResponseFromEcu()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TestJoinDiagResponseFromEcu(char * ecuQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
