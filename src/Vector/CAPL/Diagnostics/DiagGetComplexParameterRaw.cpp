#include "DiagGetComplexParameterRaw.h"

#include <iostream>

namespace capl
{

long DiagGetComplexParameterRaw(DiagResponse obj, char * parameterName, dword iteration, char * subParameter, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetComplexParameterRaw(DiagRequest obj, char * parameterName, dword iteration, char * subParameter, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
