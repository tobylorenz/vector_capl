#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Communicates to CANoe, that a communication channel is available to the communication partner and data can be sent.
 *
 * This function communicated to CANoe, that a communication channel is
 * available to the communication partner and data can be sent.
 *
 * For connectionless transport protocols this function can be called out of
 * the callback _Diag_SetupChannelReq.
 *
 * @return
 *   Error code
 */
long Diag_SetupChannelCon(void);

}
