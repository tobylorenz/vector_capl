#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Communicates to CANoe that the communication channel is not longer available.
 *
 * @todo
 */

}
