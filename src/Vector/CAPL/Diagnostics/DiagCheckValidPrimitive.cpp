#include "DiagCheckValidPrimitive.h"

#include <iostream>

namespace capl
{

long DiagCheckValidPrimitive(DiagRequest obj)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagCheckValidPrimitive(DiagRequest obj, dword * reasonOut)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagCheckValidPrimitive(DiagResponse obj)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagCheckValidPrimitive(DiagResponse obj, dword reasonOut[])
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagCheckValidPrimitive(DiagResponse obj, char * primitiveQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagCheckValidPrimitive(DiagResponse obj, char * primitiveQualifier, dword * reasonOut)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
