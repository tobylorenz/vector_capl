#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Sends the response object back to the tester. Can only be called in the ECU simulation.
 *
 * @todo
 */

}
