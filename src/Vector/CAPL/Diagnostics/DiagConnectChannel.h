#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Connects a channel.
 *
 * Connects a channel. State of the channel is Connected afterwards.
 *
 * The channel the function refers to is set with the last call of DiagSetTarget.
 *
 * @return
 *   On success 0, otherwise a value lesser 0.
 */
long DiagConnectChannel();

}
