#include "CANdelaLibCloseChannel.h"

#include <iostream>

namespace capl
{

void CANdelaLibCloseChannel(char * ECUqualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
