#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Indicates the state of the diagnostic channel.
 *
 * Indicates the state of the diagnostic channel.
 *
 * @param newState
 *   State of the Channel.
 *   - 0: Closed. Channel not opened yet or already closed.
 *   - 1: Opened. Channel opened but no request successfully send yet or the
 *     attempt to sent a request failed.
 *   - 2: Connected. Request successfully send, no response received yet or
 *     expected response missed.
 *   - 3: Online. Request and response successfully send and received.
 *   - 4: ClosedPending. Close requested but still pending, state Closed will
 *     follow.
 *   - 5: ConnectPending. StartCommunication requested but still pending, if
 *     succeed state Connected will follow, otherwise state Opened.
 */
void _Diag_ChannelStateChanged(long newState);

}
