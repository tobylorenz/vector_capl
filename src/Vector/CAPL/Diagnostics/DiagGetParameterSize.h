#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns the size of a parameter in bits.
 *
 * @deprecated
 *   This function should not longer be used, use DiagGetParameterSizeCoded and DiagGetParameterSizeRaw.
 *
 * Returns the size of a parameter in bits.
 *
 * @param obj
 *   Diagnostics object
 *
 * @param parameterName
 *   Parameter qualifier
 *
 * @return
 *   Error code
 */
long DiagGetParameterSize(DiagResponse obj, char * parameterName);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns the size of a parameter in bits.
 *
 * @deprecated
 *   This function should not longer be used, use DiagGetParameterSizeCoded and DiagGetParameterSizeRaw.
 *
 * Returns the size of a parameter in bits.
 *
 * @param obj
 *   Diagnostics object
 *
 * @param parameterName
 *   Parameter qualifier
 *
 * @return
 *   Error code
 */
long DiagGetParameterSize(DiagRequest obj, char * parameterName);

}
