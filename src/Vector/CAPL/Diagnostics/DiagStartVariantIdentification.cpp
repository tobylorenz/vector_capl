#include "DiagStartVariantIdentification.h"

#include <iostream>

namespace capl
{

long DiagStartVariantIdentification()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagStartVariantIdentification(char * ecuQualifier)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
