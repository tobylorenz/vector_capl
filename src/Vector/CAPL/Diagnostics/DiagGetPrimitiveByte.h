#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reads one byte of a diagnostic object.
 *
 * Reads one byte of a diagnostic object.
 *
 * @param request
 *   Request
 *
 * @param response
 *   Response
 *
 * @param bytePos
 *   Position of the byte in the object
 *
 * @return
 *   - >= 0: Requested value or "no error"
 *   - <0: Error code
 */
long DiagGetPrimitiveByte(DiagRequest request, dword bytePos);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Reads one byte of a diagnostic object.
 *
 * Reads one byte of a diagnostic object.
 *
 * @param request
 *   Request
 *
 * @param response
 *   Response
 *
 * @param bytePos
 *   Position of the byte in the object
 *
 * @return
 *   - >= 0: Requested value or "no error"
 *   - <0: Error code
 */
long DiagGetPrimitiveByte(DiagResponse response, dword bytePos);

}
