#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Read the complex parameter to the specified raw value.
 *
 * Reads the complex parameter to the specified raw value.
 *
 * This function offers access to parameters contained in a received response object, whereby the function is addressed directly by the request.
 * This eliminates the need to generate a separate response object.
 * If no response is available yet for the request, an error is reported.
 *
 */
long DiagGetComplexRespParameterRaw(DiagRequest req, char * parameterName, dword iteration, char * subParameter, byte * buffer, dword bufferLen);

}
