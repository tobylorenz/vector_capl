#include "TestWaitForDiagResponse.h"

#include <iostream>

namespace capl
{

long TestWaitForDiagResponse(DiagRequest request, dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TestWaitForDiagResponse(dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
