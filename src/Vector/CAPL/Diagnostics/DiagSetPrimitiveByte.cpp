#include "DiagSetPrimitiveByte.h"

#include <iostream>

namespace capl
{

long DiagSetPrimitiveByte(DiagRequest request, dword bytePos, dword newValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagSetPrimitiveByte(DiagResponse response, dword bytePos, dword newValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
