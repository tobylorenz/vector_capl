#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns the code of the last received response (for the specified request).
 *
 * Returns the code of the specified response or last received response (for the specified request).
 *
 * @param resp
 *   Response
 *
 * @brief
 *   - -1: The response was positive, i.e. there is no error code.
 *   - 0: No response has been received yet.
 *   - >0: (KWP) Error code of the negative response.
 */
long DiagGetResponseCode(DiagResponse resp);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns the code of the last received response (for the specified request).
 *
 * Returns the code of the specified response or last received response (for the specified request).
 *
 * @param req
 *   Request
 *
 * @brief
 *   - -1: The response was positive, i.e. there is no error code.
 *   - 0: No response has been received yet.
 *   - >0: (KWP) Error code of the negative response.
 */
long DiagGetLastResponseCode(DiagRequest req);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns the code of the last received response (for the specified request).
 *
 * Returns the code of the specified response or last received response (for the specified request).
 *
 * @brief
 *   - -1: The response was positive, i.e. there is no error code.
 *   - 0: No response has been received yet.
 *   - >0: (KWP) Error code of the negative response.
 */
long DiagGetLastResponseCode();

}
