#include "DiagGetLastResponseCode.h"

#include <iostream>

namespace capl
{

long DiagGetResponseCode(DiagResponse resp)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetLastResponseCode(DiagRequest req)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetLastResponseCode()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
