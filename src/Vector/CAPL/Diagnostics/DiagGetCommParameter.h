#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Finds the value of a numeric communication parameter of the interface that was selected for the description in the configuration dialog.
 *
 * Finds the value of a numeric communication parameter of the interface that
 * was selected for the description in the configuration dialog.
 * Parameter qualifiers are defined in the CDDT and depend on the manufacturer
 * or concrete template.
 *
 * @param qualifier
 *   The following qualifiers can be used in addition to the CDD interface
 *   parameters:
 *   - CANoe.AddressMode: ISO TP address mode:
 *     - 0: Normal
 *     - 1: Extended
 *     - 2: NormalFixed
 *     - 3: Mixed
 *     - <0: No ISO TP
 *   - CANoe.TxId [0 1*]: CAN Id for transmitted frames
 *   - CANoe.RxId [0 1*]: CAN Id for received frames
 *   - CANoe.BaseAddress [1*]: TP base address
 *   - CANoe.EcuAddr [1 2 3]: Number of this node
 *   - CANoe.TgtAddr [1 2 3]: Target node number
 *   - CANoe.AddrExt [3]: Address extension byte
 *   - CANoe.TxPrio [2 3]: Frame transmit priority
 *   Further Qualifier, independent of address mode:
 *   - CANoe.UudtId: If the ECU delivers responses also via UUDT frames, their
 *     CAN-Id can be determined here as well.
 *   - CANoe.Blocksize: ISO TP parameter, number of consecutive frames between
 *     two flow control frames.
 *   - CANoe.STmin: ISO TP parameter, minimum interval [ms] between sent
 *     consecutive frames, which the sender should maintain.
 *   - CANoe.FCDelay: ISO TP parmaeter, delay of flow control frames sent.
 *   Timing Parameter with ISO 15765-3:
 *   - CANoe.S3ClientTime: Time the diagnostic client (=tester) should wait
 *     before sending a tester present request.
 *     This value is also returned for the deprecated qualifier CANoe.S3Time.
 *   - CANoe.S3ServerTime: Timeout in the diagnostic server for leaving a
 *     non-default session.
 *   - CANoe.P3TimePhys: Minimum waiting time after physical transmitting of a
 *     request.
 *   - CANoe.P3TimeFunc: Minimum waiting time after functional transmitting of
 *     a request.
 *   - CANoe.Padding:
 *     - -1: No padding (i.e. DLC variable)
 *     - 0..255: Use this value to pad to DLC=8.
 *     - <-1: Normal diagnostic error codes.
 *     - rest: reserved
 *   - CANoe.UseAllFC:
 *     - 0: Use the values of the first flow control frame only.
 *     - 1: Use the values from ALL flow control frames.
 *   - CANoe.UseFFPrio:
 *     - 0: Use the TX priority given.
 *     - 1: Use the priority of the received "first frame" as priority of flow
 *       control frame.
 *   - CANoe.FirstSN: 0..15: The sequence number sent in the first
 *     "consecutive frame".
 *   - CANoe.BSOff: 0..255: Block size value indicating "do not expect further
 *     flow controls".
 *   - CANoe.FixedFrameLength: 0..8: All CAN frames should have at least this
 *     many bytes.
 *   - DoIP.VEHICLE_Interface: See DoIP_SetVehicleInterface
 *   - DoIP.VEHICLE_Address: See DoIP_SetVehicleAddress
 *   - DoIP.VEHICLE_LogicalAddress: Returns the logical DoIP address of the
 *     DoIP entity.
 *   - DoIP.VEHICLE_UDP_DATA: See DoIP_SetVehicleUdpPort
 *   - DoIP.VEHICLE_TCP_DATA: See DoIP_SetVehicleTcpPort
 *   - DoIP.TESTER_LogicalAddress: Returns the logical DoIP address of the
 *     test equipment.
 *   - DoIP.TESTER_UDP_DATA: See DoIP_SetTesterUdpPort
 *   - DoIP.T_TCP_Generic_Inactivity: See DoIP_SetGenericTimeout
 *   - DoIP.T_TCP_Initial_Inactivity: See DoIP_SetInitialTimeout
 *   - DoIP.T_TCP_Alive_Check: See DoIP_SetAliveCheckTimeout
 *
 * @return
 *   Error code or value or the parameter.
 */
long DiagGetCommParameter(const char * qualifier);

/**
 * @ingroup Diagnostics
 *
 * Finds the value of a numeric communication parameter of the interface that
 * was selected for the description in the configuration dialog.
 * Parameter qualifiers are defined in the CDDT and depend on the manufacturer
 * or concrete template.
 *
 * @param qualifier
 *   The following qualifiers can be used in addition to the CDD interface
 *   parameters:
 *   - CANoe.AddressMode: ISO TP address mode:
 *     - 0: Normal
 *     - 1: Extended
 *     - 2: NormalFixed
 *     - 3: Mixed
 *     - <0: No ISO TP
 *   - CANoe.TxId [0 1*]: CAN Id for transmitted frames
 *   - CANoe.RxId [0 1*]: CAN Id for received frames
 *   - CANoe.BaseAddress [1*]: TP base address
 *   - CANoe.EcuAddr [1 2 3]: Number of this node
 *   - CANoe.TgtAddr [1 2 3]: Target node number
 *   - CANoe.AddrExt [3]: Address extension byte
 *   - CANoe.TxPrio [2 3]: Frame transmit priority
 *   Further Qualifier, independent of address mode:
 *   - CANoe.UudtId: If the ECU delivers responses also via UUDT frames, their
 *     CAN-Id can be determined here as well.
 *   - CANoe.Blocksize: ISO TP parameter, number of consecutive frames between
 *     two flow control frames.
 *   - CANoe.STmin: ISO TP parameter, minimum interval [ms] between sent
 *     consecutive frames, which the sender should maintain.
 *   - CANoe.FCDelay: ISO TP parmaeter, delay of flow control frames sent.
 *   Timing Parameter with ISO 15765-3:
 *   - CANoe.S3ClientTime: Time the diagnostic client (=tester) should wait
 *     before sending a tester present request.
 *     This value is also returned for the deprecated qualifier CANoe.S3Time.
 *   - CANoe.S3ServerTime: Timeout in the diagnostic server for leaving a
 *     non-default session.
 *   - CANoe.P3TimePhys: Minimum waiting time after physical transmitting of a
 *     request.
 *   - CANoe.P3TimeFunc: Minimum waiting time after functional transmitting of
 *     a request.
 *   - CANoe.Padding:
 *     - -1: No padding (i.e. DLC variable)
 *     - 0..255: Use this value to pad to DLC=8.
 *     - <-1: Normal diagnostic error codes.
 *     - rest: reserved
 *   - CANoe.UseAllFC:
 *     - 0: Use the values of the first flow control frame only.
 *     - 1: Use the values from ALL flow control frames.
 *   - CANoe.UseFFPrio:
 *     - 0: Use the TX priority given.
 *     - 1: Use the priority of the received "first frame" as priority of flow
 *       control frame.
 *   - CANoe.FirstSN: 0..15: The sequence number sent in the first
 *     "consecutive frame".
 *   - CANoe.BSOff: 0..255: Block size value indicating "do not expect further
 *     flow controls".
 *   - CANoe.FixedFrameLength: 0..8: All CAN frames should have at least this
 *     many bytes.
 *   - DoIP.VEHICLE_Interface: See DoIP_SetVehicleInterface
 *   - DoIP.VEHICLE_Address: See DoIP_SetVehicleAddress
 *   - DoIP.VEHICLE_LogicalAddress: Returns the logical DoIP address of the
 *     DoIP entity.
 *   - DoIP.VEHICLE_UDP_DATA: See DoIP_SetVehicleUdpPort
 *   - DoIP.VEHICLE_TCP_DATA: See DoIP_SetVehicleTcpPort
 *   - DoIP.TESTER_LogicalAddress: Returns the logical DoIP address of the
 *     test equipment.
 *   - DoIP.TESTER_UDP_DATA: See DoIP_SetTesterUdpPort
 *   - DoIP.T_TCP_Generic_Inactivity: See DoIP_SetGenericTimeout
 *   - DoIP.T_TCP_Initial_Inactivity: See DoIP_SetInitialTimeout
 *   - DoIP.T_TCP_Alive_Check: See DoIP_SetAliveCheckTimeout
 *
 * @param index
 *   Set to 0.
 *
 * @param buffer
 *   Buffer to retrieve string parameters.
 *
 * @param size
 *   Size of the buffer in bytes.
 *
 * @return
 *   Error code or value or the parameter.
 */
long DiagGetCommParameter(const char * qualifier, dword index, char * buffer, dword size);

}
