#include "DiagGetNegCode.h"

#include <iostream>

namespace capl
{

long DiagGetNegCode(char * mnemonic)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
