#include "DiagResize.h"

#include <iostream>

namespace capl
{

long DiagResize(DiagResponse obj)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagResize(DiagRequest obj)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagResize(DiagResponse obj, dword byteCount)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagResize(DiagRequest obj, dword byteCount)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
