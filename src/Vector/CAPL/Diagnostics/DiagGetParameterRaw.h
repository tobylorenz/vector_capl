#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Specifies the value of a (complex) parameter directly via uncoded data bytes.
 *
 * Specifies the value of a (complex) parameter directly via uncoded data bytes.
 *
 * @param obj
 *   Diagnostics object
 *
 * @param parameterName
 *   Parameter qualifier
 *
 * @param buffer
 *   Input/output buffer
 *
 * @param buffersize
 *   Buffer size
 *
 * @return
 *   0 if bytes were copied, otherwise <0 for an error code
 */
long DiagGetParameterRaw(DiagResponse obj, char * parameterName, byte * buffer, dword buffersize);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Specifies the value of a (complex) parameter directly via uncoded data bytes.
 *
 * Specifies the value of a (complex) parameter directly via uncoded data bytes.
 *
 * @param obj
 *   Diagnostics object
 *
 * @param parameterName
 *   Parameter qualifier
 *
 * @param buffer
 *   Input/output buffer
 *
 * @param buffersize
 *   Buffer size
 *
 * @return
 *   0 if bytes were copied, otherwise <0 for an error code
 */
long DiagGetParameterRaw(DiagRequest obj, char * parameterName, byte * buffer, dword buffersize);

}
