#include "DiagGetLastResponse.h"

#include <iostream>

namespace capl
{

long DiagGetLastResponse(DiagRequest req, DiagResponse respOut)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetLastResponse(DiagResponse respOut)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
