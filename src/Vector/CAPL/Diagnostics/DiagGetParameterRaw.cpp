#include "DiagGetParameterRaw.h"

#include <iostream>

namespace capl
{

long DiagGetParameterRaw(DiagResponse obj, char * parameterName, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetParameterRaw(DiagRequest obj, char * parameterName, byte * buffer, dword buffersize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
