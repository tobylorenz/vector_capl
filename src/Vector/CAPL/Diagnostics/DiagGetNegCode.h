#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns the error code for a KWP2000 mnemonic.
 *
 * Returns the error code for a KWP2000 mnemonic, e.g. 0x10 for "GR" ("General Reject").
 *
 * @brief mnemonic
 *   Abbreviation for the error code.
 *
 * @return
 *   Diagnostics/KWP2000 Error code
 */
long DiagGetNegCode(char * mnemonic);

}
