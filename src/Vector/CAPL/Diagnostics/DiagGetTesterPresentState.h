#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns the state of autonomous/cyclical Tester Present requests from CANoe to the specified or current ECU.
 *
 * @todo
 */

}
