#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns 1 if the parameter is declared constant in the diagnostic description.
 *
 * @todo
 */

}
