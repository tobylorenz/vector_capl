#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Determines the CAN ID on which functional requests are sent by the diagnostic tester.
 *
 * @todo
 */

}
