#include "_Diag_SetChannelParameters.h"

#include <iostream>

namespace capl
{

void _Diag_SetChannelParameters(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
}

}
