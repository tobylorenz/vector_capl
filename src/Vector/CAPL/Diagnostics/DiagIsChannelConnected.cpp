#include "DiagIsChannelConnected.h"

#include <iostream>

namespace capl
{

long DiagIsChannelConnect()
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
