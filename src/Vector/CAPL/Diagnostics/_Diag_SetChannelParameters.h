#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   This function will be called after measurement start (in ECU simulations) or after DiagSetTarget() (in test nodes) and enables the CAPL program to configure the transport protocol.
 *
 * This function will be called after measurement start (in ECU simulations) or
 * after DiagSetTarget() (in test nodes) and enables the CAPL program to
 * configure the transport protocol.
 *
 * If the protocol is configured by the data base nothing has to be done.
 */
void _Diag_SetChannelParameters(void);

}
