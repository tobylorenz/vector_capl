#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Returns value != 0 if the object is a negative response to a request.
 *
 * Returns value != 0 if the object is a negative response to a request.
 *
 * @param obj
 *   Diagnostics object
 *
 * @return
 *   0 or !0
 */
long DiagIsNegativeResponse(DiagResponse obj);

}
