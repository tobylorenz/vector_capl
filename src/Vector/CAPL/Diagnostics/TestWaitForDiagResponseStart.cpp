#include "TestWaitForDiagResponseStart.h"

#include <iostream>

namespace capl
{

long TestWaitForDiagResponseStart(DiagRequest request, dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long TestWaitForDiagResponseStart(dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
