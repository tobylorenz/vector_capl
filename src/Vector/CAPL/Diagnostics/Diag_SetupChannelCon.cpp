#include "Diag_SetupChannelCon.h"

#include <iostream>

namespace capl
{

long Diag_SetupChannelCon(void)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
