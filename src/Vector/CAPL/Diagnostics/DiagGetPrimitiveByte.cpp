#include "DiagGetPrimitiveByte.h"

#include <iostream>

namespace capl
{

long DiagGetPrimitiveByte(DiagRequest request, dword bytePos)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long DiagGetPrimitiveByte(DiagResponse response, dword bytePos)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
