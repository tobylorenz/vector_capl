#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Treats the data of the response object as the specified primitive.
 *
 * Treats the data of the response object as the specified primitive.
 * This will cause a reinterpretation of the data, but the data will not be changed.
 *
 * @param response
 *   Diagnostics object
 *
 * @param primitiveQualifier
 *   Qualifier of the service primitive that should be used for reinterpretation.
 *
 * @return
 *   - 0: No error, OK
 *   - <0: Error code
 */
long DiagInterpretAs(DiagResponse response, char * primitiveQualifier);

}
