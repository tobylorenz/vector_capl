#pragma once

#include "../DataTypes.h"
#include "Diagnostics.h"

namespace capl
{

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Writes the name of the parameter in the transmitted field.
 *
 * Writes the name of the parameter in the transmitted field.
 *
 * @param obj
 *   Diagnostics object
 *
 * @param paramNo
 *   Which parameter in the object, beginning with 0.
 *
 * @param buffer
 *   Output buffer
 *
 * @param buffersize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagGetParameterName(DiagResponse obj, dword paramNo, char * buffer, dword buffersize);

/**
 * @ingroup Diagnostics
 *
 * @brief
 *   Writes the name of the parameter in the transmitted field.
 *
 * Writes the name of the parameter in the transmitted field.
 *
 * @param obj
 *   Diagnostics object
 *
 * @param paramNo
 *   Which parameter in the object, beginning with 0.
 *
 * @param buffer
 *   Output buffer
 *
 * @param buffersize
 *   Buffer size
 *
 * @return
 *   Error code
 */
long DiagGetParameterName(DiagRequest obj, dword paramNo, char * buffer, dword buffersize);

}
