#include "DiagIsRawResp.h"

#include <iostream>

namespace capl
{

long DiagIsRawResp(DiagRequest request)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
