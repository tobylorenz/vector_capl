#include "TestWaitForDiagRequestSent.h"

#include <iostream>

namespace capl
{

long TestWaitForDiagRequestSent(DiagRequest request, dword timeout)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
