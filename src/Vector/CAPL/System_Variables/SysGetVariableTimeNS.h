#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Returns the time stamp of the last update to the variable value.
 *
 * Returns the time stamp of the last update to the variable value.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::".
 *   The name must be preceded by "sysVar::".
 *
 * @return
 *   Time stamp of the last update to the variable value, in nanoseconds since measurement
 *   start.
 */
int64 SysGetVariableTimeNS(sysvar & SysVarName);

}
