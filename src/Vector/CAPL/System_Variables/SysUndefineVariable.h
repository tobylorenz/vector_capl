#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Deletes a variable.
 *
 * Deletes a variable.
 *
 * @param namespace_
 *   Name of the name space
 *
 * @param variable
 *   Name of the variable
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysUndefineVariable(char * namespace_, char * variable);

}
