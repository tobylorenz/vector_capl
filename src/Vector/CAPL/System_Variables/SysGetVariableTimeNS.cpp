#include "SysGetVariableTimeNS.h"

#include <iostream>

namespace capl
{

int64 SysGetVariableTimeNS(sysvar & SysVarName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
