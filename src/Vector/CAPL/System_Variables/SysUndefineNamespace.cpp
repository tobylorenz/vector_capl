#include "SysUndefineNamespace.h"

#include <iostream>

namespace capl
{

long SysUndefineNamespace(char * namespace_)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
