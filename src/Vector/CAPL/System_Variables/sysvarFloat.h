#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * System variable of type Float
 */
class sysvarFloat : public sysvar
{
public:
    sysvarFloat();
    virtual ~sysvarFloat();
};

}
