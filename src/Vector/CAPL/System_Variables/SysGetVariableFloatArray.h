#pragma once

#include "../DataTypes.h"
#include "sysvarFloatArray.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Returns the value of a variable of the float* type. (Form 1)
 *
 * Returns the value of a variable of the float* type.
 *
 * @param namespace_
 *   Name of the name space (form 1)
 *
 * @param variable
 *   Name of the variable (form 1)
 *
 * @param values
 *   Gets the values of the variable (both forms)
 *
 * @param arraySize
 *   Size of the array (both forms)
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysGetVariableFloatArray(char * namespace_, char * variable, double * values, long arraySize);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Returns the value of a variable of the float* type. (Form 2)
 *
 * Returns the value of a variable of the float* type.
 *
 * @param values
 *   Gets the values of the variable (both forms)
 *
 * @param arraySize
 *   Size of the array (both forms)
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::". The name must be preceded by "sysVar::". (form 2)
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysGetVariableFloatArray(sysvarFloatArray & SysVarName, double * values, long arraySize);

}
