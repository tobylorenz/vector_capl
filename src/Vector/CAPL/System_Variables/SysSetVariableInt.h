#pragma once

#include "../DataTypes.h"
#include "sysvarInt.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Sets the value of a variable of the int type. (Form 1)
 *
 * Sets the value of a variable of the int type.
 *
 * @param namespace_
 *   Name of the name space (form 1)
 *
 * @param variable
 *   Name of the variable (form 1)
 *
 * @param value
 *   New value of the variable (both forms)
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysSetVariableInt(char * namespace_, char * variable, long value);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Sets the value of a variable of the int type. (Form 2)
 *
 * Sets the value of a variable of the int type.
 *
 * @param value
 *   New value of the variable (both forms)
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::". The name must be preceded by "sysVar::". (form 2)
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysSetVariableInt(sysvarInt & SysVarName, long value);

}
