#include "SysSetVariableMemberPhys.h"

#include <iostream>

namespace capl
{

long SysSetVariableMemberPhys(char * namespace_, char * variableAndMemberName, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysSetVariableMemberPhys(sysvar & SysVarMemberName, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
