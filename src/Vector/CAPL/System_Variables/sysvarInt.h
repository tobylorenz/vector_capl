#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * System variable of type Int
 */
class sysvarInt : public sysvar
{
public:
    sysvarInt();
    virtual ~sysvarInt();
};

typedef sysvarInt sysvarLong;

}
