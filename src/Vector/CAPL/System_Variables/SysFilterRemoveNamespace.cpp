#include "SysFilterRemoveNamespace.h"

#include <iostream>

namespace capl
{

long sysFilterRemoveNamespace(char * namespace_)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysFilterRemoveNamespace(char * namespace_, char * filterName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
