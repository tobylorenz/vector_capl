#pragma once

#include "../DataTypes.h"
#include "sysvarString.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Returns the value of a variable of the String (char*) type. (Form 1)
 *
 * Returns the value of a variable of the String (char*) type.
 *
 * @param namespace_
 *   Name of the name space (form 1)
 *
 * @param variable
 *   Name of the variable (form 1)
 *
 * @param buffer
 *   Buffer that takes the value of the variable (form 1)
 *
 * @param bufferSize
 *   Size of the buffer
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysGetVariableString(char * namespace_, char * variable, char * buffer, long bufferSize);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Returns the value of a variable of the String (char*) type. (Form 2)
 *
 * Returns the value of a variable of the String (char*) type.
 *
 * @param buffer
 *   Buffer that takes the value of the variable (form 1)
 *
 * @param bufferSize
 *   Size of the buffer
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::". The name must be preceded by "sysVar::". (form 2)
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysGetVariableString(sysvarString & SysVarName, char * buffer, long bufferSize);

}
