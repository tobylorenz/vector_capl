#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * System variable of type Data
 */
class sysvarData : public sysvar
{
public:
    sysvarData();
    virtual ~sysvarData();
};

}
