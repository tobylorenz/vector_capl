#include "SysGetVariableFloatArray.h"

#include <iostream>

namespace capl
{

long SysGetVariableFloatArray(char * namespace_, char * variable, double * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysGetVariableFloatArray(sysvarFloatArray & SysVarName, double * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
