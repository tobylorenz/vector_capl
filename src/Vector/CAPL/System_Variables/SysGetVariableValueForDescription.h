#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Retrieves the value for a value description of a system variable of type long or long array.
 * In this way, you can access the value table of the system variable.
 *
 * @param description
 *   Description for which the value shall be retrieved.
 *
 * @param value
 *   Receives the value for the description.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces, separated by "::".
 *   The name must be preceded by "sysVar::".
 *
 * @return
 *   - 0: no error, function successful
 *   - 2: variable was not found or second try to define the same variable
 *   - 5: there is no description of this value for the variable
 */
long SysGetVariableValueForDescription(sysvar & SysVarName, char * description, long & value);

/**
 * @ingroup System_Variables
 *
 * Retrieves the value for a value description of a system variable of type long or long array.
 * In this way, you can access the value table of the system variable.
 *
 * @param namespace_
 *   Name of the name space.
 *
 * @param variable
 *   Name of the variable.
 *
 * @param description
 *   Description for which the value shall be retrieved.
 *
 * @param value
 *   Receives the value for the description.
 *
 * @return
 *   - 0: no error, function successful
 *   - 2: variable was not found or second try to define the same variable
 *   - 5: there is no description of this value for the variable
 */
long SysGetVariableValueForDescription(char * namespace_, char * variable, char * description, long & value);

}
