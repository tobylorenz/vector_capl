#include "SysDefineNamespace.h"

#include <iostream>

namespace capl
{

long SysDefineNamespace(char * namespace_)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
