#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * System variable of type String
 */
class sysvarString : public sysvar
{
public:
    sysvarString();
    virtual ~sysvarString();
};

}
