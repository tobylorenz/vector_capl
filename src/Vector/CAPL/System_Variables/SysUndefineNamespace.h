#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Deletes a name space.
 *
 * Deletes a name space. Also all variables in this name space are automatically deleted.
 *
 * @param namespace_
 *   Name of the name space
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 */
long SysUndefineNamespace(char * namespace_);

}
