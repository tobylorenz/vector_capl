#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Retrieves the minimum of a variable (Form 1)
 *
 * Retrieves the minimum of a variable of type Integer or Integer Array.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::".
 *   The name must be preceded by "sysVar::". (form 1 and 2)
 *
 * @param minimum
 *   Receives the minimum of the variable if no errors occur (reference parameter). Must be
 *   the correct type for the system variable.
 *
 * @return
 *   - 0: success
 *   - 4: variable is of the wrong type or has no minimum defined
 */
long sysGetVariableMin(sysvar & SysVarName, long & minimum);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Retrieves the minimum of a variable (Form 2)
 *
 * Retrieves the minimum of a variable of type Integer or Integer Array.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::".
 *   The name must be preceded by "sysVar::". (form 1 and 2)
 *
 * @param minimum
 *   Receives the minimum of the variable if no errors occur (reference parameter). Must be
 *   the correct type for the system variable.
 *
 * @return
 *   - 0: success
 *   - 4: variable is of the wrong type or has no minimum defined
 */
long sysGetVariableMin(sysvar & SysVarName, double & minimum);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Retrieves the minimum of a variable (Form 3)
 *
 * Retrieves the minimum of a variable of type Integer or Integer Array.
 *
 * @param namespace_
 *   Name of the namespace (form 3 and 4)
 *
 * @param variable
 *   Name of the variable (form 3 and 4)
 *
 * @param minimum
 *   Receives the minimum of the variable if no errors occur (reference parameter). Must be
 *   the correct type for the system variable.
 *
 * @return
 *   - 0: success
 *   - 4: variable is of the wrong type or has no minimum defined
 */
long sysGetVariableMin(char * namespace_, char * variable, long & minimum);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Retrieves the minimum of a variable (Form 4)
 *
 * Retrieves the minimum of a variable of type Integer or Integer Array.
 *
 * @param namespace_
 *   Name of the namespace (form 3 and 4)
 *
 * @param variable
 *   Name of the variable (form 3 and 4)
 *
 * @param minimum
 *   Receives the minimum of the variable if no errors occur (reference parameter). Must be
 *   the correct type for the system variable.
 *
 * @return
 *   - 0: success
 *   - 4: variable is of the wrong type or has no minimum defined
 */
long sysGetVariableMin(char * namespace_, char * variable, double & minimum);

}
