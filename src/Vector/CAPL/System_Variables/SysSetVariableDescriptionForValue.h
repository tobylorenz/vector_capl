#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Sets a description for a particular value of a system variable of type long or long array.
 * In this way, you can define a value table for a variable which you have created with SysDefineVariableInt/SysDefineVariableIntArray.
 *
 * @param namespace_
 *   Name of the name space.
 *
 * @param variable
 *   Name of the variable.
 *
 * @param value
 *   Value for which the description shall be set.
 *
 * @param description
 *   New description for the value.
 *
 * @return
 *   - 0: no error, function successful
 *   - 2: variable was not found or second try to define the same variable
 */
long SysSetVariableDescriptionForValue(char * namespace_, char * variable, long value, char * description);

}
