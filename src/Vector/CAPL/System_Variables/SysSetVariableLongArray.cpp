#include "SysSetVariableLongArray.h"

#include <iostream>

namespace capl
{

long SysSetVariableLongArray(char * namespace_, char * variable, long * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysSetVariableLongArray(sysvarIntArray & SysVarName, long * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
