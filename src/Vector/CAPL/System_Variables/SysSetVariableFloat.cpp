#include "SysSetVariableFloat.h"

#include <iostream>

namespace capl
{

long SysSetVariableFloat(char * namespace_, char * variable, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysSetVariableFloat(sysvarFloat & SysVarName, double value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
