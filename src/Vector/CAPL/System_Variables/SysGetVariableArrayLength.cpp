#include "SysGetVariableArrayLength.h"

#include <iostream>

namespace capl
{

dword SysGetVariableArrayLength(char * namespace_, char * variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

dword SysGetVariableArrayLength(sysvar & SysVarName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
