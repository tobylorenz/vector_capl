#include "SysBeginVariableStructUpdate.h"

#include <iostream>

namespace capl
{

long SysBeginVariableStructUpdate(char * namespace_, char * variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysBeginVariableStructUpdate(sysvar & sysvarName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
