#include "SysSetVariableFloatArray.h"

#include <iostream>

namespace capl
{

long SysSetVariableFloatArray(char * namespace_, char * variable, double * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysSetVariableFloatArray(sysvarFloatArray & SysVarName, double * values, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
