#include "SysGetVariableFloat.h"

#include <iostream>

namespace capl
{

double SysGetVariableFloat(char * namespace_, char * variable)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

double SysGetVariableFloat(sysvarFloat & SysVarName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
