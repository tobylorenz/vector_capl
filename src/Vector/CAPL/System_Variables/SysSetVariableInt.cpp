#include "SysSetVariableInt.h"

#include <iostream>

namespace capl
{

long SysSetVariableInt(char * namespace_, char * variable, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysSetVariableInt(sysvarInt & SysVarName, long value)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
