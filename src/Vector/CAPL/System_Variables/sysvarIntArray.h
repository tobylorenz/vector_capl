#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * System variable of type IntArray
 */
class sysvarIntArray : public sysvar
{
public:
    sysvarIntArray();
    virtual ~sysvarIntArray();
};

typedef sysvarIntArray sysvarLongArray;

}
