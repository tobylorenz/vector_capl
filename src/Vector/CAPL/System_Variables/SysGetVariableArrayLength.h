#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Returns the length of an array system variable. (Form 1)
 *
 * Returns the length of the array of the system variable.
 *
 * @param namespace_
 *   Name of the name space (form 1).
 *
 * @param variable
 *   Name of the variable (form 1).
 *
 * @return
 *   For system variables of type long or float array, returns the number of elements in the
 *   array.
 *   For system variables of type string, returns the length of the string, excluding the
 *   terminating 0 character.
 *   For system variables of type long or float, returns 1.
 */
dword SysGetVariableArrayLength(char * namespace_, char * variable);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Returns the length of an array system variable. (Form 2)
 *
 * Returns the length of the array of the system variable.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::".
 *   The name must be preceded by "sysVar::". (form 2)
 *
 * @return
 *   For system variables of type long or float array, returns the number of elements in the
 *   array.
 *   For system variables of type string, returns the length of the string, excluding the
 *   terminating 0 character.
 *   For system variables of type long or float, returns 1.
 */
dword SysGetVariableArrayLength(sysvar & SysVarName);

}
