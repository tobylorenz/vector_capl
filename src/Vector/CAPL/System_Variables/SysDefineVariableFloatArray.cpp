#include "SysDefineVariableFloatArray.h"

#include <iostream>

namespace capl
{

long SysDefineVariableFloatArray(char * namespace_, char * variable, double * initialValues, long arraySize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysDefineVariableFloatArray(char * namespace_, char * variable, double * initialValues, long arraySize, double minimum, double maximum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
