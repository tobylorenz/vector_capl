#include "SysDefineVariableData.h"

#include <iostream>

namespace capl
{

long SysDefineVariableData(char * namespace_, char * variable, byte * initialData, long initialSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
