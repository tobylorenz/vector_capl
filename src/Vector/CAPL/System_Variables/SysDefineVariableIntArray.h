#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Defines a variable of the int* type. (Form 1)
 *
 * Defines a variable of the int* type.
 *
 * @param namespace_
 *   Name of the name space
 *
 * @param variable
 *   Name of the variable
 *
 * @param initialValues
 *   Initial value of the variable
 *
 * @param arraySize
 *   Number of values in the array
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 *   - 5: given minimum is larger than the given maximum
 */
long SysDefineVariableIntArray(char * namespace_, char * variable, int * initialValues, long arraySize);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Defines a variable of the int* type. (Form 2)
 *
 * Defines a variable of the int* type.
 *
 * @param namespace_
 *   Name of the name space
 *
 * @param variable
 *   Name of the variable
 *
 * @param initialValues
 *   Initial value of the variable
 *
 * @param arraySize
 *   Number of values in the array
 *
 * @param minimum
 *   The minimum value of the variable
 *
 * @param maximum
 *   The maximum value of the variable
 *
 * @return
 *   - 0: no error, function successful
 *   - 1: name space was not found or second try to define the same name space
 *   - 2: variable was not found or second try to define the same variable
 *   - 3: no writing right for the name space available
 *   - 4: the variable has no suitable type for the function
 *   - 5: given minimum is larger than the given maximum
 */
long SysDefineVariableIntArray(char * namespace_, char * variable, int * initialValues, long arraySize, long minimum, long maximum);

}
