#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * System variable of type FloatArray
 */
class sysvarFloatArray : public sysvar
{
public:
    sysvarFloatArray();
    virtual ~sysvarFloatArray();
};

}
