#include "SysCreateVariableFilter.h"

#include <iostream>

namespace capl
{

long sysCreateVariableFilter(long filterType)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long sysCreateVariableFilter(long filterType, char * filterName)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
