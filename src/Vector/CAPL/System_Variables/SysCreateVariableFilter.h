#pragma once

#include "../DataTypes.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Creates a new variable filter behind the node calling the function.
 * If no filterName is given, the default filter with an empty name is created.
 *
 * @param filterType
 *   Specifies the filter behavior:
 *   - 0: stop filter. Stops all variables that are added to it.
 *   - 1: pass filter. Stops all variables that are not added to it.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 1: The filter type is not valid. The filter cannot be created.
 *   - 4: A filter with the given name already exists. The filter cannot be created. Note: if no name or an empty name was given, this return value means that the default filter already exists. It cannot be created a second time.
 */
long sysCreateVariableFilter(long filterType);

/**
 * @ingroup System_Variables
 *
 * Creates a new variable filter behind the node calling the function.
 * If no filterName is given, the default filter with an empty name is created.
 *
 * @param filterType
 *   Specifies the filter behavior:
 *   - 0: stop filter. Stops all variables that are added to it.
 *   - 1: pass filter. Stops all variables that are not added to it.
 *
 * @param filterName
 *   The name of the new filter.
 *   The name is used to identify the filter later, when variables or namespaces are added.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 1: The filter type is not valid. The filter cannot be created.
 *   - 4: A filter with the given name already exists. The filter cannot be created. Note: if no name or an empty name was given, this return value means that the default filter already exists. It cannot be created a second time.
 */
long sysCreateVariableFilter(long filterType, char * filterName);

}
