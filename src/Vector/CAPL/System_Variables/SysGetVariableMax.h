#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Retrieves the maximum of a variable. (Form 1)
 *
 * Retrieves the maximum of a variable.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::".
 *   The name must be preceded by "sysVar::". (form 1 and 2)
 *
 * @param maximum
 *   Receives the maximum of the variable if no errors occur (reference parameter). Must be
 *   the correct type for the system variable.
 *
 * @return
 *   - 0: success
 *   - 4: variable is of the wrong type or has no maximum defined
 */
long sysGetVariableMax(sysvar & SysVarName, long & maximum);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Retrieves the maximum of a variable. (Form 2)
 *
 * Retrieves the maximum of a variable.
 *
 * @param SysVarName
 *   Name of the fully qualified name of the system variable, including all name spaces,
 *   separated by "::".
 *   The name must be preceded by "sysVar::". (form 1 and 2)
 *
 * @param maximum
 *   Receives the maximum of the variable if no errors occur (reference parameter). Must be
 *   the correct type for the system variable.
 *
 * @return
 *   - 0: success
 *   - 4: variable is of the wrong type or has no maximum defined
 */
long sysGetVariableMax(sysvar & SysVarName, double & maximum);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Retrieves the maximum of a variable. (Form 3)
 *
 * Retrieves the maximum of a variable.
 *
 * @param namespace_
 *   Name of the namespace (form 3 and 4)
 *
 * @param variable
 *   Name of the variable (form 3 and 4)
 *
 * @param maximum
 *   Receives the maximum of the variable if no errors occur (reference parameter). Must be
 *   the correct type for the system variable.
 *
 * @return
 *   - 0: success
 *   - 4: variable is of the wrong type or has no maximum defined
 */
long sysGetVariableMax(char * namespace_, char * variable, long & maximum);

/**
 * @ingroup System_Variables
 *
 * @brief
 *   Retrieves the maximum of a variable. (Form 4)
 *
 * Retrieves the maximum of a variable.
 *
 * @param namespace_
 *   Name of the namespace (form 3 and 4)
 *
 * @param variable
 *   Name of the variable (form 3 and 4)
 *
 * @param maximum
 *   Receives the maximum of the variable if no errors occur (reference parameter). Must be
 *   the correct type for the system variable.
 *
 * @return
 *   - 0: success
 *   - 4: variable is of the wrong type or has no maximum defined
 */
long sysGetVariableMax(char * namespace_, char * variable, double & maximum);

}
