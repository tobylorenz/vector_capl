#include "SysGetVariableDescriptionForValue.h"

#include <iostream>

namespace capl
{

long SysGetVariableDescriptionForValue(sysvar & SysVarName, long value, char * buffer, long bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysGetVariableDescriptionForValue(char * namespace_, char * variable, long value, char * buffer, long bufferSize)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
