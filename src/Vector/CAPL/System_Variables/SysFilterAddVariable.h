#pragma once

#include "../DataTypes.h"
#include "sysvar.h"

namespace capl
{

/**
 * @ingroup System_Variables
 *
 * Adds a variable a variable filter.
 * If no filterName is given, the variable will be added to the default filter.
 *
 * @param variable
 *   The variable to add.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The variable filter does not exist. The variable cannot be added.
 *   - 3: The variable is already added to the filter.
 *   - 6: The variable does not exist. The variable cannot be added.
 */
long sysFilterAddVariable(sysvar & variable);

/**
 * @ingroup System_Variables
 *
 * Adds a variable a variable filter.
 * If no filterName is given, the variable will be added to the default filter.
 *
 * @param variable
 *   The variable to add.
 *
 * @param filterName
 *   The name of the filter the variable should be added to.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The variable filter does not exist. The variable cannot be added.
 *   - 3: The variable is already added to the filter.
 *   - 6: The variable does not exist. The variable cannot be added.
 */
long sysFilterAddVariable(sysvar & variable, char * filterName);

/**
 * @ingroup System_Variables
 *
 * Adds a variable a variable filter.
 * If no filterName is given, the variable will be added to the default filter.
 *
 * @param namespace_
 *   The namespace that contains the variable.
 *
 * @param variable
 *   The name of the variable.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The variable filter does not exist. The variable cannot be added.
 *   - 3: The variable is already added to the filter.
 *   - 6: The variable does not exist. The variable cannot be added.
 */
long sysFilterAddVariable(char * namespace_, char * variable);

/**
 * @ingroup System_Variables
 *
 * Adds a variable a variable filter.
 * If no filterName is given, the variable will be added to the default filter.
 *
 * @param filterName
 *   The name of the filter the variable should be added to.
 *
 * @param namespace_
 *   The namespace that contains the variable.
 *
 * @param variable
 *   The name of the variable.
 *
 * @return
 *   - 0: No error, function successful.
 *   - 2: The variable filter does not exist. The variable cannot be added.
 *   - 3: The variable is already added to the filter.
 *   - 6: The variable does not exist. The variable cannot be added.
 */
long sysFilterAddVariable(char * namespace_, char * variable, char * filterName);

}
