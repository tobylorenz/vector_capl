#include "SysDefineVariableFloat.h"

#include <iostream>

namespace capl
{

long SysDefineVariableFloat(char * namespace_, char * variable, double initialValue)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

long SysDefineVariableFloat(char * namespace_, char * variable, double initialValue, double minimum, double maximum)
{
    std::cerr << "Unsupported function: " << __FUNCTION__ << std::endl;
    return 0;
}

}
