/**
 * The CAPL language allows you to write programs for individual applications. For example, in the
 * development of network nodes, the problem often arises that the remaining bus nodes are not available
 * yet for tests. The system environment can be emulated with the help of CAPL, e.g. the data traffic of all
 * remaining stations can be emulated.
 *
 * With CAPL you can also write programs to analyze data traffic which are adapted to your problem, or
 * programs for a gateway — a connection element between two buses — so that data can be exchanged
 * between different buses.
 *
 * The emphasis in modeling lies in the description of the node‘s bus behavior, for which the language
 * capabilities of CAPL are normally sufficient.
 */

#pragma once

#include "DataTypes.h"

/* General CAPL Functions */
#include "General/General.h"

/* General Event Procedures */
#include "General/General.h" // ... yes it's there as well

/* AFDX CAPL Functions */
#include "AFDX/AFDX.h"

/* Associative Fields */
#include "Associative_Fields/Associative_Fields.h"

/* BEAN CAPL Functions */
#include "BEAN/BEAN.h"

/* CAN Functions */
#include "CAN/CAN.h"

/* CANstress CAPL Functions */
#include "CANstress/CANstress.h"

/* CANoe IL CAPL Functions */
#include "CANoe_IL/CANoe_IL.h"

/* Car2x CAPL Functions */
#include "Car2x/Car2x.h"

/* Diagnostics CAPL Functions */
#include "Diagnostics/Diagnostics.h"

/* FlexRay CAPL Functions */
#include "FlexRay/FlexRay.h"

/* FRstress CAPL Functions */
#include "FRstress/FRstress.h"

/* GMLAN */
#include "GMLAN/GMLAN.h"

/* GPIB CAPL Functions */
#include "GPIB/GPIB.h"

/* IP CAPL Functions */
#include "IP/IP.h"

/* ISO11783 CAPL Functions */
#include "ISO11783/ISO11783.h"

/* J1587 CAPL Functions */
#include "J1587/J1587.h"

/* J1939 CAPL Functions */
#include "J1939/J1939.h"

/* K-Line CAPL Functions */
#include "K-Line/K-Line.h"

/* LIN CAPL Functions */
#include "LIN/LIN.h"

/* MOST CAPL Functions */
#include "MOST/MOST.h"

/* OSEK NM CAPL Functions */
#include "OSEK_NM/OSEK_NM.h"

/* OSEK TP CAPL Functions */
#include "OSEK_TP/OSEK_TP.h"

/* RS232 CAPL Functions */
#include "RS232/RS232.h"

/* Scope CAPL Functions */
#include "Scope/Scope.h"

/* Struct Byte Access CAPL Functions */
#include "Struct_Byte_Access/Struct_Byte_Access.h"

/* System Variables CAPL Functions */
#include "System_Variables/System_Variables.h"

/* Test Feature Set (TFS) CAPL Functions */
#include "TFS/TFS.h"

/* Test Service Library (TSL) Functions */
#include "TSL/TSL.h"

/* TCP/IP API */
#include "TCP_IP/TCP_IP.h"

/* VT System */
#include "VT_System/VT_System.h"

/* XCP and CCP */
#include "XCP_and_CCP/XCP_and_CCP.h"
